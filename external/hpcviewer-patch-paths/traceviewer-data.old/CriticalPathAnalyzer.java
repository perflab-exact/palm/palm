package edu.rice.cs.hpc.traceviewer.data.timeline;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import java.util.regex.Pattern;

import edu.rice.cs.hpc.common.util.ProcedureAliasMap;
import edu.rice.cs.hpc.data.experiment.BaseExperiment;
import edu.rice.cs.hpc.data.experiment.ExperimentWithoutMetrics;
import edu.rice.cs.hpc.data.experiment.InvalExperimentException;
import edu.rice.cs.hpc.data.experiment.extdata.BaseData;
import edu.rice.cs.hpc.data.experiment.extdata.IBaseData;
import edu.rice.cs.hpc.data.experiment.extdata.TraceAttribute;
import edu.rice.cs.hpc.data.experiment.metric.MetricValue;
import edu.rice.cs.hpc.data.experiment.scope.AlienScope;
import edu.rice.cs.hpc.data.experiment.scope.CallSiteScope;
import edu.rice.cs.hpc.data.experiment.scope.FileScope;
import edu.rice.cs.hpc.data.experiment.scope.GroupScope;
import edu.rice.cs.hpc.data.experiment.scope.LineScope;
import edu.rice.cs.hpc.data.experiment.scope.LoadModuleScope;
import edu.rice.cs.hpc.data.experiment.scope.LoopScope;
import edu.rice.cs.hpc.data.experiment.scope.ProcedureScope;
import edu.rice.cs.hpc.data.experiment.scope.RootScope;
import edu.rice.cs.hpc.data.experiment.scope.Scope;
import edu.rice.cs.hpc.data.experiment.scope.ScopeVisitType;
import edu.rice.cs.hpc.data.experiment.scope.StatementRangeScope;
import edu.rice.cs.hpc.data.experiment.scope.visitors.IScopeVisitor;
import edu.rice.cs.hpc.data.experiment.scope.visitors.PercentScopeVisitor;
import edu.rice.cs.hpc.traceviewer.data.db.DataRecord;
import edu.rice.cs.hpc.traceviewer.data.db.TraceDataByRank;
import edu.rice.cs.hpc.traceviewer.data.graph.CallPath;
import edu.rice.cs.hpc.traceviewer.data.timeline.ProcessTimeline;

public class CriticalPathAnalyzer {

	protected BaseExperiment m_experiment;
	protected IBaseData m_trace;
	protected HashMap<Integer, CallPath> m_scopeMap;

	public CriticalPathAnalyzer(BaseExperiment experiment, IBaseData trace, HashMap<Integer, CallPath> scopeMap) {
		m_experiment = experiment;
		m_trace = trace;
		m_scopeMap = scopeMap;
	}

	public CriticalPathAnalyzer(File experimentFile, File traceFile) {
		// ------------------------------------------------------------
		// cf. edu.rice.cs.hpc.traceviewer.spaceTimeData.SpaceTimeDataController.SpaceTimeDataController()
		// ------------------------------------------------------------
		m_experiment = new ExperimentWithoutMetrics();
		try {
			m_experiment.open(experimentFile, new ProcedureAliasMap());
		}
		catch (InvalExperimentException e) {
			System.out.println("Parse error in Experiment XML at line " + e.getLineNumber());
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		// ------------------------------------------------------------
		// cf. edu.rice.cs.hpc.traceviewer.spaceTimeData.SpaceTimeDataControllerLocal.SpaceTimeDataControllerLocal()
		// ------------------------------------------------------------
		m_trace = null;
		try {
			final TraceAttribute traceAttr = m_experiment.getTraceAttribute();
			m_trace = new BaseData(traceFile.getAbsolutePath(), traceAttr.dbHeaderSize, 24);
		}
		catch (IOException e) {
			System.err.println("Master trace buffer could not be created");
		}

		// ------------------------------------------------------------
		// cf. edu.rice.cs.hpc.traceviewer.spaceTimeData.SpaceTimeDataController.init()
		// cf. edu.rice.cs.hpc.traceviewer.data.timeline.ProcessTimeline.ProcessTimeline()
		// ------------------------------------------------------------
		m_scopeMap = new HashMap<Integer, CallPath>();
		
		// TraceDataVisitor visitor = new TraceDataVisitor(m_scopeMap);
		// ColorTable colorTable = new ColorTable();
		// colorTable.addProcedure(CallPath.NULL_FUNCTION);
		// int maxDepth = exp.getRootScope().dfsSetup(visitor, colorTable, 1);
		// colorTable.setColorTable();
	}

	// -------------------------------------------------------------------------
	//
	// -------------------------------------------------------------------------

	protected static class Args {
		public enum RunTy {
			path,
			treeGraph, treeGraphCombo, treeGraphEdgeSort,
			graph, graphCombo, graphEdgeSort, graphEdgeSortCombo
		}

		public enum OutputTy {
			dot, gml, graphml, txt
		}

		static final int pathLenOne = 1;
		static final int pathLenSome = 4;
		static final int pathLenAll = Integer.MAX_VALUE;
		
		// ------------------------------------------------------------

		// Short circuiting
		public static boolean doMergeOnly = false;
		public static boolean doOnePathOnly = true; // true

		// ------------------------------------------------------------
		// Task merging

		static final boolean taskMerge_useCPId = false; // true [full context, including callsites]

		static final int     taskNameId_pathLen = pathLenAll; // pathLenAll
		static final boolean taskName_useCallsite = false; //true [will affect taskNameId as well]

		static final boolean treeGraph_useTopDownCallPaths = false; //true
		
		// ------------------------------------------------------------

		// Extra debug info
		static final boolean doProgress = true;
		static final boolean doDumpTrace = false; //false
		static final boolean doDumpPath  = false; //false
		static final boolean doDebugPath = false; //false

		// ------------------------------------------------------------

		static final String fmt_int = "% 4d";
		static final String fmt_dbl = "%.4f";
		static final String fmt_dbl_big = "%.2g"; // "%x"

		// ------------------------------------------------------------

		static final long TaskPoint_TimeId_NULL = 0;
		static final long TaskPoint_Rank_EndFlag = -1;
		static final long TaskPoint_Rank_ModelTaskFlag = -2;
		
		static final int numPalmModelRecords = 10;
	}

	protected ProgressMsg progressMsg = new ProgressMsg();
	
	public void run() {
		progressMsg.dump_L1("critical path analysis");

		if (Args.doMergeOnly) {
			throw new RuntimeException("*** Done merging! ***"); //	System.exit(0);
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		Args.RunTy runTy = Args.RunTy.graphEdgeSort; // graphCombo+, treeGraph*, treeGraphCombo, graphEdgeSort
		Args.OutputTy outTy = Args.OutputTy.txt; // dot+, gml, graphml*, txt

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------
		
		File experimentFile = m_experiment.getXMLExperimentFile();
		File experimentDir  = experimentFile.getParentFile();

		PrintStream outExperimentOS = null;
		try {
			File outExperimentFile = new File(experimentDir, "0-experiment.xml");
			outExperimentFile.createNewFile();
			FileOutputStream fos2 = new FileOutputStream(outExperimentFile);
			outExperimentOS = new PrintStream(fos2);			
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		final TraceAttribute traceAttr = m_experiment.getTraceAttribute();

		int n_ranks = m_trace.getNumberOfRanks(); // FIXME: handle threads

		long minBegTime = traceAttr.dbTimeMin;
		long maxEndTime = traceAttr.dbTimeMax;

		final long time = maxEndTime;

		if (Args.doDumpTrace) {
			progressMsg.dump_L1("dumping trace");
			PrintStream outTraceOS = makeFilePrintStream(experimentDir, "0", "debug-trace", "", "txt");
			for (int rank = 0; rank < n_ranks; rank++) {
				makeTrace(outTraceOS, rank, minBegTime, maxEndTime);
			}
		}
		
		LinkedList<PathTy> pathTyL = new LinkedList<PathTy>();
		pathTyL.push(PathTy.most);
		pathTyL.push(PathTy.least);
		//if (runTy == Args.RunTy.treeGraphCombo || runTy == Args.RunTy.graphCombo || runTy == Args.RunTy.graphEdgeSortCombo) {
		//	pathTyL.push(PathTy.least);
		//}

		// N.B.: Retain Graphs instead of Paths b/c of memory consumption
		LinkedList<Graph> graphL_mst = new LinkedList<Graph>();
		LinkedList<Graph> graphL_lst = new LinkedList<Graph>();
		
		rankloop:
		for (int rank = 0; rank < n_ranks; rank++) {			
			LinkedList<PathTy> pathTyL_me = (LinkedList<PathTy>) pathTyL.clone(); // necessary!
			for (PathTy pathTy : pathTyL_me) {
				Path path = makePath(rank, time, pathTy);

				// ------------------------------------------------------------
				// if a valid path is found...
				// ------------------------------------------------------------
				if (path.taskL.size() > 1) {					
					if (Args.doOnePathOnly) {
						pathTyL.removeFirstOccurrence(pathTy);
					}

					if (Args.doDumpPath) {
						PrintStream outPathOS = makeFilePrintStream(experimentDir, "0", "debug-path", pathTy.toString() + rank, "txt");
						dumpPath_txt(outPathOS, path);
					}
					
					progressMsg.dump_L1(path.mkInfoStr());
					LinkedList<Graph> graphL_x = (pathTy == PathTy.most) ? graphL_mst : graphL_lst;

					EnumSet<Graph.MergeTy> mergeTy = Graph.MergeTy.full;
					if (runTy == Args.RunTy.treeGraphCombo || runTy == Args.RunTy.graphCombo || runTy == Args.RunTy.graphEdgeSortCombo) {
						mergeTy = (pathTy == PathTy.most) ? EnumSet.of(Graph.MergeTy.most) : EnumSet.of(Graph.MergeTy.least);
					}

					if (runTy == Args.RunTy.path) {
						Graph graph = makePathGraph(path, mergeTy, runTy.toString());
						PrintStream outOS = makeFilePrintStream(experimentDir, "0", runTy.toString(), pathTy.toString() + rank, outTy.toString());
						dumpGraph(outOS, graph, graph.mkInfoStr(), outTy);
					}
					else if (runTy == Args.RunTy.treeGraph || runTy == Args.RunTy.treeGraphCombo
							|| runTy == Args.RunTy.treeGraphEdgeSort) {
						Graph graph = makeTreeGraph(path, mergeTy);

						if (runTy == Args.RunTy.treeGraph || runTy == Args.RunTy.treeGraphEdgeSort) {
							progressMsg.dump_L3("tree-graph-pruning: " + graph.mkInfoStr());
							int numVertices_beg = graph.numVertices();
							int numEdges_beg = graph.numEdges();
							double thresholdV = 0.01;   // 1% / inclusive
							double thresholdE = 0.0005; // .05% / exclusive
							graph.makeMetricsIncl_treegraph();
							graph.pruneByInclCost_treegraph(thresholdV, thresholdE);

							String info = "[pruning with vertex/back-edge cost threshold=" + thresholdV + "/" + thresholdE + " gives " + graph.numVertices() + "/" + numVertices_beg + " vertices and " + graph.numEdges() + "/" + numEdges_beg + " edges]";
							graph.add_provenanceInfo(info);
						}
						
						graphL_x.add(graph);
					}
					else if (runTy == Args.RunTy.graph || runTy == Args.RunTy.graphCombo
							|| runTy == Args.RunTy.graphEdgeSort || runTy == Args.RunTy.graphEdgeSortCombo) {
						Graph graph = makeGraph(path, mergeTy);
						graphL_x.add(graph);
					}
				}
				
				path.nullify();
			}
			//if (!doAllPaths && pathTyL.isEmpty()) { break rankloop; }
		}

		if (runTy == Args.RunTy.treeGraph || runTy == Args.RunTy.treeGraphEdgeSort
			|| runTy == Args.RunTy.graph || runTy == Args.RunTy.graphEdgeSort) {
			graphL_mst.addAll(graphL_lst);
			for (Graph graph : graphL_mst) {
				String info = graph.mkInfoStr();
				PrintStream outOS = makeFilePrintStream(experimentDir, "0", runTy.toString(), graph.provenanceTy() + graph.provenanceRank(), outTy.toString());
				// pathTy.toString()
				if (runTy == Args.RunTy.treeGraph) {
					graph.makeDenseIds();
					dumpGraph(outOS, graph, info, outTy);
					dumpGraph_experiment(outExperimentOS, graph, info);
				}
				else if (runTy == Args.RunTy.graph) {
					dumpGraph(outOS, graph, info, outTy);
				}
				else if (runTy == Args.RunTy.treeGraphEdgeSort || runTy == Args.RunTy.graphEdgeSort) {
					dumpGraphEdges(outOS, graph, info);
				}
			}
		}
		else if (runTy == Args.RunTy.treeGraphCombo || runTy == Args.RunTy.graphCombo || runTy == Args.RunTy.graphEdgeSortCombo) {
			if (graphL_mst.isEmpty() || graphL_lst.isEmpty()) {
				throw new RuntimeException(progressMsg.error("what! #most/least-critical-paths: " + graphL_mst.size() + "/" + graphL_lst.size()));
			}

			Graph graph = graphL_mst.getFirst(); // graphL_mst.get(3);
			Graph graph_lst = graphL_lst.getFirst();

			PrintStream outOS = makeFilePrintStream(experimentDir, "0", runTy.toString(), graph.provenanceTy() + graph.provenanceRank(), outTy.toString());

			if (runTy == Args.RunTy.treeGraphCombo) {
				mergeTreeGraph(graph, graph_lst, EnumSet.of(Graph.MergeTy.least));
				graph.makeDenseIds();
				dumpGraph(outOS, graph, graph.mkInfoStr(), outTy);
				dumpGraph_experiment(outExperimentOS, graph, graph.mkInfoStr());
			}
			else if (runTy == Args.RunTy.graphCombo) {
				mergeGraph(graph, graph_lst, EnumSet.of(Graph.MergeTy.least));
				dumpGraph(outOS, graph, graph.mkInfoStr(), outTy);
			}
			else if (runTy == Args.RunTy.graphEdgeSortCombo) {
				mergeGraph(graph, graph_lst, EnumSet.of(Graph.MergeTy.least));
				dumpGraphEdges(outOS, graph, "*** sorted edges based on " + graph.mkInfoStr());
			}
		}
		
		progressMsg.dump_L1("All Done!");
		throw new RuntimeException("*** All Done! ***"); //	System.exit(0);
	}

	
	// -------------------------------------------------------------------------
	//
	// -------------------------------------------------------------------------	
	
	protected void makeTrace(PrintStream os, int rank, long beg_timeId, long end_timeId) {
		// Build a trace for rank

		boolean dbg = false;
				
		// ------------------------------------------------------------
		// Read trace records
		// ------------------------------------------------------------

		final long timeRange = end_timeId - beg_timeId;
		final int linenum = 0;
		final int numPixelH = (int)timeRange; // FIXME

		os.println("*** Trace at rank " + rank + " (min/max times: " + beg_timeId + " / " + end_timeId + "; range: " + timeRange + " ) ***");

		ProcessTimeline timeline = new ProcessTimeline(linenum, m_scopeMap, m_trace, rank, numPixelH, timeRange, beg_timeId);

		if (timeline.getData().isEmpty()) {
			timeline.readInData();
		}

		TraceDataByRank timelineData = timeline.getData();
		Vector<DataRecord> recordVec = timelineData.getListOfData();
		
		// ------------------------------------------------------------
		// Process trace records
		// ------------------------------------------------------------

		for (DataRecord x : recordVec) {
			if (dbg) { System.out.println("Record: " + x.toString()); }

			CallPath callpath = TaskPoint.getCallPath(m_scopeMap, x.cpId);
			
			TaskPoint taskPnt = ((/*TaskPoint.prev_rank()*/ x.metricId == Args.TaskPoint_Rank_ModelTaskFlag)
								 ? new TaskPointMdl(rank, x, callpath)
								 : new TaskPointSync(rank, x, callpath));
			os.println(taskPnt.toString(Args.pathLenSome, TaskNameArgs.debug_txt));
		}
		os.println();
	}
	
	
	protected Path makePath(int end_rank, long end_timeId, PathTy ty) {
		// Build a Task list, beginning from the *end*
		
		// There are two kinds of TaskPoint pairs:
		// - Two TaskPoints on the same rank represent computation (and may have model tasks).
		// - Two TaskPoints on different ranks represent joins (at synchronization). They do not represent computation.

		HashMap<Integer, Long> taskPointProgressMap = new HashMap<Integer, Long>();
		boolean didProgress = false;
		
		Path path = new Path(ty, end_rank);

		if (Args.doDebugPath) { System.out.println("makePath(): " + path.mkInfoStr(/*doXtra*/false)); }

		int rank = end_rank;
		long timeId = end_timeId;

		TaskPointSync taskPnt2 = null;
		
		while (true) {
			if (!didProgress && path.taskL.size() == 2) {
				progressMsg.dump_L1("makePath(): recovering " + path.mkInfoStr(/*doXtra*/false));
				didProgress = true;
			}
			
			boolean isTail = (taskPnt2 == null);

			TaskPointSync taskPnt1 = makeTaskPointSync(rank, timeId, isTail, ty);
			if (taskPnt1 == null) {
				break; // makeTaskPoint() throws exception if !isTail
			}

			if (Args.doDebugPath) { System.out.println("  sync: " + taskPnt1.toString(Args.pathLenAll, TaskNameArgs.debug_txt)); }
			
			// confirm we are not in a cycle (<rank, timeId> should not have been seen)
			Integer rank_int = new Integer(rank);
			Long lastTimeIdAtRank = taskPointProgressMap.get(rank_int);
			if ( !(lastTimeIdAtRank == null || timeId < lastTimeIdAtRank.longValue()) ) {
				throw new RuntimeException(progressMsg.error(path.mkInfoStr() + ": found cycle! repeated record: " + taskPnt1.toString(Args.pathLenAll, TaskNameArgs.debug_txt)));
			}
			taskPointProgressMap.put(rank_int, timeId); // returns old value

			// Task: <'taskPnt1', 'taskPnt2'>
			if (taskPnt2 != null) {
				makeTasks(path, taskPnt1, taskPnt2);
			}

			// prepare for next iteration
			taskPnt2 = taskPnt1;
			rank = taskPnt2.prev_rank();
			timeId = taskPnt2.prev_timeId();
			
			if (timeId == Args.TaskPoint_TimeId_NULL || rank < 0) {
				break; // at the beginning
			}
		}

		return path;
	}

	protected TaskPointSync makeTaskPointSync(int rank, long timeId, boolean isTail, PathTy ty) {
		if (!((ty == PathTy.most) || (ty == PathTy.least))) {
			throw new RuntimeException("invalid type: " + ty.toString());
		}

		// ------------------------------------------------------------
		// Find the TaskPoint's 'anchor' trace record.
		//
		// Find possible 'anchor' records. Co-opt built-in trace record
		// finder, which is not designed for exact matches
		// ------------------------------------------------------------

		final long timeRange = (isTail) ? 2 : 0;
		final int linenum = 0;
		final int numPixelH = 2 + Args.numPalmModelRecords; // edu.rice.cs.hpc.traceviewer.data.db.TraceDataByRank.getData() returns at least 2 records

		final int minRecords = (isTail) ? 2 : 1;
		final int maxRecords = numPixelH + 2;

		ProcessTimeline timeline = new ProcessTimeline(linenum, m_scopeMap, m_trace, rank, numPixelH, timeRange, timeId);

		if (timeline.getData().isEmpty()) {
			timeline.readInData();
		}
		TraceDataByRank timelineData = timeline.getData();
		Vector<DataRecord> recordVec = timelineData.getListOfData();

		// ------------------------------------------------------------
		// Find the actual 'anchor' record
		// ------------------------------------------------------------
		DataRecord fnd_record = null;

		if (recordVec.size() == 0 && isTail) {
			// an empty path is ok
			return null;
		}
		else if (minRecords <= recordVec.size() && recordVec.size() <= maxRecords) {
			if (isTail) {
				// assume final records are committed in following order: ...<most><least>
				int fnd_idx = (ty == PathTy.most) ? recordVec.size() - 2 : recordVec.size() - 1;
				fnd_record = recordVec.get(fnd_idx);
			}
			else {
				for (DataRecord x : recordVec) {
					if (x.timestamp == timeId) {
						fnd_record = x;
						break;
					}
				}
			}

			if (fnd_record == null) {
				throw new RuntimeException(progressMsg.error("no match! " + makeTask_msg(rank, timeId, recordVec)));
			}
		}
		else {
			throw new RuntimeException(progressMsg.error("#records! " + makeTask_msg(rank, timeId, recordVec)));
		}

		// INVARIANT: 'fnd_record' is valid
		assert (fnd_record != null);
				
		// ------------------------------------------------------------
		// Make the TaskPointSync
		// ------------------------------------------------------------		
		CallPath callpath = TaskPoint.getCallPath(m_scopeMap, fnd_record.cpId);
		TaskPointSync taskPnt = new TaskPointSync(rank, fnd_record, callpath);

		return taskPnt;
	}
	
	
	protected void makeTasks(Path path, TaskPointSync taskPnt_beg, TaskPointSync taskPnt_end) {
		
		boolean isComputeTask = (taskPnt_beg.rank() == taskPnt_end.rank());
		
		// Filter Tasks that represent joins where the source and sink are the same call site.
		// This join occurs for all-to-all synchronization (e.g., barrier (rank x) -> barrier (rank y)) 
		if (!isComputeTask) {
			if (taskPnt_beg.cctId() == taskPnt_end.cctId() && taskPnt_end.cost_us() == 0) {
				return;
			}
		}
		
		LinkedList<TaskPoint> taskPntL = new LinkedList<TaskPoint>();
		taskPntL.addLast(taskPnt_beg);
		
		// ------------------------------------------------------------
		// Find TaskPointMdl trace records representing computation
		// ------------------------------------------------------------
		if (isComputeTask) {
			final int rank = taskPnt_end.rank();
			final long begTimeId = taskPnt_beg.timeId(); // exclusive bound
			final long endTimeId = taskPnt_end.timeId(); // exclusive bound
	
			final int linenum = 0;
			final long timeRange = endTimeId - begTimeId;
			final int numPixelH = Args.numPalmModelRecords; // timeRange
			
			ProcessTimeline timeline = new ProcessTimeline(linenum, m_scopeMap, m_trace, rank, numPixelH, timeRange, begTimeId);
			
			if (timeline.getData().isEmpty()) {
				timeline.readInData();
			}
	
			TraceDataByRank timelineData = timeline.getData();
			Vector<DataRecord> recordVec = timelineData.getListOfData();		
			
			for (DataRecord record : recordVec) {
				if (/*TaskPoint.prev_rank()*/ record.metricId == Args.TaskPoint_Rank_ModelTaskFlag
					&& begTimeId < record.timestamp && record.timestamp < endTimeId) {
						
					CallPath callpath = TaskPoint.getCallPath(m_scopeMap, record.cpId);
					TaskPointMdl taskPnt = new TaskPointMdl(rank, record, callpath);
	
					if (Args.doDebugPath) { System.out.println("  mdl:  " + taskPnt.toString(Args.pathLenAll, TaskNameArgs.debug_txt)); }

					taskPntL.addLast(taskPnt);
				}
			}
		}
		
		// ------------------------------------------------------------
		//  Make Tasks (in reverse order)
		// ------------------------------------------------------------
		taskPntL.addLast(taskPnt_end);
		
		assert (taskPntL.size() >= 2);
		
		TaskPoint taskPnt2 = taskPntL.removeLast();
		boolean lastTaskInGroup = true;

		while (!taskPntL.isEmpty()) {
			TaskPoint taskPnt1 = taskPntL.removeLast();

			Task task = new Task(taskPnt1, taskPnt2);
			path.prependTask(task, lastTaskInGroup);
			
			lastTaskInGroup = false;
			taskPnt2 = taskPnt1; // prepare for next iteration
		}
	}
	
	
	private static String makeTask_msg(int rank, long timeId, Vector<DataRecord> recordVec) {
		String recordStr = "";
		for (DataRecord x : recordVec) {
			recordStr += "<" + x.toString() + "> ";
		}
		String str = "find-record(" + rank + ", " + timeId + ") gives " + recordVec.size() + " trace records: " + recordStr;
		return str;
	}
	
	// -------------------------------------------------------------------------
	//
	// -------------------------------------------------------------------------

	protected Graph makePathGraph(Path path, EnumSet<Graph.MergeTy> mergeTy, String info) {
		Graph graph = new Graph("path-graph");
		graph.add_provenance(path.mkInfoStr(), path.type().toString(), "" + path.endRank());

		// INVARIANT: for each task/edge, call 'graph.demandVertex()' exactly once
		loop: {
			ListIterator<Task> iter = path.taskL.listIterator();
			if (!iter.hasNext()) {
				break loop;
			}

			Task task1 = iter.next();
			Vertex vtx1 = graph.demandVertex(task1, Graph.MergeTy.full);

			while (iter.hasNext()) {
				Task task2 = iter.next();

				Vertex vtx2 = graph.demandVertex(task2, Graph.MergeTy.full);
				graph.demandEdge(vtx1, vtx2, Edge.Ty.forward, mergeTy);

				// prepare for next iteration
				task1 = task2;
				vtx1 = vtx2;
			}

			vtx1.m_isExit = true;
		}
		
		return graph;
	}

	// -------------------------------------------------------------------------
	//
	// -------------------------------------------------------------------------

	protected Graph makeTreeGraph(Path path, EnumSet<Graph.MergeTy> mergeTy) {
		Graph graph = new Graph("tree-graph");
		mergeTreeGraph(graph, path, mergeTy);
		return graph;
	}

	// Merge 'path' into 'graph'
	protected void mergeTreeGraph(Graph graph, Path path, EnumSet<Graph.MergeTy> mergeTy) {
		progressMsg.dump_L2("mergeTreeGraph(path): " + path.mkInfoStr());

		graph.add_provenance(path.mkInfoStr(), path.type().toString(), "" + path.endRank());

		loop: {
			ListIterator<Task> iter = path.taskL.listIterator();
			if (!iter.hasNext()) {
				break loop;
			}

			Task task1 = iter.next();
			Vertex vtx1 = graph.demandMatchingVertex(task1, mergeTy);
			
			while (iter.hasNext()) {
				Task task2 = iter.next();
								
				Graph.MatchingEdgeRet ret = graph.findMatchingOKEdge(vtx1, task2, mergeTy);
				Vertex vtx2 = ret.edge.target();
				if (!ret.isTask2Used) {
					task2.nullify();
				}
				
				// prepare for next iteration
				task1 = vtx2.task(); // use canonical task since task2 may be nullified
				vtx1 = vtx2;
			}

			vtx1.isExit(true); // TODO: make part of merge
		}
	}

	// Merge 'graph_y' into 'graph_x'
	protected void mergeTreeGraph(Graph graph_x, Graph graph_y, EnumSet<Graph.MergeTy> mergeTy) {
		progressMsg.dump_L2("mergeTreeGraph(graph): " + graph_y.mkInfoStr());

		graph_x.add_provenance(graph_y.mkInfoStr(), graph_y.provenanceTy(), graph_y.provenanceRank());

		Vertex root_y = graph_y.root();
		if (root_y == null) {
			return;
		}
		
		Vertex root_x = graph_x.demandMatchingVertex(root_y, mergeTy);
		graph_x.mergeVertexDeep(root_x, graph_y, root_y, mergeTy);
	}


	// -------------------------------------------------------------------------
	//
	// -------------------------------------------------------------------------

	protected Graph makeGraph(Path path, EnumSet<Graph.MergeTy> mergeTy) {
		Graph graph = new Graph("graph");
		mergeGraph(graph, path, mergeTy);
		return graph;
	}

	// Merge 'path' into 'graph'
	protected void mergeGraph(Graph graph, Path path, EnumSet<Graph.MergeTy> mergeTy) {
		progressMsg.dump_L2("mergeGraph(path) beg: " + path.mkInfoStr());
		
		graph.add_provenance(path.mkInfoStr(), path.type().toString(), "" + path.endRank());

		// INVARIANT: for each task/edge, call 'graph.demandMatchingVertex()' exactly once
		loop: {
			ListIterator<Task> iter = path.taskL.listIterator();
			if (!iter.hasNext()) {
				break loop;
			}

			Task task1 = iter.next();
			Vertex vtx1 = graph.demandMatchingVertex(task1, mergeTy);
			vtx1.isEntry(true); // TODO: make part of merge

			while (iter.hasNext()) {
				Task task2 = iter.next();

				Vertex vtx2 = graph.demandMatchingVertex(task2, mergeTy);
				graph.demandMergeMatchingEdge(vtx1, vtx2, Edge.Ty.forward, mergeTy);

				// prepare for next iteration
				task1 = task2;
				vtx1 = vtx2;
			}

			vtx1.isExit(true); // TODO: make part of merge
		}
		
		progressMsg.dump_L2("mergeGraph(path) end");
	}

	// Merge 'graph_y' into 'graph_x'
	protected void mergeGraph(Graph graph_x, Graph graph_y, EnumSet<Graph.MergeTy> mergeTy) {
		progressMsg.dump_L2("mergeGraph(graph) beg: " + graph_y.mkInfoStr());
	
		graph_x.add_provenance(graph_y.mkInfoStr(), graph_y.provenanceTy(), graph_y.provenanceRank());

		// INVARIANT: for each task/edge, call 'graph.demandMatchingVertex()' exactly once

		for (Vertex vertex_y : graph_y.vertexL) {
			Vertex vertex_x = graph_x.demandMatchingVertex(vertex_y, mergeTy);
			if (vertex_y.isEntry()) { vertex_x.isEntry(true); /* TODO: make part of merge */ }
			if (vertex_y.isExit()) { vertex_x.isExit(true); /* TODO: make part of merge */ }
		}

		for (Edge edge_y : graph_y.edgeL) {
			graph_x.demandMergeMatchingEdge(edge_y, Edge.Ty.forward, mergeTy);
		}
		
		progressMsg.dump_L2("mergeGraph(graph) end");
	}

	
	// -------------------------------------------------------------------------
	//
	// -------------------------------------------------------------------------

	protected static void dumpPath_txt(PrintStream os, Path path) {
		os.println("*** " + path.mkInfoStr() + " ***");
		int i_record = 0;
		TaskPoint prev_taskPoint_end = null;
		for (Task x : path.taskL) {			
			boolean doBegEnd = (prev_taskPoint_end == null || x.task_beg().cctId() != prev_taskPoint_end.cctId());
			os.println("--- task " + i_record + ": " + x.toString(doBegEnd, Args.pathLenAll, TaskNameArgs.debug_txt));

			prev_taskPoint_end = x.task_end();
			i_record++;
		}
	}

	
	protected void dumpGraph(PrintStream os, Graph graph, String comment, Args.OutputTy outTy) {
		//assert (graph.hasDenseIds());

		progressMsg.dump_L2("dumpGraph(): " + graph.mkInfoStr());

		// ------------------------------------------------------------
		// Header
		// ------------------------------------------------------------
		String hdr = "";
		switch (outTy) {
		case dot:
			hdr = "// " + comment + "\n" + "digraph critical_path {";
			// node [shape=rectangle];
			break;
		case gml:
			hdr = "graph [\n" + "comment \"" + comment + "\"\n" + "directed 1";
			break;
		case graphml:
			hdr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
					+ "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd\" xmlns:y=\"http://www.yworks.com/xml/graphml\">\n"
					+ "  <!--" + comment + "-->\n"
					+ "  <key for=\"node\" id=\"" + Vertex.attrNode         + "\" yfiles.type=\"nodegraphics\"/>\n"
					+ "  <key for=\"node\" id=\"" + Vertex.attrNodeCostPct  + "\" attr.name=\"task_costPct\"  attr.type=\"double\"><default>0.0</default></key>\n"
					+ "  <key for=\"node\" id=\"" + Vertex.attrNodeCostIPct + "\" attr.name=\"task_costIPct\" attr.type=\"double\"><default>0.0</default></key>\n"
					+ "  <key for=\"node\" id=\"" + Vertex.attrNodeSlackPct + "\" attr.name=\"task_slackPct\" attr.type=\"double\"><default>0.0</default></key>\n"
					+ "\n"
					+ "  <key for=\"edge\" id=\"" + Edge.attrEdge           + "\" yfiles.type=\"edgegraphics\"/>\n"
					+ "  <key for=\"edge\" id=\"" + Edge.attrEdgeInstancesPct_cost  + "\" attr.name=\"edge_instancesPct_cost\"  attr.type=\"double\"><default>0.0</default></key>\n"
					+ "  <key for=\"edge\" id=\"" + Edge.attrEdgeInstancesPct_slack + "\" attr.name=\"edge_instancesPct_slack\" attr.type=\"double\"><default>0.0</default></key>\n"	
					+ "\n"
					+ "  <graph edgedefault=\"directed\" id=\"critical_path\">\n";
				break;
		case txt:
			hdr = "graph: " + comment + "\n";
			break;
		}
		os.println(hdr);

		// ------------------------------------------------------------
		// Vertices
		// ------------------------------------------------------------

		double ttl_cost_us = graph.cost_us();
		double ttl_slack_us = graph.slack_us();

		for (Vertex x : graph.vertexL) {
			int pathLen = Math.min(Args.pathLenSome, Args.taskNameId_pathLen);
			String str = "";
			switch (outTy) {
			case dot:
				str = x.toString_dot(pathLen, TaskNameArgs.txt, "  ");
				break;
			case gml:
				str = x.toString_gml(pathLen, TaskNameArgs.txt, "  ");
				break;
			case graphml:
				str = x.toString_graphml(pathLen, TaskNameArgs.xml, ttl_cost_us, ttl_slack_us, "    ");
				break;
			case txt:
				str = x.toString(pathLen, TaskNameArgs.txt);
				break;
			}
			os.println(str);
		}

		// ------------------------------------------------------------
		// Edges
		// ------------------------------------------------------------

		double ttl_n_instances_cost = graph.edge_weight_cost();
		double ttl_n_instances_slack = graph.edge_weight_slack();

		for (Edge x : graph.edgeL) {
			String str = "";
			switch (outTy) {
			case dot:
				str = x.toString_dot("  ");
				break;
			case gml:
				str = x.toString_gml("  ");
				break;
			case graphml:
				str = x.toString_graphml(ttl_n_instances_cost, ttl_n_instances_slack, "    ");
				break;
			case txt:
				//str = x.toString();
				break;
			}
			os.println(str);
		}

		// ------------------------------------------------------------
		// Footer
		// ------------------------------------------------------------
		String ftr = "";
		switch (outTy) {
		case dot:
			ftr = "}";
			break;
		case gml:
			ftr = "]";
			break;
		case graphml:
			ftr = "  </graph>\n" + "</graphml>\n";
			break;
		case txt:
			break;
		}
		os.println(ftr);
	}

	
	protected void dumpGraph_experiment(PrintStream os, Graph graph, String comment) {
		// Notes on 'Experiment' format (unwritten documentation):
		// - LoadModuleTable and FileTable ids must be unique across *different* tables
		// - PF elements must have f attribute
		// - PF elements must have n (ProcedureTable index) or p (string) attribute
		// - PF cannot be directly nested within another PF
		// - PF elements *can* be leaves without special syntax
		
		progressMsg.dump_L2("dumpGraph_experiment(): " + graph.mkInfoStr());

		assert (graph.hasDenseIds());
		
		XMLTablesScopeVisitor xmlTables = new XMLTablesScopeVisitor();
		m_experiment.getRootScope().dfsVisitScopeTree(xmlTables);
		
		String nm = "criticalpaths";
		if (m_experiment.getRootScope().getSubscopeCount() > 0) {
			nm = m_experiment.getRootScope().getSubscope(0).getName();
		}
		
		String[] m_id_vec = { "1", "2", "3", "4" };
		String[] m_nm_vec = { "cost-us", "slack-us", "cost-#", "slack-#" };
		
		os.println("<HPCToolkitExperiment version=\"2.0\">");
		os.println("<!--" + comment + "-->");
		os.println("<Header n=\"" + nm + "\">");
		os.println("</Header>");
		os.println("<SecCallPathProfile i=\"0\" n=\"" + nm + "\">");
		os.println("<SecHeader>");
		os.println("  <MetricTable>");
		for (int i = 0; i < m_id_vec.length; i++) {
			os.println("    <Metric i=\"" + m_id_vec[i] + "\" n=\"" + m_nm_vec[i] + "\" v=\"raw\" show=\"1\" show-percent=\"1\"></Metric>");
		}
		os.println("  </MetricTable>");

		os.println("  <MetricDBTable>");
		os.println("  </MetricDBTable>");
		
		os.println("  <LoadModuleTable>");
		for (Map.Entry<String, Integer> entry : xmlTables.lmMap.entrySet()) {
		    String key = XMLTags.escape(entry.getKey());
		    Integer value = entry.getValue();
			os.println("  <LoadModule i=\"" + value + "\" n=\"" + key + "\"/>");
		}
		os.println("  </LoadModuleTable>");

		os.println("  <FileTable>");
		for (Map.Entry<String, Integer> entry : xmlTables.fileMap.entrySet()) {
		    String key = XMLTags.escape(entry.getKey());
		    Integer value = entry.getValue();
			os.println("  <File i=\"" + value + "\" n=\"" + key + "\"/>");
		}
		os.println("  </FileTable>");

		os.println("  <ProcedureTable>");
		for (Map.Entry<String, Integer> entry : xmlTables.procMap.entrySet()) {
		    String key = XMLTags.escape(entry.getKey());
		    Integer value = entry.getValue();
			os.println("  <Procedure i=\"" + value + "\" n=\"" + key + "\"/>");
		}
		os.println("  </ProcedureTable>");
		os.println("</SecHeader>");

		os.println("<SecCallPathProfileData>");
		
		Vertex root = graph.root();
		if (root != null) {
			String x = root.toString_experiment("", xmlTables, m_id_vec);
			os.println(x);
		}
		
		os.println("</SecCallPathProfileData>");

		os.println("</SecCallPathProfile>");
		os.println("</HPCToolkitExperiment>");
	}

	
	protected void dumpGraphEdges(PrintStream os, Graph graph, String comment) {
		final String leader = "\n*** ";

		os.println(comment);

		// ------------------------------------------------------------
		// Edges sorted by cost
		// ------------------------------------------------------------

		os.println(leader + "edges, sorted by cost at edge's target (vertex's ending TaskPoint). NOTE: Misses cost of first task!");

		Collections.sort(graph.edgeL, new Comparator<Edge>() {
			@Override
			public int compare(Edge x, Edge y) {
				return Edge.compare_ty_name_weightCost(x, y);
				//return Edge.compare_ty_cost(x, y);
			}
		});

		
		String             prev_nm = "";
		int                prev_nm_id = 0;
		HashSet<Vertex>    prev_tgtSet = new HashSet<Vertex>();
		HashSet<ModelArgs> prev_argsSet = new HashSet<ModelArgs>();
		ModelArgs          prev_argsSum = new ModelArgs(); // weighted args
		double             prev_costSum = 0.0;
		long      		   prev_weightSum = 0;

		for (Edge x : graph.edgeL) {
			String indent = "  ";
			String nm = x.target().name(Args.pathLenOne, TaskNameArgs.txt);
			long weight = x.weight_cost(); // incoming edge count
			ModelArgs args = x.target().modelArgs(); // for Model tasks, 'args' will be same for all in-coming edges to a vertex 
			
			if (nm.compareTo(prev_nm) != 0) {
				if (prev_nm_id > 0) {
					String args_str = "";
					if (!prev_argsSet.isEmpty()) {
						assert (prev_argsSum.weight() == prev_weightSum);
						args_str = " " + prev_argsSum.toString_txt();
						//if (prev_argsSum.weight() != prev_weightSum) {
						//	args_str += " +++ args/weight: " + prev_argsSum.weight() + ", "+ prev_weightSum + "+++";
						//}
					}

					os.println(indent + "* total(cost-weight-args): " + prev_nm_id + ". " + prev_nm + " " + prev_costSum + " " + prev_weightSum + args_str + "\n");
				}
				prev_nm = nm;
				prev_nm_id++;
				prev_tgtSet.clear();
				prev_argsSet.clear();
				prev_argsSum.clear();
				prev_costSum = 0.0;
				prev_weightSum = 0;
			}
			
			String info = indent + prev_nm_id + ". " + nm + ", " + weight + "\n";		
			os.println(info + x.toString_txt(Math.min(Args.pathLenSome, Args.taskNameId_pathLen), indent, true/*doMultiLine*/));

			if (!prev_tgtSet.contains(x.target())) {
				prev_tgtSet.add(x.target());
				prev_costSum += x.cost_s();
			}
			prev_weightSum += weight;
			if (args != null && !prev_argsSet.contains(args)) {
				prev_argsSet.add(args);
				prev_argsSum.merge(args);
				//os.println(indent + "^^^ args/weight: " + args.weight() + ", " + weight + " // " + prev_argsSum.weight() + ", " + prev_weightSum + "^^^");
			}
		}

		// ------------------------------------------------------------
		// Edges sorted by slack
		// ------------------------------------------------------------
		
		os.println(leader + "edges, sorted by slack at edge's target (vertex's ending TaskPoint)");

		Collections.sort(graph.edgeL, new Comparator<Edge>() {
			@Override
			public int compare(Edge x, Edge y) {
				return Edge.compare_slack(x, y);
			}
		});

		for (Edge x : graph.edgeL) {
			os.println(x.toString_txt(Args.pathLenAll, "  ", true/*doMultiLine*/));
		}

	}
		
	
	protected static PrintStream makeFilePrintStream(File dir, String name_pfx, String name_base, String name_sfx, String ext) {
		PrintStream stream = null;
		try {
			String fnm = "";

			if (!name_pfx.isEmpty()) {
				fnm += name_pfx + "-";
			}
			fnm += name_base;
			if (!name_sfx.isEmpty()) {
				fnm += "-" + name_sfx;
			}
			fnm += "." + ext;
				
			File file = new File(dir, fnm);
			file.createNewFile();
			FileOutputStream file_os = new FileOutputStream(file);
			stream = new PrintStream(file_os);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return stream;
	}
	
	// -------------------------------------------------------------------------
	// PathTy
	// -------------------------------------------------------------------------

	protected enum PathTy {
		most, least
	}

	// -------------------------------------------------------------------------
	// Path:
	// -------------------------------------------------------------------------

	protected static class Path {
		public LinkedList<Task> taskL;

		private PathTy m_ty;
		private int m_endRank;

		private long m_cost_us;  // total path cost
		private long m_slack_us; // total path slack		
		
		public Path(PathTy ty, int endRank) {
			taskL = new LinkedList<Task>();

			m_ty = ty;
			m_endRank = endRank;

			m_cost_us = 0;
			m_slack_us = 0;
			
			m_taskSync_end = null;
			m_task_seq_cost_us = 0;
		}
		
		public void nullify() {
			taskL.clear();
		}

		public PathTy type() {
			return m_ty;
		}

		public int endRank() {
			return m_endRank;
		}

		public long cost_us() {
			return m_cost_us;
		}

		public double cost_s() {
			return (double) (cost_us()) / 1.0e6;
		}

		public long slack_us() {
			return m_slack_us;
		}

		public double slack_s() {
			return (double) (slack_us()) / 1.0e6;
		}

		public String mkInfoStr() {
			return mkInfoStr(/*doXtra*/true);
		}

		public String mkInfoStr(boolean doXtra) {
			String info = "path '" + type().toString() + "_" + endRank() + "'";
			if (doXtra) {
				info += " (length: " + taskL.size()  + "; cost (s): " + String.format(Args.fmt_dbl, cost_s()) + "; slack (s): " + String.format(Args.fmt_dbl, slack_s()) + ")";
			}
			return info;
		}
		
		// ------------------------------------------------------------
		// Construction helper that maintains state (yuck!)
		// ------------------------------------------------------------

		private Task m_taskSync_end; // last seen TaskPointSync
		private long m_task_seq_cost_us;

		public void prependTask(Task task1, boolean lastTaskInGroup) {
			// ----------------------------------------
			// Use task processing sequence to update task metrics.
			// Assumes the following INVARIANTS:
			// 1. Tasks are added in groups. The tasks are formed from a sequence of TaskPoints.
			//    The first and last TaskPoints are 'TaskPointSync'; there may be TaskPointMdl in between.
			//    TaskPoints: [TaskPointSync_1 -> TaskPointMdl_2 -> TaskPointMdl_3 -> TaskPointSync_4]
			//    Tasks:      1:[TaskPointSync_1 -> TaskPointMdl_2] ->  2:[TaskPointMdl_2 -> TaskPointMdl_3] -> 3:[TaskPointMdl_3 -> TaskPointSync_4]
			// 2. Tasks are added in reverse order, i.e. task 3 (above) is added before task 2.
			//    Thus, when task 3 is added, 'lastTaskInGroup' is true.
			// 3. Both task1 and task2 (below) should be within the *same* group
			// ----------------------------------------
			
			boolean firstTaskInGroup = (task1.task_beg() instanceof TaskPointSync);
			
			Task task2 = taskL.isEmpty() ? null : taskL.getFirst();			
			if (lastTaskInGroup) {
				task2 = m_taskSync_end;
			}
			
			if (task1.isSync()) {
				m_cost_us += task1.cost_us();
			}

			taskL.addFirst(task1);
			//if ((taskL.size() % (2 << 15)) == 0) { System.out.println("* path size: " + taskL.size()); }

			// ----------------------------------------
			// Process pairs of task within a task group
			// ----------------------------------------
			if (task2 != null) {						
				if (task1.isSync() && task2.isSync()) {
					// 1. cost1: done; slack1: compute; cost2: done; slack2: done 
					long slack1_us = ((TaskPointSync) task2.task_end()).slack_prev_us();
					task1.slack_us(slack1_us);
					m_slack_us += slack1_us;
				}
				else if (task1.isModel() && task2.isSync()) {
					// 2. cost1: compute with 3|4; slack1 = 0; cost2: done; slack2 = done
				}
				else if (task1.isModel() && task2.isModel()) {
					// 3. cost1: compute with 4; slack1 = 0; cost2: compute; slack2 = 0
					assert (m_taskSync_end != null);
					long max_us = m_taskSync_end.cost_us() - m_task_seq_cost_us;
					long proposed_us = Math.max(0, task2.task_end().timeId() - task1.task_end().timeId());
					long cost2_us = Math.min(max_us, proposed_us);
					task2.cost_us(cost2_us);
					m_task_seq_cost_us += cost2_us;
				}
				else { throw new RuntimeException("Must perform one of Case 1..3!"); }

				// Extra step for 'cost1'
				if (firstTaskInGroup && task1.isModel()) {
					// 4. cost1: <compute>; slack1 = 0; cost2: <done>; slack2 = 0
					assert (m_taskSync_end != null);
					long cost1_us = Math.max(0, m_taskSync_end.cost_us() - m_task_seq_cost_us);
					task1.cost_us(cost1_us);
					m_task_seq_cost_us += cost1_us; // unnecessary

					// Convert cost of 'm_taskSync_end' to exclusive metric
					m_taskSync_end.cost_us(0);
				}
			}
			
			// ----------------------------------------
			// Update helper state for the task group
			// ----------------------------------------
			if (task1.isSync()) {
				m_taskSync_end = task1; // end of the task group
				m_task_seq_cost_us = 0;
			}
		}

	}

	// -------------------------------------------------------------------------
	// TaskPointSync: A task point for synchronization.
	// -------------------------------------------------------------------------	
	protected static class TaskPointSync extends TaskPoint {
		private DataRecord m_data;
				
		public TaskPointSync(int rank, DataRecord data, CallPath callpath) {
			super(rank, callpath);
			m_data = data;
		}
	
		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------
		
		public long timeId() {
			return m_data.timestamp; // tail_timeId
		}
	
		public int cctId() {
			return m_data.cpId; // tail_cctNodeId
		}
	
		// See notes on 'Task'
		public long cost_us() {
			final long cost_slack = m_data.time3; // tail_cost_slack_us
			return (cost_slack >>> 32); // top 32 bits
		}
		
		// See notes on 'Task'
		public long slack_prev_us() {
			final long cost_slack = m_data.time3; // tail_cost_slack_us
			return (long)((int)cost_slack); // bottom 32 bits
		}
	
		public int prev_rank() {
			return m_data.metricId; // prev_rank
		}
	
		public long prev_timeId() {
			return m_data.time2; // prev_time
		}
	
		public String toString(int pathLen, final EnumSet<TaskNameArgs> args) {
			return makeName(pathLen, args);
		}
		
		public String toStringAnnotated(int pathLen, final EnumSet<TaskNameArgs> args) {
			return makeName(pathLen, args);
		}

		public String xtraString() {
			return "prev " + String.format(Args.fmt_int, prev_rank()) + ":" + prev_timeId() + " cst/slk " + cost_us() + " " + slack_prev_us();
		}
	}
	
	
	// -------------------------------------------------------------------------
	// TaskPointMdl: A task point for modeling. (A task is defined by two consecutive TaskPoints.)
	// -------------------------------------------------------------------------

	protected static class TaskPointMdl extends TaskPoint {
		private DataRecord m_data; // list or vector
		private ModelArgs m_args;

		public TaskPointMdl(int rank, DataRecord data, CallPath callpath) {
			super(rank, callpath);
			m_data = data;
			m_args = new ModelArgs();
			
			final long val12 = m_data.time2; // prev_timeId 
			final long val34 = m_data.time3; // tail_cost_us 
			
			m_args.set(0, (val12 >>> 32));     // top 32 bits
			m_args.set(1, (long)((int)val12)); // bottom 32 bits
			m_args.set(2, (val34 >>> 32));     // top 32 bits
			m_args.set(3, (long)((int)val34)); // bottom 32 bits
			
			assert (rank_flag() == Args.TaskPoint_Rank_ModelTaskFlag);
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public long timeId() {
			return m_data.timestamp; // tail_timeId
		}

		public int cctId() {
			return m_data.cpId; // tail_cctNodeId
		}

		public int rank_flag() {
			return m_data.metricId; // prev_rank
		}

		public ModelArgs args() {
			return m_args;
		}
		
		public String toString(int pathLen, final EnumSet<TaskNameArgs> args) {
			return makeName(pathLen, args);
		}
		
		public String toStringAnnotated(int pathLen, final EnumSet<TaskNameArgs> args) {
			String x = makeName(pathLen, args);
			x += " " + m_args.toString();
			return x;
		}
		
		public String xtraString() {
			return m_args.toString();
		}

		//protected String makeName(boolean doCPId, boolean doLong, boolean doXML) {
		//	return super.makeName(doCPId, doLong, doXML);
		//}
	}

	
	// -------------------------------------------------------------------------
	// TaskPoint: A task 'end point.' A task is defined by two consecutive task end points.
	// -------------------------------------------------------------------------
	
	protected static abstract class TaskPoint {
		int m_rank;
		private CallPath m_callpath; // corresponds to 'm_cctId'
		//private String m_name; // corresponds to 'm_cctId'
	
		public TaskPoint(int rank, CallPath callpath) {
			m_rank = rank;
			m_callpath = callpath;
			//m_name = null; // constructed on demand
		}

		public int rank() {
			return m_rank;
		}

		public abstract long timeId();

		public abstract int cctId();
		
		public String toString(int pathLen, final EnumSet<TaskNameArgs> args) {
			return makeName(pathLen, args);
		}
		
		public abstract String toStringAnnotated(int pathLen, final EnumSet<TaskNameArgs> args); // hack

		public abstract String xtraString();

	
		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------
	
		public String makeName_experiment_topdown(XMLTablesScopeVisitor xmlTables) {
			String str_beg = "";
			String str_end = "";
			
			if (m_callpath != null) {			
				LinkedList<Scope> fnPathL = getScopePath(m_callpath, /*doTopDown*/true, Args.pathLenAll);
				for (Scope scope : fnPathL) {
					str_beg += getName_experiment_topdown(scope, xmlTables);
					str_end += "</PF>\n";
					str_end += "</C>\n";
				}
			}
			
			return str_beg + str_end;
		}
	
		public String makeName_experiment_bottomup(XMLTablesScopeVisitor xmlTables) {
			String str_beg = "";
			String str_end = "";
			
			if (m_callpath != null) {
				final int runtimePathLen = 2;
				LinkedList<Scope> fnPathL = getScopePath(m_callpath, /*doTopDown*/false, Args.pathLenAll);

				// N.B.: The (forward) iteration is bottom-up
				Scope callee = null;
				ListIterator<Scope> iter = fnPathL.listIterator();
				for (int i = 0; iter.hasNext(); i++) {
					Scope caller = iter.next();

					if (i >= runtimePathLen && callee != null) {
						str_beg += getName_experiment_bottomup(caller, callee, xmlTables);
						str_end += "</C>\n";
						str_end += "</Pr>\n";
						str_end += "</PF>\n";
					}
					
					callee = caller;
				}
			}
			
			return "  <C l=\"0\">\n"
				 + str_beg
				 + str_end
				 + "  </C>\n";
		}
	
		
		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------
		
		// N.B.: Matcher.match() requires that the RE match the entire input string. Matcher.find() simply finds the RE.
		// (?:hpcrun.+|CriticalPath_addTask.*|doSample_.+)
		
		protected String makeName(int pathLen, final EnumSet<TaskNameArgs> args) {
			//if (m_name != null) {
			//	return m_name;
			//}
			// m_name = makeName_x(...);
			return makeName_x(pathLen, args);
		}

		protected String makeName_x(int pathLen, final EnumSet<TaskNameArgs> args) {
			final boolean doXML = args.contains(TaskNameArgs.fmt_xml);
			final boolean doOneLine = !args.contains(TaskNameArgs.line_multi);

			final String newline_str = (doXML) ? XMLTags.newline : "\\n";
			
			final String field_beg_str = (doOneLine && !doXML) ? "{" : "";
			final String field_end_str = (doOneLine && !doXML) ? "}" : "";

			final String field_sep_str = (doOneLine) ? " " : newline_str; // separator

			final String fn_call_str = (!doOneLine) ? ((doXML) ? "&gt;" : ">") : "";
			final String fn_beg_str = (doOneLine && !doXML) ? "" : "";
			final String fn_end_str = (doOneLine && !doXML) ? "" : "";			
			final String fn_sep_str = (doOneLine) ? "," : newline_str;


			String infoStr = "";
			if (args.contains(TaskNameArgs.xtra_traceId)) {
				infoStr += field_beg_str + String.format(Args.fmt_int, rank()) + ":" + timeId() + field_end_str + field_sep_str;
				infoStr += field_beg_str + xtraString() + field_end_str + field_sep_str;
			}

			if (args.contains(TaskNameArgs.xtra_cpId)) {
				infoStr += field_beg_str + "cpId " + cctId() + field_end_str + field_sep_str;
			}

			String nmStr = "";
			if (m_callpath != null) {
				
				final int runtimeCtxtSz = 3;
				int pathLen_xtra = (int)Math.min(((long)pathLen + (long)runtimeCtxtSz), (long)Integer.MAX_VALUE);

				LinkedList<Scope> fnPathL = getScopePath(m_callpath, /*doTopDown*/false, pathLen_xtra);

				// The name must include inner-most "application" frame. The other (outer) frames are helpful context. 
				// Therefore, use bottom-up (forward) iteration.
				boolean showFn = false;
				String nm_qual = null;
				String nm_callee = null;
				ListIterator<Scope> iter = fnPathL.listIterator();
				for (int i = 0; iter.hasNext() && (i < pathLen); /**/) {
					Scope scope = iter.next();
				
					String nm = getName(scope, /*doCallsite*/Args.taskName_useCallsite); // scope.getToolTip();

					if (mpi_fn_re.matcher(nm).find()) {
						if (!Args.taskName_useCallsite) {
							nm_qual = "[" + getNameCallsite(scope) + "]"; // capture callsite for task end
						}
						showFn = true; // begin appending with this frame
					}
					
					if (showFn) {
						// N.B.: hack to remove useless inlining information
						if (nm_callee != null && nm_callee.contains(inlineTag_old) && nm_callee.contains(nm)) {
							// strip the 'inline' tag on 'nm_callee', which is at the beginning of 'nmStr' (and normalized)
							nmStr = inlineTag_new_re.matcher(nmStr).replaceFirst("*");
							continue;
						}

						String nm_norm = normalizeName(nm, doXML);
						if (nm_qual != null && nmStr.isEmpty()) {
							nm_norm += nm_qual;
						}
						
						String sep = (iter.hasNext() && (i < (pathLen - 1))) ? (fn_sep_str + fn_call_str) : "";
						
						nmStr = sep + fn_beg_str + nm_norm + fn_end_str + nmStr;
						i++;
						nm_callee = nm;
					}
					
					if (palm_task_fn_re.matcher(nm).find()) {
						nm_qual = "[task:" + getNameCallsite(scope) + "]"; // capture callsite for task end
						showFn = true; // begin appending with next frame
					}
				}
			}
			
			if (nmStr.isEmpty()) {
				nmStr = "{*}";
			}
	
			// Record newly constructed name
			String str = infoStr + nmStr;
			return str;
		}
		
		static final String inlineTag_old = "[I] ";
		static final String inlineTag_new = "i.";
		static final Pattern inlineTag_old_re = Pattern.compile("\\[I\\] "); // N.B.: one backslash is needed for Java!
		static final Pattern inlineTag_new_re = Pattern.compile("i\\.");

		static final Pattern mpi_fn_re = Pattern.compile("[pP]?[mM][pP][iI]_");
		static final Pattern palm_task_fn_re = Pattern.compile("palm_task");

	
		public static String normalizeName(String str, boolean doXML) {
			String str_out = "";

			//String str_in = str.replace(Args.inline_tag_old, Args.inline_tag_new);
			String str_in = inlineTag_old_re.matcher(str).replaceFirst(inlineTag_new);
			
			int level_paren = 0;
			int level_angleBracket = 0;
			for (int i = 0; i < str_in.length(); i++) {
				char c = str_in.charAt(i);
	
				if (c == '(') {
					if (level_paren == 0) {
						str_out += c;
					}
					level_paren++;
				}
				else if (c == ')') {
					level_paren--;
					if (level_paren == 0) {
						str_out += c;
					}
				}
				else if (c == '<' && level_paren == 0) {
					if (level_angleBracket == 0) {
						String c1 = (doXML) ? "[" /*XMLTags.html_lt*/ : Character.toString(c);
						str_out += c1;
					}
					level_angleBracket++;
				}
				else if (c == '>' && level_paren == 0) {
					level_angleBracket--;
					if (level_angleBracket == 0) {
						String c1 = (doXML) ? "]" /*XMLTags.html_gt*/ : Character.toString(c);
						str_out += c1;
					}
				}
				else {
					if (level_paren == 0 && level_angleBracket == 0) {
						String c1 = Character.toString(c);
						if (doXML && c == '&') {
							c1 = XMLTags.html_amp;
						}
						str_out += c1;
					}
				}
			}
	
			return str_out;
		}
		
		// edu.rice.cs.hpc.traceviewer.data.timeline.ProcessTimeline.getCallPath()
		public static CallPath getCallPath(HashMap<Integer, CallPath> scopeMap, int cpid) {
			if (cpid == 0) {
				return null;
			}
			else {
				return scopeMap.get(cpid);
			}
		}	
		
		// FIXME: edu.rice.cs.hpc.traceviewer.data/graph/CallPath.java
		public static LinkedList<Scope> getScopePath(CallPath callpath, boolean doTopDown, int pathLen) {
			// N.B.: CallSiteScope represents the arc <callsite->procedure>, where the LineScope represents the source callsite and the ProcedureScope represents the target procedure
	
			LinkedList<Scope> path = new LinkedList<Scope>();
	
			for (Scope scope = callpath.getBottomScope(); !(scope instanceof RootScope) && (path.size() <= pathLen); scope = scope.getParentScope()) {
				if ((scope instanceof ProcedureScope) || (scope instanceof CallSiteScope)) {
				//if (scope instanceof CallSiteScope) {
					if (doTopDown) {
						path.add(0, scope);
					}
					else {
						path.add(scope);
					}
				}
			}
	
			return path;
		}
	
		public static String getName(Scope scope, boolean doCallsite) {
			String nm = null;
			if (scope instanceof CallSiteScope) {
				CallSiteScope x = (CallSiteScope) scope;
				nm = x.getProcedureScope().getName();
			}
			else {
				nm = scope.getName();
			}
			
			if (doCallsite) {
				nm += "[" + getNameCallsite(scope) + "]";
			}

			return nm;
		}
		
		public static String getNameCallsite(Scope scope) {
			// N.B.: The first line number seems to be off by 1...
			String nm = null;
			if (scope instanceof CallSiteScope) {
				CallSiteScope x = (CallSiteScope) scope;
				nm = x.getLineScope().getSourceFile().getName() + ":" + (x.getLineScope().getFirstLineNumber() + 1);
			}
			else {
				nm = scope.getSourceFile().getName() + ":" + (scope.getFirstLineNumber() + 1);
			}
			
			return nm;
		}

		
		public static String getName_experiment_topdown(Scope scope, XMLTablesScopeVisitor xmlTables) {			
			// N.B.: CallSiteScope represents the arc <callsite->procedure>, where the LineScope represents the source callsite and the ProcedureScope represents the target procedure
	
			String callsite_ln = "0";
			if (scope instanceof CallSiteScope) {
				CallSiteScope x = (CallSiteScope) scope;
				callsite_ln = "" + (x.getLineScope().getFirstLineNumber() + 1);
			}
			
			// for CallSiteScope: getName() and getSourceFile() are w.r.t. getProcedureScope()
			String frame_f = scope.getSourceFile().getFilename().toString();
			String frame_n = scope.getName();
			String frame_ln = "" + (scope.getFirstLineNumber() + 1);
	
			String frame_f_id = "" + xmlTables.demandId_file(frame_f);
			String frame_n_id = "" + xmlTables.demandId_proc(frame_n);
			
			return "<C l=\"" + callsite_ln + "\">\n"
				 + "<PF f=\"" + frame_f_id + "\" n=\"" + frame_n_id + "\" l=\"" + frame_ln + "\">\n";
		}
	
		public static String getName_experiment_bottomup(Scope caller, Scope callee, XMLTablesScopeVisitor xmlTables) {			
			// N.B.: CallSiteScope represents the arc <callsite->procedure>, where the LineScope represents the source callsite and the ProcedureScope represents the target procedure
			
			// Transformation to render in "top-down" view
			//   PF caller file/name/line       ==>  PF callee file/name/line    
	 		//     C callsite line [in caller]         Pr caller file/name [alien]
			//       PF callee file/name/line            C callsite line [now in caller]
			//                                             PF caller file/name/line
			
			String frame_f = callee.getSourceFile().getFilename().toString();
			String frame_n = callee.getName();
			String frame_ln = "" + (callee.getFirstLineNumber() + 1);
	
			String frame_f_id = "" + xmlTables.demandId_file(frame_f);
			String frame_n_id = "" + xmlTables.demandId_proc(frame_n);
	
			String callsite_f = "";
			String callsite_n = caller.getName();
			String callsite_ln = "";
	
			if (caller instanceof CallSiteScope) {
				CallSiteScope x = (CallSiteScope) caller;
				callsite_f = x.getLineScope().getSourceFile().getFilename().toString();
				callsite_ln = "" + (x.getLineScope().getFirstLineNumber() + 1);
			}
			else {
				callsite_f = caller.getSourceFile().getFilename().toString();
				callsite_ln = "" + (caller.getFirstLineNumber() + 1);
			}
			
			String callsite_f_id = "" + xmlTables.demandId_file(callsite_f);
			String callsite_n_id = "" + xmlTables.demandId_proc(callsite_n);
			
			return "<PF f=\"" + frame_f_id + "\" n=\"" + frame_n_id + "\" l=\"" + frame_ln + "\">\n"
				 + "<Pr f=\"" + callsite_f_id + "\" n=\"" + callsite_n_id + "\" l=\"" + callsite_ln + "\" a=\"1\">\n"
				 + "<C l=\"" + callsite_ln + "\">\n";
		}
	
	
	}

	
	
	// -------------------------------------------------------------------------
	// Task:
	//
	// A task is defined by two consecutive TaskPoints. A task's contents/type is defined
	// by the *ending* TaskPoint. A task always *begins* with computation (cost) and ends
	// with some amount of slack.
	//   
	// Trace records and tasks do not perfectly align. Because trace records commit *before*
	// a task's slack begins, any slack is captured by the *next* commit.  In the diagram below,
	// brackets {} indicate trace records and bars | indicate task points. The 'cost' and 'slack'
	// segments are labeled with respect to *tasks* rather than TaskPoints.
	//
	//               |       task1        |
	//    { [slack0]  [cost1] }{ [slack1]  [cost2] }
	//   TP0                  TP1                   TP2
	//
	// The Path.prependTask() routine properly computes each task's slack.   
	// -------------------------------------------------------------------------

	protected static class Task {
		private TaskPoint m_task_beg;
		private TaskPoint m_task_end;
		
		private long m_cost_us;  // (exclusive) cost
		private long m_slack_us; // (exclusive) slack
		private ModelArgs m_modelArgs;

		//private String m_name;    // display name
		private String m_name_id; // canonical name

		public Task(TaskPoint task_beg, TaskPoint task_end) {
			m_task_beg = task_beg;
			m_task_end = task_end;
			
			// computed by Path.prependTask() using task sequence, if necessary
			m_cost_us = (isSync()) ? ((TaskPointSync) task_end()).cost_us() : 0;
			m_slack_us = 0;
			m_modelArgs = (isModel()) ? ((TaskPointMdl) task_end()).args() : null;

			//m_name = null;    // constructed on demand
			m_name_id = null; // constructed on demand
		}

		public void nullify() {
			m_task_beg = null; // N.B.: TaskPoint's are not explicitly managed
			m_task_end = null;
			m_modelArgs = null;
			//m_name = null;
			m_name_id = null;
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public boolean isSync() {
			return (m_task_end instanceof TaskPointSync);
		}
		
		public boolean isModel() {
			return (m_task_end instanceof TaskPointMdl);
		}

		
		public TaskPoint task_beg() {
			return m_task_beg;
		}

		public TaskPoint task_end() {
			return m_task_end;
		}

		
		public double cost_s() {
			return (double) (cost_us()) / 1.0e6;
		}

		public long cost_us() {
			return m_cost_us;
		}
		
		public void cost_us(long x) {
			m_cost_us = x;
		}

		public long slack_us() {
			return m_slack_us;
		}

		public double slack_s() {
			return (double) (slack_us()) / 1.0e6;
		}
		
		public void slack_us(long x) {
			m_slack_us = x;
		}

		public ModelArgs modelArgs() {
			return m_modelArgs;
		}
		

		public String toString(boolean doBegEnd, int pathLen, final EnumSet<TaskNameArgs> args) {
			return makeNameAnnotated(cost_s(), slack_s(), modelArgs(), doBegEnd, pathLen, args);
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------
		
		protected String makeName(boolean doBegEnd, int pathLen, final EnumSet<TaskNameArgs> args) {
			//if (m_name != null) {
			//	return m_name;
			//}
			
			String m_name;
			
			String nmStr_end = m_task_end.makeName(pathLen, args);

			if (doBegEnd) {
				String nmStr_beg = m_task_beg.makeName(pathLen, args);

				if (args.contains(TaskNameArgs.fmt_xml)) {
					m_name = XMLTags.makeTable(nmStr_beg, nmStr_end);
				}
				else {
					m_name = nmStr_beg + " || " + nmStr_end;
				}
			}
			else {
				m_name = nmStr_end;
			}

			return m_name;
		}
		
		protected String makeNameId() {
			if (m_name_id != null) {
				return m_name_id;
			}

			String nmStr_beg = m_task_beg.makeName_x(Args.taskNameId_pathLen, TaskNameArgs.txt);
			String nmStr_end = m_task_end.makeName_x(Args.taskNameId_pathLen, TaskNameArgs.txt);

			m_name_id = nmStr_beg + " || " + nmStr_end;

			return m_name_id;
		}
		
		protected String makeNameAnnotated(double cost_s, double slack_s, ModelArgs modelArgs, boolean doBegEnd, int pathLen, final EnumSet<TaskNameArgs> args) {
			final String beg_str = (args.contains(TaskNameArgs.fmt_xml)) ? XMLTags.html_beg : "";
			final String end_str = (args.contains(TaskNameArgs.fmt_xml)) ? XMLTags.html_end : "";
			final String newline_str = (args.contains(TaskNameArgs.fmt_xml)) ? XMLTags.newline : "\\n";

			String metricStr = "cst/slk(s) " + String.format(Args.fmt_dbl, cost_s) + "/" + String.format(Args.fmt_dbl, slack_s);
			
			String mdlArgStr = "";
			if (modelArgs != null) {
				mdlArgStr += modelArgs.toString_txt();
			}
			
			String nmStr = makeName(doBegEnd, pathLen, args);

			if (args.contains(TaskNameArgs.fmt_xml)) {
				metricStr = XMLTags.center_beg + XMLTags.ul_beg + metricStr + XMLTags.ul_end + XMLTags.center_end;
				if (!mdlArgStr.isEmpty()) {	
					nmStr += newline_str;
				}
			}
			else {
				metricStr += " ";
				nmStr += " ";
			}

			return beg_str + metricStr + nmStr + mdlArgStr + end_str;
		}

	}

	// -------------------------------------------------------------------------
	// 
	// -------------------------------------------------------------------------

	public enum TaskNameArgs {
		xtra_cpId, xtra_traceId,
		line_multi,
		fmt_xml;
		
		public static final EnumSet<TaskNameArgs> txt = EnumSet.noneOf(TaskNameArgs.class);
		public static final EnumSet<TaskNameArgs> xml = EnumSet.of(TaskNameArgs.fmt_xml, TaskNameArgs.line_multi);
		public static final EnumSet<TaskNameArgs> debug_txt = EnumSet.of(TaskNameArgs.xtra_cpId, TaskNameArgs.xtra_traceId);
	}


	// -------------------------------------------------------------------------
	// Graph:
	// -------------------------------------------------------------------------

	protected static class Graph {
		public enum MergeTy {
			most, // most critical
			least; // least critical

			public static final EnumSet<MergeTy> none = EnumSet.noneOf(MergeTy.class);
			public static final EnumSet<MergeTy> full = EnumSet.allOf(MergeTy.class);
		}

		public LinkedList<Vertex> vertexL;
		public LinkedList<Edge> edgeL;

		// Maps merge vertices and edges by Comparable.compareTo()
		//   HashMap, TreeMap (<map>.values())
		// [Avoid a Set because it does not provide get(); only contains()]
		private TreeMap<Vertex, Vertex> m_vertexMap; // Comparable.compareTo()
		private TreeMap<Edge, Edge> m_edgeMap; // Comparable.compareTo()

		private Vertex m_root;
		
		private long m_cost_us;  // total (exclusive) cost
		private long m_slack_us; // total slack

		private long m_edge_weight_cost; // total edge instances
		private long m_edge_weight_slack;

		private String m_graphTy;
		private String m_provenanceInfo;
		private String m_provenanceTy;
		private String m_provenanceRank;
		
		private boolean m_hasDenseIds;

		public Graph(String ty) {
			vertexL = new LinkedList<Vertex>();
			edgeL = new LinkedList<Edge>();

			m_vertexMap = new TreeMap<Vertex, Vertex>();
			m_edgeMap = new TreeMap<Edge, Edge>();

			m_root = null;
			
			m_cost_us = 0;
			m_slack_us = 0;

			m_edge_weight_cost = 0;
			m_edge_weight_slack = 0;

			m_graphTy = ty;
			m_provenanceInfo = "";
			m_provenanceTy = "";
			m_provenanceRank = "";
		}

		
		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------
		
		public Vertex demandVertex(Task task, EnumSet<Graph.MergeTy> mergeTy) {
			Vertex vtx = new Vertex(task, mergeTy);
			if (vertexL.isEmpty()) { m_root = vtx; }
			vertexL.add(vtx);
			mergeVertexStats(task.cost_us(), task.slack_us(), mergeTy);
			return vtx;
		}

		public Edge demandEdge(Vertex src, Vertex tgt, Edge.Ty ty, EnumSet<Graph.MergeTy> mergeTy) {
			return demandEdge(src, tgt, ty, 1, 1, mergeTy);
		}

		public Edge demandEdge(Vertex src, Vertex tgt, Edge.Ty ty, long weight_cost, long weight_slack, EnumSet<Graph.MergeTy> mergeTy) {
			Edge edge = new Edge(src, tgt, ty, weight_cost, weight_slack, mergeTy);
			edgeL.add(edge);
			mergeEdgeStats(weight_cost, weight_slack, mergeTy);
			return edge;
		}
		
		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public Vertex findMatchingVertex(Task task) {
			Vertex vtx_key = new Vertex(task, Graph.MergeTy.none);
			return m_vertexMap.get(vtx_key);
		}

		public Vertex demandMatchingVertex(Vertex vertex, EnumSet<Graph.MergeTy> mergeTy) {
			// N.B.: Assume 'vertex' is from another Graph
			return demandMatchingVertex(vertex.task(), vertex.cost_us(), vertex.slack_us(), vertex.modelArgs(), mergeTy);
		}

		public Vertex demandMatchingVertex(Task task, EnumSet<Graph.MergeTy> mergeTy) {
			return demandMatchingVertex(task, task.cost_us(), task.slack_us(), task.modelArgs(), mergeTy);
		}

		public Vertex demandMatchingVertex(Task task, long cost_us, long slack_us, ModelArgs modelArgs, EnumSet<Graph.MergeTy> mergeTy) {
			Vertex vtx_key = new Vertex(task, Graph.MergeTy.none);

			Vertex vtx = m_vertexMap.get(vtx_key);
			if (vtx == null) {
				vtx = new Vertex(task, cost_us, slack_us, modelArgs, mergeTy);
				if (vertexL.isEmpty()) { m_root = vtx; }
				m_vertexMap.put(vtx_key, vtx);
				vertexL.add(vtx);
			}
			else {
				vtx.merge(task, cost_us, slack_us, modelArgs, mergeTy);
			}

			mergeVertexStats(cost_us, slack_us, mergeTy);

			return vtx;
		}
		
		
		protected static class MatchingEdgeRet {
			public Edge edge;
			public boolean isTask2Used;
			
			public MatchingEdgeRet(Edge _edge, boolean _isTask2Used) {
				edge = _edge;
				isTask2Used = _isTask2Used;
			}
		}

		
		// findMatchingOKEdge: Given a Vertex 'vtx1' in this Graph, find the edge [vtx1, vtx2], where vtx2 = task2 and where the edge has certain constraints; if 'mergeTy' is non-Graph.MergeTy.none, merge as well
		public MatchingEdgeRet findMatchingOKEdge(Vertex vtx1, Task task2, EnumSet<Graph.MergeTy> mergeTy) {
			// N.B.: -] means "there exits"

			// 1. if -] (forward|back) Edge [vtx1 -> vtx2], use it
			// 2. if -] ancestor Vertex [vtx2 ... vtx1], create *back* Edge [vtx1 -> task2]
			// 3. create *forward* Edge/Vertex [vtx1 -> task2]

			Edge edge = vtx1.findOutEdge(task2);
			Vertex vtx2 = null;
			
			boolean isTask2Used = false; // assume representative for task2 already exists 

			if (edge != null) {
				// 1.
				vtx2 = edge.target();
				if (mergeTy != Graph.MergeTy.none) {
					mergeVertex(vtx2, task2, mergeTy);
					mergeEdge(edge, vtx1, vtx2, mergeTy);
				}
			}
			else {
				vtx2 = vtx1.findOKAncestor(task2);
				if (vtx2 != null) {
					// 2.
					if (mergeTy != Graph.MergeTy.none) {
						mergeVertex(vtx2, task2, mergeTy);
					}
					edge = demandEdge(vtx1, vtx2, Edge.Ty.back, mergeTy);
				}
				else {
					// 3.
					vtx2 = demandVertex(task2, mergeTy);
					edge = demandEdge(vtx1, vtx2, Edge.Ty.forward, mergeTy);
					isTask2Used = true;
				}
			}
			
			//System.out.println("* " + edge.toString_txt(""));
			//System.out.println("- " + edge.toString_gml(""));

			return new MatchingEdgeRet(edge, isTask2Used);
		}


		public Edge demandMergeMatchingEdge(Edge edge, Edge.Ty ty, EnumSet<Graph.MergeTy> mergeTy) {
			// N.B.: Assume 'edge' is from another Graph
			Vertex vtx_src = findMatchingVertex(edge.source().task());
			Vertex vtx_snk = findMatchingVertex(edge.target().task());
			return demandMergeMatchingEdge(vtx_src, vtx_snk, ty, edge.weight_cost(), edge.weight_slack(), mergeTy);
		}

		public Edge demandMergeMatchingEdge(Vertex src, Vertex tgt, Edge.Ty ty, EnumSet<Graph.MergeTy> mergeTy) {
			return demandMergeMatchingEdge(src, tgt, ty, 1, 1, mergeTy);
		}

		public Edge demandMergeMatchingEdge(Vertex src, Vertex tgt, Edge.Ty ty, long weight_cost, long weight_slack, EnumSet<Graph.MergeTy> mergeTy) {
			Edge edge_key = new Edge(src, tgt, ty, Graph.MergeTy.none);

			Edge edge = m_edgeMap.get(edge_key);
			if (edge == null) {
				edge = new Edge(src, tgt, ty, weight_cost, weight_slack, mergeTy);
				m_edgeMap.put(edge_key, edge);
				edgeL.add(edge);
			}
			else {
				edge.merge(weight_cost, weight_slack, mergeTy);
			}

			mergeEdgeStats(weight_cost, weight_slack, mergeTy);

			return edge;
		}
		
		
		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		// mergeVertexDeep: Given (a) Vertex x_vertex1 in 'this' Graph x_graph and (b) Vertex y_vertex1 in Graph y_graph, merge y_vertex1 into x_vertex1
		public void mergeVertexDeep(Vertex x_vertex1, Graph y_graph, Vertex y_vertex1, EnumSet<Graph.MergeTy> mergeTy) {
			mergeVertex(x_vertex1, y_vertex1, mergeTy); // shallow merge
			
			// Visit each descendant of 'y_graph' exactly once:
			//   for each of y_vertex1's outgoing edges: y_edge [y_vertex1, y_vertex2]
			for (Map.Entry<Vertex, Edge> entry : y_vertex1.m_outEdge.entrySet()) {
				Edge y_edge = entry.getValue(); //Vertex key = entry.getKey();
				Vertex y_vertex2 = y_edge.target();
				
				// Ensure x_vertex1 has analogous outgoing edge 'x_edge' [x_vertex1, x_vertex2]
				// N.B.: it is possible that x_edge.type() != y_edge.type()
				Graph.MatchingEdgeRet ret = findMatchingOKEdge(x_vertex1, y_vertex2.task(), Graph.MergeTy.none);
				Edge x_edge = ret.edge;
				// FIXME: nullify y/y_vertex2 if possible
				
				mergeEdge(x_edge, y_edge, mergeTy); // shallow merge

				// Recur on structure of y_vertex1
				//if (y_edge.type() == Edge.Ty.back), y_vertex2 has already been merged
				if (y_edge.type() == Edge.Ty.forward) {
					Vertex x_vertex2 = x_edge.target();
					mergeVertexDeep(x_vertex2, y_graph, y_vertex2, mergeTy);
				}
			}
		}
		
		
		public void mergeVertex(Vertex vertex, Task new_task, EnumSet<Graph.MergeTy> mergeTy) {
			// 'new_task' represents new Vertex
			vertex.merge(new_task, mergeTy);			
			mergeVertexStats(new_task.cost_us(), new_task.slack_us(), mergeTy);
		}
		
		public void mergeVertex(Vertex vertex, Vertex new_vertex, EnumSet<Graph.MergeTy> mergeTy) {
			vertex.merge(new_vertex.task(), new_vertex.cost_us(), new_vertex.slack_us(), new_vertex.modelArgs(), mergeTy);			
			mergeVertexStats(new_vertex.cost_us(), new_vertex.slack_us(), mergeTy);
		}

		

		public void mergeEdge(Edge edge, Vertex new_src, Vertex new_tgt, EnumSet<Graph.MergeTy> mergeTy) {
			// <new_src, new_tgt> represents new Edge
			edge.merge(mergeTy);
			mergeEdgeStats(1, 1, mergeTy);
		}
		
		public void mergeEdge(Edge edge, Edge new_edge, EnumSet<Graph.MergeTy> mergeTy) {
			edge.merge(new_edge.weight_cost(), new_edge.weight_slack(), mergeTy);
			mergeEdgeStats(new_edge.weight_cost(), new_edge.weight_slack(), mergeTy);
		}

		
		private void mergeVertexStats(long cost_us, long slack_us, EnumSet<Graph.MergeTy> mergeTy) {
			if (mergeTy.contains(Graph.MergeTy.most)) {
				m_cost_us += cost_us;
			}
			if (mergeTy.contains(Graph.MergeTy.least)) {
				m_slack_us += slack_us;
			}
		}

		private void mergeEdgeStats(long weight_cost, long weight_slack, EnumSet<Graph.MergeTy> mergeTy) {
			if (mergeTy.contains(Graph.MergeTy.most)) {
				m_edge_weight_cost += weight_cost;
			}
			if (mergeTy.contains(Graph.MergeTy.least)) {
				m_edge_weight_slack += weight_slack;
			}
		}
		

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public Vertex root() {
			return m_root;
		}
		
		public long cost_us() {
			return m_cost_us;
		}

		public double cost_s() {
			return (double) (cost_us()) / 1.0e6;
		}

		public long slack_us() {
			return m_slack_us;
		}

		public double slack_s() {
			return (double) (slack_us()) / 1.0e6;
		}

		public long edge_weight_cost() {
			return m_edge_weight_cost;
		}

		public long edge_weight_slack() {
			return m_edge_weight_slack;
		}

		public String mkInfoStr() {
			String ty = m_provenanceTy + "_" + m_provenanceRank;
			String metrics = "cost (s): " + String.format(Args.fmt_dbl, cost_s()) + "; slack (s): " + String.format(Args.fmt_dbl, slack_s());
			String info = m_graphTy + " [" + ty + "] " + metrics + " [based on " + m_provenanceInfo + "]";
			return info;
		}

		public String provenanceInfo() {
			return m_provenanceInfo;
		}

		public String provenanceTy() {
			return m_provenanceTy;
		}

		public String provenanceRank() {
			return m_provenanceRank;
		}
		
		public void add_provenanceInfo(String str) {
			m_provenanceInfo += str;
		}

		public void add_provenance(String info, String ty, String rank) {
			if (info != null) {
				m_provenanceInfo += " " + info;
			}
			if (ty != null) {
				m_provenanceTy += ((!m_provenanceTy.isEmpty()) ? "_" : "") + ty;
			}
			if (rank != null) {
				m_provenanceRank += ((!m_provenanceRank.isEmpty()) ? "_" : "") + rank;
			}
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public boolean hasDenseIds() {
			return m_hasDenseIds;
		}

		public void makeDenseIds() {
			if (root() != null) {
				root().makeDenseIds();
			}
			m_hasDenseIds = true;
		}
		
		public int numVertices() {
			return vertexL.size();
		}

		public int numEdges() {
			return edgeL.size();
		}
		
		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		// N.B.: Only for Tree-graphs (not generic graphs)
		public void makeMetricsIncl_treegraph() {
			assert (root() != null);
			root().makeMetricsIncl_treegraph();
		}
		
		// Given 'total cost' and a 'threshold' [0..1], prune nodes by inclusive cost
		// N.B.: Only for Tree-graphs (not generic graphs)
		
		public void pruneByInclCost_treegraph(double threshold_vertexCost, double threshold_backedgeCost) {			
			pruneByInclCost_treegraph(root(), cost_us(), threshold_vertexCost, edge_weight_cost(), threshold_backedgeCost);
			
			assert (root() != null);
			
			// Remove vertices from Graph.m_vertexL with no predecessor and no children
			// N.B. Do not update Graph's cumulative vertex statistics 
			for (Iterator<Vertex> it = vertexL.iterator(); it.hasNext(); /**/) {
				Vertex x = it.next(); // advance iterator beyond 'x'
				if (x.predecessor() == null && x.isLeaf()) {
					it.remove(); // remove 'x'
				}
			}
			
			// Remove edges from Graph.m_edgeL with null src/target
			// N.B. Do not update Graph's cumulative edge statistics 
			for (Iterator<Edge> it = edgeL.iterator(); it.hasNext(); /**/) {
				Edge x = it.next(); // advance iterator beyond 'x'
				if (x.source() == null && x.target() == null) {
					it.remove(); // remove 'x'
				}
			}
		}
		
		public void pruneByInclCost_treegraph(Vertex vertex1, double costTotal_us, double vertexThreshold, double edgeWeightTotal, double edgeThreshold) {
			// N.B.: To facilitate Vertex/Edge unlinking, prune/delete on vertex1's children.
			//       Implication: 'vertex1' will *not* be deleted.
			LinkedList<Edge> backedgeL = new LinkedList<Edge>();

			// ----------------------------------------
			// Prune descendant vertices
			// ----------------------------------------
			for (Iterator<Map.Entry<Vertex, Edge>> it = vertex1.m_outEdge.entrySet().iterator(); it.hasNext(); /**/) {
				Map.Entry<Vertex, Edge> entry = it.next(); // advance iterator beyond 'entry'
				Edge edge = entry.getValue(); //Vertex key = entry.getKey();
				if (edge.type() == Edge.Ty.forward) {
					Vertex vertex2 = edge.target();
					
					double cost_relative = ((double) vertex2.costIncl_us()) / costTotal_us;
					if (cost_relative < vertexThreshold) {
						it.remove(); // remove 'edge' from m_outEdge
						edge.nullify();
						
						deleteVertex(vertex2, backedgeL);
						// N.B.: Assume caller removes 'edge' from Graph.m_edgeL
					}
					else {
						pruneByInclCost_treegraph(vertex2, costTotal_us, vertexThreshold, edgeWeightTotal, edgeThreshold);
					}
				}
			}
			
			// ----------------------------------------
			// Relocate back-edges from pruned vertices 
			// N.B. edge.source() has been deleted; edge.target has not
			// ----------------------------------------
			for (Edge new_edge : backedgeL) {
				assert (new_edge.type() ==  Edge.Ty.back);
				Vertex new_target = new_edge.target();
				Edge edge = vertex1.findOutEdge(new_target);
				if (edge != null) {
					edge.merge(new_edge.weight_cost(), new_edge.weight_slack(), Graph.MergeTy.full);
					new_edge.nullify();
				}
				else {
					new_edge.source(vertex1);
					vertex1.outEdge(new_edge, new_target);
					//avoid mergeEdgeStats(): demandEdge(vertex1, new_target, new_edge.type(), new_edge.weight_cost(), new_edge.weight_slack(), Graph.MergeTy.full);
				}
			}
			backedgeL.clear();
			
			// ----------------------------------------
			// Prune all back edges below threshold_edge
			// ----------------------------------------
			for (Iterator<Map.Entry<Vertex, Edge>> it = vertex1.m_outEdge.entrySet().iterator(); it.hasNext(); /**/) {
				Map.Entry<Vertex, Edge> entry = it.next(); // advance iterator beyond 'entry'
				Edge edge = entry.getValue(); //Vertex key = entry.getKey();
				if (edge.type() == Edge.Ty.back) {
					double edgeCost_relative = ((double) edge.weight_cost()) / edgeWeightTotal;
					if (edgeCost_relative < edgeThreshold) {
						it.remove(); // remove 'edge' from m_outEdge
						edge.nullify();
						// N.B.: Assume caller removes 'edge' from Graph.m_edgeL
					}
				}
			}
		}
		
		public void deleteVertex(Vertex vertex1, LinkedList<Edge> backedgeL) {
			// N.B.: Delete 'vertex1'. Assume that caller cleans up pointers *to* 'vertex1'

			vertex1.predictNullify(); // set flag in pre-order fashion
			
			// ----------------------------------------
			// Cleanup Vertex state
			// ----------------------------------------
						
			for (Map.Entry<Vertex, Edge> entry : vertex1.m_outEdge.entrySet()) {
				Edge edge = entry.getValue(); //Vertex key = entry.getKey();
				Vertex vertex2 = edge.target();
				if (edge.type() == Edge.Ty.forward) {
					deleteVertex(vertex2, backedgeL);
					edge.nullify();
				}
				else if (edge.type() == Edge.Ty.back) {
					if (!vertex2.isPredictNullify()) {
						backedgeL.add(edge);
					}
					else {
						edge.nullify();
					}
				}
			}

			// N.B.: Must do this in post-order fashion to propagate upward
			if (vertex1.isExit()) {
				if (vertex1.predecessor() != null) {
					vertex1.predecessor().isExit(true);
				}
			}

			vertex1.nullify(); // clears 'vertex1.m_outEdge'
			
			// ----------------------------------------
			// Cleanup Graph state
			// ----------------------------------------
			// N.B.: Assume caller removes 'vertex1' from Graph.m_vertexL
			if (vertex1 == root()) {
				m_root = null;
			}
			// FIXME: remove from Graph.m_vertexMap if no more reference to canonical Task
		}
		
		
	}

	// -------------------------------------------------------------------------
	// Vertex:
	// -------------------------------------------------------------------------

	protected static class Vertex implements Comparable<Vertex> {

		public enum Ty {
			loopHead, loopTail;

			public static final EnumSet<Ty> none = EnumSet.noneOf(Ty.class);
			public static final EnumSet<Ty> all  = EnumSet.allOf(Ty.class);
		}

		// Vertex may have one predecessor (parent) and multiple successors  (children)
		private Edge m_inEdge;
		private TreeMap<Vertex, Edge> m_outEdge; // Comparable.compareTo()

		// private HashSet<Task> m_taskSet; // ensure unique set of Task's (by pointer)
		private Task m_task; // representative (first) task

		private long m_cost_us;  // total (exclusive) cost
		private long m_slack_us; // total (exclusive) slack
		private ModelArgs m_modelArgs;
		
		private int m_numMerges;

		private long m_costIncl_us;  // total (inclusive) cost
		private long m_slackIncl_us; // total (inclusive) slack
		
		private EnumSet<Ty> m_ty;
		private boolean m_isEntry;
		private boolean m_isExit;
		
		private int m_denseId; // valid id is greater than 0

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public Vertex(Task task, EnumSet<Graph.MergeTy> mergeTy) {
			this(task, task.cost_us(), task.slack_us(), task.modelArgs(), mergeTy);
		}

		public Vertex(Task task, long cost_us, long slack_us, ModelArgs modelArgs, EnumSet<Graph.MergeTy> mergeTy) {
			m_inEdge = null;
			m_outEdge = new TreeMap<Vertex, Edge>();
			
			// m_taskSet = new HashSet<Task>();
			m_task = task;

			m_cost_us = 0;
			m_slack_us = 0;
			m_modelArgs = (task.isModel()) ? new ModelArgs() : null;

			m_numMerges = 0;
			
			m_costIncl_us = 0;
			m_slackIncl_us = 0;
			
			m_ty = Ty.none.clone();
			m_isExit = false;
			
			m_denseId = 0;

			merge(task, cost_us, slack_us, modelArgs, mergeTy);
		}

		public void nullify() {
			m_task.nullify();
			m_inEdge = null;
			m_outEdge.clear();
			m_outEdge = null;
			m_task = null;
			m_modelArgs = null;
			m_denseId = -1;
		}
		
		public void predictNullify() {
			m_denseId = -1;
		}

		public boolean isPredictNullify() {
			return (m_denseId < 0);
		}

		public int id() {
			return System.identityHashCode(this);
		}

		public int id_dense() {
			return m_denseId;
		}
		
		public int id_best() {
			return ((m_denseId > 0) ? m_denseId : id());
		}
		
		
		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------
		
		public Edge findOutEdge(Task tgt) {
			Vertex vtx_key = new Vertex(tgt, Graph.MergeTy.none);
			return m_outEdge.get(vtx_key);
		}

		public Edge findOutEdge(Vertex tgt) {
			return findOutEdge(tgt.task());
		}
		
		public void outEdge(Edge edge, Vertex tgt) {
			Vertex vtx_key = new Vertex(tgt.task(), Graph.MergeTy.none);
			m_outEdge.put(vtx_key, edge);
		}

		public Edge inEdge() {
			return m_inEdge;
		}

		public void inEdge(Edge edge) {
			m_inEdge = edge;
		}

		public Vertex predecessor() {
			return (m_inEdge != null) ? m_inEdge.source() : null;
		}
		
		public boolean isLeaf() {
			return (m_outEdge == null || m_outEdge.size() == 0);
		}
	

		public Vertex findOKAncestor(Task task) {
			// N.B. ancestor may include 'this' (i.e., ancestor is not a strict ancestor)
			return _findOKAncestor(task, 0, 0);
		}

		private Vertex _findOKAncestor(Task task, int _loopNestingVal, int level) {
			// '_loopNestingVal' represents the number of open loop intervals
			
			int loopNestingVal = _loopNestingVal;

			// Potentially open loop intervals (iff not at beginning)
			if (level > 0 && isTy(Ty.loopTail)) {
				loopNestingVal++; // FIXME: incr by # outgoing back edges
			}

			// Potentially close open loop intervals
			if (loopNestingVal > 0 && isTy(Ty.loopHead)) {
				loopNestingVal = 0; // FIXME: decr by # incoming back edges
			}
			
			// Have we found the task?
			Vertex vtx = new Vertex(task, Graph.MergeTy.none);
			if (compareTo(vtx) == 0) {
				return (loopNestingVal == 0) ? this : null;
			}
			//vtx.nullify();
						
			// Traverse predecessor
			Vertex pred = predecessor();
			if (pred == null) {
				return null;
			}
			else {
				return pred._findOKAncestor(task, loopNestingVal, level + 1);
			}
		}


		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public boolean merge(Task task, EnumSet<Graph.MergeTy> mergeTy) {
			return merge(task, task.cost_us(), task.slack_us(), task.modelArgs(), mergeTy);
		}

		public boolean merge(Task task, long cost_us, long slack_us, ModelArgs modelArgs, EnumSet<Graph.MergeTy> mergeTy) {			
			if (mergeTy.contains(Graph.MergeTy.most)) {
				m_cost_us += cost_us;
			}
			if (mergeTy.contains(Graph.MergeTy.least)) {
				m_slack_us += slack_us;
			}
			
			if (modelArgs != null) {
				assert (m_modelArgs != null);
				m_modelArgs.merge(modelArgs);
			}
			
			return true;
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public Task task() {
			return m_task; // representative task
		}

		public long cost_us() {
			return m_cost_us;
		}

		public double cost_s() {
			return (double) (cost_us()) / 1.0e6;
		}

		public long slack_us() {
			return m_slack_us;
		}

		public double slack_s() {
			return (double) (slack_us()) / 1.0e6;
		}

		public long costIncl_us() {
			return m_costIncl_us;
		}
		
		public long slackIncl_us() {
			return m_slackIncl_us;
		}
		
		
		public ModelArgs modelArgs() {
			return m_modelArgs;
		}
		
		
		public boolean isTy(Ty ty) {
			return m_ty.contains(ty);
		}

		public void addTy(Ty ty) {
			m_ty.add(ty);
		}
		
		public void ty(Ty ty) {
			m_ty = EnumSet.of(ty); // clear and set
		}


		public boolean isEntry() {
			if (inEdge() == null && m_outEdge.isEmpty()) {
				return m_isEntry; // most likely a graph
			}
			else {
				return (inEdge() == null); // tree-graph
			}
		}
		
		public void isEntry(boolean x) {
			m_isEntry = x;
		}

		
		public boolean isExit() {
			return m_isExit;
		}

		public void isExit(boolean x) {
			m_isExit = x;
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		// public int hashCode() { return ...; }

		// Object.equals
		public boolean equals(Object x) {
			if (!(x instanceof Vertex)) {
				return false;
			}

			Vertex _x = (Vertex) x;
			return (id() == _x.id());
		}

		// Comparable.compareTo: negative, zero, or positive as 'this' is less
		// than, equal to, or greater than 'x'
		@Override
		public int compareTo(Vertex y) {
			int cmp = 0;
			if (Args.taskMerge_useCPId) {
				// Integer.compare(x, y) in 1.7
				cmp = m_task.m_task_beg.cctId() - y.m_task.m_task_beg.cctId();
				if (cmp == 0) {
					cmp = m_task.m_task_end.cctId() - y.m_task.m_task_end.cctId();
				}
			}
			else {
				String x_nm = nameId();
				String y_nm = y.nameId();
				cmp = x_nm.compareTo(y_nm);
			}
			return cmp;
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public String name(int pathLen, final EnumSet<TaskNameArgs> args) {
			boolean doBegEnd = (isEntry() /*|| isExit()*/);
			return m_task.makeName(/*doBegEnd*/doBegEnd, pathLen, args);
		}

		public String nameId() {
			return m_task.makeNameId();
		}

		public String nameAnnotated(int pathLen, final EnumSet<TaskNameArgs> args) {
			boolean doBegEnd = (isEntry() /*|| isExit()*/);
			return m_task.makeNameAnnotated(cost_s(), slack_s(), modelArgs(), doBegEnd, pathLen, args);
		}

		public String toString(int pathLen, final EnumSet<TaskNameArgs> args) {
			return nameAnnotated(pathLen, args);
		}

		public String toString_dot(int pathLen, final EnumSet<TaskNameArgs> args, String indent) {
			// {vertex} [ label="..." ];
			String nm = nameAnnotated(pathLen, args);
			return (indent + id_best() + " [ label=\"" + nm + "\" ];");
		}

		public String toString_gml(int pathLen, final EnumSet<TaskNameArgs> args, String indent) {
			String nm = nameAnnotated(pathLen, args);
			String wght = "" + slack_s(); // + "/" + cost_s();
			return (indent + "node [ id " + id_best() + " label \"" + nm + "\" value " + wght + " ]");
		}

		public String toString_graphml(int pathLen, final EnumSet<TaskNameArgs> args, double ttl_cost_us, double ttl_slack_us, String indent) {
			String i = indent;
			String nm = nameAnnotated(pathLen, args);
			
			double costPct  = (ttl_cost_us > 0)  ? ((100 * (double) cost_us())     / ttl_cost_us) : 0.0;
			double costIPct = (ttl_cost_us > 0)  ? ((100 * (double) costIncl_us()) / ttl_cost_us) : 0.0;
			double slackPct = (ttl_slack_us > 0) ? ((100 * (double) slack_us())    / ttl_slack_us) : 0.0;

			String costPct_str = "" + costPct;
			String costIPct_str = "" + costIPct;
			String slackPct_str = "" + slackPct;

			String node_ty = "com.yworks.flowchart.delay";
			if (isExit()) {
				node_ty = "com.yworks.flowchart.terminator";
			}
			else if (task().isModel()) {
				node_ty = "com.yworks.flowchart.process";
			}

		    return i + "<node id=\"" + id() + "\">\n"
	    	 	 + i + "  <data key=\"" + attrNodeCostPct  + "\">" + costPct_str   + "</data>\n"
 	    	 	 + i + "  <data key=\"" + attrNodeCostIPct + "\">" + costIPct_str  + "</data>\n"
	    	 	 + i + "  <data key=\"" + attrNodeSlackPct + "\">" + slackPct_str  + "</data>\n"
	    	 	 + i + "  <data key=\"" + attrNode + "\">\n"
	    	 	 + i + "    <y:GenericNode configuration=\"" + node_ty + "\">\n"
	    	 	 + i + "      <y:Fill color=\"#E8EEF7\" color2=\"#B7C9E3\"/>\n"
	    	 	 + i + "      <y:NodeLabel visible=\"true\">" + nm + "</y:NodeLabel>\n"
	    	 	 + i + "    </y:GenericNode>\n"
	    	 	 + i + "  </data>\n"
	    	 	 + i + "</node>\n";
		}
		
		
		public String toString_experiment(String indent, XMLTablesScopeVisitor xmlTables, String[] m_id_vec) {			
			String str = "";
			String i = indent;

			// ------------------------------------------------------------

			String task_backedges = "";
			int n_forward_edges = 0;
			
			long n_instances_cost = 0;
			long n_instances_slack = 0;

			for (Map.Entry<Vertex, Edge> entry : m_outEdge.entrySet()) {
				Edge edge = entry.getValue(); //Vertex key = entry.getKey();
				if (edge.type() == Edge.Ty.back) {
					task_backedges += edge.target().id_dense() + " ";
				}
				else if (edge.type() == Edge.Ty.forward) {
					n_forward_edges++;
				}
				n_instances_cost += edge.weight_cost();
				n_instances_slack += edge.weight_slack();
			}
			
			boolean isLeaf = (n_forward_edges == 0);

			// ------------------------------------------------------------
			//if (m_inEdge != null) {
			//	n_instances_cost = m_inEdge.weight_cost();
			//	n_instances_slack = m_inEdge.weight_slack();
			//}
			
			String metrics = "<M n=\"" + m_id_vec[0] + "\" v=\"" + cost_us()  + "\"/>"
						   + "<M n=\"" + m_id_vec[1] + "\" v=\"" + slack_us() + "\"/>"
						   + "<M n=\"" + m_id_vec[2] + "\" v=\"" + n_instances_cost + "\"/>"
			   			   + "<M n=\"" + m_id_vec[3] + "\" v=\"" + n_instances_slack + "\"/>";
			
			String f_id = "" + xmlTables.demandId_file(xmlTables.nullFile);

			//String task_id = "" + xmlTables.demandId_proc(xmlTables.taskNm);
			String task_nm = xmlTables.taskNm + " " + m_denseId;
			if (!task_backedges.isEmpty()) {
				task_nm += " " + XMLTags.html_lt + task_backedges;
			}

			String taskBeg_nm = xmlTables.taskBegNm + " " + m_denseId; // xmlTables.demandId_proc(xmlTables.taskBegNm);
			String taskEnd_nm = xmlTables.taskEndNm + " " + m_denseId; // xmlTables.demandId_proc(xmlTables.taskEndNm);
			
			String taskBeg_info = "";
			String taskEnd_info = "";
			if (Args.treeGraph_useTopDownCallPaths) {
				taskBeg_info += m_task.task_beg().makeName_experiment_topdown(xmlTables);
				// if (isLeaf) { }
				taskEnd_info += m_task.task_end().makeName_experiment_topdown(xmlTables);
			}
			else {
				assert (m_task != null);
				taskBeg_info += m_task.task_beg().makeName_experiment_bottomup(xmlTables);
				// if (isLeaf) { }
				taskEnd_info += m_task.task_end().makeName_experiment_bottomup(xmlTables);
			}

			// ------------------------------------------------------------
			
			str += i + "<PF f=\"" + f_id + "\" p=\"" + task_nm + "\">\n"
				 + i + "  " + metrics + "\n";
			
			str += i + "  <C l=\"0\">\n"
				 + i + "  <PF f=\"" + f_id + "\" p=\"" + taskBeg_nm + "\">\n"
				 + taskBeg_info
				 + i + "  </PF>\n"
				 + i + "  </C>\n";
			
			//if (isLeaf) { }
			str += i + "  <C l=\"0\">\n"
				 + i + "  <PF f=\"" + f_id + "\" p=\"" + taskEnd_nm + "\">\n"
				 + taskEnd_info
				 + i + "  </PF>\n"
				 + i + "  </C>\n";

			for (Map.Entry<Vertex, Edge> entry : m_outEdge.entrySet()) {
				Edge edge = entry.getValue(); //Vertex key = entry.getKey();
				if (edge.type() == Edge.Ty.forward) {
					Vertex vertex2 = edge.target();
					str += i + "  <C l=\"0\">\n";
					str += vertex2.toString_experiment(indent, xmlTables, m_id_vec);
					str += i + "  </C>\n";
				}
			}
			
			str += i + "</PF>\n";
			return str;
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		// N.B.: Only for Tree-graphs (not generic graphs)
		public void makeMetricsIncl_treegraph() {
			// ----------------------------------------
			// local contribution: 
			// ----------------------------------------
			m_costIncl_us  += cost_us();
			m_slackIncl_us += slack_us();

			// ----------------------------------------
			// contribution from children
			// ----------------------------------------
			for (Map.Entry<Vertex, Edge> entry : m_outEdge.entrySet()) {
				Edge edge = entry.getValue(); //Vertex key = entry.getKey();
				if (edge.type() == Edge.Ty.forward) {
					Vertex vertex2 = edge.target();
					vertex2.makeMetricsIncl_treegraph();

					m_costIncl_us  += vertex2.costIncl_us();
					m_slackIncl_us += vertex2.slackIncl_us();					
				}
			}
		}
		
		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		// N.B.: Only for Tree-graphs (not generic graphs)
		public void makeDenseIds() {
			_makeDenseIds(1);
		}

		public int _makeDenseIds(int nextId) {
			m_denseId = nextId;
			nextId++;
			
			for (Map.Entry<Vertex, Edge> entry : m_outEdge.entrySet()) {
				Edge edge = entry.getValue(); //Vertex key = entry.getKey();
				if (edge.type() == Edge.Ty.forward) {
					Vertex vertex2 = edge.target();
					nextId = vertex2._makeDenseIds(nextId);
				}
			}
			
			return nextId;
		}
		
		public static final String attrNode = "d0";
		public static final String attrNodeCostPct = "d1";  // exclusive
		public static final String attrNodeCostIPct = "d2"; // inclusive
		public static final String attrNodeSlackPct = "d3";
	}

	// -------------------------------------------------------------------------
	//
	// -------------------------------------------------------------------------

	protected static class Edge implements Comparable<Edge> {
		public enum Ty {
			forward, back;
		}

		private Vertex m_src; // source
		private Vertex m_tgt; // target
		private Ty m_ty;

		private long m_weight_cost;
		private long m_weight_slack;

		public Edge(Vertex src, Vertex tgt, Ty ty, EnumSet<Graph.MergeTy> mergeTy) {
			this(src, tgt, ty, 1, 1, mergeTy);
		}

		public Edge(Vertex src, Vertex tgt, Ty ty, long weight_cost, long weight_slack, EnumSet<Graph.MergeTy> mergeTy) {
			m_src = src;
			m_tgt = tgt;
			m_ty = ty;
			
			m_src.outEdge(this, m_tgt);
			if (ty == Ty.forward) {
				m_tgt.inEdge(this);
			}
			if (ty == Ty.back) {
				src.addTy(Vertex.Ty.loopTail);
				tgt.addTy(Vertex.Ty.loopHead);
			}

			m_weight_cost = 0;
			m_weight_slack = 0;

			merge(weight_cost, weight_slack, mergeTy);
		}

		public void nullify() {
			m_src = null;
			m_tgt = null;
		}

		public int id() {
			return System.identityHashCode(this);
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public void merge(EnumSet<Graph.MergeTy> mergeTy) {
			merge(1, 1, mergeTy);
		}

		public void merge(long weight_cost, long weight_slack, EnumSet<Graph.MergeTy> mergeTy) {
			if (mergeTy.contains(Graph.MergeTy.most)) {
				m_weight_cost += weight_cost;
			}
			if (mergeTy.contains(Graph.MergeTy.least)) {
				m_weight_slack += weight_slack;
			}
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public Vertex source() {
			return m_src;
		}
		
		public void source(Vertex x) {
			m_src = x;
		}

		public Vertex target() {
			return m_tgt;
		}
		
		public void target(Vertex x) {
			m_tgt = x;
		}
		

		public Ty type() {
			return m_ty;
		}

		public long weight_cost() {
			return m_weight_cost;
		}

		public long weight_slack() {
			return m_weight_slack;
		}

		public long cost_us() {
			return target().cost_us();
		}

		public double cost_s() {
			return (double) (cost_us()) / 1.0e6;
		}

		public long slack_us() {
			return target().slack_us();
		}

		public double slack_s() {
			return (double) (slack_us()) / 1.0e6;
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		// Object.equals
		public boolean equals(Object x) {
			if (!(x instanceof Edge)) {
				return false;
			}

			Edge _x = (Edge) x;
			return (_x.m_src.id() == m_src.id() && _x.m_tgt.id() == m_tgt.id());
			// src.equals(x.src)
		}

		// public int hashCode() { return src + tgt; }

		// Comparable.compareTo: negative, zero, or positive as 'this' is less
		// than, equal to, or greater than 'x'
		@Override
		public int compareTo(Edge x) {
			// negative, zero, or positive as 'this' is less than, equal to, or
			// greater than 'x'
			int cmp_src = m_src.compareTo(x.m_src);
			if (cmp_src == 0) {
				int cmp_tgt = m_tgt.compareTo(x.m_tgt);
				return cmp_tgt;
			}
			else {
				return cmp_src;
			}
		}

		static public int compare_ty_cost(Edge x, Edge y) {
			int x_ty = (x.target().task().isModel()) ? 1 : 0;
			int y_ty = (y.target().task().isModel()) ? 1 : 0;
			
			int cmp_ty = (y_ty - x_ty);
			if (cmp_ty == 0) {
				long cmp = (y.cost_us() - x.cost_us()); // Long.compare() in 1.7
				return (cmp == 0) ? 0 : (cmp > 0 ? 1 : -1);				
			}
			else {
				return cmp_ty;
			}
		}
		
		static public int compare_ty_name_weightCost(Edge x, Edge y) {
			int x_ty = (x.target().task().isModel()) ? 1 : 0;
			int y_ty = (y.target().task().isModel()) ? 1 : 0;
			
			int cmp_ty = (y_ty - x_ty);
			if (cmp_ty == 0) {
				String x_nm = x.target().name(Args.pathLenOne, TaskNameArgs.txt);
				String y_nm = y.target().name(Args.pathLenOne, TaskNameArgs.txt);
				int cmp_nm = x_nm.compareTo(y_nm);
				//int cmp_nm = x.target().compareTo(y.target());
				
				if (cmp_nm == 0) {
					long cmp = (y.weight_cost() - x.weight_cost()); // Long.compare() in 1.7
					return (cmp == 0) ? 0 : (cmp > 0 ? 1 : -1);
				}
				else {
					return cmp_nm;
				}
			}
			else {
				return cmp_ty;
			}
		}


		static public int compare_slack(Edge x, Edge y) {
			long cmp = (y.slack_us() - x.slack_us()); // Long.compare() in 1.7
			return (cmp == 0) ? 0 : (cmp > 0 ? 1 : -1);
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public String toString() {
			return "" + m_src.id() + "-" + m_tgt.id();
		}

		public String toString_txt(int pathLen, String indent, boolean doMultiLine) {
			String str_sep = (doMultiLine) ? ("\n  " + indent) : "; ";
			String str_end = (doMultiLine) ? "\n" : "";

			// slack sum, avg, src-nm, tgt-nm
			double cst_weight = weight_cost();
			double cost_sum = cost_s();
			double cost_avg = (cst_weight > 0) ? (cost_s() / cst_weight) : 0.0;
			String m_cst = "cost(s)[S,M,weight] " + String.format(Args.fmt_dbl, cost_sum) + ", "
						+ String.format(Args.fmt_dbl, cost_avg) + ", "
						+ String.format(Args.fmt_dbl, cst_weight);

			double slack_sum = slack_s();
			double slack_avg = (weight_slack() > 0) ? (slack_s() / weight_slack()) : 0.0;
			String m_slk = "slck(s)[S,M] " + String.format(Args.fmt_dbl, slack_sum) + ", "
						+ String.format(Args.fmt_dbl, slack_avg);
				
			String metrics = m_cst + "; " + m_slk;
			String ty = m_ty.toString();
			
			String name_src = "'" + source().name(pathLen, TaskNameArgs.txt) + "'";
			String name_tgt = "-> '" + target().nameAnnotated(pathLen, TaskNameArgs.txt);

			return (indent + metrics + "; " + ty + str_sep + name_src + str_sep + name_tgt + str_end);
		}

		public String toString_dot(String indent) {
			// {vertex} -> {vertex} [ label="..." ];
			String lbl = "" + weight_cost();
			return (indent + source().id_best() + " -> " + target().id_best() + " [ label=\"" + lbl + "\" ];");
		}

		public String toString_gml(String indent) {
			String lbl = "" + weight_cost();
			return (indent + "edge [ source " + source().id_best() + " target " + target().id_best() + " label \"" + lbl + "\" value " + weight_cost() + " ]");
		}

		public String toString_graphml(double ttl_n_instances_cost, double ttl_n_instances_slack, String indent) {
			String i = indent;

			double n_instances_pct_cost  = (ttl_n_instances_cost > 0)  ? ((100 * (double) weight_cost())  / ttl_n_instances_cost)  : 0.0;
			double n_instances_pct_slack = (ttl_n_instances_slack > 0) ? ((100 * (double) weight_slack()) / ttl_n_instances_slack) : 0.0;

			String costStr = "" + weight_cost();
			//String slackStr = "" + weight_slack();
			String costPct = String.format(Args.fmt_dbl, n_instances_pct_cost) + "%";
			String lbl = costStr + "/" + costPct;

		    return i + "<edge id=\"" + id() + "\" source=\"" + source().id() + "\" target=\"" + target().id() + "\">\n"
		    	 + i + "  <data key=\"" + attrEdgeInstancesPct_cost  + "\">" + n_instances_pct_cost  + "</data>\n"
		    	 + i + "  <data key=\"" + attrEdgeInstancesPct_slack + "\">" + n_instances_pct_slack + "</data>\n"
		    	 + i + "  <data key=\"" + attrEdge + "\">\n"
		    	 + i + "    <y:PolyLineEdge>\n"
		    	 + i + "      <y:Arrows source=\"none\" target=\"standard\"/>\n"
		    	 + i + "      <y:EdgeLabel>" + lbl + "</y:EdgeLabel>\n"
		    	 + i + "    </y:PolyLineEdge>\n"
		    	 + i + "  </data>\n"
		    	 + i + "</edge>\n";
		}

		public static final String attrEdge = "d10";
		public static final String attrEdgeInstancesPct_cost = "d11";
		public static final String attrEdgeInstancesPct_slack = "d12";
	}

	// -------------------------------------------------------------------------
	//
	// -------------------------------------------------------------------------

	protected static class ModelArgs {
		final int maxArgSz = 4;

		private long[] m_argV_sum;
		private long m_weight;
		
		public ModelArgs() {
			m_argV_sum = new long[maxArgSz]; // auto-zeroed
			m_weight = 0;
		}

		public ModelArgs clone() {
			ModelArgs args = new ModelArgs();
			args.merge(this);
			return args;
		}
		
		public void clear() {
			for (int i = 0; i < maxArgSz; i++) {
				m_argV_sum[i] = 0;
			}
			m_weight = 0;
		}
		
		public void merge(ModelArgs args) {
			for (int i = 0; i < maxArgSz; i++) {
				m_argV_sum[i] += args.get_sum(i);
			}
			m_weight += args.m_weight;
		}

		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------
		
		public int size() {
			return m_argV_sum.length;
		}

		public long weight() {
			return m_weight;
		}

		public void set(int index, long value) {
			m_argV_sum[index] = value;
			if (m_weight == 0) {
				m_weight = 1;
			}
		}

		public long get_sum(int index) {
			return m_argV_sum[index];
		}
		
		public double get_mean(int index) {
			return ((double)m_argV_sum[index]) / (double)m_weight;
		}
		
		// ------------------------------------------------------------
		//
		// ------------------------------------------------------------

		public String toString() {
			return toString_txt();
		}

		public String toString_txt() {
			String str = "";
			for (int i = 0; i < size(); ++i) {
				double arg = get_mean(i);
				//if ( !(arg > 0) ) { break; }
				if (i > 0) { str += ","; }
				str += String.format(Args.fmt_dbl_big, arg);
			}
			return str;
		}

	}
	
	// -------------------------------------------------------------------------
	//
	// -------------------------------------------------------------------------

	protected static class XMLTags {
		public static String html_lt = "&lt;";
		public static String html_gt = "&gt;";
		public static String html_amp = "&amp;";

		public static String html_beg = "&lt;html&gt;";  // <html>
		public static String html_end = "&lt;/html&gt;"; // </html>

		public static String table_beg = "&lt;table&gt;";  // <table>
		public static String table_end = "&lt;/table&gt;"; // </table>

		public static String tablerow_beg = "&lt;tr&gt;";  // <tr>
		public static String tablerow_end = "&lt;/tr&gt;"; // </tr>

		public static String tabledata_beg = "&lt;td&gt;";  // <td>
		public static String tabledata_end = "&lt;/td&gt;"; // </td>

		public static String ul_beg = "&lt;u&gt;";  // <u>
		public static String ul_end = "&lt;/u&gt;"; // </u>

		public static String center_beg = "&lt;center&gt;";  // <center>
		public static String center_end = "&lt;/center&gt;"; // </center>

		public static String newline = "&lt;br&gt;"; // <br/>

		
		public static String escape(String str) {
			String str1 = "";

			for (int i = 0; i < str.length(); i++) {
				char c = str.charAt(i);

				if (c == '<') {
					str1 += html_lt;
				}
				else if (c == '>') {
					str1 += html_gt;
				}
				else if (c == '&') {
					str1 += html_amp;
				}
				else {
					str1 += c;
				}
			}

			// final String templateArgsRE1 = "<([^<>]+)([<>])";
			// final String templateArgsRE2 = ">([^<>]+)>";
			// String x1 = (x.replaceAll(templateArgsRE1, "<$2")).replaceAll(templateArgsRE2, ">>");

			return str1;
		}

		
		public static String makeTable(String row1, String row2) {
			String col1_str = tabledata_beg + row1 + tabledata_end;
			String col2_str = tabledata_beg + row2 + tabledata_end;

			String tbl_str = table_beg + tablerow_beg + col1_str + col2_str + tablerow_end + table_end;
			return tbl_str;
		}
	}

	// -------------------------------------------------------------------------
	//
	// -------------------------------------------------------------------------

	public class XMLTablesScopeVisitor implements IScopeVisitor {
		public HashMap<String, Integer> lmMap;
		public HashMap<String, Integer> fileMap;
		public HashMap<String, Integer> procMap;

		private int m_nextId_file;
		private int m_nextId_proc;
		
		final static String nullFile = "(empty)";

		final static String taskNm = "task";
		final static String taskBegNm = "me-beg";
		final static String taskEndNm = "me-end";

		
		public XMLTablesScopeVisitor() {
			lmMap   = new HashMap<String, Integer>();
			fileMap = new HashMap<String, Integer>();
			procMap = new HashMap<String, Integer>();
			
			m_nextId_file = 1;
			m_nextId_proc = 1;

			demandId_file(nullFile);

			demandId_proc(taskNm);
			demandId_proc(taskBegNm);
			demandId_proc(taskEndNm);
		}
		
		//----------------------------------------------------
		// visitor pattern instantiations for each Scope type
		//----------------------------------------------------

		public void visit(Scope scope, ScopeVisitType ty) {
		}
		
		public void visit(RootScope scope, ScopeVisitType ty) {
		}

		public void visit(LoadModuleScope scope, ScopeVisitType ty) {
			if (ty == ScopeVisitType.PreVisit) {
				demandId_lm(scope.getModuleName());
			}
		}
		
		public void visit(FileScope scope, ScopeVisitType ty) {
			if (ty == ScopeVisitType.PreVisit) {
				demandId_file(scope.getSourceFile().getFilename().toString());
			}
		}
		
		public void visit(ProcedureScope scope, ScopeVisitType ty) {
			if (ty == ScopeVisitType.PreVisit) {
				demandId_file(scope.getSourceFile().getFilename().toString());
				demandId_proc(scope.getName());
			}
		}
		
		public void visit(AlienScope scope, ScopeVisitType ty) {
		}
		
		public void visit(LoopScope scope, ScopeVisitType ty) {
		}
		
		public void visit(StatementRangeScope scope, ScopeVisitType ty) {
		}
		
		public void visit(CallSiteScope scope, ScopeVisitType ty) {
			if (ty == ScopeVisitType.PreVisit) {
				demandId_file(scope.getSourceFile().getFilename().toString());
				demandId_proc(scope.getName());
			}
		}
		
		public void visit(LineScope scope, ScopeVisitType ty) {
			if (ty == ScopeVisitType.PreVisit) {
				demandId_file(scope.getSourceFile().getFilename().toString());
			}
		}
		
		public void visit(GroupScope scope, ScopeVisitType ty) {
		}

		//----------------------------------------------------
		// 
		//----------------------------------------------------

		protected void doit(Scope scope, ScopeVisitType ty) {
			//if (ty == ScopeVisitType.PreVisit) {
			//	String nm = scope.getName();
			//	String f = scope.getSourceFile().getFilename().toString();
			//	int fileId = scope.getSourceFile().getFileID();
			//	int procId = scope.getFlatIndex();
			//}
			// else if (ty == ScopeVisitType.PostVisit)
		}
		
		//----------------------------------------------------
		// 
		//----------------------------------------------------
		
		public int demandId_lm(String key) {
			Integer val = lmMap.get(key);
			if (val == null) {
				val = new Integer(m_nextId_file);
				lmMap.put(key, val); // returns old value
				m_nextId_file++;
			}
			return val.intValue();
		}
		
		public int demandId_file(String key) {
			Integer val = fileMap.get(key);
			if (val == null) {
				val = new Integer(m_nextId_file);
				fileMap.put(key, val); // returns old value
				m_nextId_file++;
			}
			return val.intValue();
		}
		
		public int demandId_proc(String key) {
			Integer val = procMap.get(key);
			if (val == null) {
				val = new Integer(m_nextId_proc);
				procMap.put(key, val); // returns old value
				m_nextId_proc++;
			}
			return val.intValue();
		}
	}

	// -------------------------------------------------------------------------
	// 
	// -------------------------------------------------------------------------

	protected class ProgressMsg {
		private long m_timestamp_beg_ms;
		private long m_timestamp_prev_ms;
		private static final int s_tagFieldLen = 3;

		public ProgressMsg() {
			reset();
		}
		
		public void reset() {
			m_timestamp_beg_ms = System.currentTimeMillis();
			m_timestamp_prev_ms = m_timestamp_beg_ms;
		}

		public String error(String msg) {
			return mkMsg(msg, s_tagFieldLen - 0);
		}

		public void dump_L1(String msg) {
			dump(msg, s_tagFieldLen - 0);
		}

		public void dump_L2(String msg) {
			dump(msg, s_tagFieldLen - 1);
		}

		public void dump_L3(String msg) {
			dump(msg, s_tagFieldLen - 2);
		}

		private void dump(String msg, int tagLen) {
			if (Args.doProgress) {
				System.out.println(mkMsg(msg, tagLen));
			}
		}
		
		private String mkMsg(String msg, int tagLen) {
			int wsLen = s_tagFieldLen - tagLen;
			
			String tag = "";
			for (int i = 0; i < wsLen; i++) { tag += " "; }
			for (int i = 0; i < tagLen; i++) { tag += "*"; }
			tag += " ";
			
			long timestamp_cur_ms = System.currentTimeMillis();
			double time_total_s = (timestamp_cur_ms - m_timestamp_beg_ms) / 1000.0;
			double time_diff_s  = (timestamp_cur_ms - m_timestamp_prev_ms) / 1000.0;
			m_timestamp_prev_ms = timestamp_cur_ms;
					
			String fmt1 = "%6.1f";
			String fmt2 = "%5.1f";
			String time = String.format(fmt1, time_total_s) + "/" + String.format(fmt2, time_diff_s);
			String out_str = "[" + time + " s]: " + tag + msg;
			return out_str;
		}
	}

	
}
