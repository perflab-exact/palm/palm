#!/bin/bash
# -*-Mode: sh;-*-

#*BeginPNNLCopyright*********************************************************
#
# $HeadURL$
# $Id$
#
#***********************************************************EndPNNLCopyright*

#echo $0
#echo $@

#----------------------------------------------------------------------------

: ${HPCTRACEVIEWER_ROOT:=${HOME}/hpctoolkit}
: ${PATHS_PROJECT_ROOT:=${HOME}/1me-hpcgroup-svn/projects/critical-paths/hpcviewer-patch}

#----------------------------------------------------------------------------

cmd_base=$( basename $0 )
#echo ${cmd_base}

driver=$( echo ${cmd_base} | $MYCMD_SED_RE "s/.*-(\w+).sh$/\1/" )
echo "Driver: ${driver}"

workspace="${HOME}/.hpctoolkit/${driver}"

database=$1

if test -z "${database}" ; then
    cat <<EOF
  usage: ${0} <database>
EOF
    exit
fi

#----------------------------------------------------------------------------

if ${HPCTRACEVIEWER_ROOT}/hpctraceviewer-paths-${driver}/hpctraceviewer \
 			 -data "${workspace}" -configuration "${workspace}" \
			 "${database}" \
	| grep "All Done" >& /dev/null ; then
    echo "* processed critical paths"
else
    echo "error processing critical paths!"
    exit
fi

#----------------------------------------------------------------------------

database_base=$( basename ${database} )

db_run_id=$( echo ${database_base} | $MYCMD_SED_RE "s/.*-([0-9]+).*$/\1/" )
#echo "${db_run_id}"

#----------------------------------------

outfile_pfx="0-${driver}"

outpathL=$( find "${database}" -name "${outfile_pfx}*.txt" -print )

for outpath in ${outpathL} ; do

    outfile=$( basename ${outpath} )

    if [[ "${outfile}" =~ -[[:digit:]]+- ]] ; then
	# already processed
	continue
    fi

    outfile_new=$( echo ${outfile} | $MYCMD_SED_RE "s/(${outfile_pfx})(.*)\.(.*)$/\1-${db_run_id}\2.\3/" )

    outpath_new=${database}/${outfile_new}
    #echo "${outpath} --> ${outpath_new}"
    
    if mv "${outpath}" "${outpath_new}" ; then
	printf "* rename: ${outfile} --> ${outfile_new}\n"
	${PATHS_PROJECT_ROOT}/cp-edges-sum.sh "${outpath_new}"
    else
	printf "* error renaming: ${outfile} --> ${outfile_new}\n"
    fi
    
done
