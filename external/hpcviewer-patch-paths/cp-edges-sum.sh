#!/bin/bash
# -*-Mode: sh;-*-

#*BeginPNNLCopyright*********************************************************
#
# $HeadURL$
# $Id$
#
#***********************************************************EndPNNLCopyright*

sfx_done="-sum"

files="$@"
if test -z "$1" ; then
    files=$( ls *[Ee]dgeSort*.txt )
fi

#echo ${files}

for x in ${files} ; do

    if echo ${x} | egrep -e "${sfx_done}" >& /dev/null; then
	:
    else
	x_new=$( echo ${x} | $MYCMD_SED_RE "s@(.*)\.(.*)@\1${sfx_done}.\2@" )
	echo "* summary: ${x} --> ${x_new}"
	grep -h "graph \[" ${x} > ${x_new}
	grep -h "\* total" ${x} >> ${x_new}
    fi
    
done

unset files
unset sfx_done
