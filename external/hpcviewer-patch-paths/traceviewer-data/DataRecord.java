package edu.rice.cs.hpc.traceviewer.data.db;

/***
 * struct trace record
 * 
 *
 */
public class DataRecord {
	public long timestamp;
	public int cpId;
	// Intentionally remove Nathan's metric ID. 
	// Nathan: could you please derive this data record if you want to add additional field ?
	public int metricId;
	public long time2;
	public long time3;

	public DataRecord(long _timestamp, int _cpId, int _metricId, long _time2, long _time3) {
		this.timestamp = _timestamp;
		this.cpId = _cpId;
		this.metricId = _metricId;
		this.time2 = _time2;
		this.time3 = _time3;
	}
	@Override
	public String toString() {
		//return String.format("Time: %d, Call Path: %d", timestamp, cpId);
		return String.format("Id: %d, CallPathID: %d, prev-rank/id: %d/%d; cost: %d", timestamp, cpId, metricId, time2, time3);
	}

}
