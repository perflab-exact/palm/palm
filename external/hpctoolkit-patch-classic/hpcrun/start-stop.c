// -*-Mode: C++;-*- // technically C99

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// --------------------------------------------------------------------------
// Part of HPCToolkit (hpctoolkit.org)
//
// Information about sources of support for research and development of
// HPCToolkit is at 'hpctoolkit.org' and in 'README.Acknowledgments'.
// --------------------------------------------------------------------------
//
// Copyright ((c)) 2002-2017, Rice University
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
// * Neither the name of Rice University (RICE) nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// This software is provided by RICE and contributors "as is" and any
// express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular
// purpose are disclaimed. In no event shall RICE or contributors be
// liable for any direct, indirect, incidental, special, exemplary, or
// consequential damages (including, but not limited to, procurement of
// substitute goods or services; loss of use, data, or profits; or
// business interruption) however caused and on any theory of liability,
// whether in contract, strict liability, or tort (including negligence
// or otherwise) arising in any way out of the use of this software, even
// if advised of the possibility of such damage.
//
// ******************************************************* EndRiceCopyright *

//***************************************************************************
//
// File:
// $HeadURL$
//
// Purpose:
// This file provides an API for the application to start and stop
// sampling with explicit function calls in the application source.
//
// Note:
// (1) The actions are process-wide.
//
// (2) The start and stop functions turn the sample sources on and off
// in the current thread.  The sources in the other threads continue
// to run but don't unwind and don't record samples.  Stopping
// sampling in all threads is more complicated because it would have
// to be done in a signal handler which can't really be done safely.
//
// (3) Sampling is initally on.  If you want it initially off, then
// set HPCRUN_DELAY_SAMPLING in the environment.
//
//***************************************************************************

#include <stdlib.h>
#include <ucontext.h>

#include <include/min-max.h>

#include "sample_event.h"
#include "sample_sources_all.h"
#include "safe-sampling.h"
#include "start-stop.h"

static int sampling_is_active = 1;
static int dont_reinit = 0;


//***************************************************************************
// interface functions
//***************************************************************************

void
hpcrun_start_stop_internal_init(void)
{
  // Make sure we don't run init twice.  This is for the case that the
  // application turns sampling on/off and then forks.  We want to
  // preserve the state across fork and not reset it in the child.
  if (dont_reinit) {
    return;
  }
  if (getenv("HPCRUN_DELAY_SAMPLING") != NULL) {
    sampling_is_active = 0;
  }
  dont_reinit = 1;
}


int
hpctoolkit_sampling_is_active(void)
{
  return sampling_is_active;
}


void
hpctoolkit_sampling_start(void)
{
  sampling_is_active = 1;
  dont_reinit = 1;
  if (! SAMPLE_SOURCES(started)) {
    SAMPLE_SOURCES(start);
  }
}


void
hpctoolkit_sampling_stop(void)
{
  sampling_is_active = 0;
  dont_reinit = 1;
  if (SAMPLE_SOURCES(started)) {
    SAMPLE_SOURCES(stop);
  }
}


// Fortran aliases

// FIXME: The Fortran functions really need a separate API with
// different names to handle arguments and return values.  But
// hpctoolkit_sampling_start() and _stop() are void->void, so they're
// a special case.

void hpctoolkit_sampling_start_ (void) __attribute__ ((alias ("hpctoolkit_sampling_start")));
void hpctoolkit_sampling_start__(void) __attribute__ ((alias ("hpctoolkit_sampling_start")));

void hpctoolkit_sampling_stop_ (void) __attribute__ ((alias ("hpctoolkit_sampling_stop")));
void hpctoolkit_sampling_stop__(void) __attribute__ ((alias ("hpctoolkit_sampling_stop")));


//***************************************************************************
// 
//***************************************************************************

void
hpcrun_metric_std_min(int mId, metric_set_t* set,  hpcrun_metricVal_t value)
{
  metric_desc_t* minfo = hpcrun_id2metric(mId);
  if (!minfo) {
    return;
  }

  hpcrun_metricVal_t* loc = hpcrun_metric_set_loc(set, mId);
  switch (minfo->flags.fields.valFmt) {
    case MetricFlags_ValFmt_Int:
      loc->i = (loc->i == 0) ? value.i : MIN(loc->i, value.i); break;
    case MetricFlags_ValFmt_Real:
      loc->r = (loc->r == 0) ? value.r : MIN(loc->r, value.i); break;
    default:
      assert(false);
  }
}


void
hpcrun_metric_std_max(int mId, metric_set_t* set,  hpcrun_metricVal_t value)
{
  metric_desc_t* minfo = hpcrun_id2metric(mId);
  if (!minfo) {
    return;
  }

  hpcrun_metricVal_t* loc = hpcrun_metric_set_loc(set, mId);
  switch (minfo->flags.fields.valFmt) {
    case MetricFlags_ValFmt_Int:
      loc->i = MAX(loc->i, value.i); break;
    case MetricFlags_ValFmt_Real:
      loc->r = MAX(loc->r, value.r); break;
    default:
      assert(false);
  }
}


void
hpctoolkit_sample_x(int metric_id, long value /*, ...*/)
{
  // N.B.: Because of 'skipInner', the path to here from all external
  // APIs should be the same length.

#if 0
  // FIXME: see hpcrun_fmt_hdr_fwrite() and hpcfmt_nvpairs_vfwrite()

  va_list ap;
  int nb = 0; /* the length of the result (bytes to allocate) */
  char* nstr; /* the result */
  char* tstr; /* temporary */

  /* Compute the length of the result */
  va_start(ap, s1);
  {
    nb = strlen(s1);
    for (int i = 0; i < n-1; i++) nb += strlen(va_arg(ap, char*));
  }
  va_end(ap);
#endif

  if (hpcrun_safe_enter()) {
    TMSG(SYNC_CTL, "hpctoolkit_sample: %d, %ld", metric_id, value);

    ucontext_t uc;
    getcontext(&uc);

    int mId_sum = metric_id;
    int mId_cnt = mId_sum + 1; // FIXME
    int mId_min = mId_sum + 2;
    int mId_max = mId_sum + 3;

    /* N.B.: when tracing, this call generates a trace record */
    sample_val_t smplVal =
      hpcrun_sample_callpath(&uc, mId_sum, value/*metricIncr*/,
			     0/*skipInner*/, 1/*isSync*/);

    // use/register increment function (hpcrun_metric_std_inc())
    // control whether or not to take a trace sample?
    
    metric_set_t* metricVec =
      metricVec = hpcrun_get_metric_set(smplVal.sample_node);

    hpcrun_metric_std_inc(mId_cnt, metricVec, (cct_metric_data_t){.i = 1});
    hpcrun_metric_std_min(mId_min, metricVec, (cct_metric_data_t){.i = value});
    hpcrun_metric_std_max(mId_max, metricVec, (cct_metric_data_t){.i = value});

    hpcrun_safe_exit();
  }
}

//***************************************************************************

extern void
hpctoolkit_sample1(int metric_id, long value)
{
  hpctoolkit_sample_x(metric_id, value);
}


extern void
hpctoolkit_sample1f_(int* metric_id, long* value)
{
  hpctoolkit_sample_x(*metric_id, *value);
}


void hpctoolkit_sample1f__(int* metric_id, long* value) 
  __attribute__ ((alias ("hpctoolkit_sample1f_")));

