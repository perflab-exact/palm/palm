// -*-Mode: C++;-*-

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// --------------------------------------------------------------------------
// Part of HPCToolkit (hpctoolkit.org)
//
// Information about sources of support for research and development of
// HPCToolkit is at 'hpctoolkit.org' and in 'README.Acknowledgments'.
// --------------------------------------------------------------------------
//
// Copyright ((c)) 2002-2012, Rice University
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
// * Neither the name of Rice University (RICE) nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// This software is provided by RICE and contributors "as is" and any
// express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular
// purpose are disclaimed. In no event shall RICE or contributors be
// liable for any direct, indirect, incidental, special, exemplary, or
// consequential damages (including, but not limited to, procurement of
// substitute goods or services; loss of use, data, or profits; or
// business interruption) however caused and on any theory of liability,
// whether in contract, strict liability, or tort (including negligence
// or otherwise) arising in any way out of the use of this software, even
// if advised of the possibility of such damage.
//
// ******************************************************* EndRiceCopyright *

// * BeginPNNLCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// Copyright ((c)) 2012, Pacific Northwest National Laboratory
// All rights reserved.
//
// ...
//
// ******************************************************* EndPNNLCopyright *

//***************************************************************************
//
// File:
//   $HeadURL$
//
// Purpose:
//   [The purpose of this file]
//
// Description:
//   [The set of functions, macros, etc. defined in the file]
//
//***************************************************************************

#ifndef Model_Expr_hpp
#define Model_Expr_hpp

//************************* System Include Files ****************************

#include <iostream>
#include <vector>
#include <string>

#include <climits>

//*************************** User Include Files ****************************

#include <include/uint.h>

#include <lib/prof/CallPath-Profile.hpp>
#include <lib/prof/Metric-IData.hpp>
#include <lib/prof/Struct-Tree.hpp>

#include <lib/support/NaN.h>
#include <lib/support/NonUniformDegreeTree.hpp>
#include <lib/support/Unique.hpp>
#include <lib/support/WordSet.hpp>


//*************************** Forward Declarations **************************

//***************************************************************************

//***************************************************************************
// 
//***************************************************************************

namespace Model {

// forward declaration
namespace Tree {
  class ANode;
  typedef std::list<ANode*> ANodeList; // Tree::ANode::List
}


namespace Annot {

//-----------------------------------------------------------------------
// Name: modeling name
//-----------------------------------------------------------------------

class Name
{
public:
  Name(std::string name)
    : m_name(name)
  { }
  
  Name(const Name& x)
    : m_name(x.m_name)
  { }

  Name&
  operator=(const Name& x)
  {
    if (this != &x) {
      m_name = x.m_name;
    }
    return *this;
  }

  ~Name()
  { }

  Name*
  clone()
  { return new Name(*this); }


  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  const std::string&
  name() const
  { return m_name; }

  std::string&
  name()
  { return m_name; }


  std::string
  nameBase() const;


  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  std::ostream&
  writeMdl(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  std::ostream&
  writeSexp(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  std::string
  toString(uint oFlags = 0) const;


private:
  std::string m_name;
};


//-----------------------------------------------------------------------
// Expr: A modeling expression
//-----------------------------------------------------------------------

class Expr
{
public:
  Expr(std::string expr)
    : m_expr(expr)
  { }

  Expr(const Expr& x)
    : m_expr(x.m_expr)
  { }

  Expr&
  operator=(const Expr& x)
  {
    if (this != &x) {
      m_expr = x.m_expr;
    }
    return *this;
  }

  ~Expr()
  { }

  Expr*
  clone()
  { return new Expr(*this); }


  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  const std::string&
  expr() const
  { return m_expr; }

  std::string&
  expr()
  { return m_expr; }


  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  std::ostream&
  writeMdl(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  std::ostream&
  writeSexp(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  std::string
  toString(uint oFlags = 0) const;

private:
  std::string m_expr;
};


//-----------------------------------------------------------------------
// Env: modeling expression environment
//-----------------------------------------------------------------------

class Env
{
public:
  typedef std::vector<Env*> List;

public:
  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  class Entry
  {
  public:
    Entry()
      : m_value(c_FP_NAN_d), m_metricId(Prof::Metric::IData::npos),
	m_exprL(NULL), m_exprVariant(0)
    { }

    Entry(const std::string& var, const std::string& cls, uint metricId)
      : m_var(var), m_mdlClass(cls), m_value(c_FP_NAN_d), m_metricId(metricId),
	m_exprL(NULL), m_exprVariant(0)
    { }

    Entry(const Entry& x)
      : m_var(x.m_var), m_mdlClass(x.m_mdlClass),
	m_value(x.m_value), m_metricId(x.m_metricId),
	m_exprL(x.m_exprL), m_exprVariant(x.m_exprVariant)
    { }
    
    Entry&
    operator=(const Entry& x)
    {
      if (this != &x) {
	m_var      = x.m_var;
	m_mdlClass = x.m_mdlClass,
	m_value    = x.m_value;
	m_metricId = x.m_metricId;
	m_exprL    = x.m_exprL;
	m_exprVariant = x.m_exprVariant;
      }
      return *this;
    }

    ~Entry()
    {
      delete m_exprL;
    }


    const std::string&
    var() const
    { return m_var; }
    
    void
    var(const std::string& x)
    { m_var = x; }


    const std::string&
    mdlClass() const
    { return m_mdlClass; }
    
    void
    mdlClass(const std::string& x)
    { m_mdlClass = x; }


    double
    value() const
    { return m_value; }
    
    void
    value(double x)
    { m_value = x; }


    uint
    metricId() const
    { return m_metricId; }
    
    void
    metricId(uint x)
    { m_metricId = x; }


    const Tree::ANodeList*
    exprL() const
    { return m_exprL; }
    
    void
    exprL(Tree::ANodeList* x)
    { m_exprL = x; }


    uint
    exprVariant() const
    { return m_exprVariant; }
    
    void
    exprVariant(uint x)
    { m_exprVariant = x; }


    std::ostream&
    writeMdl(std::ostream& os, uint oFlags = 0) const;

    // FIXME: remove
    std::ostream&
    writeMdlParms(std::ostream& os, bool doFormalParams, uint oFlags = 0) const;
    
    std::ostream&
    writeSexp(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

    std::string
    toString(uint oFlags = 0, const char* pfx = "") const;
    
    std::ostream&
    dump(std::ostream& os = std::cout, uint oFlags = 0, const char* pfx = "") const;
    
    std::ostream&
    ddump() const;
    
  private:
    // FIXME: distinguish between pgm-var-ref & mdl-class-ref entries?
    std::string m_var;
    std::string m_mdlClass; // only used for mdl-class-ref
    double m_value;
    uint m_metricId;
    
    Tree::ANodeList* m_exprL; // do not own contents of m_exprL
    uint m_exprVariant;
    // FIXME: 'm_exprL' subsumes above two fields...
  };

private:
  typedef std::list<Entry*> EntryList;

public:
  typedef EntryList::iterator       iterator;
  typedef EntryList::const_iterator const_iterator;

public:
  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  Env()
  { }
  
  Env(const Env& x)
    : m_env(x.m_env)
  { }
  
  Env&
  operator=(const Env& x)
  {
    if (this != &x) {
      m_env = x.m_env;
    }
    return *this;
  }

  ~Env()
  {
    for (iterator it = m_env.begin(); it != m_env.end(); ++it) {
      Entry* x = *it;
      delete x;
    }
  }

  // push_back: assumes ownership of 'entry'
  void
  push_back(Entry* entry)
  { m_env.push_back(entry); }


  iterator
  begin()
  { return m_env.begin(); }

  const_iterator
  begin() const
  { return m_env.begin(); }


  iterator
  end()
  { return m_env.end(); }

  const_iterator
  end() const
  { return m_env.end(); }


  bool
  empty() const
  { return m_env.empty(); }

  size_t
  size() const
  { return m_env.size(); }


  void
  splice(Env& y)
  { m_env.splice(m_env.end(), y.m_env); }


  Entry*
  find(const std::string& var) const
  {
    for (const_iterator it = m_env.begin(); it != m_env.end(); ++it) {
      Entry* x = *it;
      if (x->var() == var) {
	return x;
      }
    }
    return NULL;
  }


  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  std::ostream&
  writeMdl(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  // FIXME: remove
  std::ostream&
  writeMdlParms(std::ostream& os, bool doFormalParams,
		uint oFlags = 0, const char* pfx = "") const;

  std::ostream&
  writeSexp(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  std::string
  toString(uint oFlags = 0, const char* pfx = "") const;
  
  std::ostream&
  dump(std::ostream& os = std::cout, uint oFlags = 0, const char* pfx = "") const;
  
  std::ostream&
  ddump() const;

private:
  EntryList m_env;
};


//-----------------------------------------------------------------------
// AAnnot: 
//-----------------------------------------------------------------------

class AAnnot
{
public:
  typedef std::vector<AAnnot*> List;

public:

  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  AAnnot(Name* name = NULL, Expr* expr = NULL, Env* env = NULL)
    : m_name(name), m_expr(expr), m_env(env)
  { }

  AAnnot(const AAnnot& x)
  {
    m_name = clone_name(x.m_name);
    m_expr = clone_expr(x.m_expr);
    m_env = clone_env(x.m_env);
  }

  AAnnot&
  operator=(const AAnnot& x)
  {
    if (this != &x) {
      m_name = clone_name(x.m_name);
      m_expr = clone_expr(x.m_expr);
      m_env = clone_env(x.m_env);
    }
    return *this;
  }

  virtual ~AAnnot()
  {
    delete m_name;
    delete m_expr;
    delete m_env;
  }

  virtual AAnnot*
  cloneAAnnot() = 0;

  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  const Name*
  name() const
  { return m_name; }

  void
  name(Name* x)
  { m_name = x; }

  static Name*
  clone_name(const Name* x)
  { return (x) ? new Name(*x) : NULL; }


  const Expr*
  expr() const
  { return m_expr; }

  void
  expr(Expr* x)
  { m_expr = x; }

  static Expr*
  clone_expr(const Expr* x)
  { return (x) ? new Expr(*x) : NULL; }


  const Env*
  env() const
  { return m_env; }

  void
  env(Env* env)
  { m_env = env; }

  static Env*
  clone_env(const Env* x)
  { return (x) ? new Env(*x) : NULL; }


  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  // writeMdlDef(): write as a model definition (as opposed to a use)
  virtual std::ostream&
  writeMdlDef(std::ostream& os, uint oFlags = 0, const char* pfx = "") const = 0;

protected:
  Name* m_name;
  Expr* m_expr;
  Env* m_env;
};


//-----------------------------------------------------------------------
// Def:
//-----------------------------------------------------------------------

class Def
  : public AAnnot
{
public:

  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  Def(Name* name, Expr* expr = NULL, Env* env = NULL)
    : AAnnot(name, expr, env)
  { }

  Def(const Def& x)
    : AAnnot(x)
  { }

  Def&
  operator=(const Def& x)
  {
    if (this != &x) {
      AAnnot::operator=(x);
    }
    return *this;
  }

  virtual ~Def()
  { }

  virtual AAnnot*
  cloneAAnnot()
  { return new Def(*this); }


  //---------------------------------------------------------
  // 
  //---------------------------------------------------------


  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  virtual std::ostream&
  writeMdlDef(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

private:
};


} // namespace Annot
} // namespace Model


//***************************************************************************
// 
//***************************************************************************

namespace Model {
namespace Tree {

//-----------------------------------------------------------------------
// ANode: A synthesized expression tree node
//-----------------------------------------------------------------------

class ANode
  : public NonUniformDegreeTreeNode
{
public:
  typedef ANodeList List;

private:
  enum MyBool_t {
    MyBool_NULL,
    MyBool_false,
    MyBool_true
  };

public:

  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  ANode(ANode* parent)
    : NonUniformDegreeTreeNode(parent),
      m_maySimplify(true), m_hasMdlExpr(MyBool_NULL)
  { }

  // deep copy of internals, but without children
  ANode(const ANode& x)
    : NonUniformDegreeTreeNode(x),
      m_maySimplify(x.m_maySimplify), m_hasMdlExpr(MyBool_NULL)
  {
    NonUniformDegreeTreeNode::zeroLinks();
  }

  // deep copy of internals, but without children
  ANode&
  operator=(const ANode& x)
  {
    if (this != &x) {
      NonUniformDegreeTreeNode::operator=(x);
      NonUniformDegreeTreeNode::zeroLinks();
      m_maySimplify = x.m_maySimplify;
      m_hasMdlExpr = MyBool_NULL;
    }
    return *this;
  }

  virtual ~ANode()
  { }

  virtual ANode*
  cloneANode() = 0;


  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  // sortId(): smaller values mean higher priority
  virtual uint
  sortId() const = 0;

  ANode*
  parent() const
  { return static_cast<ANode*>(NonUniformDegreeTreeNode::Parent()); }

  ANode*
  firstChild() const
  { return static_cast<ANode*>(NonUniformDegreeTreeNode::FirstChild()); }


  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  bool
  hasMdlExpr() const;


  bool
  maySimplify() const
  { return m_maySimplify; }

  void
  maySimplify(bool x)
  { m_maySimplify = x; }


  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  // writeMdlUse(): write as a model expression use (as in use vs. def)
  //
  //   [outdated] Currently, we don't have a great method for
  //   organizing an expression.  Simple rules lead to conflicting
  //   priorities.
  //   - generally want compound expressions first (as they represent
  //     bigger values)
  //   - within a Tree::Product representing a loop, want loop
  //     expression before compound expression
  //
  //   Rely on a combination of ANodeSortedChildIterator::cmpByType(),
  //   ANode::sortId(), and fixed child pointers in Tree::Product and
  //   Tree::Model.

  virtual std::ostream&
  writeMdlUse(std::ostream& os, uint oFlags = 0, const char* pfx = "") const = 0;

  // writeSexp(): write in "s-expr format" (as text)
  virtual std::ostream&
  writeSexp(std::ostream& os, uint oFlags = 0, const char* pfx = "") const = 0;


  std::string
  toString(uint oFlags = 0, const char* pfx = "") const;

  virtual std::string
  toStringMe(uint oFlags = 0) const = 0;

  std::ostream&
  dump(std::ostream& os = std::cout, uint oFlags = 0, const char* pfx = "") const;

  std::ostream&
  ddump() const;

  static std::string
  toStringMdlUse_list(const Tree::ANode::List& factorL, std::string op,
		      uint oFlags, const char* pfx = "");

protected:
  std::ostream&
  writeMdlUse_tree(std::ostream& os,
		   std::string lparen, std::string op, std::string rparen,
		   uint oFlags = 0, const char* pfx = "") const;
  
  std::ostream&
  writeSexp_tree(std::ostream& os,
		 std::string lparen, std::string name, std::string rparen,
		 uint oFlags = 0, const char* pfx = "") const;

protected:

private:
  bool m_maySimplify;
  mutable MyBool_t m_hasMdlExpr; // subtree (including me) has model expression
};


//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

class Product
  : public ANode
{
public:
  Product(ANode* parent, ANode* expr1 = NULL, ANode* expr2 = NULL)
    : ANode(parent), m_expr1(NULL), m_expr2(NULL)
  {
    this->expr1(expr1);
    this->expr2(expr2);
  }

  virtual ~Product()
  { }

  virtual ANode*
  cloneANode()
  { return new Product(*this); }

  virtual uint
  sortId() const
  { return 0x10; }

  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  ANode*
  expr1() const
  { return m_expr1; }

  void
  expr1(ANode* x)
  {
    DIAG_Assert(!m_expr1, DIAG_UnexpectedInput);
    m_expr1 = x;
    if (m_expr1) { m_expr1->link(this); }
  }


  ANode*
  expr2() const
  { return m_expr2; }

  void
  expr2(ANode* x)
  {
    DIAG_Assert(!m_expr2, DIAG_UnexpectedInput);
    m_expr2 = x;
    if (m_expr2) { m_expr2->link(this); }
  }


  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  virtual std::ostream&
  writeMdlUse(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::ostream&
  writeSexp(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::string
  toStringMe(uint oFlags = 0) const;

private:
  ANode* m_expr1;
  ANode* m_expr2;
};


//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

class Quotient
  : public ANode
{
public:
  Quotient(ANode* parent, ANode* dividend = NULL, ANode* divisor = NULL)
    : ANode(parent), m_dividend(NULL), m_divisor(NULL)
  {
    this->dividend(dividend);
    this->divisor(divisor);
  }

  virtual ~Quotient()
  { }

  virtual ANode*
  cloneANode()
  { return new Quotient(*this); }

  virtual uint
  sortId() const
  { return 0x11; }

  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  ANode*
  dividend() const
  { return m_dividend; }

  void
  dividend(ANode* x)
  {
    DIAG_Assert(!m_dividend, DIAG_UnexpectedInput);
    m_dividend = x;
    if (m_dividend) { m_dividend->link(this); }
  }


  ANode*
  divisor() const
  { return m_divisor; }

  void
  divisor(ANode* x)
  {
    DIAG_Assert(!m_divisor, DIAG_UnexpectedInput);
    m_divisor = x;
    if (m_divisor) { m_divisor->link(this); }
  }


  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  virtual std::ostream&
  writeMdlUse(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::ostream&
  writeSexp(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::string
  toStringMe(uint oFlags = 0) const;

private:
  ANode* m_dividend;
  ANode* m_divisor;
};


//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

class Sum
  : public ANode
{
public:
  Sum(ANode* parent)
    : ANode(parent)
  { }

  virtual ~Sum()
  { }

  virtual ANode*
  cloneANode()
  { return new Sum(*this); }

  virtual uint
  sortId() const
  { return 0x12; }

  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  virtual std::ostream&
  writeMdlUse(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::ostream&
  writeSexp(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::string
  toStringMe(uint oFlags = 0) const;

private:
};


//-----------------------------------------------------------------------
// Loop:
//-----------------------------------------------------------------------

class Loop
  : public ANode,
    public Annot::AAnnot
{
public:

  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  Loop(ANode* parent, const Prof::CCT::ANode* cctNode,
       Annot::Name* name, Annot::Expr* expr = NULL,
       Annot::Env* env = NULL, Tree::ANode* exprBody = NULL)
    : ANode(parent), Annot::AAnnot(name, expr, env), m_cctNode(cctNode),
      m_exprBody(NULL)
  {
    maySimplify(false);
    this->exprBody(exprBody);
  }

  Loop(const Loop& x)
    : ANode(x), Annot::AAnnot(x), m_cctNode(x.m_cctNode)
  {
    m_exprBody = NULL; DIAG_Assert(!x.m_exprBody, DIAG_UnexpectedInput);
  }

  Loop&
  operator=(const Loop& x)
  {
    if (this != &x) {
      ANode::operator=(x);
      Annot::AAnnot::operator=(x);
      m_cctNode = x.m_cctNode;
      m_exprBody = NULL; DIAG_Assert(!x.m_exprBody, DIAG_UnexpectedInput);
    }
    return *this;
  }

  virtual ~Loop()
  { }

  virtual ANode*
  cloneANode()
  { return new Loop(*this); }

  virtual AAnnot*
  cloneAAnnot()
  { return new Loop(*this); }

  virtual uint
  sortId() const
  { return 0x1; }

  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  const ANode*
  exprBody() const
  { return m_exprBody; }

  void
  exprBody(ANode* x)
  {
    DIAG_Assert(!m_exprBody, DIAG_UnexpectedInput);
    m_exprBody = x;
    if (m_exprBody) {
      m_exprBody->link(this);
      m_exprBody->maySimplify(false);
    }
  }

  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  virtual std::ostream&
  writeMdlUse(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  std::ostream&
  writeMdlFctr(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::ostream&
  writeMdlDef(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  std::ostream&
  writeMdl_name(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::ostream&
  writeSexp(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::string
  toStringMe(uint oFlags = 0) const;

private:
  const Prof::CCT::ANode* m_cctNode; // do not own
  ANode* m_exprBody;
};


//-----------------------------------------------------------------------
// Model:
//-----------------------------------------------------------------------

class Model
  : public ANode,
    public Annot::AAnnot
{
public:
  Model(ANode* parent, const Prof::CCT::ANode* cctNode,
	Annot::Name* name, Annot::Expr* expr = NULL,
	Annot::Env* env = NULL, Tree::ANode* exprSyn = NULL)
    : ANode(parent), Annot::AAnnot(name, expr, env), m_cctNode(cctNode),
      m_exprSyn(NULL)
  {
    this->exprSyn(exprSyn);
  }

  // deep copy of internals includes copy of 'name' and 'expr'
  Model(const Model& x)
    : ANode(x), Annot::AAnnot(x), m_cctNode(x.m_cctNode)
  {
    m_exprSyn = NULL; DIAG_Assert(!x.m_exprSyn, DIAG_UnexpectedInput);
  }

  // deep copy of internals includes copy of 'name' and 'expr'
  Model&
  operator=(const Model& x)
  {
    if (this != &x) {
      ANode::operator=(x);
      Annot::AAnnot::operator=(x);
      m_cctNode = x.m_cctNode;
      m_exprSyn = NULL; DIAG_Assert(!x.m_exprSyn, DIAG_UnexpectedInput);
    }
    return *this;
  }

  virtual ~Model()
  {
    // m_exprSyn deleted via ANode::~ANode
  }

  virtual ANode*
  cloneANode()
  { return new Model(*this); }

  virtual AAnnot*
  cloneAAnnot()
  { return new Model(*this); }

  virtual uint
  sortId() const
  { return 0x2; }

  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  const ANode*
  exprSyn() const
  { return m_exprSyn; }

  void
  exprSyn(ANode* x)
  {
    DIAG_Assert(!m_exprSyn, DIAG_UnexpectedInput);
    m_exprSyn = x;
    if (m_exprSyn) { m_exprSyn->link(this); }
  }


  const Prof::CCT::ANode*
  cctNode() const
  { return m_cctNode; }

  
  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  virtual std::ostream&
  writeMdlUse(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::ostream&
  writeMdlDef(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  std::ostream&
  writeMdl_name(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  std::ostream&
  writeMdl_nameDbg(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  // writeMdl_expr1: 1 instance of 'expr'
  std::ostream&
  writeMdl_expr1(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  // writeMdl_exprN: N instances of 'expr' (multi-instance)
  std::ostream&
  writeMdl_exprN(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  // writeMdl_exprSyn1: 1 instance of 'exprSyn'
  std::ostream&
  writeMdl_exprSyn1(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  // writeMdl_exprSynN: N instances of 'exprSyn' (multi-instance)
  std::ostream&
  writeMdl_exprSynN(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::ostream&
  writeSexp(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  static std::string
  toStringMdl_factor(const Tree::ANode::List& factorL,
		     uint oFlags, const char* pfx = "");

  virtual std::string
  toStringMe(uint oFlags = 0) const;

private:
  const Prof::CCT::ANode* m_cctNode; // do not own
  ANode* m_exprSyn;
};


//-----------------------------------------------------------------------
// Const:
//-----------------------------------------------------------------------

class Const
  : public ANode
{
public:
  Const(ANode* parent, const Prof::CCT::ANode* cctNode, uint metricId)
    : ANode(parent), m_cctNode(cctNode), m_metricId(metricId)
  { }

  Const(const Const& x)
    : ANode(x), m_cctNode(x.m_cctNode), m_metricId(x.m_metricId)
  { }

  Const&
  operator=(const Const& x)
  {
    if (this != &x) {
      ANode::operator=(x);
      m_cctNode = x.m_cctNode;
      m_metricId = x.m_metricId;
    }
    return *this;
  }

  virtual ~Const()
  { }

  virtual ANode*
  cloneANode()
  { return new Const(*this); }

  virtual uint
  sortId() const
  { return UINT_MAX; }


  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  bool
  hasVal() const
  {
    return (m_cctNode && m_metricId != Prof::Metric::IData::npos
	    && m_cctNode->hasMetricSlow(m_metricId));
  }

  double
  val() const
  {
    if (hasVal()) {
      return m_cctNode->metric(m_metricId);
    }
    else {
      return c_FP_NAN_d;
    }
  }

  // TODO: do we need these getters/setters?
  const Prof::CCT::ANode*
  cctNode() const
  { return m_cctNode; }

  void
  cctNode(Prof::CCT::ANode* x)
  { m_cctNode = x; }


  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  virtual std::ostream&
  writeMdlUse(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::ostream&
  writeSexp(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  virtual std::string
  toStringMe(uint oFlags = 0) const;

private:
  const Prof::CCT::ANode* m_cctNode; // do not own
  uint m_metricId;
};


} // namespace Tree
} // namespace Model


//***************************************************************************
// 
//***************************************************************************

namespace Model {
namespace Tree {

// NOTE: If modifying the object during iteration, see the note in
//   <lib/prof/CCT-TreeIterator.hpp>

class ANodeChildIterator
  : public NonUniformDegreeTreeNodeChildIterator
{
public:

  ANodeChildIterator(const ANode* root)
  : NonUniformDegreeTreeNodeChildIterator(root, /*forward*/true)
  { }


  ANode*
  current() const
  { return static_cast<ANode*>(Current()); }


  virtual NonUniformDegreeTreeNode*
  Current() const
  { return NonUniformDegreeTreeNodeChildIterator::Current(); }
  
private: 
};


//-----------------------------------------------------------------------
// 
//-----------------------------------------------------------------------

class ANodeSortedChildIterator {
public:
  // return -1, 0, or 1 for x < y, x = y, or x > y, respectively
  typedef int (*cmp_fptr_t) (const void* x, const void* y);

  static int cmpByType(const void* x, const void* y);
  
public:
  ANodeSortedChildIterator(const ANode* root, cmp_fptr_t compare_fn);

  ~ANodeSortedChildIterator()
  {
    delete ptrSetIt;
  }

  ANode*
  current() const
  {
    ANode* x = NULL;
    if (ptrSetIt->Current()) {
      x = reinterpret_cast<ANode*>(*ptrSetIt->Current());
    }
    return x;
  }

  void
  operator++(int)
  { (*ptrSetIt)++; }

  void
  reset()
  {
    ptrSetIt->Reset();
  }

  void
  dumpAndReset(std::ostream &os = std::cerr);

private:
  WordSet scopes;
  WordSetSortedIterator* ptrSetIt;
};


} // namespace Tree
} // namespace Model


//***************************************************************************
// 
//***************************************************************************

namespace Model {

class Mdl
  : public Unique
{
public:
  enum {
    // Output flags
    OFlg_Debug     = (1 << 1),
    OFlg_DebugAll  = (1 << 2),

    OFlg_MdlUseVariant_x1  = (1 << 3), // per-instance expr
    OFlg_MdlUseVariant_x1s = (1 << 4), // per-instance synthesized expr
    OFlg_MdlUseVariant_xN  = (1 << 5), // multi-instance expr
    OFlg_MdlUseVariant_xNm = (1 << 6)  // multi-instance synthesized expr
  };

  typedef std::map<Prof::CCT::ANode*, Tree::Model*> CCTToModelMap;


public:
  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  Mdl(/*const*/ Prof::CallPath::Profile& prof)
    : m_prof(prof),
      m_timeNDesc_avg(NULL),
      m_progValMDesc_avg(NULL),
      m_progValMDesc_max(NULL),
      m_expr(NULL)
  { }

  virtual ~Mdl();

  // make: Given a Prof::CallPath::Profile, return a Model::Mdl.
  // The model has the following properties:
  //   - the model is a subset of the node in Prof::CallPath::Profile
  //   - ...
  static Mdl*
  make(/*const*/ Prof::CallPath::Profile& prof);
  
  //---------------------------------------------------------
  // 
  //---------------------------------------------------------  

  std::ostream&
  write(std::ostream& os, uint oFlags = 0, const char* pfx = "") const;

  std::string
  toString(uint oFlags = 0, const char* pfx = "") const;

  std::ostream&
  dump(std::ostream& os = std::cout, uint oFlags = 0, const char* pfx = "") const;

  std::ostream&
  ddump() const;

protected:

  void
  make0();

  Tree::ANode*
  make0(const Prof::CCT::ANode* node);

  Tree::ANode*
  make0_noPALDirective(const Prof::CCT::ANode* node,
		       Tree::ANode::List& childModels);

  bool
  make0_bindEnv1(Annot::Env* env, const Prof::CCT::ANode& dirNode);

  bool
  make0_bindEnv2(Annot::Env* env, const Tree::Model& mdl);

protected:
  Prof::CallPath::Profile& m_prof;
  const Prof::Metric::ADesc* m_timeNDesc_avg; // multi-instance
  const Prof::Metric::ADesc* m_progValMDesc_avg;
  const Prof::Metric::ADesc* m_progValMDesc_max;

  Annot::AAnnot::List m_defFnL;
  Annot::AAnnot::List m_modelExprFnL; // owned by 'm_expr'
  Annot::AAnnot::List m_loopExprFnL;  // owned by 'm_expr'
  Tree::ANode* m_expr;

  CCTToModelMap m_cctToMdlMap;
};

} // namespace Model


//****************************************************************************

#endif // Model_Expr_hpp
