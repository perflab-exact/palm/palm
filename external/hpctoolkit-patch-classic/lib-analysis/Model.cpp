// -*-Mode: C++;-*-

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// --------------------------------------------------------------------------
// Part of HPCToolkit (hpctoolkit.org)
//
// Information about sources of support for research and development of
// HPCToolkit is at 'hpctoolkit.org' and in 'README.Acknowledgments'.
// --------------------------------------------------------------------------
//
// Copyright ((c)) 2002-2012, Rice University
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
// * Neither the name of Rice University (RICE) nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// This software is provided by RICE and contributors "as is" and any
// express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular
// purpose are disclaimed. In no event shall RICE or contributors be
// liable for any direct, indirect, incidental, special, exemplary, or
// consequential damages (including, but not limited to, procurement of
// substitute goods or services; loss of use, data, or profits; or
// business interruption) however caused and on any theory of liability,
// whether in contract, strict liability, or tort (including negligence
// or otherwise) arising in any way out of the use of this software, even
// if advised of the possibility of such damage.
//
// ******************************************************* EndRiceCopyright *

// * BeginPNNLCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// Copyright ((c)) 2012, Pacific Northwest National Laboratory
// All rights reserved.
//
// ...
//
// ******************************************************* EndPNNLCopyright *

//***************************************************************************
//
// File:
//   $HeadURL$
//
// Purpose:
//   [The purpose of this file]
//
// Description:
//   [The set of functions, macros, etc. defined in the file]
//
//***************************************************************************

//************************* System Include Files ****************************

#include <iostream>
using std::hex;
using std::dec;

#include <fstream>

#include <string>
using std::string;

#include <climits>
#include <cstring>

#include <typeinfo>


//*************************** User Include Files ****************************

#include <include/uint.h>
#include <include/gcc-attr.h>

#include "Model.hpp"

#include <lib/support/diagnostics.h>
#include <lib/support/Logic.hpp>
#include <lib/support/IOUtil.hpp>
#include <lib/support/StrUtil.hpp>


//*************************** Forward Declarations **************************


//****************************************************************************
// 
//****************************************************************************

namespace Model {


class PAL
{
public:
  //---------------------------------------------------------
  // 
  //---------------------------------------------------------

  static bool
  isDirective(const Prof::CCT::ANode& node)
  {
    const string& str = getPossibleDirStr(node);
    return (str.rfind(s_dirListEnd) != string::npos);
  }


  //---------------------------------------------------------
  // Properties of a directive-group and directive
  //   directive ::= <type><clause:name><clause:env><clause:expr>
  //---------------------------------------------------------

  enum DirTy {
    DirTy_NULL,
    DirTy_def,
    DirTy_model,
    DirTy_loop
  };

  static DirTy
  getDirType(const Prof::CCT::ANode& node)
  {
    string dirStr = getDirStr(node);

    DirTy ty = DirTy_NULL;

    if (dirStr.compare(0, s_dirTag_def.length(), s_dirTag_def) == 0) {
      ty = DirTy_def;
    }
    else if (dirStr.compare(0, s_dirTag_model.length(), s_dirTag_model) == 0) {
      ty = DirTy_model;
    }
    else if (dirStr.compare(0, s_dirTag_loop.length(), s_dirTag_loop) == 0) {
      ty = DirTy_loop;
    }
    
    return ty;
  }


  static Annot::Name*
  getClause_name(const Prof::CCT::ANode& node)
  {
    string dirStr = getDirStr(node);
    string name = getClauseStr(dirStr, s_dirClause0Beg);
    return (new Annot::Name(name));
  }


  static Annot::Expr*
  getClause_expr(const Prof::CCT::ANode& node)
  {
    string dirStr = getDirStr(node);
    string expr = getClauseStr(dirStr, s_dirClause1Beg);
    return ((!expr.empty()) ? new Annot::Expr(expr) : NULL);
  }


  static Annot::Env*
  getClause_env1(const Prof::CCT::ANode& node,
		 uint val0_metricId_avg, uint val0_metricId_max)
  {
    string dirStr = getDirStr(node);
    string envStr = getClauseStr(dirStr, s_dirClause2Beg);

    //--------------------------------------------------------
    // gather list of program references
    //--------------------------------------------------------
    std::vector<std::string> refVec;
    StrUtil::tokenize_str(envStr, s_dirClause_Env_Sep.c_str(), refVec);

    DIAG_Assert(refVec.size() <= 1, DIAG_UnexpectedInput
		<< " Currently expect max of 1 prog-val-ref");

    Annot::Env* env = new Annot::Env;

    for (size_t i = 0; i < refVec.size(); ++i) {
      const string& refStr = refVec[i];

      //--------------------------------------------------------
      // gather fields for a program reference
      //--------------------------------------------------------
      std::vector<std::string> refFields;
      StrUtil::tokenize_str(refStr, s_dirClause_EnvFld_Sep.c_str(), refFields);

      DIAG_Assert(refFields.size() == 3, DIAG_UnexpectedInput);

      string var   = refFields[0];
      string idStr = refFields[1];
      string stat  = refFields[2];

      // Note: each 'id' expands into a set of CCT metric ids at run
      // time.  To find the appropriate CCT metric id given a 'id', we
      // can probably use a linear function of 'id' with an offset.
      uint id = (uint)StrUtil::toLong(idStr);
      DIAG_Assert(id == 0, DIAG_UnexpectedInput);

      uint mId = 0;
      if (stat == "avg")      { mId = val0_metricId_avg; }
      else if (stat == "max") { mId = val0_metricId_max; }
      else { DIAG_Die(DIAG_UnexpectedInput); }
      
      env->push_back(new Annot::Env::Entry(var, "", mId));
    }

    if (env->empty()) {
      delete env;
      env = NULL;
    }

    return env;
  }


  static Annot::Env*
  getClause_env2(const Prof::CCT::ANode& node, uint time_metricId_avg)
  {
    string dirStr = getDirStr(node);
    string envStr = getClauseStr(dirStr, s_dirClause3Beg);

    //--------------------------------------------------------
    // gather list of model-class references
    //--------------------------------------------------------
    std::vector<std::string> refVec;
    StrUtil::tokenize_str(envStr, s_dirClause_Env_Sep.c_str(), refVec);

    Annot::Env* env = new Annot::Env;

    for (size_t i = 0; i < refVec.size(); ++i) {
      const string& refStr = refVec[i];

      //--------------------------------------------------------
      // gather fields for a program reference
      //--------------------------------------------------------
      std::vector<std::string> refFields;
      StrUtil::tokenize_str(refStr, s_dirClause_EnvFld_Sep.c_str(), refFields);

      DIAG_Assert(refFields.size() == 4, DIAG_UnexpectedInput);

      string var    = refFields[0];
      string mdlCls = refFields[1];
      string metric = refFields[2];
      string stat   = refFields[3];

      DIAG_Assert(metric == "time", DIAG_UnexpectedInput);
      DIAG_Assert(stat   == "avg", DIAG_UnexpectedInput);

      if (!env->find(var)) {
	uint mId = time_metricId_avg;
	env->push_back(new Annot::Env::Entry(var, mdlCls, mId));
      }
    }

    if (env->empty()) {
      delete env;
      env = NULL;
    }

    return env;
  }
 

public:

  static string
  getDirStr(const Prof::CCT::ANode& node)
  {
    string dirStr;

    const string& str = getPossibleDirStr(node);

    // find last (top) directive in the list (stack)
    size_t dirListEndPos = str.rfind(s_dirListEnd);

    if (dirListEndPos != string::npos) {
      size_t dirEndPos = str.rfind(s_dirEnd, dirListEndPos);
      size_t dirBegPos = str.rfind(s_dirBeg, dirEndPos);
      dirBegPos += s_dirBeg.length();       // points just after s_dirBeg
      dirStr = str.substr(dirBegPos, dirEndPos - dirBegPos);
    }
    return dirStr;
  }


private:
  static const string&
  getPossibleDirStr(const Prof::CCT::ANode& node)
  {
    if (typeid(node) == typeid(Prof::CCT::Proc)) {
      const Prof::CCT::Proc& proc = static_cast<const Prof::CCT::Proc&>(node);
      if (proc.isAlien()) {
	return proc.fileName();
      }
    }

    return s_BOGUS;
  }

  
  static string
  getClauseStr(const string& dirStr, const string& clauseBeg)
  {
    string clauseStr;
    
    size_t begClausePos = dirStr.find(clauseBeg);
    begClausePos += clauseBeg.length(); // first char in clause
    size_t endClausePos = dirStr.find(s_dirClauseEnd, begClausePos);

    clauseStr = dirStr.substr(begClausePos, endClausePos - begClausePos);
    return clauseStr;
  }


public:
  static const string s_mdl_doMdlExpr;

  static const string s_mdl_variant;
  static const string s_mdl_variant_x1;
  static const string s_mdl_variant_x1s;
  static const string s_mdl_variant_xN;
  static const string s_mdl_variant_xNs;

private:
  static const string s_dirListBeg;
  static const string s_dirListEnd;

  static const string s_dirBeg;
  static const string s_dirEnd;

  static const string s_dirTag_def;
  static const string s_dirTag_model;
  static const string s_dirTag_loop;

  static const string s_dirClause0Beg;
  static const string s_dirClause1Beg;
  static const string s_dirClause2Beg;
  static const string s_dirClause3Beg;
  static const string s_dirClauseEnd;

  static const string s_dirClause_Env_Sep;
  static const string s_dirClause_EnvFld_Sep;

  static const string s_BOGUS;
};


const string PAL::s_mdl_doMdlExpr = "@doModelExpr";

const string PAL::s_mdl_variant = "variant";
const string PAL::s_mdl_variant_x1  = ":x1";
const string PAL::s_mdl_variant_x1s = ":x1!";
const string PAL::s_mdl_variant_xN  = ":xN";
const string PAL::s_mdl_variant_xNs = ":xN!";

const string PAL::s_dirListBeg = "{pal>";
const string PAL::s_dirListEnd = "<pal}";

const string PAL::s_dirBeg = "{>";
const string PAL::s_dirEnd = "<}";

const string PAL::s_dirTag_def   = "def";
const string PAL::s_dirTag_model = "mdl";
const string PAL::s_dirTag_loop  = "lop";

const string PAL::s_dirClause0Beg = "[0!>";
const string PAL::s_dirClause1Beg = "[1!>";
const string PAL::s_dirClause2Beg = "[2!>";
const string PAL::s_dirClause3Beg = "[3!>";
const string PAL::s_dirClauseEnd = "<]";

const string PAL::s_dirClause_Env_Sep    = ";;";
const string PAL::s_dirClause_EnvFld_Sep = "::";

const string PAL::s_BOGUS;


//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

static Prof::CCT::ANode*
cct_findProcFrmDescendent(const Prof::CCT::ANode* node, const string& name);


// mdl_makeSum(): construct a Tree::Sum, applying the following
//   expression simplification rules:
//     a. (Sum)     => null
//     b. (Sum x)   => x
//     c. (Sum x 0) => (Sum x)
//     d. (Sum x_1 ... x_m (Sum y_1 ... y_n) ... (Sum z_1 ... z_o))
//          where x_1 ... x_m may not be 'Sum'
//        => (Sum x_1 ... x_m y_1 ... y_n ... z_1 ... z_o)
Tree::ANode*
mdl_makeSum(Tree::ANode* parent, Tree::ANode::List& children)
{
  Tree::ANode* model = new Tree::Sum(parent);

  // 1. construct model
  for (Tree::ANode::List::iterator i = children.begin();
       i != children.end(); ++i) {
    Tree::ANode*& x = *i;
    
    // expression simplification case (c)
    if (typeid(*x) == typeid(Tree::Const)) {
      Tree::Const* x_const = static_cast<Tree::Const*>(x);
      if (!x_const->hasVal() || x_const->val() == 0.0) {
	delete x_const;
	x = NULL;
	continue;
      }
    }
    
    // expression simplification case (d)
    if (typeid(*x) == typeid(Tree::Sum) && x->maySimplify()) {
      for (Tree::ANodeChildIterator i_x(x); i_x.current(); /**/) {
	Tree::ANode* y = i_x.current();
	i_x++;
	y->unlink();
	y->link(model);
      }
      
      delete x;
      x = NULL;
      continue;
    }

    x->link(model);
  }

  // 2. apply additional simplification rules
  if (model->childCount() == 0) {
    // expression simplification case (a)
    model->unlink();
    delete model;
    model = NULL;
  }
  else if (model->childCount() == 1) {
    // expression simplification case (b)
    Tree::ANode* model_old = model;
    Tree::ANode* model_new = model = model_old->firstChild();

    model_old->unlink();
    model_new->unlink();
    model_new->link(parent);

    DIAG_Assert(model_old->isLeaf() && !model_old->parent(),
		DIAG_UnexpectedInput);
    delete model_old;
  }

  return model;
}


// mdl_findInstanceFactors: Gather all ancestor loops (Tree::Loop) to
// serve as instance factors.
void
mdl_findInstanceFactors(const Tree::ANode* node, Tree::ANode::List& factorL)
{
  if (!node) {
    return;
  }
  
  for (Tree::ANode* x = node->parent(); (x); x = x->parent()) {
    if (typeid(*x) == typeid(Tree::Loop)) {
      Tree::Loop* lp = static_cast<Tree::Loop*>(x);
      factorL.push_front(lp);
    }
  }
}


// cct_findPgmValRef: Given a PAL directive CCT node, find the node
//   corresponding to the value profile data (i.e.,
//   hpctoolkit_sample).  [FIXME: hackish]
//
// The basic tree pattern is as follows:
//   * CCT::Alien <directive node>
//     -> [CCT::Call -> CCT::ProcFrm hpctoolkit_sample_[external] ]
//        -> [CCT::Call -> CCT::ProcFrm hpctoolkit_sample_x[internal] ]
// The last two frames are often combined (via inlining) if
// HPCToolkit's profiler has been compiled with optimization.
static Prof::CCT::ANode*
cct_findPgmValRef(const Prof::CCT::ANode& dirNode)
{
  static const string hpctoolkit = "hpctoolkit_sample"; // FIXME

  Prof::CCT::ANode* data1 = cct_findProcFrmDescendent(&dirNode, hpctoolkit);
  Prof::CCT::ANode* data2 = cct_findProcFrmDescendent(data1, hpctoolkit);
  
  return (data2) ? data2 : data1;
}


// cct_findMdlFrontier: find model frontier w.r.t. 'mdlClass'
static void
cct_findMdlFrontier(const string& mdlClass, const Prof::CCT::ANode& dirNode,
		    const Mdl::CCTToModelMap& cctToMdlMap,
		    Prof::CCT::ANode::Vec& frntrL_cct,
		    Model::Tree::ANode::List& frntrL_mdl)
{
  // 1a. Find a corresponding model for 'dirNode'
  Prof::CCT::ANode* node = const_cast<Prof::CCT::ANode*>(&dirNode);
  Mdl::CCTToModelMap::const_iterator it = cctToMdlMap.find(node);
  Tree::Model* mdl = (it != cctToMdlMap.end()) ? it->second : NULL;

  // 1b. Does 'dirNode' belong to model frontier?
  if (mdl) {
    if (!mdlClass.empty()) {
      DIAG_MsgIf(0, "findMdlFrontier(" << mdlClass << ", " << node << ") " << mdl->name()->name() << " [" << mdl->name()->nameBase() << "]");
      if (mdl->name()->nameBase() == mdlClass) {
	frntrL_cct.push_back(node);
	frntrL_mdl.push_back(mdl);
	return;
      }
    }
    else {
      frntrL_cct.push_back(node);
      frntrL_mdl.push_back(mdl);
      return;
    }
  }

  // 2. Recur
  for (Prof::CCT::ANodeChildIterator i(node); i.current(); i++) {
    Prof::CCT::ANode* x = i.current();
    cct_findMdlFrontier(mdlClass, *x, cctToMdlMap, frntrL_cct, frntrL_mdl);
  }
}


// cct_findProcFrmDescendent: Given a Prof::CCT::ANode, search for the
//   following pattern:
//   * <node>
//     -> CCT::Call
//        -> CCT::ProcFrm (where this node is the first/only child and
//             where 'name' matches procedure name)
static Prof::CCT::ANode*
cct_findProcFrmDescendent(const Prof::CCT::ANode* node, const string& name)
{
  if (!node) {
    return NULL;
  }

  for (Prof::CCT::ANodeChildIterator i(node); i.current(); i++) {
    Prof::CCT::ANode* x = i.current();
    if (typeid(*x) == typeid(Prof::CCT::Call)) {
      Prof::CCT::Call* x_call = static_cast<Prof::CCT::Call*>(x);
      if (x_call->childCount() == 1) { // callsite of interest has one target
	Prof::CCT::ANode* y = x_call->firstChild();
	if (typeid(*y) == typeid(Prof::CCT::ProcFrm)) {
	  Prof::CCT::ProcFrm* y_frm = static_cast<Prof::CCT::ProcFrm*>(y);
	  if (y_frm->procName().find(name) != string::npos) {
	    return y_frm;
	  }
	}
      }
    }
  }

  return NULL;
}


static void
findPgmValRefErr(Annot::Env::Entry& ref, const Prof::CCT::ANode& dirNode,
		const Prof::CCT::ANode* valNode)
{
  Annot::Name* name = PAL::getClause_name(dirNode);
  Annot::Expr* expr = PAL::getClause_expr(dirNode);

  string nodeStr;

  const Prof::CCT::ANode* node = (valNode) ? valNode : &dirNode;

  for (const Prof::CCT::ANode* x = node; (x); x = x->parent()) {
    string x_str = x->toStringMe(Prof::CCT::Tree::OFlg_DebugAll);
    if (nodeStr.empty()) {
      nodeStr = "(" + x_str + ")";
    }
    else {
      nodeStr = "(" + x_str + ") -> " + nodeStr;
    }
  }

  string valStr = StrUtil::toStr(ref.metricId()) + " -> ";
  if (valNode && valNode->hasMetricSlow(ref.metricId())) {
    valStr += StrUtil::toStr(valNode->metric(ref.metricId()));
  }
  else {
    valStr += "null";
  }

  DIAG_WMsg(0, "Cannot find program values for '" << name->name()
	    << "': '" << expr->expr() << "'. [CCT node: " << nodeStr << "] "
	    << "[Prog value: " << valStr << "]");

  delete name;
  delete expr;
}


static bool
compareLessThan(Annot::AAnnot* x, Annot::AAnnot* y)
{
  // FIXME: cf. ANodeSortedChildIterator::cmpByType()
  const Annot::Name* x_nm = x->name();
  const Annot::Name* y_nm = y->name();
  int cmp = x_nm->name().compare(y_nm->name());
  return (cmp < 0);
}


} // namespace Model


//***************************************************************************
// 
//***************************************************************************

namespace Model {


Mdl::~Mdl()
{
  for (Annot::AAnnot::List::iterator i = m_defFnL.begin();
       i != m_defFnL.end(); ++i) {
    Annot::AAnnot* x = *i;
    delete x;
  }

  m_modelExprFnL.clear();

  m_loopExprFnL.clear();

  delete m_expr;
}


Mdl*
Mdl::make(/*const*/ Prof::CallPath::Profile& prof)
{
  Mdl* model = new Mdl(prof);
  model->make0();
  return model;
}


void
Mdl::make0()
{
  Prof::CCT::ANode* cct_root = m_prof.cct()->root();
  Prof::Metric::Mgr* metricMgr = m_prof.metricMgr();

  //--------------------------------------------------------
  // Find needed metrics
  //--------------------------------------------------------
  static const string timeMetricNm = "REALTIME"; // FIXME
  static const string timeMetricQNm = "Mean";

  for (uint i = 0; i < metricMgr->size(); ++i) {
    const Prof::Metric::ADesc* m = metricMgr->metric(i);
    if (m->nameBase().find(timeMetricNm) != string::npos
	&& m->nameBase().find(timeMetricQNm) != string::npos
	&& m->type() == Prof::Metric::ADesc::TyIncl) {
      m_timeNDesc_avg = m;
      break;
    }
  }

  DIAG_Assert(m_timeNDesc_avg, DIAG_UnexpectedInput);


#if 0
  // FIXME: many assumptions:
  //   - synchronous sampling events come first
  //   - number of metric types and stats
  const int metricStatNum = 4; // sum/num/min/max {0,1,2,3}
  const int metricTypeNum = 2; // incl/excl {0,1}
  const int metricStatOffset = 3; // max
  const int metricTypeOffset = 1; // exclusive
  uint mId = ((metricStatNum * metricTypeNum) * id
	      + (metricStatOffset * metricTypeNum) + metricTypeOffset);
  // FIXME: should use a statistic across all ranks; for now we use this
#endif


  static const string progVal_sumNm = "SYNC1:Mean"; // avg of sum
  static const string progVal_numNm = "SYNC2:Mean"; // avg of num instances
  static const string progVal_maxNm = "SYNC4:Max";  // avg of max

  const Prof::Metric::ADesc* progVal_sum = NULL;
  const Prof::Metric::ADesc* progVal_num = NULL;
  const Prof::Metric::ADesc* progVal_max = NULL;

  for (uint i = 0; i < metricMgr->size(); ++i) {
    const Prof::Metric::ADesc* m = metricMgr->metric(i);
    if (!progVal_sum && m->nameBase().find(progVal_sumNm) != string::npos
	&& m->type() == Prof::Metric::ADesc::TyExcl) {
      progVal_sum = m;
      continue;
    }
    if (!progVal_num && m->nameBase().find(progVal_numNm) != string::npos
	&& m->type() == Prof::Metric::ADesc::TyExcl) {
      progVal_num = m;
      continue;
    }
    if (!progVal_max && m->nameBase().find(progVal_maxNm) != string::npos
	&& m->type() == Prof::Metric::ADesc::TyExcl) {
      progVal_max = const_cast<Prof::Metric::ADesc*>(m);
      continue;
    }
    //break;
  }

  DIAG_Assert(progVal_sum && progVal_num && progVal_max, DIAG_UnexpectedInput);

  //--------------------------------------------------------
  // Create program value metrics
  //--------------------------------------------------------

  string nm = "SYNC:Avg:Avg";
  string desc = nm;

  Prof::Metric::Var* expr_sum =
    new Prof::Metric::Var(progVal_sumNm, progVal_sum->id());
  Prof::Metric::Var* expr_num =
    new Prof::Metric::Var(progVal_numNm, progVal_num->id());

  Prof::Metric::Divide* expr = new Prof::Metric::Divide(expr_sum, expr_num);

  Prof::Metric::ADesc* progVal_avg =
    new Prof::Metric::DerivedDesc(nm, desc, expr, false/*isVisible*/,
				  false/*isSortKey*/, false/*doDispPercent*/,
				  false/*isPercent*/);
  metricMgr->insert(progVal_avg);
  expr->accumId(progVal_avg->id());

  uint begMId = std::min(m_timeNDesc_avg->id(),
			 std::min(progVal_avg->id(), progVal_max->id()));

  uint endMId = std::max(m_timeNDesc_avg->id(),
			 std::max(progVal_avg->id(), progVal_max->id())) + 1;

  cct_root->computeMetrics(*metricMgr, begMId, endMId, /*doFinal*/true);
  cct_root->computeMetricsIncr(*metricMgr, begMId, endMId,
  			       Prof::Metric::AExprIncr::FnFini);

  progVal_avg->computedType(Prof::Metric::ADesc::ComputedTy_Final);

  m_progValMDesc_max = progVal_max;
  m_progValMDesc_avg = progVal_avg;

  //--------------------------------------------------------
  // Create model
  //--------------------------------------------------------
  m_expr = make0(cct_root); // populates function lists

  std::sort(m_defFnL.begin(), m_defFnL.end(), compareLessThan);
  std::sort(m_modelExprFnL.begin(), m_modelExprFnL.end(), compareLessThan);
  std::sort(m_loopExprFnL.begin(), m_loopExprFnL.end(), compareLessThan);
}


Tree::ANode*
Mdl::make0(const Prof::CCT::ANode* node)
{
  if (!node) {
    return NULL;
  }

  //--------------------------------------------------------
  // 1. gather models for child nodes
  //--------------------------------------------------------
  Tree::ANode::List childModels;

  for (Prof::CCT::ANodeChildIterator i(node); i.current(); i++) {
    const Prof::CCT::ANode* n = i.current();
    Tree::ANode* mdl = make0(n);
    if (mdl) {
      childModels.push_back(mdl);
    }
  }

  //--------------------------------------------------------
  // 2. build the model
  //--------------------------------------------------------
  Tree::ANode* model = NULL;

  bool isPALDirective = PAL::isDirective(*node);

  if (dynamic_cast<const Prof::CCT::Root*>(node)) {
    //--------------------------------------------------------
    // special case
    //--------------------------------------------------------

    if (childModels.size() == 1) {
      model = childModels.front();
    }
    else if (childModels.size() > 1) {
      model = mdl_makeSum(NULL, childModels);
    }

  }
  else if (!isPALDirective) {
    //--------------------------------------------------------
    // special case
    //--------------------------------------------------------

    model = make0_noPALDirective(node, childModels);
  }
  else if (isPALDirective) {
    //--------------------------------------------------------
    // PAL directive
    //--------------------------------------------------------

    PAL::DirTy ty = PAL::getDirType(*node);

    Annot::Name* name = PAL::getClause_name(*node);
    Annot::Expr* expr = PAL::getClause_expr(*node);
    Annot::Env* env = PAL::getClause_env1(*node, m_progValMDesc_avg->id(),
					  m_progValMDesc_max->id());
    Annot::Env* env2 = PAL::getClause_env2(*node, m_timeNDesc_avg->id());
    
    if (!env) {
      env = env2;
    }
    else if (env2) {
      env->splice(*env2);
      delete env2;
    }

#define make0_cleanup() { delete name; delete expr; delete env; }

    if (0) {
      string dirStr = PAL::getDirStr(*node);
      DIAG_Msg(1, "directive: '" << dirStr << "' name: '" << name->name()
	       << "' expr: '" << expr->expr() << "'");
    }

    bool isOk = make0_bindEnv1(env, *node);
    if (!isOk) {
      make0_cleanup();
      model = make0_noPALDirective(node, childModels);
      return model; // short circuit
    }
    
    //--------------------------------------------------------
    // make directive
    //--------------------------------------------------------
    if (ty == PAL::DirTy_def) {
      //--------------------------------------------------------
      // def: (must process those def's that capture program values)
      //--------------------------------------------------------
      // FIXME: for def directives, prog-val refs come from static structure
      Annot::Def* def = new Annot::Def(name, expr, env);
      m_defFnL.push_back(def);
    }
    else if (ty == PAL::DirTy_model) {
      //--------------------------------------------------------
      // model:
      //--------------------------------------------------------
      // exprSyn is non-NULL iff measurements indicate contribution
      Tree::ANode* exprSyn = mdl_makeSum(NULL, childModels);
      if (!exprSyn) {
	make0_cleanup();
	return model; // short circuit
      }

      Tree::Model* mdl = new Tree::Model(NULL, node, name, expr, env, exprSyn);
      m_modelExprFnL.push_back(mdl);
      m_cctToMdlMap.insert(std::make_pair(const_cast<Prof::CCT::ANode*>(node),
					  mdl));

      make0_bindEnv2(env, *mdl);

      model = mdl;
    }
    else if (PAL::DirTy_loop) {
      //--------------------------------------------------------
      // loop: distribute number-of-instances expression
      //--------------------------------------------------------
      DIAG_Assert(expr, DIAG_UnexpectedInput);

      // bodyN: model of *all* instances of loop body
      Tree::ANode* bodyN = mdl_makeSum(NULL, childModels);
      if (!bodyN) {
	make0_cleanup();
	return model; // short circuit
      }

      Tree::Loop* loop = new Tree::Loop(NULL, node, name, expr, env, bodyN);
      m_loopExprFnL.push_back(loop);

      model = loop;
    }
    else {
      DIAG_Die(DIAG_UnexpectedInput);
    }

#undef make0_cleanup

  }
  else {
    DIAG_Die(DIAG_UnexpectedInput);
  }

  return model;
}


Tree::ANode*
Mdl::make0_noPALDirective(const Prof::CCT::ANode* node,
			  Tree::ANode::List& childModels)
{
  Tree::ANode* model = NULL;

  bool areAllConstant = true;

  for (Tree::ANode::List::const_iterator i = childModels.begin();
       i != childModels.end(); ++i) {
    const Tree::ANode* x = *i;
    areAllConstant = (areAllConstant && dynamic_cast<const Tree::Const*>(x));
  }
  
  if (areAllConstant) {
    // model for 'node' will represent 'childModels'
    for (Tree::ANode::List::iterator i = childModels.begin();
	 i != childModels.end(); ++i) {
      Tree::ANode*& x = *i;
      delete x;
      x = NULL;
    }
    model = new Tree::Const(NULL, node, m_timeNDesc_avg->id());
  }
  else {
    // model for 'node' is the sum of its children (children > 0)
    model = mdl_makeSum(NULL, childModels);
  }
  
  return model;
}


bool
Mdl::make0_bindEnv1(Annot::Env* env, const Prof::CCT::ANode& dirNode)
{
  bool isOk = true;

  if (!env) {
    return isOk;
  }

  for (Annot::Env::iterator i = env->begin(); i != env->end(); ++i) {
    Annot::Env::Entry& ref = *(*i);

    const std::string& mdlClass = ref.mdlClass();

    if (mdlClass.empty()) {
      //--------------------------------------------------------
      // program value ref
      //--------------------------------------------------------
      Prof::CCT::ANode* valNode = cct_findPgmValRef(dirNode);
      if (valNode && valNode->hasMetricSlow(ref.metricId())) {
	double val = valNode->metric(ref.metricId());
	ref.value(val);
      }
      else {
	findPgmValRefErr(ref, dirNode, valNode);
	isOk = false;
      }
    }
  }

  return isOk;
}


bool
Mdl::make0_bindEnv2(Annot::Env* env, const Tree::Model& mdl)
{
  bool isOk = true;

  if (!env) {
    return isOk;
  }

  const Prof::CCT::ANode* dirNode = mdl.cctNode();

  for (Annot::Env::iterator i = env->begin(); i != env->end(); ++i) {
    Annot::Env::Entry& ref = *(*i);

    const std::string& mdlClass = ref.mdlClass();

    if (!mdlClass.empty()) {
      //--------------------------------------------------------
      // model class ref
      //--------------------------------------------------------
      Prof::CCT::ANode::Vec frntrL_cct;
      Model::Tree::ANode::List* frntrL_mdl = new Model::Tree::ANode::List;
      cct_findMdlFrontier(mdlClass, *dirNode, m_cctToMdlMap,
			  frntrL_cct, *frntrL_mdl);

      if (!frntrL_mdl->empty()) {
	ref.exprL(frntrL_mdl);
	uint flg = ((mdl.name()->nameBase() == mdlClass)
		    ? OFlg_MdlUseVariant_x1s : OFlg_MdlUseVariant_x1);
	ref.exprVariant(flg);
      }
      else {
	isOk = false;
      }
    }
  }

  return isOk;
}


std::ostream&
Mdl::write(std::ostream& os, uint oFlags, const char* pfx) const
{
  string pfx1 = string(pfx) + "  ";
  const char* pfx1_c = pfx1.c_str();

  string pfx2 = pfx1 + "  ";

  os << "#!/usr/bin/env ruby\n";
  os << "# -*- mode: ruby -*-\n";
  os << "\n";
  os << "require 'pal-val.rb'\n";
  os << "require 'pal-machine-pic.rb'\n";
  os << "\n";

  //---------------------------------------------------------
  // main()
  //---------------------------------------------------------
  os << pfx << "def main()\n";
  os << pfx1 << "coresL = [16, 32, 64, 128, 256, 512, 1024, 2048]\n";
  os << pfx1 << "coresPerNode = 16\n";
  os << pfx1 << "\n";
  os << pfx1 << "coresL.each { |cores|\n";
  os << pfx2 << "exeEnv = PAL::ExecutionPIC.new(cores, coresPerNode)\n";
  os << pfx2 << "model = Model.new(exeEnv)\n";
  os << pfx2 << "\n";
  os << pfx2 << "model.eval('')\n";
  os << pfx2 << "$stdout.write('=' * 77 + \"\\n\")\n";
  os << pfx2 << "$stdout.write(model.to_s())\n";
  os << pfx1 << "}\n";
  os << pfx << "end\n";
  os << "\n";

  os << pfx << "class Model\n";

  //---------------------------------------------------------
  // Model.initialize()
  //---------------------------------------------------------
  os << pfx1 << "def initialize(exeEnv, paramsGbl = nil)\n";
  os << pfx2 << "@exeEnv = exeEnv\n";
  os << pfx2 << "@paramsGbl = paramsGbl\n";
  os << pfx2 << "@paramsLcl = nil\n";

  os << pfx2 << "@expr = '";
  if (m_expr) {
    m_expr->writeMdlUse(os, oFlags, pfx);
  }
  os << "'\n";

  if (m_expr && (oFlags & OFlg_Debug)) {
    os << pfx2 << "# ";
    m_expr->writeSexp(os, oFlags, pfx);
    os << std::endl;
  }

  os << pfx2 << "@val = nil\n";

  os << pfx2 << PAL::s_mdl_doMdlExpr << " = true\n";

  os << pfx1 << "end\n";
  os << "\n";

  //---------------------------------------------------------
  // Model.eval()
  //---------------------------------------------------------
  os << pfx1 << "def eval(paramsLcl = nil)\n";
  os << pfx2 << "@paramsLcl = paramsLcl\n";
  os << pfx2 << "@val = Kernel.eval(param_str() + ';' + @expr, binding())\n";
  os << pfx1 << "end\n";
  os << "\n";

  //---------------------------------------------------------
  // Model.val()
  //---------------------------------------------------------
  os << pfx1 << "def val()\n";
  os << pfx2 << "@val\n";
  os << pfx1 << "end\n";
  os << "\n";

  //---------------------------------------------------------
  // Model.param_str()
  //---------------------------------------------------------
  os << pfx1 << "def param_str()\n";
  os << pfx2 << "if (@paramsGbl && @paramsLcl) then \"#{@paramsGbl} ; #{@paramsLcl}\"\n";
  os << pfx2 << "elsif (@paramsGbl) then \"#{@paramsGbl}\"\n";
  os << pfx2 << "elsif (@paramsLcl) then \"#{@paramsLcl}\" end\n";
  os << pfx1 << "end\n";
  os << "\n";

  //---------------------------------------------------------
  // Model.to_s()
  //---------------------------------------------------------
  os << pfx1 << "def to_s()\n";
  //os << pfx2 << "if (!@val) then eval(paramsLcl) end\n";
  os << pfx2 << "\"#{@exeEnv.to_s()}\\n\" \\\n";
  os << pfx2 << "\"Parameters:\\t#{param_str()}\\n\" \\\n";
  os << pfx2 << "\"Expression:\\t#{@expr}\\n\" \\\n";
  os << pfx2 << "\"Prediction:\\t#{@val.to_f() / 1e6} s\\n\" \\\n";
  os << pfx2 << "\"Details:   \\t#{@val.to_str()}\\n\"\n";
  os << pfx1 << "end\n";
  os << "\n";
  os << "\n";

  //---------------------------------------------------------
  // Model functions
  //---------------------------------------------------------

  Annot::AAnnot::List::const_iterator it;

  for (it = m_defFnL.begin(); it != m_defFnL.end(); ++it) {
    const Annot::AAnnot* x = *it;
    x->writeMdlDef(os, oFlags, pfx1_c);
    os << "\n";
  }

  for (it = m_modelExprFnL.begin(); it != m_modelExprFnL.end(); ++it) {
    const Annot::AAnnot* x = *it;
    x->writeMdlDef(os, oFlags, pfx1_c);
    os << "\n";
  }

  for (it = m_loopExprFnL.begin(); it != m_loopExprFnL.end(); ++it) {
    const Annot::AAnnot* x = *it;
    x->writeMdlDef(os, oFlags, pfx1_c);
    os << "\n";
  }

  //---------------------------------------------------------
  // 
  //---------------------------------------------------------
  os << pfx << "end\n";
  os << "\n";
  os << pfx << "if (__FILE__ == $0) then main() end\n";

  if (0) {
    const Prof::Metric::Mgr* metricMgr = m_prof.metricMgr();
    metricMgr->ddump();
  }

  return os;
}


string
Mdl::toString(uint oFlags, const char* pfx) const
{
  std::ostringstream os;
  write(os, oFlags, pfx);
  return os.str();
}


std::ostream&
Mdl::dump(std::ostream& os, uint oFlags, const char* pfx) const
{
  return write(os, oFlags, pfx);
}


std::ostream&
Mdl::ddump() const
{
  return dump(std::cerr);
}


} // namespace Model


//***************************************************************************
// 
//***************************************************************************

namespace Model {
namespace Tree {

//-----------------------------------------------------------------------
// ANode: A synthesized expression tree node
//-----------------------------------------------------------------------

bool
ANode::hasMdlExpr() const
{
  if (m_hasMdlExpr != MyBool_NULL) {
    return (m_hasMdlExpr == MyBool_false) ? false : true;
  }

  //---------------------------------------------------------
  // compute answer, if not cached
  //---------------------------------------------------------

  if (typeid(*this) == typeid(Tree::Model)) {
    const Tree::Model* x = static_cast<const Tree::Model*>(this);
    if (x->expr()) {
      m_hasMdlExpr = MyBool_true;
      return true;
    }
  }

  // N.B.: assumes Model::exprSyn() is a child of Model
  for (Tree::ANodeChildIterator i(this); i.current(); ++i) {
    Tree::ANode* x = i.current();
    bool hasExpr = x->hasMdlExpr();
    if (hasExpr) {
      m_hasMdlExpr = MyBool_true;
      return true;
    }
  }

  m_hasMdlExpr = MyBool_false;
  return false;
}


string
ANode::toString(uint oFlags, const char* pfx) const
{
  std::ostringstream os;
  writeSexp(os, oFlags, pfx);
  return os.str();
}


std::ostream&
ANode::dump(std::ostream& os, uint oFlags, const char* pfx) const
{
  return writeSexp(os, oFlags, pfx);
}


std::ostream&
ANode::ddump() const
{
  return dump(std::cerr);
}


// FIXME: cf. Model::toStringMdl_factor()
string
ANode::toStringMdlUse_list(const Tree::ANode::List& mdlL, std::string op,
			   uint oFlags, const char* pfx)
{
  std::ostringstream os;

  bool doParen = (mdlL.size() > 1);

  if (doParen) {
    os << "(";
  }

  for (Tree::ANode::List::const_iterator i = mdlL.begin();
       i != mdlL.end(); /**/) {
    const Tree::ANode* x = (*i);
    x->writeMdlUse(os, oFlags, pfx);
    ++i; // advance iterator
    if (i != mdlL.end()) {
      os << op;
    }
  }

  if (doParen) {
    os << ")";
  }

  return os.str();
}


std::ostream&
ANode::writeMdlUse_tree(std::ostream& os,
			string lparen, string op, string rparen,
			uint oFlags, const char* pfx) const
{
  bool doParen = (childCount() > 1);
    
  if (doParen) {
    os << lparen;
  }

  for (ANodeSortedChildIterator i(this, ANodeSortedChildIterator::cmpByType);
       i.current(); /**/) {
    const Tree::ANode* mdl = i.current();
    i++;

    mdl->writeMdlUse(os, oFlags, pfx);
    if (i.current()) {
      os << op;
    }
  }

  if (doParen) {
    os << rparen;
  }

  return os;
}


std::ostream&
ANode::writeSexp_tree(std::ostream& os,
		      string lparen, string name, string rparen,
		      uint oFlags, const char* pfx) const
{
  os << lparen;

  os << name;

  for (ANodeSortedChildIterator i(this, ANodeSortedChildIterator::cmpByType);
       i.current(); i++) {
    const Tree::ANode* mdl = i.current();
    os << " ";
    mdl->writeSexp(os, oFlags, pfx);
  }

  os << rparen;

  return os;
}


//-----------------------------------------------------------------------
// Product
//-----------------------------------------------------------------------

std::ostream&
Product::writeMdlUse(std::ostream& os, uint oFlags, const char* pfx) const
{
  if (childCount() == 2) {
    os << "(";
    m_expr1->writeMdlUse(os, oFlags, pfx);
    os << " * ";
    m_expr2->writeMdlUse(os, oFlags, pfx);
    os << ")";
  }
  else {
    writeMdlUse_tree(os, "(", " * ", ")", oFlags, pfx);
  }
  return os;
}


std::ostream&
Product::writeSexp(std::ostream& os, uint oFlags, const char* pfx) const
{
  string op = toStringMe(oFlags);
  return writeSexp_tree(os, "(", op, ")", oFlags, pfx);
}


string
Product::toStringMe(uint oFlags) const
{
  return "Product";
}


//-----------------------------------------------------------------------
// Quotient
//-----------------------------------------------------------------------

std::ostream&
Quotient::writeMdlUse(std::ostream& os, uint oFlags, const char* pfx) const
{
  os << "(";
  m_dividend->writeMdlUse(os, oFlags, pfx);
  os << " / ";
  m_divisor->writeMdlUse(os, oFlags, pfx);
  os << ")";

  return os;
}


std::ostream&
Quotient::writeSexp(std::ostream& os, uint oFlags, const char* pfx) const
{
  string op = toStringMe(oFlags);
  return writeSexp_tree(os, "(", op, ")", oFlags, pfx);
}


string
Quotient::toStringMe(uint oFlags) const
{
  return "Quotient";
}


//-----------------------------------------------------------------------
// Sum
//-----------------------------------------------------------------------

std::ostream&
Sum::writeMdlUse(std::ostream& os, uint oFlags, const char* pfx) const
{
  return writeMdlUse_tree(os, "(", " + ", ")", oFlags, pfx);
}


std::ostream&
Sum::writeSexp(std::ostream& os, uint oFlags, const char* pfx) const
{
  string op = toStringMe(oFlags);
  return writeSexp_tree(os, "(", op, ")", oFlags, pfx);
}


string
Sum::toStringMe(uint oFlags) const
{
  return "Sum";
}


//-----------------------------------------------------------------------
// Loop
//-----------------------------------------------------------------------

std::ostream&
Loop::writeMdlUse(std::ostream& os, uint oFlags, const char* pfx) const
{
  m_exprBody->writeMdlUse(os, oFlags, pfx);
  return os;
}


std::ostream&
Loop::writeMdlFctr(std::ostream& os, uint oFlags, const char* pfx) const
{
  writeMdl_name(os, oFlags, pfx);
  os << "()";
  return os;
}


std::ostream&
Loop::writeMdlDef(std::ostream& os, uint oFlags, const char* pfx) const
{
  string pfx1 = string(pfx) + "  ";
  const char* pfx1_c = pfx1.c_str();

  os << pfx << "def ";
  writeMdl_name(os, oFlags, pfx); // cf. Model::writeMdl_nameDbg()
  os << "()\n";

  if (m_env) {
    os << pfx1;
    m_env->writeMdl(os, oFlags, pfx1_c);
    os << std::endl;
  }
  
  os << pfx1;
  m_expr->writeMdl(os, oFlags, pfx1_c);
  os << std::endl;

  os << pfx << "end" << std::endl;

  return os;
}


std::ostream&
Loop::writeMdl_name(std::ostream& os, uint oFlags, const char* pfx) const
{
  m_name->writeMdl(os, oFlags, pfx);
  if (m_cctNode) {
    os << "_" << m_cctNode->id();
  }
  return os;
}


std::ostream&
Loop::writeSexp(std::ostream& os, uint oFlags, const char* pfx) const
{
  os << "(" << toStringMe(oFlags);
  if (m_env) {
    m_env->writeSexp(os, oFlags, pfx);
    os << " ";
  }

  writeMdl_name(os, oFlags | Mdl::OFlg_Debug, pfx);
  os << " ";

  m_expr->writeMdl(os, oFlags, pfx);
  os << ")";

  return os;
}


string
Loop::toStringMe(uint oFlags) const
{
  return "Loop";
}


//-----------------------------------------------------------------------
// Model
//-----------------------------------------------------------------------

std::ostream&
Model::writeMdlUse(std::ostream& os, uint oFlags, const char* pfx) const
{
  const char* param = "";
  if (oFlags & Mdl::OFlg_MdlUseVariant_xN) {
    param = ""; // :xN
  }
  else if (oFlags & Mdl::OFlg_MdlUseVariant_x1) {
    param = PAL::s_mdl_variant_x1.c_str();
  }
  else if (oFlags & Mdl::OFlg_MdlUseVariant_x1s) {
    param = PAL::s_mdl_variant_x1s.c_str();
  }

  writeMdl_name(os, oFlags, pfx);
  os << "(" << param << ")";
  return os;
}


std::ostream&
Model::writeMdlDef(std::ostream& os, uint oFlags, const char* pfx) const
{
  string pfx1 = string(pfx) + "  ";
  string pfx2 = pfx1 + "  ";
  string pfx3 = pfx2 + "  ";

  const char* pfx2_c = pfx2.c_str();

  std::ostringstream os_name;
  writeMdl_name(os_name, oFlags, pfx);
  string name = os_name.str();

#define mytst(v) \
  PAL::s_mdl_variant << " == " << v << " && " << PAL::s_mdl_doMdlExpr

#define mytstSyn(v_s, v) \
  PAL::s_mdl_variant << " == " << v_s << " || (" << PAL::s_mdl_variant << " == " << v << " && !" << PAL::s_mdl_doMdlExpr << ")"
  

  os << pfx << "def " << name
     << "(" << PAL::s_mdl_variant << " = " << PAL::s_mdl_variant_xN << ")\n";

  // writeMdl_nameDbg(os, oFlags, pfx);

  //---------------------------------------------------------
  // modeling function, one-instance
  //---------------------------------------------------------
  os << pfx1 << "if (" << mytst(PAL::s_mdl_variant_x1) << ")\n";

  if (m_expr) {
    if (m_env) {
      os << pfx2;
      m_env->writeMdl(os, oFlags, pfx2_c);
      os << std::endl;
    }

    os << pfx2;
    writeMdl_expr1(os, oFlags, pfx2_c);
    os << std::endl;
  }
  else {
    os << pfx2 << name << "(" << PAL::s_mdl_variant_x1s << ")\n";
  }

  //---------------------------------------------------------
  // synthesized modeling function, one-instance
  //---------------------------------------------------------
  os << pfx1 << "elsif ("
     << mytstSyn(PAL::s_mdl_variant_x1s, PAL::s_mdl_variant_x1) << ")\n";
  
  os << pfx2;
  writeMdl_exprSyn1(os, oFlags, pfx2_c);
  os << std::endl;

  //---------------------------------------------------------
  // modeling function, n-instances
  //---------------------------------------------------------
  os << pfx1 << "elsif (" << mytst(PAL::s_mdl_variant_xN) << ")\n";

  if (m_expr) {
    os << pfx2 << "_Val(";
    writeMdl_exprN(os, oFlags, pfx2_c);
    if (m_exprSyn) {
      os << ", ";
      os << name << "(" << PAL::s_mdl_variant_xNs << ")";
    }
    os << ")\n";
  }
  else {
    os << pfx2 << name << "(" << PAL::s_mdl_variant_xNs << ")\n";
  }

  //---------------------------------------------------------
  // synthesized modeling function, n-instances
  //---------------------------------------------------------
  os << pfx1 << "elsif ("
     << mytstSyn(PAL::s_mdl_variant_xNs, PAL::s_mdl_variant_xN) << ")\n";

  os << pfx2 << "_Val(";
  writeMdl_exprSynN(os, oFlags, pfx2_c);
  os << ")\n";

  os << pfx1 << "end\n";

  os << pfx << "end\n";

#undef mytst
#undef mytstSyn

  return os;
}


std::ostream&
Model::writeMdl_name(std::ostream& os, uint oFlags, const char* pfx) const
{
  m_name->writeMdl(os, oFlags, pfx);
  if (m_cctNode) {
    os << "_" << m_cctNode->id();
  }
  return os;
}


std::ostream&
Model::writeMdl_nameDbg(std::ostream& os, uint oFlags, const char* pfx) const
{
  if (m_cctNode) {
    if (oFlags & Mdl::OFlg_Debug) {
      Prof::Struct::ACodeNode* strct = m_cctNode->structure();
      if (strct) {
	os << " (" << strct->nameQual() << ")";
      }
    }
  }
  return os;
}


std::ostream&
Model::writeMdl_expr1(std::ostream& os, uint oFlags, const char* pfx) const
{
  m_expr->writeMdl(os, oFlags, pfx);
  return os;
}


std::ostream&
Model::writeMdl_exprN(std::ostream& os, uint oFlags, const char* pfx) const
{
  Tree::ANode::List factorL;
  mdl_findInstanceFactors(this, factorL);

  string str_factor = toStringMdl_factor(factorL, oFlags, pfx);

  if (!str_factor.empty()) {
    os << str_factor << " * ";
  }

  //writeMdl_expr1(os, oFlags, pfx);
  writeMdl_name(os, oFlags, pfx);
  os << "(" << PAL::s_mdl_variant_x1 << ")";

  return os;
}


std::ostream&
Model::writeMdl_exprSyn1(std::ostream& os, uint oFlags, const char* pfx) const
{
  Tree::ANode::List factorL;
  mdl_findInstanceFactors(this, factorL);

  string str_factor = toStringMdl_factor(factorL, oFlags, pfx);

  //m_exprSyn->writeMdlUse(os, oFlags, pfx);
  writeMdl_name(os, oFlags, pfx);
  os << "(" << PAL::s_mdl_variant_xNs << ")";

  if (!str_factor.empty()) {
    os << " / " << str_factor;
  }

  return os;
}


std::ostream&
Model::writeMdl_exprSynN(std::ostream& os, uint oFlags, const char* pfx) const
{
  m_exprSyn->writeMdlUse(os, oFlags, pfx);
  return os;
}


std::ostream&
Model::writeSexp(std::ostream& os, uint oFlags, const char* pfx) const
{
  os << "(" << toStringMe(oFlags);
  if (m_env) {
    m_env->writeSexp(os, oFlags, pfx);
    os << " ";
  }

  writeMdl_name(os, oFlags | Mdl::OFlg_Debug, pfx);
  os << " ";

  writeMdl_expr1(os, oFlags, pfx);
  os << " ";

  if (m_exprSyn) {
    m_exprSyn->writeSexp(os, oFlags, pfx);
  }

  os << ")";
  return os;
}


string
Model::toStringMdl_factor(const Tree::ANode::List& factorL,
			  uint oFlags, const char* pfx)
{
  std::ostringstream os;

  bool doParen = (factorL.size() > 1);

  if (doParen) {
    os << "(";
  }

  for (Tree::ANode::List::const_iterator i = factorL.begin();
       i != factorL.end(); /**/) {
    const Tree::Loop* x = dynamic_cast<const Tree::Loop*>(*i);
    DIAG_Assert(x, DIAG_UnexpectedInput);
    x->writeMdlFctr(os, oFlags, pfx);
    ++i; // advance iterator
    if (i != factorL.end()) {
      os << " * ";
    }
  }

  if (doParen) {
    os << ")";
  }

  return os.str();
}


string
Model::toStringMe(uint oFlags) const
{
  return "Model";
}


//-----------------------------------------------------------------------
// Const
//-----------------------------------------------------------------------

std::ostream&
Const::writeMdlUse(std::ostream& os, uint oFlags, const char* pfx) const
{
  if (hasVal()) {
    os << val();
  }
  else {
    os << "NaN";
  }
  return os;
}


std::ostream&
Const::writeSexp(std::ostream& os, uint oFlags, const char* pfx) const
{
  os << toStringMe(oFlags);
  return os;
}


string
Const::toStringMe(uint oFlags) const
{
  string x = "(Const " + StrUtil::toStr(val());
  if (oFlags & Mdl::OFlg_Debug) {
    x += " (" + StrUtil::toStr(m_metricId) + ")";
  }
  x += ")";
  return x;
}


} // namespace Tree
} // namespace Model


//***************************************************************************
// 
//***************************************************************************

namespace Model {
namespace Tree {

template<typename T, bool isSmallFirst>
static int
compare(T x, T y)
{
  // compare without the possible overflow caused by (x - y)
  if (x == y) {
    return 0;
  }
  else if (x < y) {
    return (isSmallFirst) ? -1 : 1;
  }
  else {
    return (isSmallFirst) ? 1 : -1;
  }
}


ANodeSortedChildIterator::
ANodeSortedChildIterator(const ANode* node, cmp_fptr_t compare_fn)
{
  ANodeChildIterator i(node);
  for (ANode* cur = NULL; (cur = i.current()); i++) {
    scopes.Add((unsigned long) cur);
  }
  ptrSetIt = new WordSetSortedIterator(&scopes, compare_fn);
}


void
ANodeSortedChildIterator::dumpAndReset(std::ostream& os)
{
  os << "ANodeSortedChildIterator: " << std::endl;
  while (current()) {
    os << current()->toStringMe() << std::endl;
    (*this)++;
  }
  reset();
}


int
ANodeSortedChildIterator::cmpByType(const void* a, const void* b)
{
  ANode* x = (*(ANode**)a);
  ANode* y = (*(ANode**)b);

  // 1. sortId
  int cmp_sortId = compare<uint, true>(x->sortId(), y->sortId());
  if (cmp_sortId != 0) {
    return cmp_sortId;
  }

  // 2. compare two nodes of the same type
  if (typeid(*x) == typeid(*y)) {

    // Const: compare by value
    if (typeid(*x) == typeid(Const)) {
      Const* xx = static_cast<Const*>(x);
      Const* yy = static_cast<Const*>(y);
      if (xx->hasVal() && yy->hasVal()) {
	int cmp = compare<double, false>(xx->val(), yy->val());
	return cmp; // if (cmp != 0) {}
      }
    }

    // Model: compare by name
    if (typeid(*x) == typeid(Model)) {
      Model* xx = static_cast<Model*>(x);
      Model* yy = static_cast<Model*>(y);
      // FIXME: use compareLessThan()
      const Annot::Name* xx_nm = xx->name();
      const Annot::Name* yy_nm = yy->name();
      DIAG_Assert(xx_nm && yy_nm, DIAG_UnexpectedInput);
      int cmp = xx_nm->name().compare(yy_nm->name());
      return cmp; // if (cmp != 0) {}
    }

    // Product: compare by expression length (special case)
    if (typeid(*x) == typeid(Product)) {
      int cmp = compare<uint, false>(x->childCount(), y->childCount());
      return cmp; // if (cmp != 0) {}
    }

    // Sum: compare by expression length
    if (typeid(*x) == typeid(Sum)) {
      int cmp = compare<uint, false>(x->childCount(), y->childCount());
      return cmp; // if (cmp != 0) {}
    }

    // ...
  }

  return cmp_sortId;
}


} // namespace Tree
} // namespace Model


//***************************************************************************
// 
//***************************************************************************

namespace Model {
namespace Annot {

//-----------------------------------------------------------------------
// Name
//-----------------------------------------------------------------------

std::string
Name::nameBase() const
{
  // FIXME: palm-c: distinguish between class name and qualified name

  // template: 	<class>_<filebase>_<fileext>_<lineno>
  size_t lnno_pos = m_name.find_last_of("_");
  size_t fileExt_pos = m_name.find_last_of("_", lnno_pos - 1);
  size_t fileNm_pos = m_name.find_last_of("_", fileExt_pos - 1);

  string classNm = m_name.substr(0, fileNm_pos);
  return classNm;
}


std::ostream&
Name::writeMdl(std::ostream& os, uint oFlags, const char* pfx) const
{
  os << m_name;
  return os;
}


std::ostream&
Name::writeSexp(std::ostream& os, uint oFlags, const char* pfx) const
{
  os << "(" << toString(oFlags) << ")";
  return os;
}


string
Name::toString(uint oFlags) const
{
  string x = "Name " + m_name;
  return x;
}


//-----------------------------------------------------------------------
// Expr
//-----------------------------------------------------------------------

std::ostream&
Expr::writeMdl(std::ostream& os, uint oFlags, const char* pfx) const
{
  os << m_expr;
  return os;
}


std::ostream&
Expr::writeSexp(std::ostream& os, uint oFlags, const char* pfx) const
{
  os << "(" << toString(oFlags) << ")";
  return os;
}


string
Expr::toString(uint oFlags) const
{
  return "Expr " + m_expr;
}


//-----------------------------------------------------------------------
// Env
//-----------------------------------------------------------------------

std::ostream&
Env::writeMdl(std::ostream& os, uint oFlags, const char* pfx) const
{
  for (const_iterator i = m_env.begin(); i != m_env.end(); /**/) {
    const Entry* x = *i;
    ++i;
    x->writeMdl(os, oFlags);
    if (i != m_env.end()) {
      //os << "; ";
      os << std::endl << pfx;
    }
  }
  return os;
}


std::ostream&
Env::writeMdlParms(std::ostream& os, bool doFormalParams,
		     uint oFlags, const char* pfx) const
{
  for (const_iterator i = m_env.begin(); i != m_env.end(); /**/) {
    const Entry* x = *i;
    ++i;
    x->writeMdlParms(os, doFormalParams, oFlags);
    if (i != m_env.end()) {
      os << ", ";
    }
  }
  return os;
}


std::ostream&
Env::writeSexp(std::ostream& os, uint oFlags, const char* pfx) const
{
  string pfx1 = string(pfx) + "  ";
  os << "(Env"; // << std::endl;
  for (const_iterator i = m_env.begin(); i != m_env.end(); ++i) {
    const Entry* x = *i;
    os << " ";
    x->writeSexp(os, oFlags /*, pfx1.c_str()*/);
    // os << std::endl;
  }
  os << ")"; // << std::endl;
  return os;
}


std::string
Env::toString(uint oFlags, const char* pfx) const
{
  std::ostringstream os;
  dump(os, oFlags, pfx);
  return os.str();
}


std::ostream&
Env::dump(std::ostream& os, uint oFlags, const char* pfx) const
{
  return writeSexp(os, oFlags, pfx);
}


std::ostream&
Env::ddump() const
{
  return dump(std::cerr);
}


//-----------------------------------------------------------------------
// Env::Entry
//-----------------------------------------------------------------------

std::ostream&
Env::Entry::writeMdl(std::ostream& os, uint oFlags) const
{
  if (m_exprL) {
    uint flgs = oFlags | m_exprVariant;
    string expr = Tree::ANode::toStringMdlUse_list(*m_exprL, " + ", flgs);
    os << m_var << " = " << expr;
  }
  else {
    os << m_var << " = " << m_value;
    if (oFlags & Model::Mdl::OFlg_Debug) {
      os << "[" << m_metricId << "]";
    }
  }
  return os;
}


std::ostream&
Env::Entry::writeMdlParms(std::ostream& os, bool doFormalParams,
			  uint oFlags) const
{
  if (doFormalParams) {
    os << m_var;
  }
  else {
    os << m_value;
    if (oFlags & Model::Mdl::OFlg_Debug) {
      os << "[" << m_metricId << "]";
    }
  }
  return os;
}


std::ostream&
Env::Entry::writeSexp(std::ostream& os, uint oFlags, const char* pfx) const
{
  os << pfx << "(" << m_var << " " << m_value << " (" << m_metricId << "))";
  return os;
}


std::string
Env::Entry::toString(uint oFlags, const char* pfx) const
{
  std::ostringstream os;
  dump(os, oFlags, pfx);
  return os.str();
}


std::ostream&
Env::Entry::dump(std::ostream& os, uint oFlags, const char* pfx) const
{
  return writeSexp(os, oFlags, pfx);
}


std::ostream&
Env::Entry::ddump() const
{
  return dump(std::cerr);
}


//-----------------------------------------------------------------------
// Def
//-----------------------------------------------------------------------

std::ostream&
Def::writeMdlDef(std::ostream& os, uint oFlags, const char* pfx) const
{
  string pfx1 = string(pfx) + "  ";
  const char* pfx1_c = pfx1.c_str();

  // N.B.: We take advantage of the fact that in Ruby one can define a
  // function of arity 0 (cf. a variable) without an argument list and
  // reference it like a variable (without argument list)

  os << pfx << "def ";
  m_name->writeMdl(os, oFlags, pfx);
  os << std::endl;

  if (m_env) {
    os << pfx1;
    m_env->writeMdl(os, oFlags, pfx1_c);
    os << std::endl;
  }
  
  os << pfx1;
  m_expr->writeMdl(os, oFlags, pfx1_c);
  os << std::endl;
  
  os << pfx << "end" << std::endl;
  
  return os;
}


} // namespace Annot
} // namespace Model

//***************************************************************************

