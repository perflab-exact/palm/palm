-*-Mode: markdown;-*- [outline]
=============================================================================

$HeadURL$
$Id$

=============================================================================
HPCToolkit Github:
=============================================================================

20170602: [includes SymTabAPI-based 'inline' support.]
  - HPCToolkit: 
    GitHub: 305854c2fe05b03786f0bbbb750ea764c09ebae9 (2017/06/02 09:17:01)

  - HPCToolkit-externals:
    GitHub: 5e5ad10bbd6bd8467a8de049ab4dc10f5b4d3bd6 (2017/05/12 14:57:49)


20160625: [includes SymTabAPI-based 'inline' support.]
  - HPCToolkit: 
    GitHub: cf392aaee8c3a681cdb11b71aadc09b2121f4562 (2016/06/25 15:42:10)

  - HPCToolkit-externals:
    GitHub: 3d2953623357bb06e9a4b51eca90a4b039c2710e (2016/02/10 15:24:03)

-----------------------------------------------------------------------------

[Some of this could be committed?]

src/tool/hpcrun/hpctoolkit.h
  hpctoolkit_sample1(): C interface

src/tool/hpcrun/start-stop.c
  hpcrun_metric_std_min(): new
  hpcrun_metric_std_max(): new
  hpctoolkit_sample_x(): new
  hpctoolkit_sample1(): 
  hpctoolkit_sample1f_(): 


src/tool/hpcrun/sample-sources/sync.c
  METHOD_FN(process_event_list): The routine hpcrun_set_metric_info()
    simply copies the metric 'name' pointer. Allocate space for metric
    names.

-----------------------------------------------------------------------------

src/tool/hpcprof/main.cpp
src/lib/analysis/Makefile.{am,in}
src/lib/analysis/Model.{hpp,cpp}

-----------------------------------------------------------------------------

src/lib/banal/Struct-LocationMgr.{hpp,cpp}

  * Enable recovery in the presense of Palm directives representing
    known structure

  -  strings in struct AlienStrctMapKey should be refs
  ???


=============================================================================
HPCToolkit / Google r4770, r4771
=============================================================================

Includes SymTabAPI-based 'inline' support

r4771:
  - Google r4771: http://hpctoolkit.googlecode.com/svn/externals
  - Google r4771: http://hpctoolkit.googlecode.com/svn/trunk

src/lib/banal/Struct.cpp [committed r4770]

  BAnal::Struct::coalesceDuplicateStmts():
  [BAnal::Struct::]cds_*:
  [BAnal::Struct::]StmtKeyToStmtMap, StmtKey, StmtData:
    Two changes:
    1) Rewrite to an algorithm that does not require
    backtracking. Specifically, apply transformations in ordered
    phases so that a fixed point is reached without any backtracking.

    Given a procedure, the backtracking algorithm had a complexity of
    O(n^2 * d), whereas the new algorithm has a complexity of O(n *
    d). Here, n is number of statements in a procedure and where d is
    the procedure's loop nest depth (since loop nests are
    merged). However, in practice, there seems to be little
    performance difference.


    2) Correctly coalesce duplicate statements in two distinct Alien
    contexts.

  [BAnal::Struct::]mergePerfectlyNestedLoops(): Cleanup.

  [BAnal::Struct::]removeEmptyNodes(): Cleanup


src/lib/prof/Struct-Tree.{cpp,hpp} [committed r4771]

  Prof::Struct::ANode::mergePaths(): Replace a correct but convoluted
  merge with straightforward version.

  Prof::Struct::ANode::merge(): Minor cleanup.

  Prof::Struct::ANode::isMergable(): Minor cleanup.

  Prof::Struct::ANode::ddump*(): Dump with debug info.

  Prof::Struct::Tree::ddump*(): Dump with debug info.


=============================================================================
HPCToolkit / Google r4365
=============================================================================

src/lib/banal/Struct-LocationMgr.{hpp,cpp}

  --- Notable changes for Palm ---

  Palm directive parser:
    Palm::isDirective()
    Palm::getDirListStr()

  BAnal::Struct::LocationMgr::Ctxt::containsLine(): Special case for Palm directives.

  BAnal::Struct::LocationMgr::determineContext():
   - Cleanup
   - Special-case handling of Palm directives

  BAnal::Struct::LocationMgr::demandAlienStrct(): 
   - Separate existing Alien structure lookup and creation of new
     Alien structure
   - Special-case handling of Palm directives where nesting is known

  BAnal::Struct::LocationMgr::findAlienStrct(): Handle Alien structure lookup.


  --- Trivial changes for Palm ---

  RELOCATEDcmp(): Unused; remove.

  BAnal::Struct::LocationMgr::locate(Prof::Struct::Loop): Renaming cleanup.
  BAnal::Struct::LocationMgr::locate(Prof::Struct::Stmt): Renaming cleanup.
  BAnal::Struct::LocationMgr::dump(): Renaming cleanup.

  BAnal::Struct::LocationMgr::fixContextStack(): Renaming cleanup.
  BAnal::Struct::LocationMgr::alienateScopeTree(): Renaming cleanup.

  BAnal::Struct::LocationMgr::revertToLoop(): Unused; remove.


  BAnal::Struct::LocationMgr::AlienStrctMapKey: Cleanup;
    convert lt_AlienStrctMapKey to AlienStrctMapKey::operator<.


  --- New from r4765 trunk ---

  BAnal::Struct::LocationMgr::locate(Prof::Struct::Loop): 
  BAnal::Struct::LocationMgr::locate(Prof::Struct::Stmt): 
    New 'targetScopeID' paramter; call determineContext with VMA.

  BAnal::Struct::LocationMgr::determineContext():
    - Gross hacks to use inline information

  BAnal::Struct::LocationMgr::fixScopeTree(): Pass along 'targetScopeID'
  BAnal::Struct::LocationMgr::alienateScopeTree(): Pass along 'targetScopeID'

  BAnal::Struct::LocationMgr::demandAlienStrct(): Use 'targetScopeID'.


-----------------------------------------------------------------------------

src/lib/banal/Struct.cpp

  + [BAnal::Struct::]mergeBogusAlienStrct(): Cleanup

