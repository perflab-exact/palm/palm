// -*-Mode: C++;-*- // technically C99

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// ******************************************************* EndRiceCopyright *

//***************************************************************************
//
// File:
// $HeadURL$
//
// Purpose:
// "MPI Critical Path" and "MPI Blame Shifting" sampling source.
//
// Authors:
// Nathan Tallent and Abhinav Vishnu
//
//***************************************************************************

#ifndef _HPCRUN_MPI_BARRIER_SLACK_H_
#define _HPCRUN_MPI_BARRIER_SLACK_H_

//***************************************************************************
// system includes
//***************************************************************************

#include <stdlib.h>
#include <limits.h> // for ULONG_MAX (instead of DBL_MAX)

//***************************************************************************
// MPI
//***************************************************************************

#include <mpi.h>

//***************************************************************************
// libmonitor include files
//***************************************************************************

//#include <monitor.h>

//***************************************************************************
// local includes
//***************************************************************************

#include "mpi-util.h"
#include "mpi-decl.h"

//***************************************************************************
// SortTy_t
//***************************************************************************

typedef enum {
  SortTy_NULL = 0,
  SortTy_0_9, // ascending
  SortTy_9_0, // descending

} SortTy_t;


//***************************************************************************
// barrier, where information propagation "follows"
//***************************************************************************

// blame-shifting uses 'rank' and 'slack'
// slack-critical path uses all fields

typedef struct barrier_stat_t {
  int      rank;        // w.r.t. application's communicator
  double   slack;       // new slack (excludes 'path_slack') (us)

  double   path_slack;  // path's slack (us), excludes 'slack'
  int      path_rank;   // path's tail rank (w.r.t. prof communicator)
  uint64_t path_timeId; // path's tail time id

} barrier_stat_t;

#define barrier_stat_NULL						\
  (barrier_stat_t){							\
    .rank = MPI_PROC_NULL,						\
    .slack = 0.0,							\
    .path_slack = 0.0,						        \
    .path_rank = MPI_PROC_NULL,					        \
    .path_timeId = 0 }


static inline void
barrier_stat_init(barrier_stat_t* x, SortTy_t sort_ty)
{
  *x = barrier_stat_NULL;
  
  // selected value places NULL elements at *end* of sort
  x->path_slack = ((sort_ty == SortTy_0_9) ? (double)(ULONG_MAX) : 0.0);
}


#define barrier_n_stat_subbuf(n_stat) (n_stat + 1)

#define barrier_n_stat_buf(n_stat) (2 * n_stat + 2)

#define barrier_stat_sndbuf(stat_buf) (stat_buf)

#define barrier_stat_rcvbuf(stat_buf, n_stat_subbuf) \
  (((void*)stat_buf) + (sizeof(barrier_stat_t) * n_stat_subbuf))


static inline void
barrier_stat_buf_init(barrier_stat_t* stats_buf, int n_stat_buf,
		      SortTy_t sort_ty)
{
  int i;
  for (i = 0 ; i < n_stat_buf; i++) {
    barrier_stat_init(&(stats_buf[i]), sort_ty);
  }
}


static inline void
barrier_stat_buf_dump(barrier_stat_t* buf, int n_stat, const char* ctxt_str)
{
  int i;
  for (i = 0 ; i < n_stat; i++) {
    barrier_stat_t* x = &(buf[i]);
    TMSG(MPI  , "%s stats[%p+%d]: <%d, %g (%g + %g)>", ctxt_str, buf, i,
	 x->rank, (x->path_slack + x->slack), x->path_slack, x->slack);
  }
}


// barrier_sort: 
// - 'bubble sort' is good when 'nelem' is very small
// - remove duplicates
static inline void
barrier_sort(barrier_stat_t* a, int nelem, SortTy_t sort_ty)
{
  int i, j;
  
  for (i = 0; i < nelem; i++) {
    for (j = i + 1; j < nelem; j++) {
      int rank_i = a[i].rank;
      int rank_j = a[j].rank;
      if (rank_i == rank_j) {
	// Remove duplicates
	if (rank_i != MPI_PROC_NULL) {
	  barrier_stat_init(&(a[j]), sort_ty);
	}
      }
      else {
	// Ensure ordering. Use 'rank' to break ties, since rank_i != rank_j
	double val_i = a[i].path_slack + a[i].slack;
	double val_j = a[j].path_slack + a[j].slack;

       	bool is_ordr = (rank_i < rank_j);
	if (val_i != val_j) {
	  is_ordr = (sort_ty == SortTy_0_9) ? val_i < val_j : val_i > val_j;
	}

	if (!is_ordr) {
	  barrier_stat_t tmp = a[i];
	  a[i] = a[j];
	  a[j] = tmp;
	}
      }
    }
  }
}


static inline void
barrierX_mkStat(int step, uint64_t time_beg, int rank_src, int rank_me,
		barrier_stat_t* stat_buf, int n_stat, SortTy_t sort_ty)
{
  const int n_stat_buf = barrier_n_stat_buf(n_stat);
  const int n_stat_subbuf = barrier_n_stat_subbuf(n_stat);

  barrier_stat_t* snd_buf = barrier_stat_sndbuf(stat_buf);
  barrier_stat_t* rcv_buf = barrier_stat_rcvbuf(stat_buf, n_stat_subbuf);

  //----------------------------------------------------------
  // Step 0: First arrival/Complete paths
  //----------------------------------------------------------
  if (step == 0) {
    double e_time = timeElapsed_us(time_beg);

    snd_buf[0].rank = MPI_PROC_NULL;
    snd_buf[0].slack += e_time; // use '+' b/c step 0 may occur twice

#if (My_DoCriticalPath)
    // N.B.: Step 0 must only occur once!

    // Complete input paths in snd_buf[1..k]
    for (int i = 1; i < (n_stat + 1); ++i) {
      snd_buf[i].rank  = rank_me; // rank where slack occurs
      snd_buf[i].slack = e_time;
      // .path_slack, .path_rank, .path_timeId: input values
    }

    // Clear rcv_buf, which contains *incomplete* input paths from partner
    barrier_stat_buf_init(rcv_buf, n_stat_subbuf, sort_ty);

#elif (My_DoBlameShifting)
    // Step 0 can occur twice when ranks are not a power-of-2. On the
    // second instance, snd_buf[0] contains my slack, snd_buf[1]
    // contains slack caused by me (from first set of partners) and
    // rcv_buf[0] is empty (from second set of partners).
    rcv_buf[0].rank  = rank_src; // rank responsible for slack
    rcv_buf[0].slack = e_time;
    // .path_slack, .path_rank, .path_timeId: unused
#else
#  error "unimplemented"
#endif
  }
  //----------------------------------------------------------
  // Step > 0
  //----------------------------------------------------------
  else {
#if (My_DoBlameShifting)
    // rcv_buf[] just arrived from partner's snd_buf[]
    snd_buf[0].slack += rcv_buf[0].slack;
#endif

    barrier_stat_init(&(rcv_buf[0]), sort_ty);
  }

  // sort ensures top k statistics are in 'stat_buf/snd_buf[1..k]'
  barrier_sort(stat_buf + 1, n_stat_buf - 1, sort_ty);
}


// hpcrun_barrierX: Let k = n_stat.
//
//   Extra step for capturing and propagating slack.
//
// *** Critical path: k "most" critical paths ***
//
//   stat_buf INVARIANTS: [snd_buf | rcv_buf]
//   - snd_buf[0]   : slack for my rank (.slack)
//   - snd_buf[1..k]: top k paths based on (.path_slack + .slack);
//                    .rank is the rank owning the path's tail
//   - rcv_buf[0]   : <unused>
//   - rcv_buf[1..k]: <from other ranks>
//
//   - stat_buf has size 'n_stat_buf'
//   - each sub-buffer has size 'n_stat_subbuf'
//
//   Input:
//   - stat_buf[1..k]: top k paths (.path_slack, .path_rank, .path_timeId)
//
//   Output:
//   - stat_buf[0] and stat_buf[1..k]
// 
//   INVARIANTS:
//   - step = 0 when rank arrives at barrier; and step > 0 thereafter
//
// *** Blame shifting: k ranks most responsible for waiting ***
//
//   stat_buf INVARIANTS: [snd_buf | rcv_buf ]
//   - snd_buf[0]   : total slack (.slack)
//   - snd_buf[1..k]: top k values of slack (.slack) and the rank
//                    (.rank) that caused that slack (approx.)
//   - rcv_buf[0]   : <new input; intermediate slack from other ranks>
//   - rcv_buf[1..k]: <from other ranks>
//
//   Input: <no extra inputs>
//
//   Output:
//     stat_buf[0] and stat_buf[1..k]
//
//   N.B.: permit (step == 0) more than once
// 
// *** Notes ***
// - use sendrecv(src, dst) at rank 'me' to estimate slack
//   - send (eager) indicates the rank's arrival at barrier
//   - recv (blocking) estimates the rank's slack
//     (not perfect because the rank could block at a later step)
//   - sender still needs to know info obtained at receiver
//
// - This algorithm estimates slack using waiting time for the first
//   partner synchronization. Unfortunately, the rank may still wait;
//   there is no general way to capture all slack without using an
//   extra barrier.

static int
hpcrun_barrierX(int n_stat, barrier_stat_t* stat_buf, int n_stat_buf,
		MPI_Datatype subbuf_ty, MPI_Comm comm)
{
  int rc;

  assert(comm != MPI_COMM_NULL);

  int comm_sz, me;
  rc = PMPI_Comm_size(comm, &comm_sz);
  assert(rc == MPI_SUCCESS);

  rc = PMPI_Comm_rank(comm, &me);
  assert(rc == MPI_SUCCESS);

  //----------------------------------------------------------
  //----------------------------------------------------------

  const int n_stat_subbuf = barrier_n_stat_subbuf(n_stat);

  barrier_stat_t* snd_buf = barrier_stat_sndbuf(stat_buf);
  barrier_stat_t* rcv_buf = barrier_stat_rcvbuf(stat_buf, n_stat_subbuf);

  const int subbuf_ct = 1;

  assert(n_stat_buf == barrier_n_stat_buf(n_stat));

  //----------------------------------------------------------
  // input (most critical path or blame shifting)
  //----------------------------------------------------------

#if (My_DoCriticalPath) /* Critical path slack */
  SortTy_t stat_srt = SortTy_0_9; // most c.p.: ascending (by cost)
#elif (My_DoBlameShifting)
  SortTy_t stat_srt = SortTy_9_0; // blame-shift: descending (by slack)
#else
# error "unimplemented"
#endif

  barrier_stat_init(&(stat_buf[0]), stat_srt);

  if (0) { barrier_stat_buf_dump(stat_buf, n_stat_buf, "most/before"); }

  //----------------------------------------------------------
  // input (least critical path)
  //----------------------------------------------------------

  SortTy_t stat1_srt = SortTy_9_0; // least c.p.: descending (by slack)

#if (My_DoCriticalPath_least) /* Critical path slack */
  const int subbuf_sz = sizeof(barrier_stat_t) * n_stat_subbuf;

  barrier_stat_t* stat1_buf = ((void*)stat_buf) + (2 * subbuf_sz);
#else
  barrier_stat_t* stat1_buf = NULL;
#endif

  if (stat1_buf) {
    barrier_stat_init(&(stat1_buf[0]), stat1_srt);

    if (0) { barrier_stat_buf_dump(stat1_buf, n_stat_buf, "least/before"); }
  }

  //----------------------------------------------------------
  //----------------------------------------------------------

  // sz_pof2 will be set to largest power of 2 <= comm_sz
  int sz_pof2 = 1;
  while (sz_pof2 <= comm_sz) {
    sz_pof2 <<= 1;
  }
  sz_pof2 >>= 1;

  int sz_rem = comm_sz - sz_pof2;
  assert(sz_rem >= 0);

  int step = 0; // step > 0 after rank first arrives

  MPI_Status st;
  const int tag = s_prof_tag;

  //----------------------------------------------------------
  // Special case for ranks >= sz_pof2: select partner ranks
  //----------------------------------------------------------
  
  if (sz_rem > 0) {
    // capture slack for ranks >= sz_pof2 and send to partners
    if (me >= sz_pof2) {
      int othr = me - sz_pof2;

      uint64_t t_beg = time_getTSC();
      rc = real_PMPI_Recv(rcv_buf, subbuf_ct, subbuf_ty, othr, tag, comm, &st);
      assert(rc == MPI_SUCCESS);

      barrierX_mkStat(step, t_beg, othr, me, stat_buf,  n_stat, stat_srt);
      if (stat1_buf) {
      barrierX_mkStat(step, t_beg, othr, me, stat1_buf, n_stat, stat1_srt);
      }

      rc = real_PMPI_Send(snd_buf, subbuf_ct, subbuf_ty, othr, tag, comm);
      assert(rc == MPI_SUCCESS);

      step++; // arrived; paths have been completed (and sent)
    }
    else if (me < sz_rem) {
      int othr = sz_pof2 + me;

      rc = real_PMPI_Send(snd_buf, subbuf_ct, subbuf_ty, othr, tag, comm);
      assert(rc == MPI_SUCCESS);

      uint64_t t_beg = time_getTSC();
      rc = real_PMPI_Recv(rcv_buf, subbuf_ct, subbuf_ty, othr, tag, comm, &st);
      assert(rc == MPI_SUCCESS);

      barrierX_mkStat(step, t_beg, othr, me, stat_buf,  n_stat, stat_srt);
      if (stat1_buf) {
      barrierX_mkStat(step, t_beg, othr, me, stat1_buf, n_stat, stat1_srt);
      }

      // arrived; paths have been completed (but not sent)

#if (My_DoCriticalPath)
      // Critical Path: B/c rank has arrived, consider the path complete.
      // Blame Shifting: This rank cannot cause waiting, but it can wait;
      //   remain at step 0 so we can receive blame on step 0 below.
      step++;
#endif
    }
  }

 
  //----------------------------------------------------------
  // Dissemination-style partners for ranks < sz_pof2
  //----------------------------------------------------------

  int mask = 0x1;
  if (me < sz_pof2) {
    while (mask < sz_pof2) {
      int dst = (me + mask) % sz_pof2;
      int src = (me + sz_pof2 - mask) % sz_pof2;
      
      uint64_t t_beg = time_getTSC();
      rc = real_PMPI_Sendrecv(snd_buf, subbuf_ct, subbuf_ty, dst, tag,
			      rcv_buf, subbuf_ct, subbuf_ty, src, tag,
			      comm, &st);
      assert(rc == MPI_SUCCESS);

      barrierX_mkStat(step, t_beg, src, me, stat_buf,  n_stat, stat_srt);
      if (stat1_buf) {
      barrierX_mkStat(step, t_beg, src, me, stat1_buf, n_stat, stat1_srt);
      }

      step++; // step > 0 after rank first arrives
      mask <<= 1;
    }
    
    // extra step to so that first-step senders receive stats
    mask = 0x1;
    int dst = (me + mask) % sz_pof2;
    int src = (me + sz_pof2 - mask) % sz_pof2;
    rc = real_PMPI_Sendrecv(snd_buf, subbuf_ct, subbuf_ty, dst, tag,
			    rcv_buf, subbuf_ct, subbuf_ty, src, tag, comm, &st);
    assert(rc == MPI_SUCCESS);

    barrierX_mkStat(step, 0, src, me, stat_buf,  n_stat, stat_srt);
    if (stat1_buf) {
    barrierX_mkStat(step, 0, src, me, stat1_buf, n_stat, stat1_srt);
    }
  }

  //----------------------------------------------------------
  // Special case for ranks >= sz_pof2: receive final 'allreduced' stats
  //----------------------------------------------------------

  if (sz_rem > 0) {
    if (me >= sz_pof2) {
      int othr = me - sz_pof2;
      rc = real_PMPI_Recv(rcv_buf, subbuf_ct, subbuf_ty, othr, tag, comm, &st);
      assert(rc == MPI_SUCCESS);

      barrierX_mkStat(step, 0, othr, me, stat_buf,  n_stat, stat_srt);
      if (stat1_buf) {
      barrierX_mkStat(step, 0, othr, me, stat1_buf, n_stat, stat1_srt);
      }
    }
    else if (me < sz_rem) {
      int othr = sz_pof2 + me;
      rc = real_PMPI_Send(snd_buf, subbuf_ct, subbuf_ty, othr, tag, comm);
      assert(rc == MPI_SUCCESS);
    }
  }

  if (0) { barrier_stat_buf_dump(stat_buf, n_stat_buf, "most/after"); }
  if (0 && stat1_buf) { barrier_stat_buf_dump(stat1_buf, n_stat_buf, "least/after"); }
  
  return 0;
}

//***************************************************************************

#endif // _HPCRUN_MPI_BARRIER_SLACK_H_
