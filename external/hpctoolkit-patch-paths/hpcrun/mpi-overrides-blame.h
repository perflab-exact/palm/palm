// -*-Mode: C++;-*- // technically C99

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// ******************************************************* EndRiceCopyright *

//***************************************************************************
//
// File:
// $HeadURL$
//
// Purpose:
// "MPI Critical Path" and "MPI Blame Shifting" sampling source.
//
// Authors:
// Nathan Tallent and Abhinav Vishnu
//
//***************************************************************************

#ifndef _HPCRUN_MPI_OVERRIDES_BLAME_H_
#define _HPCRUN_MPI_OVERRIDES_BLAME_H_

//***************************************************************************
// system includes
//***************************************************************************

#include <stdlib.h>

//***************************************************************************
// MPI
//***************************************************************************

#include <mpi.h>

//***************************************************************************
// libmonitor include files
//***************************************************************************

//#include <monitor.h>

//***************************************************************************
// local includes
//***************************************************************************

#include "mpi-util.h"


//***************************************************************************
// ProfCmd_t / ProfCmdRequest_t
//***************************************************************************

typedef struct ProfCmd_t {
  double   t_blame;
  OpTy_t   op_ty;
  MPI_Comm app_comm;  // application communicator
  int      prof_rank; // operation's partner rank (w.r.t. profiling server)
                      // (a) when srvr sends: rank used by operation
                      // (b) when srvr rcvs : rank that initiated operation
  int      tag;
} ProfCmd_t;


#define ProfCmd_NULL							\
  (ProfCmd_t)								\
  { .t_blame = 0.0, .op_ty = OpTy_NULL, .app_comm = MPI_COMM_NULL,	\
      .prof_rank = MPI_PROC_NULL, .tag = MPI_UNDEFINED }

/*static const ProfCmd_t ProfCmd_NULL*/


static inline void
ProfCmd_dump(const ProfCmd_t* x, const char* ctxt_str)
{
  const int cmm_str_sz = 16;
  char cmm_str[cmm_str_sz];
  mpicomm_str(x->app_comm, cmm_str, cmm_str_sz);

  const int rnk_str_sz = 16;
  char rnk_str[rnk_str_sz];
  mpirank_str(x->prof_rank, rnk_str, rnk_str_sz);

  const int tag_str_sz = 16;
  char tag_str[tag_str_sz];
  mpitag_str(x->tag, tag_str, tag_str_sz);

  TMSG(MPI, "%s <op %s, comm %s, op_rank %s, tag %s> blm (us) %g",
       ctxt_str, OpTy_str(x->op_ty), cmm_str, rnk_str, tag_str, x->t_blame);
}


typedef struct ProfCmdRequest_t {
  MPI_Request request;
  ProfCmd_t cmd;
} ProfCmdRequest_t;

#define ProfCmdRequest_NULL						\
  (ProfCmdRequest_t){ .request = MPI_REQUEST_NULL, .cmd = ProfCmd_NULL }


//***************************************************************************

#endif // _HPCRUN_MPI_OVERRIDES_BLAME_H_
