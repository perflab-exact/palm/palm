// -*-Mode: C++;-*- // technically C99

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// ******************************************************* EndRiceCopyright *

//***************************************************************************
//
// File:
// $HeadURL$
//
// Purpose:
// "MPI Critical Path" and "MPI Blame Shifting" sampling source.
//
// Authors:
// Nathan Tallent and Abhinav Vishnu
//
//***************************************************************************

#ifndef _HPCRUN_MPI_OVERRIDES_PATHS_H_
#define _HPCRUN_MPI_OVERRIDES_PATHS_H_

//***************************************************************************
// system includes
//***************************************************************************

#include <stdlib.h>

//***************************************************************************
// MPI
//***************************************************************************

#include <mpi.h>

//***************************************************************************
// libmonitor include files
//***************************************************************************

//#include <monitor.h>

//***************************************************************************
// local includes
//***************************************************************************

#include "mpi-util.h"


//***************************************************************************
// TaskPnt (TaskPoint)
//***************************************************************************

const int TaskPntTy_model_flag = (MPI_PROC_NULL - 1);

typedef enum {
  TaskPntTy_NULL = 0,
  TaskPntTy_sync,
  TaskPntTy_model,

} TaskPntTy_t;


typedef struct {

  //sample_val_t backtrace_val;
  cct_node_t* bt_cctTraceNode;
  uint64_t timeId;

} TaskPnt_t;


static inline uint64_t
time2timeId(uint64_t time)
{
  return (time << TaskPnt_TimeId_shift);
}


static inline uint64_t
timeId2time(uint64_t timeId)
{
  return (timeId >> TaskPnt_TimeId_shift);
}


static inline int
TimeId_qual(uint64_t timeId)
{
  uint64_t qual = (TaskPnt_TimeId_mask & timeId);
  return (int)qual;
}


static inline uint64_t
TimeId_time(uint64_t timeId_lo, uint64_t timeId_hi, bool doExclusive)
{
  uint64_t us = timeId2time(timeId_hi - timeId_lo);
  if (us > 0 && doExclusive) {
    us += 1; // exclusive bounds
  }
  return us;
}


//***************************************************************************
// 
//***************************************************************************

typedef enum {
  PathTy_NULL = 0,
  PathTy_most,
  PathTy_least,

} PathTy_t;


typedef enum {
  JoinRuleTy_NULL = 0,
  JoinRuleTy_uni_uni,
  JoinRuleTy_hi_uni,
  JoinRuleTy_uni_lo,
  JoinRuleTy_hi_lo

} JoinRuleTy_t;



static inline bool
JoinRuleTy_hasUniform(JoinRuleTy_t x)
{
  return (x == JoinRuleTy_uni_uni || x == JoinRuleTy_hi_uni
	  || x == JoinRuleTy_uni_lo);
}


//***************************************************************************
// PathSum_t / TaskSum_t
//***************************************************************************

typedef struct TaskSum_t {
  
  int rank;
  uint16_t index_lo; // inclusive: [
  uint16_t index_hi; // inclusive: ]

} TaskSum_t;


#define TaskSum_NULL							\
  (TaskSum_t){								\
    .rank = MPI_PROC_NULL,						\
    .index_lo = 0,							\
    .index_hi = 0							\
  }


static inline void
TaskSum_init(TaskSum_t* x)
{
  *x = TaskSum_NULL;
}


static inline void
TaskSum_dump(const TaskSum_t* x, const char* ctxt_str, int idx)
{
  AMSG("%sTaskSum[%d]: %d:%d-%d", ctxt_str, idx,
       x->rank, (int)x->index_lo, (int)x->index_hi);
}


//***************************************************************************

//const int PathSum_max_elem

typedef struct PathSum_t {

  TaskSum_t buf[PathSum_max_elem];
  int16_t n_elem; // N.B.: negative if overflow

  //uint64_t cost_us;

} PathSum_t;


#define PathSum_NULL							\
  (PathSum_t){								\
    /*.buf = { TaskSum_NULL   },*/					\
    .n_elem = 0								\
    /*.cost_us = 0*/							\
  }


static inline void
PathSum_init(PathSum_t* x)
{
  *x = PathSum_NULL;
}


static inline void
PathSum_reset(PathSum_t* x)
{
  x->n_elem = 0;
  //x->cost_us = 0;
}


static inline bool
PathSum_isOverflow(const PathSum_t* x)
{
  return (x->n_elem < 0 || PathSum_max_elem == 0);
}


static inline int
PathSum_nElem(const PathSum_t* x)
{
  return (PathSum_isOverflow(x) ? -(x->n_elem) : x->n_elem);
}


static inline TaskSum_t*
PathSum_at(PathSum_t* x, int index)
{
  return &(x->buf[index]);
}


// PathSum_push(): Push element onto range, with auto coalescing.
static inline bool
PathSum_push(PathSum_t* x, int rank, uint16_t idx_new)
{
  if (PathSum_isOverflow(x)) {
    return false;
  }

  // 1. Try coalescing with last range
  if (x->n_elem > 0) {
    TaskSum_t* tsum_last = PathSum_at(x, x->n_elem - 1);
    if (tsum_last->rank == rank) {
      assert(tsum_last->index_hi <= idx_new);

      if (tsum_last->index_hi < idx_new) {
	tsum_last->index_hi = idx_new;
      }
      
      return true;
    }
  }

  // 2. Try adding new range
  if (x->n_elem < PathSum_max_elem) {
    TaskSum_t* tsum_new = PathSum_at(x, x->n_elem);
    tsum_new->rank = rank;
    tsum_new->index_lo = idx_new;
    tsum_new->index_hi = idx_new;
    x->n_elem++;
    
    return true;
  }

  x->n_elem = -(x->n_elem); // flag as overflowing
  return false;
}


static inline bool
PathSum_push2(PathSum_t* x, int rank, uint16_t index_lo, uint16_t index_hi)
{
  if (PathSum_isOverflow(x)) {
    return false;
  }
  
  // 1. Try coalescing with last range
  if (x->n_elem > 0) {
    TaskSum_t* tsum_last = PathSum_at(x, x->n_elem - 1);
    if (tsum_last->rank == rank) {
      assert(tsum_last->index_lo <= index_lo); // assume ordered input

      // Case 1a: Overlap (partial or complete)
      // Case 1b: No overlap (Handle with Case 2 below)

      uint16_t tsum_last_idx_hi_incl = tsum_last->index_hi + 1;
      if (tsum_last->index_lo <= index_lo && index_lo <= tsum_last_idx_hi_incl) {
	// Case 1a
	if (index_hi > tsum_last->index_hi) {
	  //x->cost_us += (index_hi - tsum_last->index_hi);
	  tsum_last->index_hi = index_hi;
	}
	return true;
      }
      // Case 1b: fallthrough to Case 2
    }
  }

  // 2. Try adding new range
  if (x->n_elem < PathSum_max_elem) {
    TaskSum_t* tsum_new = PathSum_at(x, x->n_elem);
    tsum_new->rank = rank;
    tsum_new->index_lo = index_lo;
    tsum_new->index_hi = index_hi;
    x->n_elem++;
    //x->cost_us += (index_hi - index_lo) + 1;
    return true;
  }

  x->n_elem = -(x->n_elem); // flag as overflowing
  return false;
}


static inline void
PathSum_dump(const PathSum_t* psum, bool doElem, const char* ctxt_str, int pathId)
{
  int n_elem = PathSum_nElem(psum);
  AMSG("%s(path %d): PathSum sz %d", ctxt_str, pathId, n_elem); // psum->cost_us
  if (doElem) {
    for (uint i = 0; i < n_elem; ++i) {
      const TaskSum_t* x = PathSum_at((PathSum_t*)psum, i);
      TaskSum_dump(x, "  ", i);
    }
  }
}


static inline void
PathSum_dumpEnd(const PathSum_t* psum, const char* ctxt_str, int pathId)
{
  int n_elem = PathSum_nElem(psum);  
  AMSG("%s(path %d): PathSum sz %d", ctxt_str, pathId, n_elem);
  if (n_elem > 0) {
    int i = n_elem - 1;
    const TaskSum_t* tsum_last = PathSum_at((PathSum_t*)psum, i);
    TaskSum_dump(tsum_last, "  ", i);
  }
}


//***************************************************************************
// Path representation
//***************************************************************************

// Path_t: The 'tail' and 'prev' fields represent trace records that
//   form a linked list using 'prev' pointers
typedef struct {
  //--------------------------------------
  // path statistics
  //--------------------------------------
  double    path_cost_us;  // includes 'tail_cost_us'
  uint64_t  path_smpl_us;
  
  //--------------------------------------  
  // phase statistics
  //--------------------------------------  
  double    phase_cost_us; // includes 'tail_cost_us'
  uint64_t  phase_smpl_us;
  PathSum_t phase_sum;

  //--------------------------------------
  // Only needed for full paths
  //--------------------------------------
  
  // tail: last task in current path
  int         tail_rank;    // normalized "prof rank"
  uint64_t    tail_timeId;  // use Path_makeTimeId()
  uint        tail_cctNodeId;
  //cct_node_t* tail_cctNode;
  uint32_t    tail_cost_us;

  // prev: penultimate task in the list
  int       prev_rank;      // normalized "prof rank"
  uint64_t  prev_timeId;    // use Path_makeTimeId()
  
} Path_t;


#define Path_NULL							\
  (Path_t){								\
    .path_cost_us    = 0.0,						\
    .path_smpl_us    = 0,						\
									\
    .phase_cost_us  = 0.0,						\
    .phase_smpl_us  = 0,						\
    .phase_sum      = PathSum_NULL, 					\
									\
    .tail_rank      = MPI_PROC_NULL,                      		\
    .tail_timeId    = 0,						\
    .tail_cctNodeId = HPCRUN_FMT_CCTNodeId_NULL,			\
    /*.tail_cctNode   = NULL,*/					        \
    .tail_cost_us   = 0,						\
									\
    .prev_rank      = MPI_PROC_NULL,					\
    .prev_timeId    = 0						        \
  }


static inline void
Path_init(Path_t* x)
{
  *x = Path_NULL;
}


TaskPnt_t
Path_newTaskPnt(Path_t* x, TaskPnt_t* taskPnt, int pathId);


TaskPnt_t
Path_addTaskPnt(Path_t* x, double cost_us, TaskPnt_t* taskPnt, int pathId);


static inline void
Path_addCost(Path_t* x, double cost_us)
{
  x->path_cost_us  += cost_us;
  x->phase_cost_us += cost_us;
  x->tail_cost_us  += (uint32_t)cost_us;
}


static inline void
Path_addSmplCost(Path_t* x, uint64_t smpl_us)
{
  x->path_smpl_us  += smpl_us;
  x->phase_smpl_us += smpl_us;
}


static inline uint64_t
Path_makeTimeId(Path_t* x, int rank_me)
{
  uint64_t time_us = 0;
  time_getTimeReal(&time_us);

  uint64_t timeId = time2timeId(time_us);

  // N.B. We can only ensure montonically increasing ids if we only
  // compare against time ids frome the *same* rank.

  if (!x) {
    return timeId;
  }

  bool mayCompareTail = (x->tail_timeId != 0) && (x->tail_rank == rank_me);

  if (mayCompareTail) {
    if (timeId <= x->tail_timeId) {
      timeId = x->tail_timeId + 1; // ensure monotonically increasing id!
    }
  }
  else {
    bool mayComparePrev = (x->prev_timeId != 0) && (x->prev_rank == rank_me);

    if (mayComparePrev) {
      if (timeId <= x->prev_timeId) {
	timeId = x->prev_timeId + 1; // ensure monotonically increasing id!
      }
    }
  }

  return timeId;
}


static inline uint64_t
Path_makeTimeId1(TaskPnt_t* taskPnt)
{
  assert(taskPnt->timeId != 0);
  return taskPnt->timeId + 1; // ensure monotonically increasing id!
}


// Path_noteDiscarded(): Set a "path discard flag" to aid post-mortem
//   path recovery. Protect overall path and phase data.
static inline void
Path_noteDiscarded(Path_t* x)
{
  // Note: path analysis doesn't like 'tail_cctNodeId' to be null.
  x->prev_rank = MPI_PROC_NULL;
  x->prev_timeId = 0;
}


static inline void
Path_phaseReset(Path_t* x)
{
  x->phase_cost_us = 0.0;
  x->phase_smpl_us = 0;
  PathSum_reset(&x->phase_sum);
}


// Path_replace: Set the path of 'me' to be the path of 'other'.
//
// N.B.: Only valid when the tail rank is my own rank. Otherwise,
//   there must be a Path_addTaskPnt() before a 'commit'.
static inline void
Path_replace(Path_t* me, const Path_t* other)
{
  *me = *other; // copy Path_t struct
}


// Path_splice: Set the tail's *subpath* to point to 'othr',
//   making the tail's task a placeholder (e.g., with 0 cost).
//
// N.B.: May be immediately followed by "commit"
static inline void
Path_splice(Path_t* me, const Path_t* other)
{
  me->path_cost_us  = other->path_cost_us;
  me->path_smpl_us  = other->path_smpl_us;

  me->phase_cost_us = other->phase_cost_us;
  me->phase_smpl_us = other->phase_smpl_us;
  me->phase_sum     = other->phase_sum; // copy PathSum_t struct

  me->tail_cost_us  = 0; // tail is now a placeholder with 0 cost
  
  me->prev_rank   = other->tail_rank;
  me->prev_timeId = other->tail_timeId;
}


// Path_compare: return 'true' if 'other' is better
static inline bool
Path_compare(const Path_t* me, const Path_t* other, PathTy_t ty)
{
  switch (ty) {
  case PathTy_most:
    return other->path_cost_us > me->path_cost_us;
  case PathTy_least:
    return other->path_cost_us < me->path_cost_us;
  default: assert(false);
  }
}


static inline void
Path_dump(const Path_t* x, bool doXtra, const char* ctxt_str, int pathId)
{
  AMSG("%s(path %d): cost us/smpl(unscaled) %g/%ld; tail %d:%ld (cct %u, cost %u); prev %d:%ld",
       ctxt_str, pathId,
       x->path_cost_us, x->path_smpl_us,
       x->tail_rank, x->tail_timeId, x->tail_cctNodeId, x->tail_cost_us,
       x->prev_rank, x->prev_timeId);
  if (doXtra) {
    PathSum_dump(&x->phase_sum, /*doElem*/false, ctxt_str, pathId);
  }
}


//***************************************************************************
// PathSet_t, PathSetAux_t
//***************************************************************************

// PathSet_t: path data (that will be exchanged during joins)
typedef struct PathSet_t {

  // descending order: path [0] is most critical
  Path_t paths[PathSet_n_elem];
  int16_t n_elem;

} PathSet_t;


#define PathSet_NULL							\
  (PathSet_t){								\
    .paths   = { Path_NULL },						\
    .n_elem = PathSet_n_elem						\
  }


/*static const PathSet_t PathSet_NULL*/

static inline void
PathSet_init(PathSet_t* x)
{
  *x = PathSet_NULL;
}


static inline void
PathSet_dump(const PathSet_t* set, bool doXtra, const char* ctxt_str)
{
  AMSG("%s: PathSet (sz %d)", ctxt_str, (int)set->n_elem);
  for (int i = 0; i < set->n_elem; ++i) {
    const Path_t* x = &(set->paths[i]);
    Path_dump(x, doXtra, ctxt_str, i);
  }
}


//***************************************************************************

// PathSetAux_t: data that is static/common for all PathSet_t instances
typedef struct PathSetAux_t {

  //--------------------------------------
  // path join/select rules
  //--------------------------------------
  JoinRuleTy_t joinRuleTy;
  
  int selectIdxV[PathSet_n_elem];
  int percentileV[PathSet_n_elem];
  int metricIdV[PathSet_n_elem];
  int n_elem;

  //--------------------------------------
  // path stats (available on all ranks after phase end)
  //--------------------------------------
  int pathSumMaxSz[PathSet_n_elem];

  //--------------------------------------
  // rank stats (local to a rank)
  //--------------------------------------
  //double path_cost_me_us[PathSet_n_elem]; // path cost on my rank (cf. CCT)
  double task_me_us; // cost of tasks on my rank
  double mpiOp_me_us;
  double mpiOp_prev_us;

  uint64_t taskBeg_tsc;
 
  //--------------------------------------
  // task control state
  //--------------------------------------
  bool requireNewTask;
  bool didCoalesceTasks;

} PathSetAux_t;


#define PathSetAux_NULL							\
  (PathSetAux_t){							\
    .joinRuleTy  = JoinRuleTy_NULL,					\
									\
    .selectIdxV  = { -1 },						\
    .percentileV = { -1 },						\
    .metricIdV   = { -1 },						\
    .n_elem      = PathSet_n_elem,					\
									\
    .pathSumMaxSz = { 0 },						\
									\
    /*.path_cost_me_us = { 0.0 },*/					\
    .task_me_us = 0.0,							\
    .mpiOp_me_us = 0.0,							\
    .mpiOp_prev_us = 0.0,						\
    .taskBeg_tsc = 0,						        \
       									\
    .requireNewTask = true,						\
    .didCoalesceTasks = false					        \
  }


/*static const PathSetAux_t PathSetAux_NULL*/

static inline void
PathSetAux_init(PathSetAux_t* x, JoinRuleTy_t rule)
{
  *x = PathSetAux_NULL;

  //----------------------------------------------------------
  // INVARIANTs for join/selection rule given n paths and two PathSet in a
  //   buffer of length 2n.
  // - begin with with path 0 (most critical)
  // - end with path 2n -1 (least critical) when n >= 2
  //----------------------------------------------------------

  assert(x->n_elem >= 1);

  x->joinRuleTy = rule;

  const int i_beg = 0;
  const int i_end = x->n_elem - 1;

  const int select_i_beg = 0;                 // most critical path (0)
  const int select_i_end = 2 * x->n_elem - 1; // least critical (2n - 1)

  if (x->n_elem >= 1) {
    x->selectIdxV[i_beg] = select_i_beg;
    x->percentileV[i_beg] = 100;
  }
  
  if (x->n_elem >= 2) {
    x->selectIdxV[i_end] = select_i_end;
    x->percentileV[i_end] = 0;
  }

  if (x->n_elem >= 3) {
    const double n_2 = (double)x->n_elem / 2.0;

    for (int i = i_beg + 1; i <= i_end - 1; ++i) {
      int select_i = 0;
      double name_i = 0.0;
      
      if ((double)i < n_2) {
	switch (rule) {
	case JoinRuleTy_uni_uni: // uniform
	case JoinRuleTy_uni_lo:
	  select_i = x->selectIdxV[i] = 2 * i;
	  name_i = (select_i_end - select_i) * 100.0 / (double)select_i_end;
	  break;
	case JoinRuleTy_hi_uni: // top "k" paths
	case JoinRuleTy_hi_lo:
	  select_i = i;
	  name_i = (double)(100 - i);
	  break;
	default:
	  assert(false);
	}
      }
      else {
	switch (rule) {
	case JoinRuleTy_uni_uni: // uniform
	case JoinRuleTy_hi_uni:
	  select_i = (2 * i) + 1;
	  name_i = (select_i_end - select_i) * 100.0 / (double)select_i_end;
	  break;
	case JoinRuleTy_uni_lo: // bottom "k" paths
	case JoinRuleTy_hi_lo:
	  select_i = select_i_end - (i_end - i);
	  name_i = (double)(i_end - i);
	  break;
	default:
	  assert(false);
	}
      }
      
      assert(select_i_beg <= select_i && select_i <= select_i_end);
      
      x->selectIdxV[i] = select_i;
      x->percentileV[i] = (int)name_i;
    }
  }
}


static inline void
PathSetAux_phaseStat(PathSetAux_t* x, const Path_t* path, int pathId)
{
  const PathSum_t* pathsum = &path->phase_sum;
  int pathsum_nelem = PathSum_nElem(pathsum);
  if (pathsum_nelem > x->pathSumMaxSz[pathId]) {
    x->pathSumMaxSz[pathId] = pathsum_nelem;
  }
}


static inline void
PathSetAux_dump(PathSetAux_t* x, const char* ctxt_str)
{
  AMSG("%s: PathSetAux (sz %d)", ctxt_str, x->n_elem);
  for (uint i = 0; i < x->n_elem; ++i) {
    AMSG("%s(path-aux %d): percentile %d, selectIdx %d", ctxt_str, i, x->percentileV[i], x->selectIdxV[i]);
    //AMSG("%s(path-aux %d): path-cost-me %g", ctxt_str, i, x->path_cost_me_us[i]);
    AMSG("%s(path-aux %d): PathSum-max %d", ctxt_str, i, x->pathSumMaxSz[i]);
  }
}


//***************************************************************************
// PathJoinSum_t
//***************************************************************************

// PathJoinSum_t: Summary of a Path_t
typedef struct {
  const Path_t* path;
  double path_cost_us;
  
} PathJoinSum_t;


#define PathJoinSum_NULL						\
  (PathJoinSum_t){							\
    .path         = NULL,						\
    .path_cost_us = 0.0 						\
  }


static inline void
PathJoinSum_set(PathJoinSum_t* x, const Path_t* path)
{
  x->path         = path;
  x->path_cost_us = path->path_cost_us;
}


//***************************************************************************
// PathBuf_t, PathBufElem_t: A sequence of SyncTasks/ModelTasks
//***************************************************************************

typedef struct PathBufElem_t {

  hpctrace_fmt_datum_t datum;

} PathBufElem_t;


static inline TaskPntTy_t
PathBufElem_type(const PathBufElem_t* x)
{
  return ((x->datum.metricId == TaskPntTy_model_flag)
	  ? TaskPntTy_model : TaskPntTy_sync);
}


typedef struct PathBuf_t {

  PathBufElem_t* buf;
  uint n_elem;
  uint max_elem;

} PathBuf_t;


#define PathBuf_NULL							\
  (PathBuf_t){							        \
    .buf = NULL,							\
    .n_elem = 0,							\
    .max_elem = 0							\
  }


static inline void
PathBuf_reset(PathBuf_t* x)
{
  x->n_elem = 0;
}


static inline bool
PathBuf_isFull(const PathBuf_t* x)
{
  return (x->n_elem >= x->max_elem);
}


static inline PathBufElem_t*
PathBuf_at(const PathBuf_t* x, int index)
{
  return &(x->buf[index]);
}


static inline int
PathBuf_push(PathBuf_t* x, const hpctrace_fmt_datum_t* datum)
{
  // Add new element (assume this is valid)
  int elem_i = x->n_elem;
  PathBufElem_t* elem_new = PathBuf_at(x, elem_i);

  elem_new->datum = *datum;
  
  x->n_elem++;

  return elem_i;
}


//***************************************************************************
// SampleBuf_t, SampleBufElem_t: A collection of samples (could
//   be a sequence or histogram)
//***************************************************************************

typedef struct SampleBufElem_t {

  //uint64_t timeId;
  cct_node_t* cct_node;
  uint64_t metric_incr;

} SampleBufElem_t;


#define /*static const int*/ SampleBuf_loadV_sz 20

typedef struct SampleBuf_t {

  SampleBufElem_t* buf;
  uint n_elem;
  uint max_elem;

  // load w.r.t. number-of-element thresholds
  int load_i;
  uint loadV[SampleBuf_loadV_sz];

} SampleBuf_t;


#define SampleBuf_NULL							\
  (SampleBuf_t){							\
    .buf = NULL,							\
    .n_elem = 0,							\
    .max_elem = 0,							\
									\
    .load_i = 0,							\
    .loadV = { 0 }							\
  }


static inline void
SampleBuf_init(SampleBuf_t* x)
{
  *x = SampleBuf_NULL;
}


static inline void
SampleBuf_initBuf(SampleBuf_t *x, SampleBufElem_t* buf, uint max_elem,
		  long* sample_scaling_factor)
{
  x->buf = buf;
  x->max_elem = max_elem;

  const double pct_beg = 75.0;
  const double pct_delta = (100.0 - pct_beg) / SampleBuf_loadV_sz;

  double frac_cur = pct_beg / 100.0;
  const double frac_delta = pct_delta / 100.0;

  for (int i = 0; i < SampleBuf_loadV_sz; ++i) {
    x->loadV[i] = (uint)((double)(x->max_elem) * frac_cur);
    frac_cur += frac_delta;
  }

  if (sample_scaling_factor) {
    *sample_scaling_factor = 1;
  }
}


static inline void
SampleBuf_reset(SampleBuf_t* x, long* sample_scaling_factor)
{
  x->n_elem = 0;
  x->load_i = 0;
  if (sample_scaling_factor) {
    *sample_scaling_factor = 1;
  }
}


static inline bool
SampleBuf_isFull(const SampleBuf_t* x)
{
  return (x->n_elem >= x->max_elem);
}


static inline bool
SampleBuf_updateLoad(SampleBuf_t* x, long* sample_scaling_factor)
{
  bool didChange = false;
  
  while (x->load_i < SampleBuf_loadV_sz && !(x->n_elem < x->loadV[x->load_i]) ) {
    x->load_i++;
    if (sample_scaling_factor) {
      *sample_scaling_factor *= 2;
    }
    didChange = true;
  }

  return didChange;
}


static inline SampleBufElem_t*
SampleBuf_at(const SampleBuf_t* x, int index)
{
  return &(x->buf[index]);
}


static inline int
SampleBuf_push(SampleBuf_t* x, uint64_t timeId, cct_node_t* cct_node,
	       uint64_t metric_incr)
{
  // Add new element (assume this is valid)
  int elem_i = x->n_elem;
  SampleBufElem_t* elem_new = SampleBuf_at(x, elem_i);
 
  //elem_new->timeId     = timeId;
  elem_new->cct_node     = cct_node;
  elem_new->metric_incr  = metric_incr;

  x->n_elem++;

  return elem_i;
}


//***************************************************************************

#endif // _HPCRUN_MPI_OVERRIDES_PATHS_H_
