// -*-Mode: C++;-*- // technically C99

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// --------------------------------------------------------------------------
// Part of HPCToolkit (hpctoolkit.org)
//
// Information about sources of support for research and development of
// HPCToolkit is at 'hpctoolkit.org' and in 'README.Acknowledgments'.
// --------------------------------------------------------------------------
//
// Copyright ((c)) 2002-2016, Rice University
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
// * Neither the name of Rice University (RICE) nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// This software is provided by RICE and contributors "as is" and any
// express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular
// purpose are disclaimed. In no event shall RICE or contributors be
// liable for any direct, indirect, incidental, special, exemplary, or
// consequential damages (including, but not limited to, procurement of
// substitute goods or services; loss of use, data, or profits; or
// business interruption) however caused and on any theory of liability,
// whether in contract, strict liability, or tort (including negligence
// or otherwise) arising in any way out of the use of this software, even
// if advised of the possibility of such damage.
//
// ******************************************************* EndRiceCopyright *

//***************************************************************************
//
// File:
// $HeadURL$
//
// Purpose:
// "MPI Blame Shifting" sampling source.
//
// Authors:
// Nathan Tallent and Abhinav Vishnu
//
//***************************************************************************


//***************************************************************************
// system includes
//***************************************************************************

#include <sys/types.h>
#include <stdio.h>
#include <ucontext.h>
#include <unistd.h>
#include <limits.h>
#include <assert.h>

//***************************************************************************
// MPI
//***************************************************************************

#include <mpi.h>

//***************************************************************************
// libmonitor include files
//***************************************************************************

//#include <monitor.h>

//***************************************************************************
// local includes
//***************************************************************************

#define My_DoBlameShifting 1

#include <include/min-max.h>

#include <main.h>
#include <metrics.h>
#include <safe-sampling.h>
#include <sample_event.h>
#include <thread_data.h>
#include <trace.h>

#include <messages/messages.h>
#include <monitor-exts/monitor_ext.h>

#include <sample-sources/mpi-util.h>
#include <sample-sources/mpi-decl.h>
#include <sample-sources/mpi-barrier-blame.h>
#include <sample-sources/mpi-overrides-blame.h>
#include <sample-sources/mpi-overrides.h>
#include <sample-sources/mpi-blame.h>


#define MyDbg 0
#define My_UseBarrier 1
#define My_PruneReqMap 0


//***************************************************************************
// 
//***************************************************************************

//-------------------------------------------------------------
// profiling lib state
//-------------------------------------------------------------

static bool s_prof_is_initialized = false;
static bool s_prof_is_on = false;

//-------------------------------------------------------------
// global communicator and rank
//-------------------------------------------------------------

static MPI_Comm  s_world_comm = MPI_COMM_WORLD;
static MPI_Group s_world_group = MPI_GROUP_NULL;

static const int s_rank_root = 0;
static int       s_rank_me = MPI_PROC_NULL;
static int       s_rank_sz = 0;

//-------------------------------------------------------------
// profiling command server
//-------------------------------------------------------------

static MPI_Comm  s_prof_comm = MPI_COMM_NULL;
static MPI_Group s_prof_group = MPI_GROUP_NULL;
/*static*/ int   s_prof_tag = MPI_UNDEFINED;

static const int s_prof_cmdSz = sizeof(ProfCmd_t);

static const int s_prof_mpiBufSz = (1 * 1024 * sizeof(ProfCmd_t));
static void*     s_prof_mpiBuf = NULL;

static int s_prof_rcvBufCnt = (2 * 1024); // ProfCmdRequest_t;
static CircularBuf_t s_prof_rcvBuf;

static ProfCmd_t s_prof_rcvCmd;
static bool s_prof_is_rcv_on;

//-------------------------------------------------------------
// 
//-------------------------------------------------------------

static const int s_mpireq_memSz = (2 * 1024 * sizeof(ProfRequest_t));
static Mem_t s_mpireq_mem;

static ProfRequestMap_t s_mpireq_pend; // pending requests
static ProfRequestMap_t s_mpireq_done; // completed requests, for blame

//-------------------------------------------------------------

const int s_barrier_n_stat = 2;

static MPI_Datatype s_barrier_stat_subbuf_ty;


//***************************************************************************
// 
//***************************************************************************

static inline int
xlateRank_app2prof(int app_rank, MPI_Comm app_comm)
{
  // FIXME: cache results
  return xlateRank_app2prof_1(app_rank, app_comm,
			      s_prof_is_on, s_world_comm, s_prof_group);
}


static inline ProfRequest_t*
ProfRequest_malloc()
{
  return (ProfRequest_t*)Mem_malloc(&s_mpireq_mem);
}


static inline void
ProfRequest_free(ProfRequest_t* x)
{
  Mem_free(&s_mpireq_mem, (MemObj_t*)x);
}


static inline void
ProfRequestMap_freeByComm(ProfRequestMap_t* map, MPI_Comm app_comm)
{
  for (ProfRequest_t* x_it = map->q_head; (x_it); /* */) {
    ProfRequest_t* x = x_it;
    x_it = x_it->next; // advance iteration since we may delete 'x'

    if (x->app_comm == app_comm) {
      ProfRequestMap_delete(map, x, "ProfRequestMap_freeByComm");
      ProfRequest_free(x);
    }
  }
}


//***************************************************************************
// blame shifting server
//***************************************************************************

static inline void
server_postRecvs();


// server_sendCmd(): send one command
static inline void
server_sendCmd(ProfCmd_t* cmd, int dest)
{
  if (!s_prof_is_on) {
    return;
  }

  if (dest == MPI_PROC_NULL) {
    TMSG(MPIbs, "server_sendCmd: sending blame to bogus rank!");
    return;
  }

  int rc;
  int tag = s_rank_me;

  // N.B.: The server send must meet the following requirements:
  //   - Cannot use 'send': if send blocks, we can have deadlock
  //   - Cannot use 'isend': cannot deallocate buffer until after 'wait'
  //   - Cannot use 'ibsend': must call 'wait' to deallocate request objects
  //
  // N.B.: rely on MPI to optimize in case of (prof_rank == s_rank_me)
  
  rc = real_PMPI_Bsend(cmd, s_prof_cmdSz, MPI_BYTE, dest, tag,
		       s_prof_comm);

  if (rc == MPI_ERR_BUFFER) {
    // FIXME: increase buffer size
  }

  assert(rc == MPI_SUCCESS);
}


static inline bool
server_hasCmd(MPI_Status* status)
{
  if (!s_prof_is_on) {
    return false;
  }

  int rc;
  if (!CircularBuf_isEmpty(&s_prof_rcvBuf)) {
    ProfCmdRequest_t* req = (ProfCmdRequest_t*)CircularBuf_top(&s_prof_rcvBuf);

    int isDone = 0;
    rc = real_PMPI_Test(&(req->request), &isDone, status);
    assert(rc == MPI_SUCCESS);

    return (isDone);
  }

  return false;
}


// server_getCmd(): receive one command
static inline void
server_getCmd(MPI_Status* status, ProfCmd_t* cmd)
{
  if (!s_prof_is_on) {
    return;
  }

  // N.B.: Since 'MPI_Test' has already returned 'true', the MPI
  //   completion object may be inactive/deallocated.  As a result, it
  //   is not safe to call MPI_Test again.
  // assert(server_hasCmd());

  ProfCmdRequest_t* req = (ProfCmdRequest_t*)CircularBuf_pop(&s_prof_rcvBuf);
  *cmd = (req->cmd);

  cmd->prof_rank = status->MPI_SOURCE; // rank that initiated the operation
  assert(cmd->prof_rank != MPI_ANY_SOURCE);

  server_postRecvs();
}


static inline void
server_postRecvs()
{
  if (!s_prof_is_on) {
    return;
  }

  int rc;
  while (!CircularBuf_isFull(&s_prof_rcvBuf)) {
    ProfCmdRequest_t* req = (ProfCmdRequest_t*)CircularBuf_push(&s_prof_rcvBuf);
    *req = ProfCmdRequest_NULL;

    rc = real_PMPI_Irecv(&(req->cmd), s_prof_cmdSz, MPI_BYTE, MPI_ANY_SOURCE,
			 MPI_ANY_TAG, s_prof_comm, &(req->request));
    assert(rc == MPI_SUCCESS);
  }
}


static inline void
server_cleanup()
{
  if (!s_prof_is_on) {
    return;
  }

  int rc;
  while (!CircularBuf_isEmpty(&s_prof_rcvBuf)) {
    ProfCmdRequest_t* req = (ProfCmdRequest_t*)CircularBuf_pop(&s_prof_rcvBuf);

    int isDone = 0;
    rc = real_PMPI_Test(&(req->request), &isDone, MPI_STATUS_IGNORE);
    assert(rc == MPI_SUCCESS);
    
    if (!isDone) {
      real_PMPI_Cancel(&(req->request));
      real_PMPI_Request_free(&(req->request));
    }
  }
}


//***************************************************************************
// blame shifting
//***************************************************************************

#define defs_point2point()						\
  def_isSampled();							\
  def_time(timeBeg, isSampled);


#define defs_barrier()							\
  def_isSampled();							\
									\
  const int n_stat_buf = barrier_n_stat_buf(s_barrier_n_stat);		\
  barrier_stat_t stat_buf[n_stat_buf];				\
  barrier_stat_buf_init(stat_buf,  n_stat_buf, SortTy_9_0);		\
  									\
  def_time(timeBeg, isSampled);


#define defs_collective()						\
  def_isSampled();							\
  def_time(timeBeg, isSampled);						\


#define def_isSampled()							\
  bool isSampled = false;						\
  thread_data_t* threadData = hpcrun_get_thread_data();			\
  {									\
    lushPthr_t* xxx = &threadData->pthr_metrics;			\
    xxx->doIdlenessCnt++;						\
    if (hpcrun_mpi_bs_period > 0					\
	&& xxx->doIdlenessCnt >= hpcrun_mpi_bs_period) {		\
      xxx->doIdlenessCnt = 0;						\
      isSampled = true;							\
    }									\
  }


#define def_time(_name, isSampled)					\
  uint64_t _name = 0;      						\
  if (isSampled) {							\
    _name = time_getTSC(); /* cycles */					\
  }


//***************************************************************************

#define doMetric(metricVec, /*int*/metricId, metricIncr, type)		\
  doMetricAbs(metricVec, metricId, metricIncr * hpcrun_mpi_bs_period, type)

static inline cct_node_t*
doSample_backtrace();

static inline void
doSample_metricsMe(cct_node_t* cct_node, int num_op,
		   double e_time, double xtra_time);

static inline void
doSample_blameRecv(cct_node_t* cct_node_bkup, bool mayPause);

static inline void
doSample_blameRecv_metrics(ProfCmd_t* cmd, ProfRequest_t* myreq,
			   cct_node_t* cct_node_bkup);

static inline void
doSample_blameSend(double e_time, OpTy_t op_ty,
		   MPI_Comm app_comm, int prof_rank, int tag);

static inline void
doSample_makeReq(MPI_Request *request,
		 OpTy_t op_ty, MPI_Comm app_comm, int prof_rank, int tag,
		 MPI_Datatype datatype, int count, cct_node_t* cct_node);

//***************************************************************************

static inline void
doSample_p2p_blk(bool isSampled, uint64_t timeBeg, OpTy_t op_ty,
		 int count, MPI_Datatype datatype, int app_rank, int tag,
		 MPI_Comm app_comm)
{
  if (isSampled) {
    double e_time = timeElapsed_us(timeBeg);

    TMSG(MPIbs, "doSample_p2p_blk(op %s, comm %d, rank %d)",
	 OpTy_str(op_ty), app_comm, app_rank);

    uint64_t nbytes = xlateSz_count2bytes(count, datatype);
    bool is_blked = was_blocked(e_time, nbytes);

    //----------------------------------------------------------
    // local metrics
    //----------------------------------------------------------
    cct_node_t* cct_node = doSample_backtrace();
    doSample_metricsMe(cct_node, 1, e_time, 0.0);

    //----------------------------------------------------------
    // (a) manage blame and (b) make 'ProfRequest'
    //----------------------------------------------------------
    doSample_blameRecv(cct_node, /*mayPause*/true);

    int prof_rank = xlateRank_app2prof(app_rank, app_comm);
    if (is_blked) {
      doSample_blameSend(e_time, op_ty, app_comm, prof_rank, tag);
    }

    doSample_makeReq(NULL, op_ty, app_comm, prof_rank, tag,
		     datatype, count, cct_node);
  }
}


// For non-blocking calls:
// 1. Operation should not block; may return before msg delivery completes
// 2. Since Irecv may return before delivery completes, 'app_rank' may
//    not be resolved. (MPI_Request_get_status() may not have 'source'.)
static inline void
doSample_p2p_nb(bool isSampled, uint64_t timeBeg, OpTy_t op_ty,
		int count, MPI_Datatype datatype, int app_rank, int tag,
		MPI_Comm app_comm, MPI_Request* request)
{
  if (isSampled) {
    double e_time = timeElapsed_us(timeBeg);

    TMSG(MPIbs, "doSample_p2p_nb(req %p, op %s, comm %d, rank %d)",
	 request, OpTy_str(op_ty), app_comm, app_rank);
    
    uint64_t nbytes = xlateSz_count2bytes(count, datatype);
    bool is_blked = was_blocked(e_time, nbytes);

    //----------------------------------------------------------
    // local metrics
    //----------------------------------------------------------
    double xtra_time = (is_blked) ? e_time : 0.0; // FIXME
    cct_node_t* cct_node = doSample_backtrace();
    doSample_metricsMe(cct_node, 1, e_time, xtra_time);

    //----------------------------------------------------------
    // make 'ProfRequest'
    //----------------------------------------------------------
    int prof_rank = xlateRank_app2prof(app_rank, app_comm);
    doSample_makeReq(request, op_ty, app_comm, prof_rank, tag,
		     datatype, count, cct_node);
  }
}


static inline cct_node_t*
doSample_wait(bool isSampled, uint64_t timeBeg, double timeOp,
	      cct_node_t* cct_node_prev, MPI_Request* request, int app_src)
{
  cct_node_t* cct_node = cct_node_prev;
  
  if (isSampled) {
    double e_time = (timeOp > 0.0) ? timeOp : timeElapsed_us(timeBeg);

    TMSG(MPIbs, "doSample_wait(req %p, etime %g)", request, e_time);
  
    //----------------------------------------------------------
    // find 'ProfRequest'
    //----------------------------------------------------------

    OpTy_t op_ty = OpTy_NULL;
    MPI_Comm app_comm = MPI_COMM_NULL;
    int prof_rank = MPI_PROC_NULL;
    int tag = MPI_UNDEFINED;
    MPI_Datatype datatype = MPI_DATATYPE_NULL;
    int count = 0;

    ProfRequest_t* myreq = ProfRequestMap_findByReq(&s_mpireq_pend, request);

    if (myreq) {
      op_ty = myreq->op_ty;
      app_comm = myreq->app_comm;
      prof_rank = myreq->prof_rank;
      if (op_ty == OpTy_recv && prof_rank == MPI_ANY_SOURCE) {
	prof_rank = xlateRank_app2prof(app_src, app_comm);
      }
      tag = myreq->tag;
      datatype = myreq->datatype;
      count = myreq->count;
    }
    else {
      ProfRequest_dump1(request, "doSample_wait: cannot find");
      assert(false); // FIXME: testing
    }

    uint64_t nbytes = xlateSz_count2bytes(count, datatype);
    bool is_blked = was_blocked(e_time, nbytes);

    //----------------------------------------------------------
    // local metrics
    //----------------------------------------------------------
    if (cct_node) {
      doSample_metricsMe(cct_node, 0, e_time, 0.0);
    }
    else {
      cct_node = doSample_backtrace();
      doSample_metricsMe(cct_node, 1, e_time, 0.0);
    }

    //----------------------------------------------------------
    // (a) manage blame and (b) manage 'ProfRequest'
    //----------------------------------------------------------
    doSample_blameRecv(cct_node, /*mayPause*/true); // affects 's_mpireq_done'

    if (myreq) {
      if (is_blked) {
	doSample_blameSend(e_time, op_ty, app_comm, prof_rank, tag);
      }

      ProfRequestMap_delete(&s_mpireq_pend, myreq, "doSample_wait");
      ProfRequestMap_insert(&s_mpireq_done, myreq);
    }
  }

  return cct_node;
}


#if 0 // FIXME
static inline void
doSample_sndrcv(bool isSampled, uint64_t timeBeg, uint64_t time1,
		uint64_t time2, uint64_t time3,	bool is_rcv_first,
		int recvcount, MPI_Datatype recvtype, int recvtag, int app_src,
		int sendcount, MPI_Datatype sendtype, int sendtag, int app_dest,
		MPI_Comm app_comm)
{
  // FIXME
  if (isSampled) {
    double e_time = timeDiff_us(timeBeg, time3);

    double recvtime = timeDiff_us(time1, time2);
    double sendtime = timeDiff_us(time2, time3);

    if (!is_rcv_first) {
      double tmp = recvtime;
      recvtime = sendtime;
      sendtime = tmp;
    }

    uint64_t recvbytes = xlateSz_count2bytes(recvcount, recvtype);
    uint64_t sendbytes = xlateSz_count2bytes(sendcount, sendtype);

    int rc;

    rc = server_trysend_blame(recvtime, recvbytes, OpTy_send,
			      app_comm, app_src, recvtag);
    assert(rc == 0);

    rc = server_trysend_blame(sendtime, sendbytes, OpTy_recv,
			      app_comm, app_dest, sendtag);
    assert(rc == 0);

    double t_blame = 0.0; OpTy_t blame_ty = OpTy_NULL;
    server_tryrecv_blame(&t_blame, &blame_ty);

    doSample(e_time, recvbytes + sendbytes, t_blame, t_blame, blame_ty);
  }
}
#endif


static inline void
doSample_simple(bool isSampled, uint64_t timeBeg)
{
  if (isSampled) {
    cct_node_t* cct_node = doSample_backtrace();
    double e_time = timeElapsed_us(timeBeg);
    doSample_metricsMe(cct_node, e_time, 1, 0.0);
    doSample_blameRecv(cct_node, /*mayPause*/true);
  }
}


static inline void
doSample_collective(bool isSampled, uint64_t timeBeg, MPI_Comm app_comm)
{
  if (isSampled) {
    cct_node_t* cct_node = doSample_backtrace();
    double e_time = timeElapsed_us(timeBeg);
    doSample_metricsMe(cct_node, e_time, 1, 0.0);
    doSample_blameRecv(cct_node, /*mayPause*/true);
    //ProfRequestMap_freeByComm(&s_mpireq_pend, app_comm); // FIXME
    //ProfRequestMap_freeByComm(&s_mpireq_done, app_comm); // FIXME
  }
}


static inline cct_node_t*
doSample_backtrace()
{
  cct_node_t* sample_node = NULL;
  if (hpcrun_safe_enter()) {
    ucontext_t ctxt;
    getcontext(&ctxt);

    // N.B.: when tracing, this call generates a trace record
    sample_val_t smplVal =
      hpcrun_sample_callpath(&ctxt, HPCRUN_FMT_MetricId_NULL, 0/*metricIncr*/,
			     1/*skipInner*/, 1/*isSync*/);

    sample_node = smplVal.sample_node;
    
    hpcrun_safe_exit();
  }

  return sample_node;
}


static inline void
doSample_metricsMe(cct_node_t* cct_node, int num_op,
		   double e_time, double xtra_time)
{
  metric_set_t* metricVec = hpcrun_get_metric_set(cct_node);
    
  doMetric(metricVec, hpcrun_mpi_bs_mtrcId_op, num_op, i);

  doMetric(metricVec, hpcrun_mpi_bs_mtrcId_tOp, e_time, r);

  if (xtra_time > 0.0) {
    doMetric(metricVec, hpcrun_mpi_bs_mtrcId_tBlame, xtra_time, r);
  }
}


static inline void
doSample_blameRecv(cct_node_t* cct_node_bkup, bool mayPause)
{
  ProfCmd_t* cmd = &s_prof_rcvCmd;
  ProfRequest_t* myreq = NULL;

  bool forceRecv = (!mayPause);

  //----------------------------------------------------------
  // Attempt to clear server, if paused
  //----------------------------------------------------------
  if (!s_prof_is_rcv_on) {
    myreq = ProfRequestMap_match(&s_mpireq_done, cmd->op_ty, cmd->app_comm,
				 cmd->prof_rank, cmd->tag);

    if (myreq || forceRecv) {
      s_prof_is_rcv_on = true;
      doSample_blameRecv_metrics(cmd, myreq, cct_node_bkup);
    }
    else {
      //EMSG("blameRecv-remain-paused (req-sz %d)", s_mpireq_done.size);
      ProfCmd_dump(cmd, "doSample_blameRecv: remain paused");
      return; // early exit
    }
  }

  //----------------------------------------------------------
  // Engage server
  //----------------------------------------------------------

  MPI_Status status;

  while (s_prof_is_rcv_on && server_hasCmd(&status)) {

    //----------------------------------------------------------
    // Receive blame information from server
    //----------------------------------------------------------
    server_getCmd(&status, cmd);

    // N.B.: 'cmd->prof_rank' is rank that initiated operation

    //----------------------------------------------------------
    // Find matching operation
    //----------------------------------------------------------
    myreq = ProfRequestMap_match(&s_mpireq_done, cmd->op_ty, cmd->app_comm,
				  cmd->prof_rank, cmd->tag);
    if (mayPause && !myreq) {
      s_prof_is_rcv_on = false;

      //EMSG("blameRecv-pause (req-sz %d)", s_mpireq_done.size);
      ProfCmd_dump(cmd, "doSample_blameRecv: pause");
      return; // early exit
    }

    //----------------------------------------------------------
    // Record metrics
    //----------------------------------------------------------
    doSample_blameRecv_metrics(cmd, myreq, cct_node_bkup);
  }
}


static inline void
doSample_blameRecv_metrics(ProfCmd_t* cmd, ProfRequest_t* myreq,
			   cct_node_t* cct_node_bkup)
{
  // FIXME: cleanup / may not need 'cct_node_bkup'
  
  OpTy_t op_ty = OpTy_NULL;
  cct_node_t* cct_node = cct_node_bkup;

  if (myreq) {
    op_ty = myreq->op_ty;
    cct_node = myreq->cct_node;

#if (My_PruneReqMap)
    ProfRequestMap_delete(&s_mpireq_done, myreq, "doSample_blameRecv");
    ProfRequest_free(myreq);
#endif
  }
  else {
    EMSG("doSample_blameRecv: misattribution (req-sz %d)", s_mpireq_done.size);
    switch (cmd->op_ty) {
    case OpTy_send: op_ty = OpTy_recv; break;
    case OpTy_recv: op_ty = OpTy_send; break;
    default: assert(false); break;
    }
  }

  if (!cct_node) {
    //TMSG(MPIbs, "doSample_blameRecv: skip recording");
    return;
  }

  metric_set_t* metricVec = hpcrun_get_metric_set(cct_node);

  switch (op_ty) {
  case OpTy_NULL:
    break;
  case OpTy_send:
    doMetric(metricVec, hpcrun_mpi_bs_mtrcId_tBlameSnd, cmd->t_blame, r);
    break;
  case OpTy_recv:
    doMetric(metricVec, hpcrun_mpi_bs_mtrcId_tBlameRcv, cmd->t_blame, r);
    break;
  default: assert(false);
  }
  
  doMetric(metricVec, hpcrun_mpi_bs_mtrcId_tBlame, cmd->t_blame, r);
}


static inline void
doSample_blameSend(double e_time, OpTy_t op_ty,
		   MPI_Comm app_comm, int prof_rank, int tag)
{
  assert(prof_rank >= 0);

  ProfCmd_t cmd;
  cmd           = ProfCmd_NULL;
  cmd.t_blame   = e_time;
  cmd.op_ty     = op_ty;
  cmd.app_comm  = app_comm;
  cmd.prof_rank = prof_rank;
  cmd.tag       = tag;
  
  server_sendCmd(&cmd, prof_rank);
}


static inline void
doSample_makeReq(MPI_Request *request,
		 OpTy_t op_ty, MPI_Comm app_comm, int prof_rank, int tag,
		 MPI_Datatype datatype, int count, cct_node_t* cct_node)
{
  bool is_pending = (request != NULL);
  
  //----------------------------------------------------------
  // 1. allocate a request, checking for duplicates
  //----------------------------------------------------------
  ProfRequest_t *rq1 = NULL, *rq2 = NULL;

  // 1a. is_pending => not on 's_mpireq_pend' (if 'wait' [not 'test'] was used)
#if 0 // FIXME
  if (is_pending) {
    rq1 = ProfRequestMap_findByReq(&s_mpireq_pend, request);
    if (rq1) { }
  }
#endif

  // 1b. (is_pending || !is_pending) => could be on 's_mpireq_done'
  if (!rq1) {
    rq2 = ProfRequestMap_findByOp(&s_mpireq_done,
				  op_ty, app_comm, prof_rank, tag);
    if (rq2) {
      doSample_blameRecv(cct_node, /*mayPause*/true); // affects 's_mpireq_done'

      if (hpc_isFlag(rq2->flags, ProfRequestFlg_inContainer)) {
	ProfRequestMap_delete(&s_mpireq_done, rq2, "doSample_makeReq");
	ProfRequest_free(rq2);
      }
    }
  }

  ProfRequest_t* myreq = ProfRequest_malloc();
  assert(myreq);

  //----------------------------------------------------------
  // 2. insert 'myreq' into ProfRequestMap
  //----------------------------------------------------------
  ProfRequestMap_t* reqmap = (is_pending) ? &s_mpireq_pend : &s_mpireq_done;

  ProfRequest_init1(myreq, request, op_ty, app_comm, prof_rank, tag,
		    datatype, count, cct_node);
  ProfRequestMap_insert(reqmap, myreq);
}


//***************************************************************************
// MPI Overrides: control
//***************************************************************************

void
hpcrun_mpi_bs_init()
{
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Get_count, PMPI_Get_count);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Request_free, PMPI_Request_free);

  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Send,       PMPI_Send);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Bsend,      PMPI_Bsend);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Isend,      PMPI_Isend);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Ibsend,     PMPI_Ibsend);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Issend,     PMPI_Issend);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Irsend,     PMPI_Irsend);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Send_init,  PMPI_Send_init);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Bsend_init, PMPI_Bsend_init);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Ssend_init, PMPI_Ssend_init);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Rsend_init, PMPI_Rsend_init);

  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Start,     PMPI_Start);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Startall,  PMPI_Startall);

  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Recv,      PMPI_Recv);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Irecv,     PMPI_Irecv);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Recv_init, PMPI_Recv_init);

  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Sendrecv,  PMPI_Sendrecv);

  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Iprobe,    PMPI_Iprobe);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Test,      PMPI_Test);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Wait,      PMPI_Wait);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Waitany,   PMPI_Waitany);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Waitall,   PMPI_Waitall);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Cancel,    PMPI_Cancel);

  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Barrier,   PMPI_Barrier);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Allreduce, PMPI_Allreduce);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Allgather, PMPI_Allgather);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Alltoall,  PMPI_Alltoall);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Reduce,    PMPI_Reduce);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Bcast,     PMPI_Bcast);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Gather,    PMPI_Gather);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Scatter,   PMPI_Scatter);

  s_world_comm = MPI_COMM_WORLD;

  // invalidate all library state
  s_prof_is_initialized = false;
  s_prof_is_on = false;
}


void
hpcrun_mpi_bs_start()
{
  int rc;

  int is_initialized = 0;
  rc = PMPI_Initialized(&is_initialized);
  assert(rc == MPI_SUCCESS && is_initialized);

  rc = PMPI_Comm_rank(s_world_comm, &s_rank_me);
  assert(rc == MPI_SUCCESS);

  rc = PMPI_Comm_size(s_world_comm, &s_rank_sz);
  assert(rc == MPI_SUCCESS);

  rc = PMPI_Comm_group(s_world_comm, &s_world_group);
  assert(rc == MPI_SUCCESS);


  //-------------------------------------------------------------
  // profiling server
  //-------------------------------------------------------------

  rc = PMPI_Comm_dup(s_world_comm, &s_prof_comm);
  assert(rc == MPI_SUCCESS);

  rc = PMPI_Comm_group(s_prof_comm, &s_prof_group);
  assert(rc == MPI_SUCCESS);

  MPI_Aint* tag_ub;
  int flag = 0;
  rc = PMPI_Comm_get_attr(s_world_comm, MPI_TAG_UB, &tag_ub, &flag);
  assert(rc == MPI_SUCCESS && flag);
  s_prof_tag = *tag_ub; 

  void* buf = NULL;
  int buf_sz = 0;
  rc = PMPI_Buffer_detach(&buf, &buf_sz);
  assert(rc == MPI_SUCCESS);
  if (!buf) {
    // TODO: More care needed if application uses PMPI_Buffer_attach
    s_prof_mpiBuf = hpcrun_malloc(s_prof_mpiBufSz);
    assert(s_prof_mpiBuf);

    rc = PMPI_Buffer_attach(s_prof_mpiBuf, s_prof_mpiBufSz);
    assert(rc == MPI_SUCCESS);
  }


  int rcvBuf_elemSz = sizeof(ProfCmdRequest_t);
  ProfCmdRequest_t* rcvBuf = hpcrun_malloc(s_prof_rcvBufCnt * rcvBuf_elemSz);
  assert(rcvBuf);
 
  CircularBuf_init(&s_prof_rcvBuf, rcvBuf, s_prof_rcvBufCnt, rcvBuf_elemSz);


  s_prof_is_rcv_on = true;

  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  void* mpireq_mem = hpcrun_malloc(s_mpireq_memSz);
  
  Mem_init(&s_mpireq_mem, mpireq_mem, s_mpireq_memSz, sizeof(ProfRequest_t));

  ProfRequestMap_init(&s_mpireq_pend);
  ProfRequestMap_init(&s_mpireq_done);


  int n_stat_subbuf = barrier_n_stat_subbuf(s_barrier_n_stat);
  int stat_subbuf_sz = n_stat_subbuf * sizeof(barrier_stat_t);


  rc = PMPI_Type_contiguous(stat_subbuf_sz, // replication count
			    MPI_BYTE, &s_barrier_stat_subbuf_ty);
  assert(rc == MPI_SUCCESS);

  rc = PMPI_Type_commit(&s_barrier_stat_subbuf_ty);
  assert(rc == MPI_SUCCESS);


  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  s_prof_is_initialized = true;
  s_prof_is_on = true;

  server_postRecvs();
}


void
hpcrun_mpi_bs_fini()
{
  // N.B.: MPI_Fini has been called.
  int rc;

  s_prof_is_on = false;
  s_prof_is_initialized = false;

  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  rc = PMPI_Group_free(&s_world_group);
  assert(rc == MPI_SUCCESS);

  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  s_prof_is_rcv_on = false;

  server_cleanup();

  if (s_prof_mpiBuf) {
    void* buf = NULL;
    int buf_sz = 0;
    rc = PMPI_Buffer_detach(&buf, &buf_sz);
    assert(rc == MPI_SUCCESS);
  }
  
  rc = PMPI_Comm_free(&s_prof_comm);
  assert(rc == MPI_SUCCESS);

  rc = PMPI_Group_free(&s_prof_group);
  assert(rc == MPI_SUCCESS);

}


//***************************************************************************
// MPI Overrides: point-to-point
//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Send)(parms_PMPI_Send)
{
  defs_point2point();

  int rc = real_PMPI_Send(buf, count, datatype, dest, tag, comm);

  doSample_p2p_blk(isSampled, timeBeg, OpTy_send,
		   count, datatype, dest, tag, comm);
  
  return rc;
}

int
MPI_Send(parms_PMPI_Send)
{
  return PMPI_Send(buf, count, datatype, dest, tag, comm);
}


int
MONITOR_EXT_WRAP_NAME(PMPI_Isend)(parms_PMPI_Isend)
{
  defs_point2point();

  int rc = real_PMPI_Isend(buf, count, datatype, dest, tag, comm, request);

  doSample_p2p_nb(isSampled, timeBeg, OpTy_send,
		  count, datatype, dest, tag, comm, request);
  
  return rc;
}

int
MPI_Isend(parms_PMPI_Isend)
{
  return PMPI_Isend(buf, count, datatype, dest, tag, comm, request);
}


//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Recv)(parms_PMPI_Recv)
{
  defs_point2point();

  int rc = 0;
  int my_source = MPI_PROC_NULL;

  if (status != MPI_STATUS_IGNORE) {
    rc = real_PMPI_Recv(buf, count, datatype, source, tag, comm, status);
    my_source = getSrcRank(source, status, /*mustSucceed*/true);
  }
  else {
    MPI_Status my_status;
    rc = real_PMPI_Recv(buf, count, datatype, source, tag, comm, &my_status);
    my_source = getSrcRank(source, &my_status, /*mustSucceed*/true);
  }

  doSample_p2p_blk(isSampled, timeBeg, OpTy_recv,
		   count, datatype, my_source, tag, comm);

  return rc;
}

int
MPI_Recv(parms_PMPI_Recv)
{
  return PMPI_Recv(buf, count, datatype, source, tag, comm, status);
}


int
MONITOR_EXT_WRAP_NAME(PMPI_Irecv)(parms_PMPI_Irecv)
{
  defs_point2point();

  int rc = real_PMPI_Irecv(buf, count, datatype, source, tag, comm, request);
 
  doSample_p2p_nb(isSampled, timeBeg, OpTy_recv,
		  count, datatype, source, tag, comm, request);

  return rc;
}

int
MPI_Irecv(parms_PMPI_Irecv)
{
  return PMPI_Irecv(buf, count, datatype, source, tag, comm, request);
}


//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Sendrecv)(parms_PMPI_Sendrecv)
{
  int rc = 0;

  // FIXME: rewrite using our PMPI_IRecv, PMPI_ISend and PMPI_Waitall

  // ** however **: benefit of custom implementation is there is need
  //   to map the request objects.

#if 0
  defs_point2point();

  int rc1 = 0, rc2 = 0;

  const int sz = 2, rcv_idx = 0, snd_idx = 1;
  MPI_Request reqA[sz];
  MPI_Status  statA[sz];

  rc1 = real_PMPI_Irecv(recvbuf, recvcount, recvtype, source, recvtag,
			comm, &reqA[rcv_idx]);
  rc2 = real_PMPI_Isend(sendbuf, sendcount, sendtype, dest, sendtag,
			comm, &reqA[snd_idx]);
  assert(rc1 == MPI_SUCCESS && rc2 == MPI_SUCCESS);

  int idx1 = MPI_UNDEFINED, idx2 = MPI_UNDEFINED;

  def_time(time1, isSampled);

  rc1 = real_PMPI_Waitany(sz, reqA, &idx1, &statA[0]);

  def_time(time2, isSampled);

  rc2 = real_PMPI_Waitany(sz, reqA, &idx2, &statA[1]);

  def_time(time3, isSampled);

  assert(idx1 != MPI_UNDEFINED && idx2 != MPI_UNDEFINED && idx1 != idx2);

  bool is_rcv_first = (idx1 == rcv_idx);
  int stat_rcv_idx = (is_rcv_first) ? 0 : 1;

  if (status != MPI_STATUS_IGNORE) {
    // copy 'MPI_Get_count' as well as MPI_SOURCE, MPI_TAG, & MPI_ERROR
    memcpy(status, &statA[stat_rcv_idx], sizeof(MPI_Status));
  }

  int my_source = getSrcRank(source, &statA[stat_rcv_idx], /*mustSucceed*/true);
  doSample_sndrcv(isSampled, timeBeg, time1, time2, time3, is_rcv_first,
		  recvcount, recvtype, recvtag, my_source,
		  sendcount, sendtype, sendtag, dest, comm);

  assert(rc1 == MPI_SUCCESS);
  rc = rc2;
#else
  defs_point2point();

  rc = real_PMPI_Sendrecv(sendbuf, sendcount, sendtype, dest, sendtag,
			  recvbuf, recvcount, recvtype, source, recvtag,
			  comm, status);

  doSample_simple(isSampled, timeBeg);
#endif
  
  return rc;
}

int
MPI_Sendrecv(parms_PMPI_Sendrecv)
{
  return PMPI_Sendrecv(sendbuf, sendcount, sendtype, dest, sendtag,
		       recvbuf, recvcount, recvtype, source, recvtag,
		       comm, status);
}


//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Wait)(parms_PMPI_Wait)
{
  defs_point2point();

  int rc = 0;
  int source = MPI_PROC_NULL;

  if (status != MPI_STATUS_IGNORE) {
    rc = real_PMPI_Wait(request, status);
    source = status->MPI_SOURCE;
  }
  else {
    MPI_Status my_status;
    rc = real_PMPI_Wait(request, &my_status);
    source = my_status.MPI_SOURCE;
  }

  doSample_wait(isSampled, timeBeg, 0.0, NULL, request, source);
  
  return rc;
}

int
MPI_Wait(parms_PMPI_Wait)
{
  return PMPI_Wait(request, status);
}


int
MONITOR_EXT_WRAP_NAME(PMPI_Waitall)(parms_PMPI_Waitall)
{
  int rc = 0, i;

#if 1
  typedef struct data_t {
    double e_time;
    int source;
  } data_t;

  data_t dataA[count];
  memset(dataA, 0, sizeof(data_t) * count);

  for (i = 0; i < count; ++i) {
    defs_point2point();

    int idx = MPI_UNDEFINED;
    MPI_Status status;
    rc += real_PMPI_Waitany(count, array_of_requests, &idx, &status);

    if (idx != MPI_UNDEFINED) {
      if (isSampled) {
	dataA[idx].e_time = timeElapsed_us(timeBeg);
	dataA[idx].source = status.MPI_SOURCE;
      }

      if (array_of_statuses != MPI_STATUSES_IGNORE) {
	memcpy(&(array_of_statuses[idx]), &status, sizeof(MPI_Status));
      }
    }
  }

  cct_node_t* cct_node = NULL;
  for (i = 0; i < count; ++i) {
    double e_time = dataA[i].e_time;
    if (e_time > 0.0) { // isSampled
      MPI_Request* request = &(array_of_requests[i]);
      int source = dataA[i].source;
      cct_node = doSample_wait(true, 0, e_time, cct_node, request, source);
    }
  }
#else
  defs_point2point();

  rc = real_PMPI_Waitall(count, array_of_requests, array_of_statuses);

  doSample_simple(isSampled, timeBeg);
#endif
  
  return rc;
}

int
MPI_Waitall(parms_PMPI_Waitall)
{
  return PMPI_Waitall(count, array_of_requests, array_of_statuses);
}


//***************************************************************************
// MPI Overrides: collectives
//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Barrier)(parms_PMPI_Barrier)
{
  defs_barrier();

#if (My_UseBarrier)
  int rc = hpcrun_barrierX(s_barrier_n_stat, stat_buf, n_stat_buf,
			   s_barrier_stat_subbuf_ty, comm);
#else
  int rc = real_PMPI_Barrier(comm);
#endif

  doSample_collective(isSampled, timeBeg, comm);

  return rc;
}

int
MPI_Barrier(parms_PMPI_Barrier) // for GlobalArrays
{
  return PMPI_Barrier(comm);
}


int
MONITOR_EXT_WRAP_NAME(PMPI_Allreduce)(parms_PMPI_Allreduce)
{
  defs_barrier();

  int rc;

#if (My_UseBarrier)
  rc = hpcrun_barrierX(s_barrier_n_stat, stat_buf, n_stat_buf,
		       s_barrier_stat_subbuf_ty, comm);
#endif

  rc = real_PMPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);

#if 0
  // FIXME: get rank w.r.t. comm and check if rank is in stat_buf
  // if (me == stat_buf[0].rank) { save stat_buf[0].e_time }

  const int str_sz = 512;
  char str[str_sz];
  snprintf(str, str_sz, "%g [%g, %g] [%g, %g] [%g, %g]", stat_buf[0].e_time,
	   stat_buf[1].rank, stat_buf[1].e_time,
	   stat_buf[2].rank, stat_buf[2].e_time,
	   stat_buf[3].rank, stat_buf[3].e_time);
  str[str_sz-1] = '\0';

  TMSG(MPIbs, "barrier: %s", str);
#endif

  doSample_collective(isSampled, timeBeg, comm);
  
  return rc;
}

int
MPI_Allreduce(parms_PMPI_Allreduce) // for GlobalArrays
{
  return PMPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);
}


int
MONITOR_EXT_WRAP_NAME(PMPI_Allgather)(parms_PMPI_Allgather)
{
  defs_collective();

  int rc = real_PMPI_Allgather(sendbuf, sendcount, sendtype,
			       recvbuf, recvcount, recvtype, comm);

  doSample_collective(isSampled, timeBeg, comm);
  
  return rc;
}

int
MPI_Allgather(parms_PMPI_Allgather) // for GlobalArrays
{
  return PMPI_Allgather(sendbuf, sendcount, sendtype,
			recvbuf, recvcount, recvtype, comm);
}


int
MONITOR_EXT_WRAP_NAME(PMPI_Reduce)(parms_PMPI_Reduce)
{
  defs_collective();

  int rc = real_PMPI_Reduce(sendbuf, recvbuf, count, datatype, op, root, comm);

  doSample_collective(isSampled, timeBeg, comm);
  
  return rc;
}

int
MPI_Reduce(parms_PMPI_Reduce) // for GlobalArrays
{
  return PMPI_Reduce(sendbuf, recvbuf, count, datatype, op, root, comm);
}


int
MONITOR_EXT_WRAP_NAME(PMPI_Bcast)(parms_PMPI_Bcast)
{
  defs_collective();

  int rc = real_PMPI_Bcast(buffer, count, datatype, root, comm);

  doSample_collective(isSampled, timeBeg, comm);
  
  return rc;
}

int
MPI_Bcast(parms_PMPI_Bcast) // for GlobalArrays
{
  return PMPI_Bcast(buffer, count, datatype, root, comm);
}


int
MONITOR_EXT_WRAP_NAME(PMPI_Gather)(parms_PMPI_Gather)
{
  defs_collective();

  int rc = real_PMPI_Gather(sendbuf, sendcount, sendtype, recvbuf, recvcount,
			    recvtype, root, comm);

  doSample_collective(isSampled, timeBeg, comm);
  
  return rc;
}

int
MPI_Gather(parms_PMPI_Gather) // for GlobalArrays
{
  return PMPI_Gather(sendbuf, sendcount, sendtype, recvbuf, recvcount,
		     recvtype, root, comm);
}


int
MONITOR_EXT_WRAP_NAME(PMPI_Scatter)(parms_PMPI_Scatter)
{
  defs_collective();

  int rc = real_PMPI_Scatter(sendbuf, sendcount, sendtype, recvbuf, recvcount,
			     recvtype, root, comm);

  doSample_collective(isSampled, timeBeg, comm);
  
  return rc;
}

int
MPI_Scatter(parms_PMPI_Scatter) // for GlobalArrays
{
  return PMPI_Scatter(sendbuf, sendcount, sendtype, recvbuf, recvcount,
		      recvtype, root, comm);
}


//***************************************************************************
// 
//***************************************************************************


