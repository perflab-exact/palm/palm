// -*-Mode: C++;-*- // technically C99

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// --------------------------------------------------------------------------
// Part of HPCToolkit (hpctoolkit.org)
//
// Information about sources of support for research and development of
// HPCToolkit is at 'hpctoolkit.org' and in 'README.Acknowledgments'.
// --------------------------------------------------------------------------
//
// Copyright ((c)) 2002-2016, Rice University
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
// * Neither the name of Rice University (RICE) nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// This software is provided by RICE and contributors "as is" and any
// express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular
// purpose are disclaimed. In no event shall RICE or contributors be
// liable for any direct, indirect, incidental, special, exemplary, or
// consequential damages (including, but not limited to, procurement of
// substitute goods or services; loss of use, data, or profits; or
// business interruption) however caused and on any theory of liability,
// whether in contract, strict liability, or tort (including negligence
// or otherwise) arising in any way out of the use of this software, even
// if advised of the possibility of such damage.
//
// ******************************************************* EndRiceCopyright *


//***************************************************************************
// system includes
//***************************************************************************

#include <stddef.h>
#include <stdlib.h>
#include <string.h>


//***************************************************************************
// local includes
//***************************************************************************

#include <sample-sources/simple_oo.h>
#include <sample-sources/sample_source_obj.h>
#include <sample-sources/common.h>
#include <sample-sources/mpi-blame.h>

#include <hpcrun/metrics.h>
#include <hpcrun/thread_data.h>
#include <messages/messages.h>
#include <utilities/tokenize.h>


//***************************************************************************
// local variables
//***************************************************************************

const static long periodDefault = 1;

uint64_t hpcrun_mpi_bs_period = 0;

int hpcrun_mpi_bs_mtrcId_op  = -1;
int hpcrun_mpi_bs_mtrcId_tOp = -1;
int hpcrun_mpi_bs_mtrcId_tBlame = -1;
int hpcrun_mpi_bs_mtrcId_tBlameRcv = -1;
int hpcrun_mpi_bs_mtrcId_tBlameSnd = -1;


//***************************************************************************
// method definitions
//***************************************************************************

static void
METHOD_FN(init)
{
  TMSG(MPIbs, "init");
  self->state = INIT;

  hpcrun_mpi_bs_mtrcId_op  = -1;
  hpcrun_mpi_bs_mtrcId_tOp = -1;
  hpcrun_mpi_bs_mtrcId_tBlame = -1;
  hpcrun_mpi_bs_mtrcId_tBlameRcv = -1;
  hpcrun_mpi_bs_mtrcId_tBlameSnd = -1;
}


static void
METHOD_FN(thread_init)
{
  TMSG(MPIbs, "thread init (no-op)");
}


static void
METHOD_FN(thread_init_action)
{
  TMSG(MPIbs, "thread init action (no-op)");
}


static void
METHOD_FN(start)
{
  TMSG(MPIbs,"starting MPI_BS sample source");

  // FIXME: hpcrun's monitor_init_mpi() [libmonitor] hook should
  // signal when it is safe to perform MPI calls.  For now, we rely on
  // the fact that our monitor_init_mpi() hook call 'start' (assuming
  // MPI_RISKY is not defined) after MPI_Init is called.
  //
  // if monitor_mpi_comm_rank() >= 0, then libmonitor has intercepted MPI_Init

  // clear state for any thread
  hpcrun_mpi_bs_init();

  // start MPI monitoring for master thread
  thread_data_t* threadData = hpcrun_get_thread_data();
  if (threadData->core_profile_trace_data.id == 0) {
    int is_initialized = 0;
    PMPI_Initialized(&is_initialized);
    if (is_initialized) {
      hpcrun_mpi_bs_start();
    }
  }

  TD_GET(ss_state)[self->sel_idx] = START;
}


static void
METHOD_FN(thread_fini_action)
{
  TMSG(MPIbs, "thread fini action (no-op)");
}


static void
METHOD_FN(stop)
{
  TMSG(MPIbs,"stopping MPI_BS sample source");
  TD_GET(ss_state)[self->sel_idx] = STOP;
}


static void
METHOD_FN(shutdown)
{
  TMSG(MPIbs, "shutdown MPI_BS sample source");
  METHOD_CALL(self, stop);

  thread_data_t* threadData = hpcrun_get_thread_data();
  if (threadData->core_profile_trace_data.id == 0) {
    int is_initialized = 0;
    PMPI_Initialized(&is_initialized);
    if (is_initialized) {
      hpcrun_mpi_bs_fini();
    }
  }

  self->state = UNINIT;
}


static bool
METHOD_FN(supports_event, const char *ev_str)
{
  return hpcrun_ev_is(ev_str, "MPI_BS");
}


static void
METHOD_FN(process_event_list, int lush_metrics)
{
  TMSG(MPIbs, "create MPI_BS metrics");

  char* evStr = METHOD_CALL(self, get_event_str);

  hpcrun_mpi_bs_period = periodDefault;
  char evName[32];
  hpcrun_extract_ev_thresh(evStr, sizeof(evName), evName,
			   (long*)&hpcrun_mpi_bs_period, periodDefault);

  TMSG(MPIbs, "MPI: %s sampling period: %"PRIu64, evName, hpcrun_mpi_bs_period);

  // number of MPI operations
  hpcrun_mpi_bs_mtrcId_op = hpcrun_new_metric();
  hpcrun_set_metric_info(hpcrun_mpi_bs_mtrcId_op, "#mpi");

  // time for MPI operations
  hpcrun_mpi_bs_mtrcId_tOp = hpcrun_new_metric();
  hpcrun_set_metric_info_and_period(hpcrun_mpi_bs_mtrcId_tOp, "Tmpi (us)",
				    MetricFlags_ValFmt_Real, 1, metric_property_none);

  // blame time
  hpcrun_mpi_bs_mtrcId_tBlame = hpcrun_new_metric();
  hpcrun_set_metric_info_and_period(hpcrun_mpi_bs_mtrcId_tBlame, "Tblame",
				    MetricFlags_ValFmt_Real, 1, metric_property_none);

  hpcrun_mpi_bs_mtrcId_tBlameRcv = hpcrun_new_metric();
  hpcrun_set_metric_info_and_period(hpcrun_mpi_bs_mtrcId_tBlameRcv, "Tblame-rcv",
				    MetricFlags_ValFmt_Real, 1, metric_property_none);

  hpcrun_mpi_bs_mtrcId_tBlameSnd = hpcrun_new_metric();
  hpcrun_set_metric_info_and_period(hpcrun_mpi_bs_mtrcId_tBlameSnd, "Tblame-snd",
				    MetricFlags_ValFmt_Real, 1, metric_property_none);
}


static void
METHOD_FN(gen_event_set, int lush_metrics)
{
  TMSG(MPIbs, "gen event set (no-op)");
}


static void
METHOD_FN(display_events)
{
  printf("===========================================================================\n");
  printf("Available MPI_BS events\n");
  printf("===========================================================================\n");
  printf("Name\t\tDescription\n");
  printf("---------------------------------------------------------------------------\n");
  printf("MPI_BS\t\t...\n");
  printf("\n");
}


//***************************************************************************
// object
//***************************************************************************

// sync class is "SS_SOFTWARE" so that both synchronous and
// asynchronous sampling is possible

#define ss_name mpi_bs
#define ss_cls SS_SOFTWARE

#include "ss_obj.h"


//***************************************************************************
// interface functions
//***************************************************************************


