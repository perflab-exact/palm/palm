// -*-Mode: C++;-*- // technically C99

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// ******************************************************* EndRiceCopyright *

//***************************************************************************
//
// File:
// $HeadURL$
//
// Purpose:
// "MPI Critical Path" and "MPI Blame Shifting" sampling source.
//
// Authors:
// Nathan Tallent and Abhinav Vishnu
//
//***************************************************************************

#ifndef _HPCRUN_MPI_DECL_H_
#define _HPCRUN_MPI_DECL_H_

//***************************************************************************
// system includes
//***************************************************************************

#include <stdlib.h>

//***************************************************************************
// MPI
//***************************************************************************

#include <mpi.h>

//***************************************************************************
// libmonitor include files
//***************************************************************************

//#include <monitor.h>

//***************************************************************************
// local includes
//***************************************************************************

//***************************************************************************
// 
//***************************************************************************

int
PMPI_Comm_rank(MPI_Comm comm, int *rank);

int
PMPI_Comm_size(MPI_Comm comm, int *size);

int
PMPI_Comm_dup(MPI_Comm comm, MPI_Comm *newcomm);

int
PMPI_Comm_free(MPI_Comm *comm);

int
PMPI_Comm_group(MPI_Comm comm, MPI_Group *group);

int
PMPI_Comm_get_attr(MPI_Comm comm, int comm_keyval,
		   void *attribute_val, int *flag);

int
PMPI_Type_commit(MPI_Datatype *datatype);

int
PMPI_Type_free(MPI_Datatype *datatype);

int
PMPI_Type_size(MPI_Datatype datatype, int *size);

int
PMPI_Type_contiguous(int count, MPI_Datatype oldtype, MPI_Datatype *newtype);

int
PMPI_Type_vector(int count, int blocklength, int stride,
		 MPI_Datatype oldtype, MPI_Datatype *newtype);

#if 0
int
PMPI_Type_create_struct(int count, const int array_of_blocklengths[],
			const MPI_Aint array_of_displacements[],
			const MPI_Datatype array_of_types[],
			MPI_Datatype *newtype);
#endif

int
PMPI_Group_translate_ranks(MPI_Group group1, int n, const int ranks1[],
			   MPI_Group group2, int ranks2[]);

int
PMPI_Group_free(MPI_Group *group);

int
PMPI_Buffer_attach(void *buffer, int size);

int
PMPI_Buffer_detach(void *buffer_addr, int *size);


double
PMPI_Wtime();

//-------------------------------------------------------------

#define parms_PMPI_Get_count \
  MPI_Status *status, MPI_Datatype datatype, int *count

typedef int PMPI_Get_count_fn_t(parms_PMPI_Get_count);

//MONITOR_EXT_DECLARE_REAL_FN(PMPI_Get_count_fn_t, real_PMPI_Get_count);

//-------------------------------------------------------------

#define parms_PMPI_Request_free \
  MPI_Request *request

typedef int PMPI_Request_free_fn_t(parms_PMPI_Request_free);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Request_free_fn_t, real_PMPI_Request_free);

//-------------------------------------------------------------

#define parms_PMPI_Send \
  const void* buf, int count, MPI_Datatype datatype, int dest, int tag, \
  MPI_Comm comm

typedef int PMPI_Send_fn_t(parms_PMPI_Send);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Send_fn_t, real_PMPI_Send);

//-------------------------------------------------------------

#define parms_PMPI_Bsend parms_PMPI_Send

typedef int PMPI_Bsend_fn_t(parms_PMPI_Bsend);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Bsend_fn_t, real_PMPI_Bsend);

//-------------------------------------------------------------

#define parms_PMPI_Isend \
  const void* buf, int count, MPI_Datatype datatype, int dest, int tag, \
  MPI_Comm comm, MPI_Request *request

typedef int PMPI_Isend_fn_t(parms_PMPI_Isend);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Isend_fn_t, real_PMPI_Isend);

//-------------------------------------------------------------

#define parms_PMPI_Ibsend parms_PMPI_Isend

typedef int PMPI_Ibsend_fn_t(parms_PMPI_Ibsend);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Ibsend_fn_t, real_PMPI_Ibsend);

//-------------------------------------------------------------

#define parms_PMPI_Issend parms_PMPI_Isend

typedef int PMPI_Issend_fn_t(parms_PMPI_Issend);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Issend_fn_t, real_PMPI_Issend);

//-------------------------------------------------------------

#define parms_PMPI_Irsend parms_PMPI_Isend

typedef int PMPI_Irsend_fn_t(parms_PMPI_Irsend);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Irsend_fn_t, real_PMPI_Irsend);

//-------------------------------------------------------------

#define parms_PMPI_Send_init parms_PMPI_Isend

typedef int PMPI_Send_init_fn_t(parms_PMPI_Send_init);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Send_init_fn_t, real_PMPI_Send_init);

//-------------------------------------------------------------

#define parms_PMPI_Bsend_init parms_PMPI_Isend

typedef int PMPI_Bsend_init_fn_t(parms_PMPI_Bsend_init);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Bsend_init_fn_t, real_PMPI_Bsend_init);

//-------------------------------------------------------------

#define parms_PMPI_Ssend_init parms_PMPI_Isend

typedef int PMPI_Ssend_init_fn_t(parms_PMPI_Ssend_init);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Ssend_init_fn_t, real_PMPI_Ssend_init);

//-------------------------------------------------------------

#define parms_PMPI_Rsend_init parms_PMPI_Isend

typedef int PMPI_Rsend_init_fn_t(parms_PMPI_Rsend_init);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Rsend_init_fn_t, real_PMPI_Rsend_init);

// MPI_Bsend_init, MPI_Ssend_init, MPI_Rsend_init

//-------------------------------------------------------------

#define parms_PMPI_Start \
  MPI_Request *request

typedef int PMPI_Start_fn_t(parms_PMPI_Start);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Start_fn_t, real_PMPI_Start);

//-------------------------------------------------------------

#define parms_PMPI_Startall \
  int count, MPI_Request array_of_requests[]

typedef int PMPI_Startall_fn_t(parms_PMPI_Startall);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Startall_fn_t, real_PMPI_Startall);

//-------------------------------------------------------------

#define parms_PMPI_Recv \
  void* buf, int count, MPI_Datatype datatype, int source, int tag, \
  MPI_Comm comm, MPI_Status *status

typedef int PMPI_Recv_fn_t(parms_PMPI_Recv);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Recv_fn_t, real_PMPI_Recv);

//-------------------------------------------------------------

#define parms_PMPI_Irecv \
  void *buf, int count, MPI_Datatype datatype, int source, int tag, \
  MPI_Comm comm, MPI_Request *request

typedef int PMPI_Irecv_fn_t(parms_PMPI_Irecv);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Irecv_fn_t, real_PMPI_Irecv);

//-------------------------------------------------------------

#define parms_PMPI_Recv_init parms_PMPI_Irecv

typedef int PMPI_Recv_init_fn_t(parms_PMPI_Recv_init);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Recv_init_fn_t, real_PMPI_Recv_init);

//-------------------------------------------------------------

#define parms_PMPI_Sendrecv \
  const void *sendbuf, int sendcount, MPI_Datatype sendtype, int dest, \
  int sendtag, void *recvbuf, int recvcount, MPI_Datatype recvtype, \
  int source, int recvtag, MPI_Comm comm, MPI_Status *status

typedef int PMPI_Sendrecv_fn_t(parms_PMPI_Sendrecv);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Sendrecv_fn_t, real_PMPI_Sendrecv);

//-------------------------------------------------------------

#define parms_PMPI_Iprobe \
  int source, int tag, MPI_Comm comm, int *flag, MPI_Status *status

typedef int PMPI_Iprobe_fn_t(parms_PMPI_Iprobe);

//MONITOR_EXT_DECLARE_REAL_FN(PMPI_Iprobe_fn_t, real_PMPI_Iprobe);

//-------------------------------------------------------------

#define parms_PMPI_Test \
  MPI_Request *request, int *flag, MPI_Status *status

typedef int PMPI_Test_fn_t(parms_PMPI_Test);

//MONITOR_EXT_DECLARE_REAL_FN(PMPI_Test_fn_t, real_PMPI_Test);

//-------------------------------------------------------------

#define parms_PMPI_Wait \
  MPI_Request *request, MPI_Status *status

typedef int PMPI_Wait_fn_t(parms_PMPI_Wait);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Wait_fn_t, real_PMPI_Wait);

//-------------------------------------------------------------

#define parms_PMPI_Waitany \
  int count, MPI_Request array_of_requests[], int *indx, MPI_Status *status

typedef int PMPI_Waitany_fn_t(parms_PMPI_Waitany);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Waitany_fn_t, real_PMPI_Waitany);

//-------------------------------------------------------------

#define parms_PMPI_Waitall \
  int count, MPI_Request array_of_requests[], MPI_Status array_of_statuses[]

typedef int PMPI_Waitall_fn_t(parms_PMPI_Waitall);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Waitall_fn_t, real_PMPI_Waitall);

//-------------------------------------------------------------

#define parms_PMPI_Cancel \
  MPI_Request *request

typedef int PMPI_Cancel_fn_t(parms_PMPI_Cancel);

//MONITOR_EXT_DECLARE_REAL_FN(PMPI_Cancel_fn_t, real_PMPI_Cancel);


//-------------------------------------------------------------
//-------------------------------------------------------------

#define parms_PMPI_Barrier \
  MPI_Comm comm

typedef int PMPI_Barrier_fn_t(parms_PMPI_Barrier);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Barrier_fn_t, real_PMPI_Barrier);

//-------------------------------------------------------------

#define parms_PMPI_Allreduce \
  const void* sendbuf, void* recvbuf, int count, MPI_Datatype datatype, \
  MPI_Op op, MPI_Comm comm

typedef int PMPI_Allreduce_fn_t(parms_PMPI_Allreduce);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Allreduce_fn_t, real_PMPI_Allreduce);

//-------------------------------------------------------------

#define parms_PMPI_Allgather \
  const void* sendbuf, int sendcount, MPI_Datatype sendtype, \
  void* recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm

typedef int PMPI_Allgather_fn_t(parms_PMPI_Allgather);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Allgather_fn_t, real_PMPI_Allgather);

//-------------------------------------------------------------

#define parms_PMPI_Allgatherv					     \
  const void *sendbuf, int sendcount, MPI_Datatype sendtype,	     \
  void *recvbuf, const int *recvcounts, const int *displs,	     \
  MPI_Datatype recvtype, MPI_Comm comm

typedef int PMPI_Allgatherv_fn_t(parms_PMPI_Allgatherv);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Allgatherv_fn_t, real_PMPI_Allgatherv);
  
//-------------------------------------------------------------

#define parms_PMPI_Alltoall \
  const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, \
  int recvcount, MPI_Datatype recvtype, MPI_Comm comm

typedef int PMPI_Alltoall_fn_t(parms_PMPI_Alltoall);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Alltoall_fn_t, real_PMPI_Alltoall);

//-------------------------------------------------------------

#define parms_PMPI_Reduce \
  const void* sendbuf, void* recvbuf, int count, MPI_Datatype datatype, \
  MPI_Op op, int root, MPI_Comm comm

typedef int PMPI_Reduce_fn_t(parms_PMPI_Reduce);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Reduce_fn_t, real_PMPI_Reduce);

//-------------------------------------------------------------

#define parms_PMPI_Bcast \
  void* buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm

typedef int PMPI_Bcast_fn_t(parms_PMPI_Bcast);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Bcast_fn_t, real_PMPI_Bcast);

//-------------------------------------------------------------

#define parms_PMPI_Gather \
  const void* sendbuf, int sendcount, MPI_Datatype sendtype, void* recvbuf, \
  int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm

typedef int PMPI_Gather_fn_t(parms_PMPI_Gather);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Gather_fn_t, real_PMPI_Gather);

//-------------------------------------------------------------

#define parms_PMPI_Gatherv \
  const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, \
  const int *recvcounts, const int *displs, MPI_Datatype recvtype, int root, \
  MPI_Comm comm

typedef int PMPI_Gatherv_fn_t(parms_PMPI_Gatherv);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Gatherv_fn_t, real_PMPI_Gatherv);

//-------------------------------------------------------------

#define parms_PMPI_Scatter \
  const void* sendbuf, int sendcount, MPI_Datatype sendtype, void* recvbuf, \
  int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm

typedef int PMPI_Scatter_fn_t(parms_PMPI_Scatter);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Scatter_fn_t, real_PMPI_Scatter);

//-------------------------------------------------------------

#define parms_PMPI_Scan \
  const void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype, \
  MPI_Op op, MPI_Comm comm

typedef int PMPI_Scan_fn_t(parms_PMPI_Scan);

MONITOR_EXT_DECLARE_REAL_FN(PMPI_Scan_fn_t, real_PMPI_Scan);

//***************************************************************************

#endif // _HPCRUN_MPI_DECL_H_
