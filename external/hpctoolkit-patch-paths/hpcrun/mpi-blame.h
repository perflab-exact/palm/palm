// -*-Mode: C++;-*- // technically C99

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// --------------------------------------------------------------------------
// Part of HPCToolkit (hpctoolkit.org)
//
// Information about sources of support for research and development of
// HPCToolkit is at 'hpctoolkit.org' and in 'README.Acknowledgments'.
// --------------------------------------------------------------------------
//
// Copyright ((c)) 2002-2016, Rice University
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
// * Neither the name of Rice University (RICE) nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// This software is provided by RICE and contributors "as is" and any
// express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular
// purpose are disclaimed. In no event shall RICE or contributors be
// liable for any direct, indirect, incidental, special, exemplary, or
// consequential damages (including, but not limited to, procurement of
// substitute goods or services; loss of use, data, or profits; or
// business interruption) however caused and on any theory of liability,
// whether in contract, strict liability, or tort (including negligence
// or otherwise) arising in any way out of the use of this software, even
// if advised of the possibility of such damage.
//
// ******************************************************* EndRiceCopyright *

#ifndef _HPCRUN_MPI_BS_H_
#define _HPCRUN_MPI_BS_H_

//***************************************************************************
// system includes
//***************************************************************************

#include <stddef.h>
#include <stdio.h>
#include <string.h>

//***************************************************************************
// MPI
//***************************************************************************

#include <mpi.h>

//***************************************************************************
// local includes
//***************************************************************************

#include <include/uint.h>

//***************************************************************************

#define MPIbs MPI /* FIXME: for now, reuse the 'MPI' MSG class */

//***************************************************************************

extern uint64_t hpcrun_mpi_bs_period;

extern int hpcrun_mpi_bs_mtrcId_op;
extern int hpcrun_mpi_bs_mtrcId_tOp;
extern int hpcrun_mpi_bs_mtrcId_tBlame;
extern int hpcrun_mpi_bs_mtrcId_tBlameRcv;
extern int hpcrun_mpi_bs_mtrcId_tBlameSnd;

//***************************************************************************

extern int
PMPI_Initialized(int *flag);

extern void
hpcrun_mpi_bs_init();

extern void
hpcrun_mpi_bs_start();

extern void
hpcrun_mpi_bs_fini();


//***************************************************************************

#endif // _HPCRUN_MPI_BS_H_
