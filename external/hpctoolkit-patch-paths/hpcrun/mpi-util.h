// -*-Mode: C++;-*- // technically C99

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// ******************************************************* EndRiceCopyright *

//***************************************************************************
//
// File:
// $HeadURL$
//
// Purpose:
// "MPI Critical Path" and "MPI Blame Shifting" sampling source.
//
// Authors:
// Nathan Tallent and Abhinav Vishnu
//
//***************************************************************************

#ifndef _HPCRUN_MPI_UTIL_H_
#define _HPCRUN_MPI_UTIL_H_

//***************************************************************************
// system includes
//***************************************************************************

#include <stdlib.h>
#include <limits.h> // for ULONG_MAX (instead of DBL_MAX)

//***************************************************************************
// MPI
//***************************************************************************

#include <mpi.h>

//***************************************************************************
// libmonitor include files
//***************************************************************************

//#include <monitor.h>

//***************************************************************************
// local includes
//***************************************************************************

#include <sample_event.h>

#include <lib/support-lean/timer.h>

//***************************************************************************

extern int s_prof_tag;


//***************************************************************************
// Mem: simple memory allocator for objects of one size (assumptions
//   yield constant time allocation/deallocation and no fragmentation
//   problems)
//***************************************************************************

typedef struct MemObj_t {
  struct MemObj_t* freelist_next;
} MemObj_t;

//static const MemObj_t* MemObj_flag = (const MemObj_t*)0xffffffffdeadbeef;


typedef struct Mem_t {
  void* mem_beg; // memory begin (inclusive)
  void* mem_end; // memory end (exclusive)
  void* mem_ptr; // current pointer

  MemObj_t* freelist_head;
  MemObj_t* freelist_tail;

  size_t obj_size;
  
} Mem_t;


static inline MemObj_t*
Mem_freelistDeq(Mem_t* mem);


static inline void
Mem_freelistEnq(Mem_t* mem, MemObj_t* x);


static inline void
Mem_init(Mem_t* x, void* mem_beg, size_t mem_size, size_t obj_size)
{
  x->mem_beg = mem_beg;
  x->mem_end = mem_beg + mem_size;
  x->mem_ptr = x->mem_beg;

  x->freelist_head = NULL;
  x->freelist_tail = NULL;

  x->obj_size = obj_size;
}


static inline MemObj_t*
Mem_malloc(Mem_t* mem)
{
  MemObj_t* x = Mem_freelistDeq(mem);
  if (!x) {
    //hpcrun_malloc(mem->obj_size);
    mem->mem_ptr += mem->obj_size;
    if (mem->mem_ptr < mem->mem_end) {
      x = (MemObj_t*)(mem->mem_ptr - mem->obj_size);
    }
  }

  return x;
}


static inline void
Mem_free(Mem_t* mem, MemObj_t* x)
{
  if (x) {
    Mem_freelistEnq(mem, x);
  }
}


// Mem_freelistEnq: push x on the freelist tail
static inline void
Mem_freelistEnq(Mem_t* mem, MemObj_t* x)
{
  assert(x->freelist_next == NULL);

  x->freelist_next = NULL;

  if (!mem->freelist_tail) {
    // Case 1: empty
    mem->freelist_head = x;
    mem->freelist_tail = x;
  }
  else {
    // Case 2: non-empty
    mem->freelist_tail->freelist_next = x;
    mem->freelist_tail = x;
  }
}


// Mem_freelistDeq: pop from the freelist head, if possible
static inline MemObj_t*
Mem_freelistDeq(Mem_t* mem)
{
  if (!mem->freelist_head) {
    // Case 1: empty
    return NULL;
  }
  else {
    // Case 2: non-empty
    MemObj_t* x = mem->freelist_head;
    mem->freelist_head = x->freelist_next;
    x->freelist_next = NULL;

    // Special case: one-element queue becomes empty
    if (!mem->freelist_head) {
      mem->freelist_tail = NULL;
    }
    
    return x;
  }
}


//***************************************************************************
// Circular buffer (keeps one slot open)
//   cf. http://en.wikipedia.org/wiki/Circular_buffer
//***************************************************************************

typedef struct {
  int i_head;  // index of oldest element (inclusive)
  int i_tail;  // index at which to write new element (exclusive)
  int elemNum; // maximum number of elements (including empty elem)

  int elemSz;   // size of element (in bytes)
  void* buffer; // vector of opaque elements (including empty element)

} CircularBuf_t;


static inline void
CircularBuf_init(CircularBuf_t* x, void* buffer, int elemNum, int elemSz)
{
  x->i_head = 0;
  x->i_tail = 0;
  x->elemNum = elemNum;
  x->elemSz = elemSz;
  x->buffer = buffer;
}


static inline bool
CircularBuf_isFull(CircularBuf_t* x)
{
  return ((x->i_tail + 1) % x->elemNum) == x->i_head;
}


static inline bool
CircularBuf_isEmpty(CircularBuf_t* x)
{
  return (x->i_tail == x->i_head);
}


static inline void*
CircularBuf_at(CircularBuf_t* x, int index)
{
  return (x->buffer + (index * x->elemSz));
}


// CircularBuf_push: Push empty element, overwriting oldest element if
// buffer full.  To avoid overwrite, check if full.
static inline void*
CircularBuf_push(CircularBuf_t* x)
{
  void* elem = CircularBuf_at(x, x->i_tail);
  x->i_tail = (x->i_tail + 1) % x->elemNum;
  if (x->i_tail == x->i_head) {
    // buffer is full when tail collides with head: overwrite
    x->i_head = (x->i_head + 1) % x->elemNum;
  }
  return elem;
}


// CircularBuf_pop: Pop oldest element. Must ensure not empty.
static inline void*
CircularBuf_pop(CircularBuf_t* x)
{
  void* elem = CircularBuf_at(x, x->i_head);
  x->i_head = (x->i_head + 1) % x->elemNum;
  return elem;
}


// CircularBuf_top: Return oldest element without popping. Must ensure
// not empty.
static inline void*
CircularBuf_top(CircularBuf_t* x)
{
  void* elem = CircularBuf_at(x, x->i_head);
  return elem;
}


//***************************************************************************
// OpTy_t
//***************************************************************************

typedef enum {
  OpTy_NULL = 0,
  OpTy_send,
  OpTy_recv,
  OpTy_cltv,

} OpTy_t;


static inline const char*
OpTy_str(OpTy_t x)
{
  static const char* str[] = { "NULL", "send", "recv", "cltv" };
  return str[x];
}


//***************************************************************************
// 
//***************************************************************************

static inline bool
hpc_isFlag(int flagbits, int f)
{
  return (flagbits & f);
}


static inline void
hpc_setFlag(int* flagbits, int f)
{
  *flagbits = (*flagbits | f);
}


static inline void
hpc_unsetFlag(int* flagbits, int f)
{
  *flagbits = (*flagbits & ~f);
}


//***************************************************************************
// Util
//***************************************************************************

#define doMetricAbs(metricVec, /*int*/metricId, metricIncr, type)	\
{									\
  if (metricId >= 0) {							\
    hpcrun_metric_std_inc(metricId, metricVec,				\
			  (cct_metric_data_t){.type = metricIncr});	\
  }									\
}


// FIXME
#if defined(MY_CPU_MHZ)
static const double _cyc_per_us = MY_CPU_MHZ;
#else
# warning "mpi-cp-overrides.c: using 2100 MHz"
static const double _cyc_per_us = 2100;
#endif


// timeDiff_us: returns time us but with ns resolution
static inline double
timeDiff_us(uint64_t timeBeg_tsc, uint64_t timeEnd_tsc)
{
  static const double us_per_cyc = (1.0 / _cyc_per_us);
  return (double)(timeEnd_tsc - timeBeg_tsc) * us_per_cyc;
}


static inline double
timeDiff_ns(uint64_t timeBeg_tsc, uint64_t timeEnd_tsc)
{
  static const double ns_per_cyc = (1000.0 / _cyc_per_us);
  return (double)(timeEnd_tsc - timeBeg_tsc) * ns_per_cyc;
}


// timeElapsed_us: returns time in us but with ns resolution
static inline double
timeElapsed_us(uint64_t timeBeg_tsc)
{
  uint64_t timeEnd_tsc = time_getTSC();
  return timeDiff_us(timeBeg_tsc, timeEnd_tsc);
}


static inline bool
was_blocked(double elapsed_time_us, size_t nbytes)
{
  // compute expected time for message of size 'nbytes' using LogGP Model

  // FIXME: MY_NETWORK_IB
  const double nwlat = 3;    // network latency (us)
  const double nwbw  = 3000; // network bandwidth (bytes/us)
  
  double expected_time_us = nwlat + (nbytes / nwbw);

  const double threshold = 2.0; // FIXME: refine

  return ((elapsed_time_us / expected_time_us) > threshold);
}


static inline size_t
xlateSz_count2bytes(size_t count, MPI_Datatype datatype)
{
  int nbytes = 0;

  if (datatype != MPI_DATATYPE_NULL) {
    int type_sz = 0;
    int rc = PMPI_Type_size(datatype, &type_sz);
    assert(rc == MPI_SUCCESS);

    nbytes = (count * type_sz);
  }

  return nbytes;
}


static inline int
xlateRank_app2prof_1(int app_rank, MPI_Comm app_comm,
		     bool prof_is_on, MPI_Comm world_comm,
		     MPI_Group prof_group)
{
  int rc;
  int prof_rank = MPI_PROC_NULL;

  assert(app_comm != MPI_COMM_NULL);

  if (!prof_is_on) {
    return MPI_PROC_NULL;
  }

  //----------------------------------------------------------
  // 1. handle special rank values
  //----------------------------------------------------------
  if (app_rank == MPI_PROC_NULL || app_rank == MPI_ANY_SOURCE) {
    return app_rank; // no translation needed
  }

  //----------------------------------------------------------
  // 2. translate
  //----------------------------------------------------------
  if (app_comm == world_comm) {
    prof_rank = app_rank;
  }
  else {
    // FIXME: need [comm -> group]

    MPI_Group app_group;
    rc = PMPI_Comm_group(app_comm, &app_group);
    assert(rc == MPI_SUCCESS);
  
    rc = PMPI_Group_translate_ranks(app_group, 1, &app_rank,
				    prof_group, &prof_rank);
    assert(rc == MPI_SUCCESS);
  
    rc = PMPI_Group_free(&app_group);
    assert(rc == MPI_SUCCESS);
  }

  //TMSG(MPI, "xlateRank_app2prof: app, srvr: %d -> %d", app_rank, prof_rank);
  assert(prof_rank >= 0);

  return prof_rank;
}


static inline int
getSrcRank(int source, MPI_Status* status, bool mustSucceed)
{
  int rank = MPI_PROC_NULL;

  if (source != MPI_ANY_SOURCE) {
    rank = source;
  }
  else if (status != MPI_STATUS_IGNORE) {
    rank = status->MPI_SOURCE;
  }

  if (mustSucceed) {
    assert(rank != MPI_PROC_NULL);
  }
  
  return rank;
}


//***************************************************************************
// 
//***************************************************************************

static inline void
mpicomm_str(MPI_Comm comm, char* str, int sz)
{
  if ( !(str && sz > 0) ) {
    return;
  }

  if (comm == MPI_COMM_WORLD) {
    snprintf(str, sz, "world");
  }
  else {
    snprintf(str, sz, "%d", (int)comm);
  }
  str[sz - 1] = '\0';
}


static inline void
mpirank_str(int rank, char* str, int sz)
{
  if ( !(str && sz > 0) ) {
    return;
  }

  if (rank == MPI_PROC_NULL) {
    snprintf(str, sz, "null");
  }
  else if (rank == MPI_ANY_SOURCE) {
    snprintf(str, sz, "any");
  }
  else {
    snprintf(str, sz, "%d", rank);
  }
  str[sz - 1] = '\0';
}


static inline void
mpitag_str(int tag, char* str, int sz)
{
  if ( !(str && sz > 0) ) {
    return;
  }

  if (tag == s_prof_tag) {
    snprintf(str, sz, "ub");
  }
  else if (tag == MPI_ANY_TAG) {
    snprintf(str, sz, "any");
  }
  else {
    snprintf(str, sz, "%d", tag);
  }
  str[sz - 1] = '\0';
}


#if 0
static inline void
mpiOpCommRankTag_str(MPI_Comm comm, char* str, int sz)
{
  snprintf(str_dbg, str_sz, "<op %s, comm %s, rank %s, tag %s>",
	   OpTy_str(x->op_ty), cmm_str, rnk_str, tag_str);
  str_dbg[str_sz - 1] = '\0';
}
#endif

//***************************************************************************

#endif // _HPCRUN_MPI_UTIL_H_
