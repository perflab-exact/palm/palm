// -*-Mode: C++;-*- // technically C99

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// ******************************************************* EndRiceCopyright *

//***************************************************************************
//
// File:
// $HeadURL$
//
// Purpose:
// "MPI Critical Path" and "MPI Blame Shifting" sampling source.
//
// Authors:
// Nathan Tallent and Abhinav Vishnu
//
//***************************************************************************

#ifndef _HPCRUN_MPI_OVERRIDES_H_
#define _HPCRUN_MPI_OVERRIDES_H_

//***************************************************************************
// system includes
//***************************************************************************

#include <stdlib.h>

//***************************************************************************
// MPI
//***************************************************************************

#include <mpi.h>

//***************************************************************************
// libmonitor include files
//***************************************************************************

//#include <monitor.h>

//***************************************************************************
// local includes
//***************************************************************************

#include "mpi-util.h"


//***************************************************************************
// ProfRequest
//***************************************************************************

enum {
  ProfRequestFlg_NULL = 0,
  ProfRequestFlg_inContainer = (1 << 0),

  ProfRequestFlg_isPersistent = (2 << 0),
};

//***************************************************************************

typedef struct ProfRequest_t {
  MemObj_t* freelist_next; // N.B.: must be first; cf. 'MemObj_t'

  struct ProfRequest_t* next;
  struct ProfRequest_t* prev;

  // key
  MPI_Request* request;

  // key
  OpTy_t op_ty;
  MPI_Comm app_comm; // application communicator
  int prof_rank;     // profiling server rank; for recvs, may be MPI_ANY_SOURCE
  int tag;           // may be MPI_ANY_TAG

  // additional necessary MPI data
  MPI_Datatype datatype;
  int count;

#if (My_DoPaths)
  MPI_Request rcv_req;
  PathSet_t rcv_paths;
#elif (My_DoBlameShifting)
  cct_node_t* cct_node;
#else
#  error "unimplemented"
#endif

  int flags; // ProfRequestFlg (combine with op_ty?)

} ProfRequest_t;


static inline void
ProfRequest_init(ProfRequest_t* x)
{
  x->freelist_next = NULL;

  x->next = NULL;
  x->prev = NULL;

  x->request = NULL;
 
  x->op_ty = OpTy_NULL;
  x->app_comm = MPI_COMM_NULL;
  x->prof_rank = MPI_PROC_NULL;
  x->tag = MPI_UNDEFINED;

  x->datatype = MPI_DATATYPE_NULL;
  x->count = 0;

#if (My_DoPaths)
  x->rcv_req = MPI_REQUEST_NULL;
  x->rcv_paths = PathSet_NULL;
#elif (My_DoBlameShifting)
  x->cct_node = NULL;
#else
#  error "unimplemented"
#endif

  x->flags = ProfRequestFlg_NULL;
}


static inline void
ProfRequest_init1(ProfRequest_t* x, MPI_Request *request,
		  OpTy_t op_ty, MPI_Comm app_comm, int prof_rank, int tag,
		  MPI_Datatype datatype, int count, cct_node_t* cct_node)
{
  x->freelist_next = NULL;

  x->next = NULL;
  x->prev = NULL;

  x->request = request;

  x->op_ty = op_ty;
  x->app_comm = app_comm;
  x->prof_rank = prof_rank;
  x->tag = tag;

  x->datatype = datatype;
  x->count = count;

#if (My_DoPaths)
  x->rcv_req = MPI_REQUEST_NULL;
  x->rcv_paths = PathSet_NULL;
#elif (My_DoBlameShifting)
  x->cct_node = cct_node;
#else
#  error "unimplemented"
#endif

  x->flags = ProfRequestFlg_NULL;
}


static inline void
ProfRequest_dump(const ProfRequest_t* x,
		 const char* ctxt_str1, const char* ctxt_str2)
{
  const int str_sz = 196;
  char str_dbg[str_sz];

  const int cmm_str_sz = 16;
  char cmm_str[cmm_str_sz];
  mpicomm_str(x->app_comm, cmm_str, cmm_str_sz);

  const int rnk_str_sz = 16;
  char rnk_str[rnk_str_sz];
  mpirank_str(x->prof_rank, rnk_str, rnk_str_sz);

  const int tag_str_sz = 16;
  char tag_str[tag_str_sz];
  mpitag_str(x->tag, tag_str, tag_str_sz);

  const char* msg1 = (ctxt_str1) ? ctxt_str1 : "";
  const char* msg2 = (ctxt_str2) ? ctxt_str2 : "";
  
  snprintf(str_dbg, str_sz,
	   "<req %p><op %s, comm %s, rank %s, tag %s>[ty %d, cnt %d](free_nxt %p nxt %p prv %p)",
	   x->request,
	   OpTy_str(x->op_ty), cmm_str, rnk_str, tag_str,
	   (int)x->datatype, x->count,
	   x->freelist_next, x->next, x->prev);
  str_dbg[str_sz - 1] = '\0';

  TMSG(MPI, "%s:%s: %s", msg1, msg2, str_dbg);
}


static inline void
ProfRequest_dump1(MPI_Request* request, const char* ctxt_str)
{
  ProfRequest_t x;
  ProfRequest_init(&x);
  x.request = request;
  ProfRequest_dump(&x, ctxt_str, NULL);
}


static inline void
ProfRequest_dump2(MPI_Request* request, OpTy_t op_ty, MPI_Comm app_comm,
		  int prof_rank, int tag, const char* ctxt_str)
{
  ProfRequest_t x;
  ProfRequest_init1(&x, request, op_ty, app_comm, prof_rank, tag,
		     MPI_DATATYPE_NULL, 0, NULL);
  ProfRequest_dump(&x, ctxt_str, NULL);
}


//***************************************************************************

// ProfRequestMap_t: queue of 'mpi requests'
//   'q_head' points to oldest element
//   'q_tail' points to newest element
typedef struct ProfRequestMap_t {
  ProfRequest_t* q_head;
  ProfRequest_t* q_tail;
  
  int size; // convenience

} ProfRequestMap_t;


static inline void
ProfRequestMap_init(ProfRequestMap_t* map)
{
  map->q_head = NULL;
  map->q_tail = NULL;
  map->size = 0;
}


// ProfRequestMap_find*: search from queue's head
static inline ProfRequest_t*
ProfRequestMap_findByReq(ProfRequestMap_t* map, MPI_Request* request)
{
  for (ProfRequest_t* x = map->q_head; (x); x = x->next) {
    if (x->request == request) {
      return x;
    }
  }
  return NULL;
}


static inline ProfRequest_t*
ProfRequestMap_findByOp(ProfRequestMap_t* map, OpTy_t op_ty,
			MPI_Comm app_comm, int prof_rank, int tag)
{
  for (ProfRequest_t* x = map->q_head; (x); x = x->next) {
    // order comparisons to differentiate quickly
    if (x->prof_rank == prof_rank
	&& x->tag == tag
	&& x->app_comm == app_comm
	&& x->op_ty == op_ty) {
      return x;
    }
  }
  return NULL;
}


static inline ProfRequest_t*
ProfRequestMap_match(ProfRequestMap_t* map, OpTy_t op_ty, MPI_Comm app_comm,
		     int prof_rank, int tag)
{
  // N.B.: recv may use MPI_ANY_SOURCE and MPI_ANY_TAG ; send may not
  int n = 0;
  for (ProfRequest_t* x = map->q_head; (x); x = x->next, ++n) {
    if ( (/* case 1: local queue has send operation */
	  (x->op_ty == OpTy_send && op_ty == OpTy_recv)
	  && (x->app_comm == app_comm)
	  && (x->prof_rank == prof_rank) /* MPI_ANY_SOURCE is resolved */
	  && (x->tag == tag || tag == MPI_ANY_TAG))
	 ||
	 (/* case 2: local queue has recv operation */
	  (x->op_ty == OpTy_recv && op_ty == OpTy_send)
	  && (x->app_comm == app_comm)
	  && (x->prof_rank == prof_rank || x->prof_rank == MPI_ANY_SOURCE)
	  && (x->tag == tag || x->tag == MPI_ANY_TAG)) ) {
      //EMSG("ProfRequestMap_match: found %dth (of %d)", n + 1, map->size);
      return x;
    }
  }

  //EMSG("ProfRequestMap_match: not found (of %d)", map->size);
  return NULL;
}


// ProfRequestMap_insert: insert on queue's tail
static inline void
ProfRequestMap_insert(ProfRequestMap_t* map, ProfRequest_t* x)
{
  if (0 && debug_flag_get(DBG_PREFIX(MPI))) {
    ProfRequest_dump(x, "ProfRequestMap_insert", NULL);
  }

  x->next = NULL;
  x->prev = NULL;
  
  if (!map->q_tail) {
    // Case 1: empty
    map->q_head = x;
    map->q_tail = x;
  }
  else {
    // Case 2: non-empty
    map->q_tail->next = x;
    x->prev = map->q_tail;
    map->q_tail = x;
  }

#if (My_DoBlameShifting)
  hpc_setFlag(&(x->flags), ProfRequestFlg_inContainer);
#endif

  map->size++;
}


static inline void
ProfRequestMap_delete(ProfRequestMap_t* map, ProfRequest_t* x,
		      const char* ctxt_str)
{
  if (!x) {
    return;
  }

  if (0 && debug_flag_get(DBG_PREFIX(MPI))) {
    ProfRequest_dump(x, ctxt_str, "ProfRequestMap_delete");
  }

  // must be non-empty
  assert(map->q_head && map->q_tail);

  if (x == map->q_head) {
    // Case 1: head of queue (and queue size >= 1)
    if (x->next) {
      x->next->prev = NULL;
    }
    map->q_head = x->next;
    
    // Special case: queue becomes empty
    if (!map->q_head) {
      map->q_tail = NULL;
    }
  }
  else if (x == map->q_tail) {
    // Case 2: tail of queue (and queue size >= 2)
    x->prev->next = NULL;
    map->q_tail = x->prev;
  }
  else {
    // Case 3: middle of queue (and queue size >= 3)
    x->prev->next = x->next;
    x->next->prev = x->prev;
  }

  x->next = NULL;
  x->prev = NULL;

#if (My_DoBlameShifting)
  hpc_unsetFlag( &(x->flags), ProfRequestFlg_inContainer);
#endif

  map->size--;
}

//***************************************************************************

#endif // _HPCRUN_MPI_OVERRIDES_H_
