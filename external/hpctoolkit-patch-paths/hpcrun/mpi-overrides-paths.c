// -*-Mode: C++;-*- // technically C99

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// --------------------------------------------------------------------------
// Part of HPCToolkit (hpctoolkit.org)
//
// Information about sources of support for research and development of
// HPCToolkit is at 'hpctoolkit.org' and in 'README.Acknowledgments'.
// --------------------------------------------------------------------------
//
// Copyright ((c)) 2002-2016, Rice University
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
// * Neither the name of Rice University (RICE) nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// This software is provided by RICE and contributors "as is" and any
// express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular
// purpose are disclaimed. In no event shall RICE or contributors be
// liable for any direct, indirect, incidental, special, exemplary, or
// consequential damages (including, but not limited to, procurement of
// substitute goods or services; loss of use, data, or profits; or
// business interruption) however caused and on any theory of liability,
// whether in contract, strict liability, or tort (including negligence
// or otherwise) arising in any way out of the use of this software, even
// if advised of the possibility of such damage.
//
// ******************************************************* EndRiceCopyright *

//***************************************************************************
//
// File:
// $HeadURL$
//
// Purpose:
// "MPI Critical Path" sampling source.
//
// Authors:
// Nathan Tallent and Abhinav Vishnu
//
//***************************************************************************


//***************************************************************************
// system includes
//***************************************************************************

#include <sys/types.h>
#include <stdio.h>
#include <ucontext.h>
#include <unistd.h>
#include <limits.h>
#include <assert.h>

//***************************************************************************
// MPI
//***************************************************************************

#include <mpi.h>

//***************************************************************************
// libmonitor include files
//***************************************************************************

//#include <monitor.h>

//***************************************************************************
// static parameters
//***************************************************************************

#define My_DoPaths 1 /* critical paths */

//***************************************************************************

#define My_DoPathPruning 1

#define My_DoSampleScaling 1

#define PathSet_n_elem 1 /* 1: [100%]; 2: [100%, 0%]; n: [100%, ..., 0%] */

#define My_JoinRule JoinRuleTy_uni_uni /*JoinRuleTy_uni_uni*/

#if (My_DoPathPruning)
# define PathSum_max_elem     (32)
#else
# define PathSum_max_elem     (0)
#endif

//-------------------------------------------------------------

// Only one of PathBuf/SampleBuf is used at a time
// ** Max size should be 64K so that indices fit with uint16_t (PathSum_t) **
#define PathBuf_max_elem   (16 * 1024) /* max 64K */
#define SampleBuf_max_elem (16 * 1024) /* max 64K */

//-------------------------------------------------------------

#define SndPathsBuf_max_elem (2 * 1024)
#define RcvPathsBuf_max_elem (2 * 1024)
#define MpiReq_max_elem      (SndPathsBuf_max_elem * 2)

//-------------------------------------------------------------

// 2^64 us / (2^20 us/s) / 2^4: 2^40 (1 trillion) sec = 300m hours
#define TaskPnt_TimeId_shift (4)
#define TaskPnt_TimeId_mask (0x000000000000000f)
  /* ~(ULONG_MAX << TaskPnt_TimeId_shift) */


//***************************************************************************
// local includes
//***************************************************************************

#include <include/min-max.h>

#include <main.h>
#include <metrics.h>
#include <safe-sampling.h>
#include <sample_event.h>
#include <thread_data.h>
#include <trace.h>

#include <messages/messages.h>
#include <monitor-exts/monitor_ext.h>

#include <sample-sources/mpi-util.h>
#include <sample-sources/mpi-decl.h>
#include <sample-sources/mpi-overrides-paths.h>
#include <sample-sources/mpi-overrides.h>

#include <sample-sources/mpi-paths.h>


#define MyDbg 0
#define My_DoBarrier_All2All 1
#define My_DoBarrier_SingleRoot 1


//***************************************************************************
// Note: Per-process state! Assumes
//   MPI_THREAD_SINGLE, MPI_THREAD_FUNNELED, MPI_THREAD_SERIALIZED
//
// TODO: use __thread
//***************************************************************************

__thread long hpcrun_itimer_scaling_factor = 1;

extern __thread long hpcrun_itimer_period_cur_us;


//-------------------------------------------------------------
// profiling status
//-------------------------------------------------------------

static bool s_is_initialized = false;
static bool s_is_on = false;

static bool s_is_inlib = false;

//-------------------------------------------------------------
// global communicator and rank
//-------------------------------------------------------------

static MPI_Comm  s_world_comm = MPI_COMM_WORLD;
static MPI_Group s_world_group = MPI_GROUP_NULL;

static const int s_rank_root = 0;
static int       s_rank_me = MPI_PROC_NULL;
static int       s_rank_sz = 0;

//-------------------------------------------------------------
// paths
//-------------------------------------------------------------

static bool s_do_paths_full = false;
static bool s_do_paths_lite = false;
static bool s_do_profile = false;

static PathSet_t    s_pathSet;
static PathSetAux_t s_pathSetAux;

static PathBuf_t   s_pathBuf;
static SampleBuf_t s_sampleBuf;

static const double s_taskCoalescingThreshold_us = 1.0;

static bool s_doPathCompletionProtocol = false;

//-------------------------------------------------------------
// sending/joining paths
//-------------------------------------------------------------

static MPI_Comm  s_my_comm = MPI_COMM_NULL;
static MPI_Group s_my_group = MPI_GROUP_NULL;
/*static*/ int   s_prof_tag = MPI_UNDEFINED;

static const int s_pathsSz = sizeof(PathSet_t);

// temporary space for selected paths
static PathSet_t s_selectPaths_barrier;

#if (PathSet_n_elem > 2)
static PathSet_t s_selectPaths_join;
#endif

static void*     s_sndPathsBuf = NULL;
static const int s_sndPathsBufSz = (SndPathsBuf_max_elem * sizeof(PathSet_t));

static ProfRequestMap_t s_mpiReqMap; // pending requests

static Mem_t     s_mpiReq_mem;
static const int s_mpiReq_memSz = (MpiReq_max_elem * sizeof(ProfRequest_t));

//-------------------------------------------------------------
// statistics, extra
//-------------------------------------------------------------

static double s_timeBeg_s = 0.0;
static double s_timeEnd_s = 0.0;

static int s_barrier_lvl = 0; // FIXME: why is this needed?
static long s_n_phase = 0;


//***************************************************************************
// 
//***************************************************************************

#define mpiPaths_lib_enter() { /* N.B.: idempotent */			\
    s_is_inlib = true;   /* disable samples into mpi-paths */		\
    hpcrun_safe_enter(); /* disable itimer samples */			\
  }

#define mpiPaths_lib_exit()  {					  \
    hpcrun_safe_exit();	 /* enable itimer samples */		  \
    s_is_inlib = false;	 /* enable samples into mpi-paths */	  \
  }

#define mpiPaths_collective1root(comm)		\
  mpiOp_barrier(comm)


#define packed64_upper(x)			\
  (((uint64_t)(x)) >> 32)


//***************************************************************************

static void
mpiPaths_initOverrides();

static void
mpiPaths_initState();

static void
mpiPaths_path_beg();

//***************************************************************************

#define mpiOp_x_beg(doBuffer, ctxt)				\
  mpiOp_x_beg_(/*doCoalesce*/false, doBuffer, ctxt)

#define mpiOp_x_beg_coalesce(doBuffer, ctxt)	         	\
  mpiOp_x_beg_(/*doCoalesce*/true, doBuffer, ctxt)

static inline uint64_t
mpiOp_x_beg_(bool doCoalesce, bool doBuffer, const char* ctxt);

static inline void
mpiOp_snd_end(uint64_t opBeg_tsc, int count, MPI_Datatype datatype,
	      int dest, int tag, MPI_Comm comm);

static inline void
mpiOp_isnd_end(uint64_t opBeg_tsc, int count, MPI_Datatype datatype,
	       int dest, int tag, MPI_Comm comm, MPI_Request* request);

static inline void
mpiOp_rcv_end(uint64_t opBeg_tsc, int count, MPI_Datatype datatype,
	      int source, int tag, MPI_Comm comm);

static inline void
mpiOp_ircv_end(uint64_t opBeg_tsc, int count, MPI_Datatype datatype,
	       int source, int tag, MPI_Comm comm, MPI_Request* request);

static inline ProfRequest_t*
mpiOp_start_beg(uint64_t* taskEnd_tsc, MPI_Request* request, const char* ctxt);

static inline void
mpiOp_start_end(uint64_t opBeg_tsc, ProfRequest_t* myreq);

static inline void
mpiOp_wait_end(uint64_t opBeg_tsc, MPI_Request* request, int app_src);

static inline void
mpiOp_xinit_beg(OpTy_t op_ty, int count, MPI_Datatype datatype,
		int rank, int tag, MPI_Comm comm, MPI_Request* request);

static inline void
mpiOp_req_free(MPI_Request* request);


//---------------------------------------------------------------------------

static inline uint64_t
mpiOp_barrier_beg(const char* ctxt);

static inline int
mpiOp_barrier(int comm);

static inline void
mpiOp_barrier_end(uint64_t opBeg_tsc, const char* ctxt);

static inline uint64_t
mpiOp_collective1root_beg(const char* ctxt);

static inline void
mpiOp_collective1root_end(uint64_t opBeg_tsc, const char* ctxt);


//***************************************************************************

static inline void
mkPaths_task_beg(uint64_t opBeg_tsc, const char* ctxt);

static inline void
mkPaths_task_end(uint64_t taskEnd_tsc, bool doCoalesce, bool doBuffer,
		 const char* ctxt);

static inline void
mkPaths_bufferPaths();

static inline void
mkPaths_bufferPath(Path_t* x, int pathId);

static inline int
mkPaths_bufferTaskPnt(unsigned int call_path_id, uint metric_id,
		      uint64_t microtime, uint64_t microtime2,
		      uint64_t microtime3);

static inline void
mkPaths_bufferSample(uint64_t timeId, cct_node_t* cct_node, int metric_id,
		     uint64_t real_us);

static inline void
mkPaths_commitPhase();

static inline void
mkPaths_commitPhase_pathsFull();

static inline void
mkPaths_commitPhase_pathsLite();

static inline void
mkPaths_commitPhase_profile();

//---------------------------------------------------------------------------

static inline void
mkPaths_joinPaths_splice(PathSet_t* paths_me, PathSet_t* paths_othr);

static inline void
mkPaths_joinPaths_copy(PathSet_t* paths_top, PathSet_t* paths_othr);

static inline void
mkPaths_selectPaths(PathSet_t* paths_selected,
		    PathSet_t* paths_x, PathSet_t* paths_y);

static inline void
mkPaths_sumPaths(PathJoinSum_t* pathJoinSumV, int pathJoinSum_n_elem,
		 const PathSet_t* x_paths, const PathSet_t* y_paths);

static inline void
mkPaths_sortPaths(PathJoinSum_t* pathJoinSumV, int n_elem);

static inline PathSum_t*
mkPaths_mergePathSums(PathSum_t* new_pathsum, int rank_me);

static int
mkPaths_pathAllreduce(PathSet_t* paths, MPI_Comm comm);

//---------------------------------------------------------------------------

static inline void
mkPaths_addTaskPnt(double cost_us);

static inline sample_val_t
mkPaths_backtrace(int metricId, uint64_t metricIncr);

static inline ProfRequest_t*
mkPaths_makeReq(MPI_Request *request, OpTy_t op_ty, MPI_Comm app_comm,
		int app_rank, int tag, MPI_Datatype datatype, int count);

static inline ProfRequest_t*
mkPaths_findReq(MPI_Request *request);

//***************************************************************************

static inline void
pathSet_snd(PathSet_t* paths, int prof_rank);

static inline void
pathSet_rcv(PathSet_t* paths, int prof_rank);

static inline void
pathSet_ircv(ProfRequest_t* myreq);

static inline void
pathSet_wait(ProfRequest_t* myreq);

static inline void
pathSet_waitrcv(ProfRequest_t* myreq, int app_src);

//***************************************************************************

static inline ProfRequest_t*
ProfRequest_malloc();

static inline void
ProfRequest_free(ProfRequest_t* x);

static inline int
xlateRank_app2prof(int app_rank, MPI_Comm app_comm);


//***************************************************************************
// [Public] Annotation hooks for MPI-Paths control
//***************************************************************************

extern void
palm_path_beg()
{
  mpiPaths_lib_enter();
  
  if (hpcrun_mpiPaths_period <= 0) {
    hpcrun_mpiPaths_period = 1;
  }
  s_is_on = true;

  mpiPaths_path_beg();

  mpiPaths_lib_exit();
}

void palm_path_beg_ (void) __attribute__ ((alias ("palm_path_beg")));
void palm_path_beg__(void) __attribute__ ((alias ("palm_path_beg")));


extern void
palm_path_end()
{
  mpiPaths_lib_enter();
  
  hpcrun_mpiPaths_fini();

  mpiPaths_lib_exit();
}


void palm_path_end_ (void) __attribute__ ((alias ("palm_path_end")));
void palm_path_end__(void) __attribute__ ((alias ("palm_path_end")));


//***************************************************************************
// [Public] Annotation hooks for Model Tasks
//***************************************************************************

// Corresponds to Palm's 'hpctoolkit_sample_x'. Whereas that function
// records metrics assuming a profile, this function records paths and
// per-task (instance) metrics.

// Notes: 
// 1. We do not formally link model task records. Rather all model
//    records between two task-end-point ids (on the task's rank)
//    belong the current task. Note that the task can still belong to
//    many paths.
// 2. The 'timeId' is not strictly necessary.  However, the
//    HPCTraceViewer trace-record finder expects time ids and would be
//    confused without them.

extern void
palm_task_model_beg()
{
  //if (s_is_on) {
  // s_pathSetAux.taskBeg_tsc = time_getTSC();
  //}
}


extern void
palm_task_model(uint32_t val1, uint32_t val2, uint32_t val3, uint32_t val4)
{
  if (!s_is_on) { return; }

  if (!s_do_paths_full) { return; }

  mpiPaths_lib_enter();

  uint64_t timeId = Path_makeTimeId(&s_pathSet.paths[0], s_rank_me);
  //double cost_us = timeDiff_us(s_pathSetAux.taskBeg_tsc, ...);
  
  uint64_t val12, val34;
  const uint64_t mask = 0x00000000ffffffff;
  val12 = ((uint64_t)val1 << 32) | (mask & (uint64_t)val2); // prev_timeId
  val34 = ((uint64_t)val3 << 32) | (mask & (uint64_t)val4); // cost_us
  
  // 'cct_node', which has the persistent trace id, is a
  // "function-proxy" node.  Because the model region calls this
  // function, the "function-proxy" node will be the first instruction
  // of this routine.
  sample_val_t bt_val = mkPaths_backtrace(s_pathSetAux.metricIdV[0], 0);
  cct_node_t* cct_node = bt_val.trace_node;
  uint cctNodeId = hpcrun_cct_persistent_id(cct_node);
    
  const int type_flg = TaskPntTy_model_flag; // prev_rank
  // any negative number smaller than -1
  
  mkPaths_bufferTaskPnt(cctNodeId, // call path id
			type_flg,  // metric_id
			timeId,    // microtime
			val12,     // microtime2
			val34);    // microtime3

  s_pathSetAux.requireNewTask = true;

  mpiPaths_lib_exit();  
}


extern void
palm_task_model_f_(int* val1, int* val2, int* val3, int* val4)
{
  // Fortran does not have an "unsigned integer"
  palm_task_model((uint32_t)*val1, (uint32_t)*val2,
		  (uint32_t)*val3, (uint32_t)*val4);
}


extern void
palm_task_model_f__(int* val1, int* val2, int* val3, int* val4)
  __attribute__ ((alias ("palm_task_model_f_")));


//***************************************************************************

extern void
palm_get_path_info(double* metric_val, int32_t path_i)
{
  if (!s_is_on) { return; }

  //if (!s_do_paths_lite) { return; }

  mpiPaths_lib_enter();

  *metric_val = 0;

  assert(s_pathSet.n_elem >= 1);
  //if (path_i < s_pathSet.n_elem) { }
  
  // (path-cost - local-task-cost) as s
  double me_us = s_pathSetAux.task_me_us;
  double cp_us = s_pathSet.paths[0].path_cost_us;

  switch (path_i) {
  case 0:
    *metric_val = ((cp_us - me_us) / 1.0e6);
    break;
  case 1:
    if (cp_us > 0.0001) {
      *metric_val = (100.0 * me_us / cp_us);
    }
    break;
  default:
    assert(false);
  }
  
  mpiPaths_lib_exit();
}


extern void
palm_get_path_info_f_(double* metric_val, int32_t* path_i)
{
  palm_get_path_info(metric_val, *path_i);
}


extern void
palm_get_path_info_f__(double* metric_val, int32_t* path_i)
  __attribute__ ((alias ("palm_get_path_info_f_")));


//***************************************************************************
// [Public] MPI-Paths Control
//***************************************************************************

void
hpcrun_mpiPaths_init()
{
  TMSG(MPI, __func__);
  
  // invalidate all library state
  s_is_initialized = false;
  s_is_on = false;

  mpiPaths_initOverrides();

  mpiPaths_initState();
  
  s_is_initialized = true;
}


void
hpcrun_mpiPaths_start()
{
  TMSG(MPI, __func__);

  s_is_on = (hpcrun_mpiPaths_period > 0);

  AMSG("MPI Paths: on=%d, paths-full/lite/prof=%d/%d/%d (PathSet bytes %d)",
       s_is_on, s_do_paths_full, s_do_paths_lite, s_do_profile, s_pathsSz);

  s_timeBeg_s = PMPI_Wtime();

  mpiPaths_path_beg();
}


void
hpcrun_mpiPaths_fini()
{
  // N.B.: MPI_Finalize has been called but not completed, so MPI
  // calls are valid.
  int rc;

  TMSG(MPI, __func__);

  // Ensure paths are completed w.r.t. 'world' communicator
  s_doPathCompletionProtocol = true;
  rc = PMPI_Barrier(s_world_comm);
  assert(rc == MPI_SUCCESS);
  

  s_timeEnd_s = PMPI_Wtime();

  double app_s = (s_timeEnd_s - s_timeBeg_s);
  double task_me_s  = s_pathSetAux.task_me_us / 1.0e6;
  double mpiOp_me_s = s_pathSetAux.mpiOp_me_us / 1.0e6;

  AMSG("MPI Paths: app time (s): %g", app_s);
  AMSG("MPI Paths: rank %d's task & MPI-op cost (s): %g, %g", s_rank_me, task_me_s, mpiOp_me_s);
  PathSet_dump(&s_pathSet, /*doXtra*/false, "MPI Paths");
  PathSetAux_dump(&s_pathSetAux, "MPI Paths");

  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------
  
  s_is_on = false;
  s_is_initialized = false;

  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  rc = PMPI_Group_free(&s_world_group);
  assert(rc == MPI_SUCCESS);

  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  if (s_sndPathsBuf) {
    void* buf = NULL;
    int buf_sz = 0;
    rc = PMPI_Buffer_detach(&buf, &buf_sz);
    assert(rc == MPI_SUCCESS);
  }

  rc = PMPI_Comm_free(&s_my_comm);
  assert(rc == MPI_SUCCESS);

  rc = PMPI_Group_free(&s_my_group);
  assert(rc == MPI_SUCCESS);
}


//***************************************************************************
// [Public] MPI-Paths Sample
//***************************************************************************

void
hpcrun_mpiPaths_sample(uint64_t time_us, cct_node_t* cct_node,
		       int metric_id, uint64_t real_us)
{
  if (!s_is_on) { return; }

  if (!s_do_profile) { return; }

  if (s_is_inlib) {
    EMSG("%s: drop sample %d:%ld, metric: %d:%ld", __func__, s_rank_me, time_us, metric_id, real_us);
    return;
  }

  // driver set 'hpcrun_thread_suppress_sample' for non-master threads

  uint64_t timeId = time2timeId(time_us); // Path_makeTimeId(NULL, s_rank_me);
  
  mkPaths_bufferSample(timeId, cct_node, metric_id, real_us);
}


//***************************************************************************
// [Public] MPI Point-to-point overrides for Synchronization Tasks
//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Request_free)(parms_PMPI_Request_free)
{
  mpiOp_req_free(request);

  int rc = real_PMPI_Request_free(request);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Request_free(parms_PMPI_Request_free)
{
  return PMPI_Request_free(request);
}
#endif

//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Send)(parms_PMPI_Send)
{
  uint64_t opBeg_tsc = mpiOp_x_beg(/*doBuffer*/true, __func__);

  int rc = real_PMPI_Send(buf, count, datatype, dest, tag, comm);

  mpiOp_snd_end(opBeg_tsc, count, datatype, dest, tag, comm);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Send(parms_PMPI_Send)
{
  return PMPI_Send(buf, count, datatype, dest, tag, comm);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Bsend)(parms_PMPI_Bsend)
{
  uint64_t opBeg_tsc = mpiOp_x_beg(/*doBuffer*/true, __func__);

  int rc = real_PMPI_Bsend(buf, count, datatype, dest, tag, comm);

  mpiOp_snd_end(opBeg_tsc, count, datatype, dest, tag, comm);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Bsend(parms_PMPI_Bsend)
{
  return PMPI_Bsend(buf, count, datatype, dest, tag, comm);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Isend)(parms_PMPI_Isend)
{
  uint64_t opBeg_tsc = mpiOp_x_beg(/*doBuffer*/true, __func__);

  int rc = real_PMPI_Isend(buf, count, datatype, dest, tag, comm, request);

  mpiOp_isnd_end(opBeg_tsc, count, datatype, dest, tag, comm, request);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Isend(parms_PMPI_Isend)
{
  return PMPI_Isend(buf, count, datatype, dest, tag, comm, request);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Ibsend)(parms_PMPI_Ibsend)
{
  uint64_t opBeg_tsc = mpiOp_x_beg(/*doBuffer*/true, __func__);

  int rc = real_PMPI_Ibsend(buf, count, datatype, dest, tag, comm, request);

  mpiOp_isnd_end(opBeg_tsc, count, datatype, dest, tag, comm, request);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Ibsend(parms_PMPI_Ibsend)
{
  return PMPI_Ibsend(buf, count, datatype, dest, tag, comm, request);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Issend)(parms_PMPI_Issend)
{
  uint64_t opBeg_tsc = mpiOp_x_beg(/*doBuffer*/true, __func__);

  int rc = real_PMPI_Issend(buf, count, datatype, dest, tag, comm, request);

  mpiOp_isnd_end(opBeg_tsc, count, datatype, dest, tag, comm, request);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Issend(parms_PMPI_Issend)
{
  return PMPI_Issend(buf, count, datatype, dest, tag, comm, request);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Irsend)(parms_PMPI_Irsend)
{
  uint64_t opBeg_tsc = mpiOp_x_beg(/*doBuffer*/true, __func__);

  int rc = real_PMPI_Irsend(buf, count, datatype, dest, tag, comm, request);

  mpiOp_isnd_end(opBeg_tsc, count, datatype, dest, tag, comm, request);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Irsend(parms_PMPI_Irsend)
{
  return PMPI_Irsend(buf, count, datatype, dest, tag, comm, request);
}
#endif

//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Send_init)(parms_PMPI_Send_init)
{
  mpiOp_xinit_beg(OpTy_send, count, datatype, dest, tag, comm, request);
  
  int rc = real_PMPI_Send_init(buf, count, datatype, dest, tag, comm, request);

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Send_init(parms_PMPI_Send_init)
{
  return PMPI_Send_init(buf, count, datatype, dest, tag, comm, request);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Bsend_init)(parms_PMPI_Bsend_init)
{
  mpiOp_xinit_beg(OpTy_send, count, datatype, dest, tag, comm, request);
  
  int rc = real_PMPI_Bsend_init(buf, count, datatype, dest, tag, comm, request);

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Bsend_init(parms_PMPI_Bsend_init)
{
  return PMPI_Bsend_init(buf, count, datatype, dest, tag, comm, request);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Ssend_init)(parms_PMPI_Ssend_init)
{
  mpiOp_xinit_beg(OpTy_send, count, datatype, dest, tag, comm, request);
  
  int rc = real_PMPI_Ssend_init(buf, count, datatype, dest, tag, comm, request);

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Ssend_init(parms_PMPI_Ssend_init)
{
  return PMPI_Ssend_init(buf, count, datatype, dest, tag, comm, request);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Rsend_init)(parms_PMPI_Rsend_init)
{
  mpiOp_xinit_beg(OpTy_send, count, datatype, dest, tag, comm, request);
  
  int rc = real_PMPI_Rsend_init(buf, count, datatype, dest, tag, comm, request);

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Rsend_init(parms_PMPI_Rsend_init)
{
  return PMPI_Rsend_init(buf, count, datatype, dest, tag, comm, request);
}
#endif


//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Start)(parms_PMPI_Start)
{
  uint64_t opBeg_tsc = 0;
  ProfRequest_t* myreq = mpiOp_start_beg(&opBeg_tsc, request, __func__);

  int rc = real_PMPI_Start(request);

  mpiOp_start_end(opBeg_tsc, myreq);

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Start(parms_PMPI_Start)
{
  return PMPI_Start(request);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Startall)(parms_PMPI_Startall)
{
  if (!s_is_on) {
    return real_PMPI_Startall(count, array_of_requests);
  }

  //----------------------------------------------------------
  //----------------------------------------------------------

  mpiPaths_lib_enter();
  
  typedef struct data_t {
    ProfRequest_t* req;
  } data_t;

  data_t dataA[count];
  memset(dataA, 0, sizeof(data_t) * count);

  bool do_snd = false;
  int i;

  for (i = 0; i < count; ++i) {
    dataA[i].req = mkPaths_findReq(&(array_of_requests[i]));
    if (dataA[i].req->op_ty == OpTy_send) {
      do_snd = true;
    }
  }

  // cf. mpiOp_start_beg()
  uint64_t opBeg_tsc = 0;
  if (do_snd) {
    opBeg_tsc = mpiOp_x_beg(/*doBuffer*/true, __func__);
  }
  else {
    opBeg_tsc = mpiOp_x_beg_coalesce(/*doBuffer*/false, __func__);
  }

  //--------------------------------------

  int rc = real_PMPI_Startall(count, array_of_requests);

  // cf. mpiOp_start_end()
  for (i = 0; i < count; ++i) {
    ProfRequest_t* myreq = dataA[i].req;
    if (myreq->op_ty == OpTy_send) { // cf. mpiOp_isnd_end()
      pathSet_snd(&s_pathSet, myreq->prof_rank);
    }
    else if (myreq->op_ty == OpTy_recv) { // cf. mpiOp_ircv_end()
      // N.B.: 'source' may be MPI_ANY_SOURCE
      if (myreq->prof_rank != MPI_ANY_SOURCE) {
	pathSet_ircv(myreq);
      }
    }
  }

  //--------------------------------------
  
  mkPaths_task_beg(opBeg_tsc, __func__);

  mpiPaths_lib_exit();
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Startall(parms_PMPI_Startall)
{
  return PMPI_Startall(count, array_of_requests);
}
#endif


//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Recv)(parms_PMPI_Recv)
{
  uint64_t opBeg_tsc = mpiOp_x_beg(/*doBuffer*/false, __func__);

  int rc = 0;
  int my_source = MPI_PROC_NULL;

  if (status != MPI_STATUS_IGNORE) {
    rc = real_PMPI_Recv(buf, count, datatype, source, tag, comm, status);
    my_source = getSrcRank(source, status, /*mustSucceed*/true);
  }
  else {
    MPI_Status my_status;
    rc = real_PMPI_Recv(buf, count, datatype, source, tag, comm, &my_status);
    my_source = getSrcRank(source, &my_status, /*mustSucceed*/true);
  }

  mpiOp_rcv_end(opBeg_tsc, count, datatype, my_source, tag, comm);

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Recv(parms_PMPI_Recv)
{
  return PMPI_Recv(buf, count, datatype, source, tag, comm, status);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Irecv)(parms_PMPI_Irecv)
{
  uint64_t opBeg_tsc = mpiOp_x_beg_coalesce(/*doBuffer*/false, __func__);

  int rc = real_PMPI_Irecv(buf, count, datatype, source, tag, comm, request);
  
  mpiOp_ircv_end(opBeg_tsc, count, datatype, source, tag, comm, request);

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Irecv(parms_PMPI_Irecv)
{
  return PMPI_Irecv(buf, count, datatype, source, tag, comm, request);
}
#endif


//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Recv_init)(parms_PMPI_Recv_init)
{
  mpiOp_xinit_beg(OpTy_recv, count, datatype, source, tag, comm, request);

  int rc = real_PMPI_Recv_init(buf, count, datatype, source, tag, comm, request);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Recv_init(parms_PMPI_Recv_init)
{
  return PMPI_Recv_init(buf, count, datatype, source, tag, comm, request);
}
#endif


//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Sendrecv)(parms_PMPI_Sendrecv)
{
  if (!s_is_on) {
    return real_PMPI_Sendrecv(sendbuf, sendcount, sendtype, dest, sendtag,
			      recvbuf, recvcount, recvtype, source, recvtag,
			      comm, status);
  }

  //----------------------------------------------------------

  uint64_t opBeg_tsc = mpiOp_x_beg(/*doBuffer*/true, __func__);

  int rc = 0;
  int rc1 = 0, rc2 = 0;

  const int count = 2, rcv_i = 0, snd_i = 1;
  MPI_Request reqA[count];

  ProfRequest_t* mysnd = NULL, *myrcv = NULL;
  int myrcv_app_src = MPI_PROC_NULL;

  rc1 = real_PMPI_Irecv(recvbuf, recvcount, recvtype, source, recvtag,
			comm, &reqA[rcv_i]);
  rc2 = real_PMPI_Isend(sendbuf, sendcount, sendtype, dest, sendtag,
			comm, &reqA[snd_i]);
  assert(rc1 == MPI_SUCCESS && rc2 == MPI_SUCCESS);

  //--------------------------------------
  // cf. mpiOp_ircv_end() and mpiOp_isnd_end().

  // N.B.: 'source' may be MPI_ANY_SOURCE
  myrcv = mkPaths_makeReq(&reqA[rcv_i], OpTy_recv, comm, source,
			  recvtag, recvtype, recvcount);
  if (myrcv->prof_rank != MPI_ANY_SOURCE) {
    pathSet_ircv(myrcv);
  }
  
  mysnd = mkPaths_makeReq(&reqA[snd_i], OpTy_send, comm, dest,
			  sendtag, sendtype, sendcount);
  pathSet_snd(&s_pathSet, mysnd->prof_rank);

  //--------------------------------------
  
  for (int i = 0; i < count; ++i) {
    int idx = MPI_UNDEFINED;
    MPI_Status mystatus;
    rc += real_PMPI_Waitany(count, reqA, &idx, &mystatus);

    if (idx != MPI_UNDEFINED && idx == rcv_i) {
      myrcv_app_src = mystatus.MPI_SOURCE;
      if (status != MPI_STATUS_IGNORE) {
	// copy 'MPI_Get_count' as well as MPI_SOURCE, MPI_TAG, & MPI_ERROR
	memcpy(status, &mystatus, sizeof(MPI_Status));
      }
    }
  }

  //--------------------------------------
  // receive and join paths [cf. mpiOp_wait_end()]

  pathSet_waitrcv(myrcv, myrcv_app_src);
  mkPaths_joinPaths_splice(&s_pathSet, &(myrcv->rcv_paths));

  mkPaths_bufferPaths();

  ProfRequestMap_delete(&s_mpiReqMap, myrcv, __func__);
  ProfRequest_free(myrcv);

  ProfRequestMap_delete(&s_mpiReqMap, mysnd, __func__);
  ProfRequest_free(mysnd);

  mkPaths_task_beg(opBeg_tsc, "");

  mpiPaths_lib_exit();

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Sendrecv(parms_PMPI_Sendrecv)
{
  return PMPI_Sendrecv(sendbuf, sendcount, sendtype, dest, sendtag,
		       recvbuf, recvcount, recvtype, source, recvtag,
		       comm, status);
}
#endif


//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Wait)(parms_PMPI_Wait)
{
  uint64_t opBeg_tsc = mpiOp_x_beg(/*doBuffer*/false, __func__);

  int rc = 0;
  int source = MPI_PROC_NULL;

  if (status != MPI_STATUS_IGNORE) {
    rc = real_PMPI_Wait(request, status);
    source = status->MPI_SOURCE;
  }
  else {
    MPI_Status my_status;
    rc = real_PMPI_Wait(request, &my_status);
    source = my_status.MPI_SOURCE;
  }

  mpiOp_wait_end(opBeg_tsc, request, source);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Wait(parms_PMPI_Wait)
{
  return PMPI_Wait(request, status);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Waitany)(parms_PMPI_Waitany)
{
  int rc = 0;

#if 1
  uint64_t opBeg_tsc = mpiOp_x_beg(/*doBuffer*/false, __func__);

  int source = MPI_PROC_NULL;

  if (status != MPI_STATUS_IGNORE) {
    rc = real_PMPI_Waitany(count, array_of_requests, indx, status);
    source = status->MPI_SOURCE;
  }
  else {
    MPI_Status my_status;
    rc = real_PMPI_Waitany(count, array_of_requests, indx, &my_status);
    source = my_status.MPI_SOURCE;
  }

  assert(*indx != MPI_UNDEFINED);
  mpiOp_wait_end(opBeg_tsc, &(array_of_requests[*indx]), source);
#else

#endif

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Waitany(parms_PMPI_Waitany)
{
  return PMPI_Waitany(count, array_of_requests, indx, status);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Waitall)(parms_PMPI_Waitall)
{
  if (!s_is_on) {
    return real_PMPI_Waitall(count, array_of_requests, array_of_statuses);
  }

  //----------------------------------------------------------
  
  s_pathSetAux.requireNewTask = true;
  uint64_t opBeg_tsc = mpiOp_x_beg(/*doBuffer*/false, __func__);

  int rc = 0, i;

  typedef struct data_t {
    int app_src;
    ProfRequest_t* req;
  } data_t;

  data_t dataA[count];
  memset(dataA, 0, sizeof(data_t) * count);

  //----------------------------------------------------------
  // To overlap profiling work with completion, use a Waitany loop.
  // (We could invoke Waitall and then process all requests.)
  //----------------------------------------------------------
  for (i = 0; i < count; ++i) {
    int idx = MPI_UNDEFINED;
    MPI_Status status;
    rc += real_PMPI_Waitany(count, array_of_requests, &idx, &status);

    if (idx != MPI_UNDEFINED) {
      dataA[idx].app_src = status.MPI_SOURCE;
      dataA[idx].req = mkPaths_findReq(&(array_of_requests[idx]));
      //nbytes_ttl += xlateSz_count2bytes(myreq->count, myreq->datatype);

      if (array_of_statuses != MPI_STATUSES_IGNORE) {
	// copy 'MPI_Get_count' as well as MPI_SOURCE, MPI_TAG, & MPI_ERROR
	memcpy(&(array_of_statuses[idx]), &status, sizeof(MPI_Status));
      }
    }
  }

  // TODO: slack

  //----------------------------------------------------------
  // Receive and join paths [cf. mpiOp_wait_end()]
  //----------------------------------------------------------
  bool did_rcv_paths = false;
  ProfRequest_t rcv_paths_top;
  memset(&rcv_paths_top, 0, sizeof(rcv_paths_top)); // prevent 'unint' warning

  for (i = 0; i < count; ++i) {
    ProfRequest_t* myreq = dataA[i].req;
    if (myreq) { // if completed
      if (myreq->op_ty == OpTy_recv) {
	pathSet_waitrcv(myreq, dataA[i].app_src);
	//AMSG("waitall-rcv-paths: prof/paths rank: %d %d",
	//     myreq->prof_rank, prof_rank);

	if (!did_rcv_paths) {
	  rcv_paths_top.rcv_paths = myreq->rcv_paths;
	  did_rcv_paths = true;
	}
	else {
	  mkPaths_joinPaths_copy(&rcv_paths_top.rcv_paths, &myreq->rcv_paths);
	}
      }

      if (!hpc_isFlag(myreq->flags, ProfRequestFlg_isPersistent)) {
	ProfRequestMap_delete(&s_mpiReqMap, myreq, __func__);
	ProfRequest_free(myreq);
      }
    }
  }

  if (did_rcv_paths) { // implies a recv
    mkPaths_joinPaths_splice(&s_pathSet, &(rcv_paths_top.rcv_paths));
  }

  mkPaths_bufferPaths();

  s_pathSetAux.requireNewTask = true;
  mkPaths_task_beg(opBeg_tsc, __func__);

  mpiPaths_lib_exit();

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Waitall(parms_PMPI_Waitall)
{
  return PMPI_Waitall(count, array_of_requests, array_of_statuses);
}
#endif


//***************************************************************************
// [Public] MPI all-to-all collective overrides for Synchronization Tasks
//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Barrier)(parms_PMPI_Barrier)
{
  uint64_t opBeg_tsc = mpiOp_barrier_beg(__func__);

  int rc = 0;

#if (My_DoBarrier_All2All)
  rc = mpiOp_barrier(comm);
  if (rc < 0) {
    rc = real_PMPI_Barrier(comm);
  }
#else
  rc = real_PMPI_Barrier(comm);
#endif

  mpiOp_barrier_end(opBeg_tsc, __func__);

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Barrier(parms_PMPI_Barrier)
{
  return PMPI_Barrier(comm);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Allreduce)(parms_PMPI_Allreduce)
{
  uint64_t opBeg_tsc = mpiOp_barrier_beg(__func__);

#if (My_DoBarrier_All2All)
  mpiOp_barrier(comm);
#endif

  int rc = real_PMPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);

  mpiOp_barrier_end(opBeg_tsc, __func__);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Allreduce(parms_PMPI_Allreduce)
{
  return PMPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Allgather)(parms_PMPI_Allgather)
{
  uint64_t opBeg_tsc = mpiOp_barrier_beg(__func__);

#if (My_DoBarrier_All2All)
  mpiOp_barrier(comm);
#endif

  int rc = real_PMPI_Allgather(sendbuf, sendcount, sendtype,
			       recvbuf, recvcount, recvtype, comm);

  mpiOp_barrier_end(opBeg_tsc, __func__);

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Allgather(parms_PMPI_Allgather)
{
  return PMPI_Allgather(sendbuf, sendcount, sendtype,
			recvbuf, recvcount, recvtype, comm);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Allgatherv)(parms_PMPI_Allgatherv)
{
  uint64_t opBeg_tsc = mpiOp_barrier_beg(__func__);

#if (My_DoBarrier_All2All)
  mpiOp_barrier(comm);
#endif

  int rc = real_PMPI_Allgatherv(sendbuf, sendcount, sendtype,
				recvbuf, recvcounts, displs, recvtype, comm);

  mpiOp_barrier_end(opBeg_tsc, __func__);

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Allgatherv(parms_PMPI_Allgatherv)
{
  return PMPI_Allgatherv(sendbuf, sendcount, sendtype,
			 recvbuf, recvcounts, displs, recvtype, comm);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Alltoall)(parms_PMPI_Alltoall)
{
  uint64_t opBeg_tsc = mpiOp_barrier_beg(__func__);

#if (My_DoBarrier_All2All)
  mpiOp_barrier(comm);
#endif

  int rc = real_PMPI_Alltoall(sendbuf, sendcount, sendtype,
			      recvbuf, recvcount, recvtype, comm);

  mpiOp_barrier_end(opBeg_tsc, __func__);

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Alltoall(parms_PMPI_Alltoall)
{
  return PMPI_Alltoall(sendbuf, sendcount, sendtype,
		       recvbuf, recvcount, recvtype, comm);
}
#endif


//***************************************************************************
// [Public] MPI single-root collective overrides for Synchronization Tasks
//***************************************************************************

int
MONITOR_EXT_WRAP_NAME(PMPI_Bcast)(parms_PMPI_Bcast)
{
  uint64_t opBeg_tsc = mpiOp_collective1root_beg(__func__);

#if (My_DoBarrier_SingleRoot)
  mpiPaths_collective1root(comm);
#endif

  int rc = real_PMPI_Bcast(buffer, count, datatype, root, comm);

  mpiOp_collective1root_end(opBeg_tsc, __func__);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Bcast(parms_PMPI_Bcast)
{
  return PMPI_Bcast(buffer, count, datatype, root, comm);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Gather)(parms_PMPI_Gather)
{
  uint64_t opBeg_tsc = mpiOp_collective1root_beg(__func__);

#if (My_DoBarrier_SingleRoot)
  mpiPaths_collective1root(comm);
#endif

  int rc = real_PMPI_Gather(sendbuf, sendcount, sendtype, recvbuf, recvcount,
			    recvtype, root, comm);

  mpiOp_collective1root_end(opBeg_tsc, __func__);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Gather(parms_PMPI_Gather)
{
  return PMPI_Gather(sendbuf, sendcount, sendtype, recvbuf, recvcount,
		     recvtype, root, comm);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Gatherv)(parms_PMPI_Gatherv)
{
  uint64_t opBeg_tsc = mpiOp_collective1root_beg(__func__);

#if (My_DoBarrier_SingleRoot)
  mpiPaths_collective1root(comm);
#endif

  int rc = real_PMPI_Gatherv(sendbuf, sendcount, sendtype, recvbuf, recvcounts,
			     displs, recvtype, root, comm);

  mpiOp_collective1root_end(opBeg_tsc, __func__);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Gatherv(parms_PMPI_Gatherv)
{
  return PMPI_Gatherv(sendbuf, sendcount, sendtype, recvbuf, recvcounts,
		      displs, recvtype, root, comm);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Reduce)(parms_PMPI_Reduce)
{
  uint64_t opBeg_tsc = mpiOp_collective1root_beg(__func__);

#if (My_DoBarrier_SingleRoot)
  mpiPaths_collective1root(comm);
#endif

  int rc = real_PMPI_Reduce(sendbuf, recvbuf, count, datatype, op, root, comm);

  mpiOp_collective1root_end(opBeg_tsc, __func__);

  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Reduce(parms_PMPI_Reduce)
{
  return PMPI_Reduce(sendbuf, recvbuf, count, datatype, op, root, comm);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Scatter)(parms_PMPI_Scatter)
{
  uint64_t opBeg_tsc = mpiOp_collective1root_beg(__func__);

#if (My_DoBarrier_SingleRoot)
  mpiPaths_collective1root(comm);
#endif

  int rc = real_PMPI_Scatter(sendbuf, sendcount, sendtype, recvbuf, recvcount,
			     recvtype, root, comm);

  mpiOp_collective1root_end(opBeg_tsc, __func__);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Scatter(parms_PMPI_Scatter)
{
  return PMPI_Scatter(sendbuf, sendcount, sendtype, recvbuf, recvcount,
		      recvtype, root, comm);
}
#endif


int
MONITOR_EXT_WRAP_NAME(PMPI_Scan)(parms_PMPI_Scan)
{
  uint64_t opBeg_tsc = mpiOp_collective1root_beg(__func__);

#if (My_DoBarrier_SingleRoot)
  mpiPaths_collective1root(comm);
#endif

  int rc = real_PMPI_Scan(sendbuf, recvbuf, count, datatype, op, comm);

  mpiOp_collective1root_end(opBeg_tsc, __func__);
  
  return rc;
}

#ifndef HPCRUN_STATIC_LINK /* for GlobalArrays */
int
MPI_Scan(parms_PMPI_Scan)
{
  return PMPI_Scan(sendbuf, recvbuf, count, datatype, op, comm);
}
#endif


//***************************************************************************
// [Private] Direct MPI-Paths helpers
//***************************************************************************

static void
mpiPaths_initOverrides()
{
  TMSG(MPI, __func__);

  //MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Get_count, PMPI_Get_count); // unused
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Request_free, PMPI_Request_free);

  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Send,       PMPI_Send);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Bsend,      PMPI_Bsend);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Isend,      PMPI_Isend);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Ibsend,     PMPI_Ibsend);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Issend,     PMPI_Issend);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Irsend,     PMPI_Irsend);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Send_init,  PMPI_Send_init);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Bsend_init, PMPI_Bsend_init);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Ssend_init, PMPI_Ssend_init);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Rsend_init, PMPI_Rsend_init);

  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Start,      PMPI_Start);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Startall,   PMPI_Startall);

  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Recv,       PMPI_Recv);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Irecv,      PMPI_Irecv);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Recv_init,  PMPI_Recv_init);

  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Sendrecv,   PMPI_Sendrecv);

  //MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Iprobe,     PMPI_Iprobe); // unused
  //MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Test,       PMPI_Test); // unused
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Wait,       PMPI_Wait);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Waitany,    PMPI_Waitany);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Waitall,    PMPI_Waitall);
  //MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Cancel,     PMPI_Cancel); // unused

  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Barrier,    PMPI_Barrier);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Allreduce,  PMPI_Allreduce);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Allgather,  PMPI_Allgather);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Allgatherv, PMPI_Allgatherv);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Alltoall,   PMPI_Alltoall);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Bcast,      PMPI_Bcast);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Gather,     PMPI_Gather);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Gatherv,    PMPI_Gatherv);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Reduce,     PMPI_Reduce);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Scatter,    PMPI_Scatter);
  MONITOR_EXT_GET_NAME_WRAP(real_PMPI_Scan,       PMPI_Scan);

  // MPI_Alltoallv
  // MPI_Scatterv

  // MPI_Group_incl...
  // MPI_Comm_split...
  // MPI_COMM_SELF 
}


static void
mpiPaths_initState()
{
  int rc;

  TMSG(MPI, __func__);

  int is_initialized = 0;
  rc = PMPI_Initialized(&is_initialized);
  assert(rc == MPI_SUCCESS && is_initialized);

  s_world_comm = MPI_COMM_WORLD;

  rc = PMPI_Comm_rank(s_world_comm, &s_rank_me);
  assert(rc == MPI_SUCCESS);

  rc = PMPI_Comm_size(s_world_comm, &s_rank_sz);
  assert(rc == MPI_SUCCESS);

  rc = PMPI_Comm_group(s_world_comm, &s_world_group);
  assert(rc == MPI_SUCCESS);


  //-------------------------------------------------------------
  // paths
  //-------------------------------------------------------------

  s_do_paths_full = s_do_paths_lite = s_do_profile = false;
  
  s_do_paths_full = (hpcrun_trace_isactive());
  if (!s_do_paths_full) {
    s_do_paths_lite = (hpcrun_itimer_period_cur_us == 0);
    s_do_profile = !(s_do_paths_lite);
  }

  //-------------------------------------------------------------
  
  PathSet_init(&s_pathSet);
  assert(s_pathSet.n_elem >= 1);

  PathSetAux_init(&s_pathSetAux, My_JoinRule);
  assert(s_pathSetAux.n_elem == s_pathSet.n_elem);

  //PathSetAux_dump(&s_pathSetAux, "");

  //-------------------------------------------------------------
  
  s_pathBuf = PathBuf_NULL;
  
  if (s_do_paths_full) {
    s_pathBuf.max_elem = PathBuf_max_elem;

    int buf_sz = s_pathBuf.max_elem * sizeof(PathBufElem_t);

    s_pathBuf.buf = hpcrun_malloc(buf_sz);
  }

  if (s_do_paths_full) {
    int mtrc_i = hpcrun_new_metric();
    hpcrun_set_metric_info(mtrc_i, "#TaskPnt-unwinds");
    s_pathSetAux.metricIdV[0] = mtrc_i;
  }

  //-------------------------------------------------------------

  SampleBuf_init(&s_sampleBuf);

  if (s_do_profile) {
    int buf_sz = SampleBuf_max_elem * sizeof(SampleBufElem_t);
    SampleBufElem_t* buf = hpcrun_malloc(buf_sz);
    SampleBuf_initBuf(&s_sampleBuf, buf, SampleBuf_max_elem,
		      &hpcrun_itimer_scaling_factor);

    // Create path metric names
    for (int path_i = 0; path_i < s_pathSet.n_elem; ++path_i) {
      int path_pct = s_pathSetAux.percentileV[path_i];

      const int nameLen = 16;
      char* name = (char*)hpcrun_malloc(nameLen);
      snprintf(name, nameLen, "path %d%% (us)", path_pct);
      name[nameLen - 1] = '\0';
      
      int mtrc_i = hpcrun_new_metric();
      hpcrun_set_metric_info_and_period(mtrc_i, name, MetricFlags_ValFmt_Int,
					1, metric_property_time);
      s_pathSetAux.metricIdV[path_i] = mtrc_i;
    }
  }
  
  
  //-------------------------------------------------------------
  // sending/joining paths
  //-------------------------------------------------------------

  rc = PMPI_Comm_dup(s_world_comm, &s_my_comm);
  assert(rc == MPI_SUCCESS);

  rc = PMPI_Comm_group(s_my_comm, &s_my_group);
  assert(rc == MPI_SUCCESS);

  MPI_Aint* tag_ub;
  int flag = 0;
  rc = PMPI_Comm_get_attr(s_world_comm, MPI_TAG_UB, &tag_ub, &flag);
  assert(rc == MPI_SUCCESS && flag);
  s_prof_tag = *tag_ub;

  void* buf = NULL;
  int buf_sz = 0;
  rc = PMPI_Buffer_detach(&buf, &buf_sz);
  assert(rc == MPI_SUCCESS);
  if (!buf) {
    // TODO: More care needed if application uses PMPI_Buffer_attach
    s_sndPathsBuf = hpcrun_malloc(s_sndPathsBufSz);
    assert(s_sndPathsBuf);

    rc = PMPI_Buffer_attach(s_sndPathsBuf, s_sndPathsBufSz);
    assert(rc == MPI_SUCCESS);
  }
  
  //-------------------------------------------------------------

  ProfRequestMap_init(&s_mpiReqMap);

  void* mpiReq_mem = hpcrun_malloc(s_mpiReq_memSz);
  
  Mem_init(&s_mpiReq_mem, mpiReq_mem, s_mpiReq_memSz, sizeof(ProfRequest_t));

  
  //-------------------------------------------------------------
  // 
  //-------------------------------------------------------------

  s_n_phase = 0;

  s_barrier_lvl = 0;
}


static void
mpiPaths_path_beg()
{
  if (s_is_on) {
    TMSG(MPI, __func__);

    // Create TaskPnt to anchor the first task
    s_pathSetAux.requireNewTask = true;
    mkPaths_task_beg(0, __func__);
  }
}


//***************************************************************************
// [Private] MPI Operation Override helpers, Point-to-Point
//***************************************************************************

static inline uint64_t
mpiOp_x_beg_(bool doCoalesce, bool doBuffer, const char* ctxt)
{
  mpiPaths_lib_enter();

  uint64_t taskEnd_tsc = time_getTSC();

  if (s_is_on) {
    mkPaths_task_end(taskEnd_tsc, doCoalesce, doBuffer, ctxt);
  }

  return taskEnd_tsc;
}


static inline void
mpiOp_snd_end(uint64_t opBeg_tsc, int count, MPI_Datatype datatype,
	      int dest, int tag, MPI_Comm comm)
{
  if (s_is_on) {
    //uint64_t nbytes = xlateSz_count2bytes(count, datatype);

    int prof_dest = xlateRank_app2prof(dest, comm);
    assert(prof_dest >= 0);

    pathSet_snd(&s_pathSet, prof_dest);

    mkPaths_task_beg(opBeg_tsc, "");
  }

  mpiPaths_lib_exit();
}


static inline void
mpiOp_isnd_end(uint64_t opBeg_tsc, int count, MPI_Datatype datatype,
	       int dest, int tag, MPI_Comm comm, MPI_Request* request)
{
  if (s_is_on) {
    //uint64_t nbytes = xlateSz_count2bytes(count, datatype);

    ProfRequest_t* myreq =
      mkPaths_makeReq(request, OpTy_send, comm, dest, tag, datatype, count);

    pathSet_snd(&s_pathSet, myreq->prof_rank);

    mkPaths_task_beg(opBeg_tsc, "");
  }

  mpiPaths_lib_exit();
}


static inline void
mpiOp_rcv_end(uint64_t opBeg_tsc, int count, MPI_Datatype datatype,
	      int source, int tag, MPI_Comm comm)
{
  if (s_is_on) {
    //uint64_t nbytes = xlateSz_count2bytes(count, datatype);

    int prof_src = xlateRank_app2prof(source, comm);
    assert(prof_src >= 0);
  
    PathSet_t paths_othr = PathSet_NULL;
    pathSet_rcv(&paths_othr, prof_src);

    mkPaths_joinPaths_splice(&s_pathSet, &paths_othr);
    
    mkPaths_bufferPaths();
    
    mkPaths_task_beg(opBeg_tsc, "");
  }

  mpiPaths_lib_exit();
}


static inline void
mpiOp_ircv_end(uint64_t opBeg_tsc, int count, MPI_Datatype datatype,
	       int source, int tag, MPI_Comm comm, MPI_Request* request)
{
  if (s_is_on) {
    // N.B.: 'source' may be MPI_ANY_SOURCE
    ProfRequest_t* myreq =
      mkPaths_makeReq(request, OpTy_recv, comm, source, tag, datatype, count);
    if (myreq->prof_rank != MPI_ANY_SOURCE) {
      pathSet_ircv(myreq);
    }

    mkPaths_task_beg(opBeg_tsc, "");
  }

  mpiPaths_lib_exit();
}


static inline ProfRequest_t*
mpiOp_start_beg(uint64_t* taskEnd_tsc, MPI_Request* request, const char* ctxt)
{
  mpiPaths_lib_enter();

  ProfRequest_t* myreq = NULL;
  
  if (s_is_on) {
    *taskEnd_tsc = 0;
    myreq = mkPaths_findReq(request);

    if (myreq->op_ty == OpTy_send) {      // cf. Isend
      *taskEnd_tsc = mpiOp_x_beg(/*doBuffer*/true, ctxt);
    }
    else if (myreq->op_ty == OpTy_recv) { // cf. IRecv
      *taskEnd_tsc = mpiOp_x_beg_coalesce(/*doBuffer*/false, ctxt);
    }
  }

  return myreq;
}


static inline void
mpiOp_start_end(uint64_t opBeg_tsc, ProfRequest_t* myreq)
{
  if (s_is_on) {
    if (myreq->op_ty == OpTy_send) { // cf. mpiOp_isnd_end()
      pathSet_snd(&s_pathSet, myreq->prof_rank);
    }
    else if (myreq->op_ty == OpTy_recv) { // cf. mpiOp_ircv_end()
      // N.B.: 'source' may be MPI_ANY_SOURCE
      if (myreq->prof_rank != MPI_ANY_SOURCE) {
	pathSet_ircv(myreq);
      }
    }
    mkPaths_task_beg(opBeg_tsc, "");
  }

  mpiPaths_lib_exit();
}


static inline void
mpiOp_wait_end(uint64_t opBeg_tsc, MPI_Request* request, int app_src)
{
  if (s_is_on) {
    ProfRequest_t* myreq = mkPaths_findReq(request);
    // N.B.: for recvs, 'myreq->prof_rank' may be MPI_ANY_SOURCE

    //AMSG("%s(req %p, etime %g)", __func__, request, e_time);
  
    if (myreq->op_ty == OpTy_recv) {
      pathSet_waitrcv(myreq, app_src);
    
      //uint64_t nbytes = xlateSz_count2bytes(myreq->count, myreq->datatype);

      mkPaths_joinPaths_splice(&s_pathSet, &(myreq->rcv_paths));
    }

    mkPaths_bufferPaths();

    if (!hpc_isFlag(myreq->flags, ProfRequestFlg_isPersistent)) {
      ProfRequestMap_delete(&s_mpiReqMap, myreq, __func__);
      ProfRequest_free(myreq);
    }

    mkPaths_task_beg(opBeg_tsc, "");
  }

  mpiPaths_lib_exit();
}


static inline void
mpiOp_xinit_beg(OpTy_t op_ty, int count, MPI_Datatype datatype,
		int rank, int tag, MPI_Comm comm, MPI_Request* request)
{
  if (s_is_on) {
    ProfRequest_t* myreq =
      mkPaths_makeReq(request, op_ty, comm, rank, tag, datatype, count);
    
    hpc_setFlag(&(myreq->flags), ProfRequestFlg_isPersistent);
  }
}


static inline void
mpiOp_req_free(MPI_Request* request)
{
  if (s_is_on) {
    ProfRequest_t* myreq = mkPaths_findReq(request);
    ProfRequestMap_delete(&s_mpiReqMap, myreq, __func__);
    ProfRequest_free(myreq);
  }
}


//***************************************************************************
// [Private] MPI Operation Override helpers, Collective
//***************************************************************************

static inline uint64_t
mpiOp_barrier_beg(const char* ctxt)
{
  mpiPaths_lib_enter(); // idempotent
  
  uint64_t taskEnd_tsc = time_getTSC();

  // N.B.: Collectives such as Allgather may be invoked by MPI_Init
  // before library is initialized

  s_barrier_lvl++;

  bool doIt = (s_is_on && s_barrier_lvl == 1);
  if (MyDbg) { AMSG("%s: barrier_beg(doit=%d, lvl=%d)", ctxt, doIt, s_barrier_lvl); }

  if (doIt) {
    //----------------------------------------------------------
    // End task with a TaskPoint that will be committed at barrier
    //   end. The TaskPoint's head may *not* change (as it is sent);
    //   if there is a join, the tail will change (via splicing)
    //----------------------------------------------------------
    s_pathSetAux.requireNewTask = true;
    mkPaths_task_end(taskEnd_tsc, /*doCoalesce*/false, /*doBuffer*/false, ctxt);

    s_n_phase++;
  }

  return taskEnd_tsc;
}


static inline int
mpiOp_barrier(int comm)
{
  int ret = 0;

  bool doIt = (s_is_on && s_barrier_lvl == 1);
  
  // Collectives may be invoked by MPI_Init before library inits
  if (doIt) {
    ret = mkPaths_pathAllreduce(&s_pathSet, comm);
  }
  else {
    ret = -1;
  }

  return ret;
}


static inline void
mpiOp_barrier_end(uint64_t opBeg_tsc, const char* ctxt)
{
  s_barrier_lvl--;
  
  bool doIt = (s_is_on && s_barrier_lvl == 0);
  
  if (MyDbg) { AMSG("%s: barrier_end(doit=%d, lvl=%d)", ctxt, doIt, s_barrier_lvl); }

  if (doIt) {
    //double barrierOp_us = timeElapsed_us(opBeg_tsc);

    static bool deferTskBuf[PathSet_n_elem]; // s_pathSet.n_elem

    //----------------------------------------------------------
    // Adopt (or discard) selected paths. Cf. mkPaths_joinPaths_splice()
    //----------------------------------------------------------
    for (int i = 0; i < s_pathSet.n_elem; ++i) {
      Path_t* path_me = &s_pathSet.paths[i];
      const Path_t* path_selected = &s_selectPaths_barrier.paths[i];
      deferTskBuf[i] = false;
      if (path_selected->tail_rank == s_rank_me) {
	Path_replace(path_me, path_selected);
	//if (!s_doPathCompletionProtocol) {
	// add barrier cost as a "sample" or "task-slack"; aggregate later
	//}
      }
      else {
	Path_splice(path_me, path_selected);
	if (s_do_paths_full) {
	  if (s_doPathCompletionProtocol) { Path_noteDiscarded(path_me); }
	  else { deferTskBuf[i] = true; }
	}
      }
    }

    if (s_do_paths_full) {
      assert(s_pathSetAux.didCoalesceTasks == false); // cf. mkPaths_bufferPaths
      for (int i = 0; i < s_pathSet.n_elem; ++i)
	if (!deferTskBuf[i]) { mkPaths_bufferPath(&s_pathSet.paths[i], i); }
    }

    mkPaths_commitPhase();

    if (s_do_paths_full) {
      for (int i = 0; i < s_pathSet.n_elem; ++i)
	if (deferTskBuf[i]) { mkPaths_bufferPath(&s_pathSet.paths[i], i); }
    }
    
    //----------------------------------------------------------
    // Prepare for new phase
    //----------------------------------------------------------
    s_pathSetAux.requireNewTask = true;
    mkPaths_task_beg(opBeg_tsc, ctxt);
  }

  mpiPaths_lib_exit();
}


static inline uint64_t
mpiOp_collective1root_beg(const char* ctxt)
{
#if (My_DoBarrier_SingleRoot)
  return mpiOp_barrier_beg(ctxt);
#else
  return time_getTSC();
#endif
}


static inline void
mpiOp_collective1root_end(uint64_t opBeg_tsc, const char* ctxt)
{
#if (My_DoBarrier_SingleRoot)
  mpiOp_barrier_end(opBeg_tsc, ctxt);
#endif
}


//***************************************************************************
// [Private] MPI-Path helpers
//***************************************************************************

static inline void
mkPaths_task_beg(uint64_t opBeg_tsc, const char* ctxt)
{
  if (MyDbg) { AMSG("%s: task_beg", ctxt); }

  if (s_do_paths_full) {
    if (s_pathSetAux.requireNewTask && opBeg_tsc == 0) {
      mkPaths_addTaskPnt(0.0);
      mkPaths_bufferPaths();
    }
  }
  
  uint64_t opEnd_tsc = time_getTSC();

  s_pathSetAux.mpiOp_prev_us = 0;
  if (opBeg_tsc != 0) {
    s_pathSetAux.mpiOp_prev_us = timeDiff_us(opBeg_tsc, opEnd_tsc);
    s_pathSetAux.mpiOp_me_us += s_pathSetAux.mpiOp_prev_us;
  }
  
  s_pathSetAux.taskBeg_tsc = opEnd_tsc; // task begin = slack end
}


static inline void
mkPaths_task_end(uint64_t taskEnd_tsc, bool doCoalesce, bool doBuffer,
		 const char* ctxt)
{
  double cost_us = 0.0;
  if (taskEnd_tsc != 0) {
    cost_us = timeDiff_us(s_pathSetAux.taskBeg_tsc, taskEnd_tsc);
    s_pathSetAux.task_me_us += cost_us;
  }

  // Create TaskPnts with task coalescing:
  // 1. A trace record id is a time stamp, with a resolution of
  //    microseconds (us). If a task's cost is less than 1 us,
  //    coalescing helps avoid duplicate trace record ids.
  // 2. When there are back-to-back unwinds from same context (loop of
  //    ISends or Waitalls) coalescing avoids multiple TaskPnts
  //    from same context, each of which could require a backtrace.
  // 3. Permit/avoid coalescing by operation type.
  //    - Permit: isends, sends, wait, waitany
  //    - Avoid: phase beginning (collective), waitall, after model
  //      tasks, after joins with splicing.
  bool doAutoCoalesce =
    (!s_pathSetAux.requireNewTask && cost_us <= s_taskCoalescingThreshold_us);
  bool doAnyCoalesce = doCoalesce || doAutoCoalesce;

  if (MyDbg) { AMSG("%s: task_end(doCoalesce=%d, doBuf=%d): coalesce=%d", ctxt, doCoalesce, doBuffer, doAnyCoalesce); }

  if (doAnyCoalesce) {
    for (int i = 0; i < s_pathSet.n_elem; ++i) {
      Path_addCost(&s_pathSet.paths[i], cost_us);
    }
    s_pathSetAux.didCoalesceTasks = true;
    // N.B.: No need to buffer
  }
  else {
    mkPaths_addTaskPnt(cost_us);
    s_pathSetAux.didCoalesceTasks = false;

    if (doBuffer) {
      mkPaths_bufferPaths();
    }
  }
}


static inline void
mkPaths_bufferPaths()
{
  // If necessary, create TaskPoint to correctly represent coalesced task's end
  if (s_pathSetAux.didCoalesceTasks) {
    TaskPnt_t tsk = Path_newTaskPnt(&s_pathSet.paths[0], NULL, 0);
    for (int i = 1; i < s_pathSet.n_elem; ++i) {
      tsk = Path_newTaskPnt(&s_pathSet.paths[i], &tsk, i);
    }
    s_pathSetAux.didCoalesceTasks = false;
  }

  if (s_do_paths_full) {
    for (int i = 0; i < s_pathSet.n_elem; ++i) {
      mkPaths_bufferPath(&s_pathSet.paths[i], i);
    }
  }
}


static inline void
mkPaths_bufferPath(Path_t* x, int pathId)
{
  // if ( !(s_do_paths_full) ) { return; }

  const uint64_t mask = 0x00000000ffffffff;

  uint64_t cost_slack_us =
    ((uint64_t)x->tail_cost_us << 32) | (mask & (uint64_t)s_pathSetAux.mpiOp_prev_us);

  int buf_i = mkPaths_bufferTaskPnt(x->tail_cctNodeId, // call path id
				    x->prev_rank,      // metric_id
				    x->tail_timeId,    // microtime
				    x->prev_timeId,    // microtime2
				    cost_slack_us);    // microtime3

  PathSum_push(&x->phase_sum, x->tail_rank, buf_i);
  
  if (MyDbg) { Path_dump(x, /*doXtra*/false, "bufferPath", pathId); }
}


static inline int
mkPaths_bufferTaskPnt(unsigned int call_path_id, uint metric_id,
		      uint64_t microtime, uint64_t microtime2,
		      uint64_t microtime3)
{
  // FIXME: changed:
  //   <hpctoolkit>/src/lib/prof-lean/hpcrun-fmt.[ch]
  //   <hpctoolkit>/src/tool/hpcrun/trace.[ch]

#if (My_DoPathPruning)
  if (PathBuf_isFull(&s_pathBuf)) {
    EMSG("bufferTaskPnt: exceeded path buffer (%d/%d)", s_pathBuf.n_elem, s_pathBuf.max_elem);
    assert(false);
    //mkPaths_commitPhase();
  }
  
  hpctrace_fmt_datum_t datum;
  datum.time = microtime;     // tail_timeId
  datum.cpId = call_path_id;
  datum.metricId = metric_id; // prev_rank
  datum.time2 = microtime2;   // prev_timeId
  datum.time3 = microtime3;

  int buf_i = PathBuf_push(&s_pathBuf, &datum);
  return buf_i;

#else
  thread_data_t* td = hpcrun_get_thread_data();
  hpcrun_trace_append_with_time(&td->core_profile_trace_data,
				call_path_id, metric_id,
				microtime, microtime2, microtime3);
  return -1;
#endif
}


static inline void
mkPaths_bufferSample(uint64_t timeId, cct_node_t* cct_node, int metric_id,
		     uint64_t real_us)
{
  if (SampleBuf_isFull(&s_sampleBuf)) {
    EMSG("bufferSample: sample buffer full! (n_elem: %d, scaling factor %ld)",
	 s_sampleBuf.n_elem, hpcrun_itimer_scaling_factor);
    assert(false);
  }

  uint64_t smpl_us = real_us;
  uint64_t period_us = hpcrun_itimer_period_cur_us;

  int smpl_i = SampleBuf_push(&s_sampleBuf, timeId, cct_node, smpl_us);

  for (int i = 0; i < s_pathSet.n_elem; ++i) {
    Path_t* path = &s_pathSet.paths[i];

    Path_addSmplCost(path, smpl_us);

    bool isOk = PathSum_push(&path->phase_sum, s_rank_me, smpl_i);
    assert(isOk);
  }
  
  bool isChng = SampleBuf_updateLoad(&s_sampleBuf, &hpcrun_itimer_scaling_factor);

  if (isChng) {
    TMSG(MPI, "bufferSample: sampling scaling factor changed to %ld",
	 hpcrun_itimer_scaling_factor);
  }

  if (0) { AMSG("bufferSample: %d:%ld, idx %d, metric %d:%ld (%ld)", s_rank_me, timeId, smpl_i, metric_id, real_us, period_us); }
}


static inline void
mkPaths_commitPhase()
{
#if (My_DoPathPruning)

  //----------------------------------------------------------
  // Commit PathBuf/SampleBuf
  //----------------------------------------------------------
  if (s_pathBuf.n_elem > 0) {
    assert(s_do_paths_full);
    mkPaths_commitPhase_pathsFull();
  }

  if (s_sampleBuf.n_elem > 0) {
    assert(s_do_profile);
    mkPaths_commitPhase_profile();
  }

  //----------------------------------------------------------
  // Reset each Path_t; and PathBuf/SampleBuf
  //----------------------------------------------------------
  for (int i = 0; i < s_pathSet.n_elem; ++i) {
    Path_t* path = &s_pathSet.paths[i];
    PathSetAux_phaseStat(&s_pathSetAux, path, i);
    Path_phaseReset(path);
  }

  PathBuf_reset(&s_pathBuf);
  SampleBuf_reset(&s_sampleBuf, &hpcrun_itimer_scaling_factor);
  
#endif
}


static inline void
mkPaths_commitPhase_pathsFull()
{
  thread_data_t* td = hpcrun_get_thread_data();
  core_profile_trace_data_t* trace_data = &td->core_profile_trace_data;

  assert(trace_data);
  
  //----------------------------------------------------------
  // 1. Merge TaskSums from all s_pathSet.*.phase_sum
  //----------------------------------------------------------
  PathSum_t new_pathsum = PathSum_NULL;
  PathSum_t* pathsum = mkPaths_mergePathSums(&new_pathsum, s_rank_me);
  
  bool pathsum_isOverflow = PathSum_isOverflow(pathsum);

  if (MyDbg) { PathSum_dump(pathsum, /*doElem*/true, "mkPaths_commitPhase_pathsFull", -1); }

  //----------------------------------------------------------
  // 2. Commit TaskPnts that fall within path TaskSums
  //    - PathBuf may begin with a 'ModelTask'
  //    - PathBuf benefits from (but does not require) precise TaskSum's
  //----------------------------------------------------------
  int tskpnt_i = 0, n_commit = 0;
  
  int n_tsum = PathSum_nElem(pathsum);
  for (int tsum_i = 0; tsum_i < n_tsum; ++tsum_i) {
    TaskSum_t* tsum = PathSum_at(pathsum, tsum_i);

    if (tsum->rank != s_rank_me) {
      continue; // May occur for special case of 'mkPaths_mergePathSums()'
    }

    //----------------------------------
    // TaskSum 'tsum' represents TaskPnts (SyncTasks/ModelTasks) in PathBuf
    //----------------------------------
    int tskpnt_i_lo = tsum->index_lo;
    int tskpnt_i_hi = tsum->index_hi;
    
    if (MyDbg) { AMSG("mkPaths_commitPhase_pathsFull(phase %d): TaskSum %d/%d, %d:%d/%d", s_n_phase, tsum_i, n_tsum, tsum->rank, tskpnt_i_lo, tskpnt_i_hi); }

    for (tskpnt_i = tskpnt_i_lo; tskpnt_i <= tskpnt_i_hi; ++tskpnt_i) {
      PathBufElem_t* x = PathBuf_at(&s_pathBuf, tskpnt_i);
      hpcrun_trace_append_with_time(trace_data, x->datum.cpId,
				    x->datum.metricId, x->datum.time,
				    x->datum.time2, x->datum.time3);
      n_commit++;

      if (MyDbg && (PathBufElem_type(x) == TaskPntTy_sync)) {
	AMSG("mkPaths_commitPhase_pathsFull(phase %d): taskpnt %d/%d, tail %d:%ld, prev %d:%ld (commited %d)", s_n_phase, tskpnt_i, s_pathBuf.n_elem, s_rank_me, x->datum.time, x->datum.metricId, x->datum.time2, n_commit);
      }
    }
  }

  //----------------------------------------------------------
  // 3. If PathSum overflowed, commit all additional TaskPnts
  //----------------------------------------------------------
  if (pathsum_isOverflow) {
    for (; tskpnt_i < s_pathBuf.n_elem; ++tskpnt_i) {
      PathBufElem_t* x = PathBuf_at(&s_pathBuf, tskpnt_i);
      hpcrun_trace_append_with_time(trace_data, x->datum.cpId,
				    x->datum.metricId, x->datum.time,
				    x->datum.time2, x->datum.time3);
      n_commit++;
      
      if (MyDbg && (PathBufElem_type(x) == TaskPntTy_sync)) {
	AMSG("mkPaths_commitPhase_pathsFull(phase %d): taskpnt %d/%d, tail %d:%ld, prev %d:%ld (commited %d)", s_n_phase, tskpnt_i, s_pathBuf.n_elem, s_rank_me, x->datum.time, x->datum.metricId, x->datum.time2, n_commit);
      }
    }
  }
  
  if (MyDbg || pathsum_isOverflow) {
    AMSG("mkPaths_commitPhase_pathsFull(phase %d): commit %d/%d, overflow %d", s_n_phase, n_commit, s_pathBuf.n_elem, pathsum_isOverflow);
  }
}


static inline void
mkPaths_commitPhase_pathsLite()
{
  for (int path_i = 0; path_i < s_pathSet.n_elem; ++path_i) {
    Path_t* path = &s_pathSet.paths[path_i];

    PathSum_t* pathsum = &path->phase_sum;

    bool pathsum_isOverflow = PathSum_isOverflow(pathsum);
    assert(!pathsum_isOverflow);

    if (MyDbg) { PathSum_dump(pathsum, /*doElem*/true, "mkPaths_commitPhase_pathsLite", path_i); }

    //----------------------------------------------------------
    // For this PathSum, commit task costs represented by TaskSums.
    // (We need precise TaskSums.)
    //----------------------------------------------------------
    int n_commit = 0;
    uint64_t us_commit = 0;

    int n_tsum = PathSum_nElem(pathsum);
    for (int tsum_i = 0; tsum_i < n_tsum; ++tsum_i) {
      TaskSum_t* tsum = PathSum_at(pathsum, tsum_i);
      
      if (tsum->rank != s_rank_me) {
	continue;
      }

      //----------------------------------
      // TaskSum 'tsum' represents TaskPnts (SyncTasks/ModelTasks) in PathBuf
      //----------------------------------
      int tskpnt_i_lo = tsum->index_lo;
      int tskpnt_i_hi = tsum->index_hi;

      if (MyDbg) { AMSG("mkPaths_commitPhase_pathsLite(phase %d, path %d): TaskSum %d/%d, %d:%d-%d", s_n_phase, path_i, tsum_i, n_tsum, tsum->rank, tskpnt_i_lo, tskpnt_i_hi); }

      for (int tskpnt_i = tskpnt_i_lo; tskpnt_i <= tskpnt_i_hi; ++tskpnt_i) {
	PathBufElem_t* x = PathBuf_at(&s_pathBuf, tskpnt_i);

	uint32_t cost_us = (uint32_t)packed64_upper(x->datum.time3);
	us_commit += cost_us;
	n_commit++;
	
	if (MyDbg) { AMSG("mkPaths_commitPhase_pathsLite(phase %d, path %d): taskpnt %d/%d, commit n/us %d/%d", path_i, s_n_phase, tskpnt_i, s_pathBuf.n_elem, n_commit, cost_us); }
      }
    }

    //TODO: s_pathSetAux.path_cost_me_us[path_i] += (double)us_commit;

    if (MyDbg) { AMSG("mkPaths_commitPhase_pathsLite(phase %d, path %d): local commit us %ld, taskpnts %d/%d; phase us %g; path us %g", s_n_phase, path_i, us_commit, n_commit, s_pathBuf.n_elem, path->phase_cost_us, path->path_cost_us); }
  }
}


static inline void
mkPaths_commitPhase_profile()
{
  for (int path_i = 0; path_i < s_pathSet.n_elem; ++path_i) {
    Path_t* path = &s_pathSet.paths[path_i];

    PathSum_t* pathsum = &path->phase_sum;

    bool pathsum_isOverflow = PathSum_isOverflow(pathsum);
    assert(!pathsum_isOverflow);

    int path_mId = s_pathSetAux.metricIdV[path_i];

    double smpl_scale = 1.0;
    double phase_us = path->phase_cost_us;
    double phase_smpl_us = (double)path->phase_smpl_us;
    
    if (My_DoSampleScaling) {
      // phase_us = smpl_scale * phase_smpl_us
      if (phase_smpl_us > 0) {
	smpl_scale = (phase_us / phase_smpl_us);
      }
    }

    if (MyDbg) { PathSum_dump(pathsum, /*doElem*/true, "commitPhase_profile", path_i); }

    //----------------------------------------------------------
    // For this PathSum, commit samples represented by TaskSums.
    // (SampleBuf requires precise TaskSums.)
    //----------------------------------------------------------
    int n_commit = 0;
    uint64_t us_commit = 0;

    int n_tsum = PathSum_nElem(pathsum);
    for (int tsum_i = 0; tsum_i < n_tsum; ++tsum_i) {
      TaskSum_t* tsum = PathSum_at(pathsum, tsum_i);
      
      if (tsum->rank != s_rank_me) {
	continue;
      }

      //----------------------------------
      // TaskSum 'tsum' represents samples in SampleBuf
      //----------------------------------
      int smpl_i_lo = tsum->index_lo;
      int smpl_i_hi = tsum->index_hi;
      
      if (MyDbg) { AMSG("commitPhase_profile(phase %d, path %d): TaskSum %d/%d, %d:%d-%d", s_n_phase, path_i, tsum_i, n_tsum, tsum->rank, smpl_i_lo, smpl_i_hi); }

      for (int smpl_i = smpl_i_lo; smpl_i <= smpl_i_hi; ++smpl_i) {
	SampleBufElem_t* smpl = SampleBuf_at(&s_sampleBuf, smpl_i);
      
	uint64_t smpl_us0 = smpl->metric_incr;
	uint64_t smpl_us = (uint64_t)((double)smpl_us0 * smpl_scale);

	metric_set_t* metricVec = hpcrun_get_metric_set(smpl->cct_node);
	doMetricAbs(metricVec, path_mId, smpl_us, i);

	us_commit += smpl_us;
	n_commit++;
	
	if (MyDbg) { AMSG("commitPhase_profile(phase %d, path %d): smpl %d/%d, metric %d:%ld/%ld; commit n/us %d/%ld", path_i, s_n_phase, smpl_i, s_sampleBuf.n_elem, path_mId, smpl_us, smpl_us0, n_commit, us_commit); }
      }
    }

    //TODO: s_pathSetAux.path_cost_me_us[path_i] += (double)us_commit;

    if (MyDbg) { AMSG("commitPhase_profile(phase %d, path %d): local commit us %ld, smpls %d/%d; phase us actual/smpl %ld/%ld (scale %g); path us actual/smpl %g/%ld", s_n_phase, path_i, us_commit, n_commit, s_sampleBuf.n_elem, (uint64_t)phase_us, (uint64_t)phase_smpl_us, smpl_scale, path->path_cost_us, path->path_smpl_us); }
  }
}


//***************************************************************************

// mkPaths_joinPaths_splice(): Join 'paths_othr' into 'paths_me'. Use
//   splicing if path comes from a remote source.
static inline void
mkPaths_joinPaths_splice(PathSet_t* paths_me, PathSet_t* paths_othr)
{
#if (PathSet_n_elem > 2)
  
  mkPaths_selectPaths(&s_selectPaths_join, paths_me, paths_othr);
  
  // join 'paths_me' with selected paths (cf. mpiOp_barrier_end())
  for (int i = 0; i < paths_me->n_elem; ++i) {
    Path_t* path_me = &paths_me->paths[i];
    const Path_t* path_selected = &s_selectPaths_join.paths[i];
    if (path_selected->tail_rank == s_rank_me) {
      Path_replace(path_me, path_selected);
    }
    else {
      Path_splice(path_me, path_selected);
      s_pathSetAux.requireNewTask = true;
    }
  }
  
#else

  if (Path_compare(&paths_me->paths[0], &paths_othr->paths[0], PathTy_most)) {
    Path_splice(&paths_me->paths[0], &paths_othr->paths[0]);
    s_pathSetAux.requireNewTask = true;
  }

#if (PathSet_n_elem == 2)
  if (Path_compare(&paths_me->paths[1], &paths_othr->paths[1], PathTy_least)) {
    Path_splice(&paths_me->paths[1], &paths_othr->paths[1]);
    s_pathSetAux.requireNewTask = true;
  }
#endif
  
#endif
}


// mkPaths_joinPaths_copy(): Join 'paths_othr' into 'paths_me'.
static inline void
mkPaths_joinPaths_copy(PathSet_t* paths_me, PathSet_t* paths_othr)
{
#if (PathSet_n_elem > 2)

  //PathSet_dump(paths_top,  /*doXtra*/false, dbg_str);
  //PathSet_dump(&paths_rcv, /*doXtra*/false, dbg_str);

  mkPaths_selectPaths(&s_selectPaths_join, paths_me, paths_othr);

  for (int i = 0; i < paths_me->n_elem; ++i) {
    Path_t* path_me = &paths_me->paths[i];
    const Path_t* path_selected = &s_selectPaths_join.paths[i];
    Path_replace(path_me, path_selected);
  }

  //PathSet_dump(paths_top, /*doXtra*/false, dbg_str);

#else
  
  if (Path_compare(&paths_me->paths[0], &paths_othr->paths[0], PathTy_most)) {
    Path_replace(&paths_me->paths[0], &paths_othr->paths[0]);
  }

#if (PathSet_n_elem == 2)
  if (Path_compare(&paths_me->paths[1], &paths_othr->paths[1], PathTy_least)) {
    Path_replace(&paths_me->paths[1], &paths_othr->paths[1]);
  }
#endif

#endif
}


static inline void
mkPaths_selectPaths(PathSet_t* paths_selected,
		    PathSet_t* paths_x, PathSet_t* paths_y)
{
  // N.B.: all arguments should be distinct PathSet_t
  assert((paths_selected != paths_x) && (paths_selected != paths_y));
  
  int paths_n_elem = paths_x->n_elem;
  int pathJoinSum_n_elem = 2 * paths_n_elem;
  PathJoinSum_t pathJoinSumV[pathJoinSum_n_elem];

  mkPaths_sumPaths(pathJoinSumV, pathJoinSum_n_elem, paths_x, paths_y);
  mkPaths_sortPaths(pathJoinSumV, pathJoinSum_n_elem);

  // gather/copy selected paths into temporary space
  for (int i = 0; i < paths_n_elem; ++i) {
    int select_i = s_pathSetAux.selectIdxV[i];
    const Path_t* path_selected = pathJoinSumV[select_i].path;
    Path_replace(&paths_selected->paths[i], path_selected);
  }
}


static inline void
mkPaths_sumPaths(PathJoinSum_t* pathJoinSumV, int pathJoinSum_n_elem,
		 const PathSet_t* x_paths, const PathSet_t* y_paths)
{
  int n_elem = x_paths->n_elem;
  assert(n_elem == y_paths->n_elem && pathJoinSum_n_elem == (2 * n_elem));

  for (int i = 0, j = n_elem; i < n_elem; ++i, ++j) {
    PathJoinSum_set(&pathJoinSumV[i], &(x_paths->paths[i]));
    PathJoinSum_set(&pathJoinSumV[j], &(y_paths->paths[i]));
  }
}


// mkPaths_sortPaths(): Descending sort, with longest past
//   first. 'bubble sort' is good when sizes are small.
//
// N.B.: Assumes that duplicates are possible (from a path reduction
//   when ranks are not a power of 2 rank)
static inline void
mkPaths_sortPaths(PathJoinSum_t* pathJoinSumV, int n_elem)
{
  for (int i = 0; i < n_elem; i++) {
    for (int j = i + 1; j < n_elem; j++) {
      double val_i = pathJoinSumV[i].path_cost_us;
      double val_j = pathJoinSumV[j].path_cost_us;

      bool is_ordr = false;

      if (val_i == val_j) {
	const Path_t* path_i = pathJoinSumV[i].path;
	const Path_t* path_j = pathJoinSumV[j].path;

	int tail_rank_i = path_i->tail_rank;
	int tail_rank_j = path_j->tail_rank;

	if (tail_rank_i == tail_rank_j) {
	  uint64_t tail_timeId_i = path_i->tail_timeId;
	  uint64_t tail_timeId_j = path_j->tail_timeId;
	
	  if (tail_timeId_i == tail_timeId_j) {
	    
	    int prev_rank_i = path_i->prev_rank;
	    int prev_rank_j = path_j->prev_rank;

	    if (prev_rank_i == prev_rank_j) {
	      uint64_t prev_timeId_i = path_i->prev_timeId;
	      uint64_t prev_timeId_j = path_j->prev_timeId;

	      if (prev_timeId_i == prev_timeId_j) {
		// path_i is a (distributed) copy of path_j
		assert(path_i != path_j);
		is_ordr = (path_i < path_j);
	      }
	      else {
		is_ordr = (prev_timeId_i < prev_timeId_j);
	      }
	    }
	    else {
	      is_ordr = (prev_rank_i < prev_rank_j);
	    }
	  }
	  else {
	    is_ordr = (tail_timeId_i < tail_timeId_j);
	  }
	}
	else {
	  is_ordr = (tail_rank_i < tail_rank_j);
	}
      }
      else {
	is_ordr = (val_i > val_j);
      }

      if (!is_ordr) {
	PathJoinSum_t tmp = pathJoinSumV[i];
	pathJoinSumV[i] = pathJoinSumV[j];
	pathJoinSumV[j] = tmp;
      }
    }
  }
}


// mkPaths_mergePathSums(): Each PathSum_t is a sorted vector
//   (ascending) of (non-overlapping) ranges qualified by rank.
//   1. Retain only ranges for my rank (if multiple path sums)
//   2. Merge overlapping ranges; retain sorted order
//   3. Preserve any overflow flag
static inline PathSum_t*
mkPaths_mergePathSums(PathSum_t* new_pathsum, int rank_me)
{
  int n_pathsum = s_pathSet.n_elem;
  
  //----------------------------------------------------------
  // Special case: One path
  //----------------------------------------------------------
  if (n_pathsum == 1) {
    PathSum_t* psum = &s_pathSet.paths[0].phase_sum;
    return psum;
  }
  
  //----------------------------------------------------------
  // General case: Multiple Paths
  //----------------------------------------------------------
  
  PathSum_t* pathsumV[n_pathsum];
  int pathsumV_nElem[n_pathsum];
  int pathsumV_tsum_i[n_pathsum];

  bool isOverflow = false; // must preserve overflow flag!
  
  for (int path_i = 0; path_i < n_pathsum; ++path_i) {
    PathSum_t* psum = &s_pathSet.paths[path_i].phase_sum;
    pathsumV[path_i] = psum;
    pathsumV_tsum_i[path_i] = 0;
    pathsumV_nElem[path_i] = PathSum_nElem(psum);
    isOverflow = isOverflow || PathSum_isOverflow(psum);
  }

  //----------------------------------------------------------
  // Create sorted vector of non-overlapping TaskSums
  //----------------------------------------------------------
  while (true) {
    int pathsum_i = -1;
    TaskSum_t* tsum_min = NULL;

    // Find next smallest TaskSum low-index on my rank
    for (int i = 0; i < n_pathsum; ++i) {
      for ( ; pathsumV_tsum_i[i] < pathsumV_nElem[i]; pathsumV_tsum_i[i]++) {
	TaskSum_t* tsum = PathSum_at(pathsumV[i], pathsumV_tsum_i[i]);
	if (tsum->rank == rank_me) {
	  if (!tsum_min || tsum->index_lo < tsum_min->index_lo) {
	    pathsum_i = i;
	    tsum_min = tsum;
	  }
	  break;
	}
      }
    }

    // Done if no more time-range candidates
    if (!tsum_min) {
      break;
    }

    // Found: process TaskSum candidate 'tsum_min'
    assert(tsum_min && pathsum_i >= 0);
    if (MyDbg) { AMSG("mergePathSums: PathSum %d; TaskSum %d/%d, %d:%d/%d", pathsum_i, pathsumV_tsum_i[pathsum_i], pathsumV_nElem[pathsum_i], tsum_min->rank, tsum_min->index_lo, tsum_min->index_hi); }
    
    pathsumV_tsum_i[pathsum_i]++;
    
    bool isOk = PathSum_push2(new_pathsum, tsum_min->rank,
			      tsum_min->index_lo, tsum_min->index_hi);
    if (!isOk) {
      break; // overflow flag is now set
    }
  }

  if (isOverflow) {
    new_pathsum->n_elem = -new_pathsum->n_elem; // preserve orig overflow flag
  }

  return new_pathsum;
}


// mkPaths_pathAllreduce(): Given my "selected" paths, performs a custom
//   "allreduce" to globally determine the top paths, which are placed
//   in 's_selectPaths_barrier'.
static int
mkPaths_pathAllreduce(PathSet_t* paths, MPI_Comm comm)
{
  int rc;

  assert(comm != MPI_COMM_NULL);

  int comm_sz, me;
  rc = PMPI_Comm_size(comm, &comm_sz);
  assert(rc == MPI_SUCCESS);

  rc = PMPI_Comm_rank(comm, &me);
  assert(rc == MPI_SUCCESS);

  //----------------------------------------------------------
  //----------------------------------------------------------

  PathSet_t* paths_top = &s_selectPaths_barrier;
  *paths_top = *paths; // copy PathSet_t struct
  
  PathSet_t paths_rcv;

  MPI_Status st;
  const MPI_Datatype ty = MPI_BYTE;
  const int cnt = s_pathsSz;
  const int tag = s_prof_tag;

  if (0) { PathSet_dump(paths_top, /*doXtra*/false, "barrier/pre"); }
  
  //----------------------------------------------------------
  //----------------------------------------------------------

  // sz_pof2 will be set to largest power of 2 <= comm_sz
  int sz_pof2 = 1;
  while (sz_pof2 <= comm_sz) {
    sz_pof2 <<= 1;
  }
  sz_pof2 >>= 1;

  int sz_rem = comm_sz - sz_pof2;
  assert(sz_rem >= 0);

  //----------------------------------------------------------
  // Special case for ranks >= sz_pof2: select partner ranks
  //----------------------------------------------------------
  
  if (sz_rem > 0) {
    if (me >= sz_pof2) {
      int othr = me - sz_pof2;

      rc = real_PMPI_Recv(&paths_rcv, cnt, ty, othr, tag, comm, &st);
      assert(rc == MPI_SUCCESS);

      mkPaths_joinPaths_copy(paths_top, &paths_rcv);

      rc = real_PMPI_Send(paths_top, cnt, ty, othr, tag, comm);
      assert(rc == MPI_SUCCESS);
    }
    else if (me < sz_rem) {
      int othr = sz_pof2 + me;

      rc = real_PMPI_Send(paths_top, cnt, ty, othr, tag, comm);
      assert(rc == MPI_SUCCESS);

      rc = real_PMPI_Recv(&paths_rcv, cnt, ty, othr, tag, comm, &st);
      assert(rc == MPI_SUCCESS);

      mkPaths_joinPaths_copy(paths_top, &paths_rcv);
    }
  }
 
  //----------------------------------------------------------
  // Dissemination-style partners for ranks < sz_pof2
  //----------------------------------------------------------

  int mask = 0x1;
  if (me < sz_pof2) {
    while (mask < sz_pof2) {
      int dst = (me + mask) % sz_pof2;
      int src = (me + sz_pof2 - mask) % sz_pof2;
      
      rc = real_PMPI_Sendrecv(paths_top, cnt, ty, dst, tag,
			      &paths_rcv, cnt, ty, src, tag, comm, &st);
      assert(rc == MPI_SUCCESS);

      mkPaths_joinPaths_copy(paths_top, &paths_rcv);
      
      mask <<= 1;
    }
  }

  //----------------------------------------------------------
  // Complete path reduction convergence
  //----------------------------------------------------------

  bool doBcast = (JoinRuleTy_hasUniform(s_pathSetAux.joinRuleTy)
		  && (paths_top->n_elem > 2));

  if (doBcast) {
    int root = s_rank_root;
    rc = real_PMPI_Bcast(paths_top, cnt, ty, root, comm);
    assert(rc == MPI_SUCCESS);
  }
  else {
    //----------------------------------------------------------
    // Special case for ranks >= sz_pof2: receive final 'allreduced' paths
    //----------------------------------------------------------
  
    if (sz_rem > 0) {
      if (me >= sz_pof2) {
	int othr = me - sz_pof2;
	rc = real_PMPI_Recv(&paths_rcv, cnt, ty, othr, tag, comm, &st);
	assert(rc == MPI_SUCCESS);
	
	mkPaths_joinPaths_copy(paths_top, &paths_rcv);
      }
      else if (me < sz_rem) {
	int othr = sz_pof2 + me;
	rc = real_PMPI_Send(paths_top, cnt, ty, othr, tag, comm);
	assert(rc == MPI_SUCCESS);
      }
    }
  }

  if (0) {
    AMSG("barrier/post(phase %d)", s_n_phase);
    PathSet_dump(paths_top, /*doXtra*/true, "barrier/post");
  }

  return 0;
}


//***************************************************************************

static inline void
mkPaths_addTaskPnt(double cost_us)
{
  TaskPnt_t tsk = Path_addTaskPnt(&s_pathSet.paths[0], cost_us, NULL, 0);
  for (int i = 1; i < s_pathSet.n_elem; ++i) {
    tsk = Path_addTaskPnt(&s_pathSet.paths[i], cost_us, &tsk, i);
  }

  s_pathSetAux.requireNewTask = false;
}


static inline sample_val_t
mkPaths_backtrace(int metricId, uint64_t metricIncr)
{
  sample_val_t smpl_val;
  hpcrun_sample_val_init(&smpl_val);

  ucontext_t ctxt;
  getcontext(&ctxt);

  // N.B.: For tracing, do not use 'skipInner'
  // FIXME: disable "hpcrun_trace_append()"
  //   <hpctoolkit>/src/tool/hpcrun/sample_event.c
  smpl_val = hpcrun_sample_callpath(&ctxt, metricId, metricIncr,
				    0/*skipInner*/, 1/*isSync*/);
  return smpl_val;
}


static inline ProfRequest_t*
mkPaths_makeReq(MPI_Request* request, OpTy_t op_ty, MPI_Comm app_comm,
		int app_rank, int tag, MPI_Datatype datatype, int count)
{
  ProfRequest_t* myreq = ProfRequest_malloc();
  assert(myreq);

  // N.B.: app_rank may be MPI_ANY_SOURCE
  int prof_rank = xlateRank_app2prof(app_rank, app_comm);
  assert(prof_rank >= 0 || (op_ty == OpTy_recv && prof_rank == MPI_ANY_SOURCE));

  ProfRequest_init1(myreq, request, op_ty, app_comm, prof_rank, tag,
		    datatype, count, NULL);
  ProfRequestMap_insert(&s_mpiReqMap, myreq);

  return myreq;
}


static inline ProfRequest_t*
mkPaths_findReq(MPI_Request* request)
{
  ProfRequest_t* myreq = ProfRequestMap_findByReq(&s_mpiReqMap, request);

  if (!myreq) {
    TMSG(MPI, "mkPaths_findReq(%p) failed", request);
    assert(myreq);
  }

  return myreq;
}


//***************************************************************************

static inline void
pathSet_snd(PathSet_t* paths, int prof_rank)
{
  assert(prof_rank >= 0);

  int rc = real_PMPI_Bsend(paths, s_pathsSz, MPI_BYTE, prof_rank,
			   s_prof_tag, s_my_comm);
  assert(rc == MPI_SUCCESS);
}


static inline void
pathSet_rcv(PathSet_t* paths, int prof_rank)
{
  assert(prof_rank >= 0);

  int rc = real_PMPI_Recv(paths, s_pathsSz, MPI_BYTE, prof_rank,
			  s_prof_tag, s_my_comm, MPI_STATUS_IGNORE);
  assert(rc == MPI_SUCCESS);
}


static inline void
pathSet_ircv(ProfRequest_t* myreq)
{
  assert(myreq->prof_rank >= 0);

  int rc = real_PMPI_Irecv(&(myreq->rcv_paths), s_pathsSz, MPI_BYTE,
			   myreq->prof_rank, s_prof_tag, s_my_comm,
			   &(myreq->rcv_req));
  assert(rc == MPI_SUCCESS);
}


static inline void
pathSet_wait(ProfRequest_t* myreq)
{
  MPI_Status status;

  int rc = real_PMPI_Wait(&(myreq->rcv_req), &status);
  assert(rc == MPI_SUCCESS);
  
  assert(myreq->op_ty == OpTy_recv && myreq->prof_rank == status.MPI_SOURCE);
}


static inline void
pathSet_waitrcv(ProfRequest_t* myreq, int app_src)
{
  assert(myreq->op_ty == OpTy_recv);
  
  if (myreq->prof_rank != MPI_ANY_SOURCE) {
    // INVARIANT: 'myreq->prof_rank' (app_src) has already been resolved
    pathSet_wait(myreq);
  }
  else {
    myreq->prof_rank = xlateRank_app2prof(app_src, myreq->app_comm);
    assert(myreq->prof_rank >= 0);
    
    pathSet_rcv(&(myreq->rcv_paths), myreq->prof_rank);
  }
}


//***************************************************************************
// 
//***************************************************************************

// Path_newTaskPnt: create new TaskPoint for 'tail'
TaskPnt_t
Path_newTaskPnt(Path_t* x, TaskPnt_t* taskPnt, int pathId)
{
  //----------------------------------------------------------
  // gather new TaskPoint info
  //----------------------------------------------------------
  
  cct_node_t* bt_cctTraceNode = NULL;
  uint bt_cctTraceNodeId = HPCRUN_FMT_CCTNodeId_NULL;
  uint64_t timeId = 0;

  if (s_do_paths_full) {
    if (taskPnt && taskPnt->bt_cctTraceNode) {
      bt_cctTraceNode = taskPnt->bt_cctTraceNode;
    }
    else {
      sample_val_t bt_val =
	mkPaths_backtrace(s_pathSetAux.metricIdV[0], hpcrun_mpiPaths_period);
      bt_cctTraceNode = bt_val.trace_node;
    }
    bt_cctTraceNodeId = hpcrun_cct_persistent_id(bt_cctTraceNode);
  }

  if (taskPnt) {
    timeId = Path_makeTimeId1(taskPnt);
  }
  else {
    timeId = Path_makeTimeId(x, s_rank_me);
  }
  
  //----------------------------------------------------------
  // replace path's tail with new TaskPoint
  //----------------------------------------------------------

  x->tail_rank = s_rank_me; // CCT node id only makes sense for this rank
  x->tail_timeId = timeId;
  x->tail_cctNodeId = bt_cctTraceNodeId;
  //x->tail_cctNode = bt_cctTraceNode;

  if (MyDbg) { Path_dump(x, /*doXtra*/false, "Path_newTaskPnt", pathId); }
  
  return (TaskPnt_t){ .bt_cctTraceNode = bt_cctTraceNode, .timeId = timeId };
}


TaskPnt_t
Path_addTaskPnt(Path_t* x, double cost_us, TaskPnt_t* taskPnt, int pathId)
{
  //----------------------------------------------------------
  // add to path's tail new TaskPoint
  //----------------------------------------------------------
  
  x->prev_rank   = x->tail_rank;
  x->prev_timeId = x->tail_timeId;

  TaskPnt_t new_taskPnt = Path_newTaskPnt(x, taskPnt, pathId);
  
  x->tail_cost_us = 0;
  Path_addCost(x, cost_us); // cost is for this new task

  if (MyDbg) { Path_dump(x, /*doXtra*/false, "Path_addTaskPnt", pathId); }

  return new_taskPnt;
}


//***************************************************************************
// 
//***************************************************************************

static inline ProfRequest_t*
ProfRequest_malloc()
{
  return (ProfRequest_t*)Mem_malloc(&s_mpiReq_mem);
}


static inline void
ProfRequest_free(ProfRequest_t* x)
{
  Mem_free(&s_mpiReq_mem, (MemObj_t*)x);
}


static inline int
xlateRank_app2prof(int app_rank, MPI_Comm app_comm)
{
  // FIXME: cache results
  return xlateRank_app2prof_1(app_rank, app_comm,
			      s_is_on, s_world_comm, s_my_group);
}
