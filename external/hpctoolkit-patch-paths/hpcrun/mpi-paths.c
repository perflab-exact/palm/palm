// -*-Mode: C++;-*- // technically C99

// * BeginRiceCopyright *****************************************************
//
// $HeadURL$
// $Id$
//
// --------------------------------------------------------------------------
// Part of HPCToolkit (hpctoolkit.org)
//
// Information about sources of support for research and development of
// HPCToolkit is at 'hpctoolkit.org' and in 'README.Acknowledgments'.
// --------------------------------------------------------------------------
//
// Copyright ((c)) 2002-2016, Rice University
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
// * Neither the name of Rice University (RICE) nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// This software is provided by RICE and contributors "as is" and any
// express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular
// purpose are disclaimed. In no event shall RICE or contributors be
// liable for any direct, indirect, incidental, special, exemplary, or
// consequential damages (including, but not limited to, procurement of
// substitute goods or services; loss of use, data, or profits; or
// business interruption) however caused and on any theory of liability,
// whether in contract, strict liability, or tort (including negligence
// or otherwise) arising in any way out of the use of this software, even
// if advised of the possibility of such damage.
//
// ******************************************************* EndRiceCopyright *


//***************************************************************************
// system includes
//***************************************************************************

#include <stddef.h>
#include <stdlib.h>
#include <string.h>


//***************************************************************************
// libmonitor
//***************************************************************************

#include <monitor.h>


//***************************************************************************
// local includes
//***************************************************************************

#include <sample-sources/simple_oo.h>
#include <sample-sources/sample_source_obj.h>
#include <sample-sources/common.h>
#include <sample-sources/mpi-paths.h>

#include <hpcrun/metrics.h>
#include <hpcrun/thread_data.h>
#include <messages/messages.h>
#include <utilities/tokenize.h>

//***************************************************************************

extern __thread bool hpcrun_thread_suppress_sample;

static __thread bool mpiPaths_thread_is_on = false;

//***************************************************************************
// local variables
//***************************************************************************

uint64_t hpcrun_mpiPaths_period = 0;

//int hpcrun_mpiPaths_mtrcId_op  = -1;
//int hpcrun_mpiPaths_mtrcId_tOp = -1;
//int hpcrun_mpiPaths_mtrcId_arg1_sum = -1;
//int hpcrun_mpiPaths_mtrcId_arg_n = -1;


//***************************************************************************
// local variables
//***************************************************************************

static const long s_periodDefault = 1;

// Support only MPI_THREAD_SINGLE, MPI_THREAD_FUNNELED, MPI_THREAD_SERIALIZED
static const int s_master_thread_id = 0;


//***************************************************************************
// method definitions
//***************************************************************************

static void
METHOD_FN(init)
{
  TMSG(MPIcp, "init");
  self->state = INIT;

  //hpcrun_mpiPaths_mtrcId_op  = -1;
  //hpcrun_mpiPaths_mtrcId_tOp = -1;
  //hpcrun_mpiPaths_mtrcId_arg1_sum = -1;
  //hpcrun_mpiPaths_mtrcId_arg_n = -1;
}


static void
METHOD_FN(thread_init)
{
  TMSG(MPIcp, "thread init (no-op)");
}


static void
METHOD_FN(thread_init_action)
{
  TMSG(MPIcp, "thread init action (no-op)");
}


static void
METHOD_FN(start)
{
  TMSG(MPIcp,"starting MPI_CP sample source");

  // Start MPI monitoring for master thread
  //
  // Note: Support only MPI_THREAD_SINGLE, MPI_THREAD_FUNNELED,
  // MPI_THREAD_SERIALIZED
  
  if (!mpiPaths_thread_is_on) {
    
    // FIXME: hpcrun's monitor_init_mpi() [libmonitor] hook should
    // signal when it is safe to perform MPI calls.  For now, we rely on
    // the fact that our monitor_init_mpi() hook calls 'start' (assuming
    // MPI_RISKY is not defined) after MPI_Init is called.
    //
    // if monitor_mpi_comm_rank() >= 0, then libmonitor has
    // intercepted MPI_Init

    thread_data_t* threadData = hpcrun_get_thread_data();
    int mytid = threadData->core_profile_trace_data.id;
    if (mytid == s_master_thread_id) {
      int is_mpi_initialized = 0;
      PMPI_Initialized(&is_mpi_initialized);
      if (is_mpi_initialized) {
	TMSG(MPIcp, "MPI Paths: Enable");
	hpcrun_mpiPaths_init();
	hpcrun_mpiPaths_start();
	mpiPaths_thread_is_on = true;
      }
    }
    else {
      TMSG(MPIcp, "MPI Paths: Disable");
      hpcrun_thread_suppress_sample = true;
    }
    
  }

  TD_GET(ss_state)[self->sel_idx] = START;
}


static void
METHOD_FN(thread_fini_action)
{
  TMSG(MPIcp, "thread fini action (no-op)");
}


static void
METHOD_FN(stop)
{
  // Avoid invalid transition from UNINIT -> STOP
  if (self->state == UNINIT) {
    return;
  }

  TMSG(MPIcp,"stopping MPI_CP sample source");
  TD_GET(ss_state)[self->sel_idx] = STOP;
}


static void
METHOD_FN(shutdown)
{
  if (self->state == UNINIT) {
    return;
  }

  TMSG(MPIcp, "shutdown MPI_CP sample source");
  METHOD_CALL(self, stop);

  if (mpiPaths_thread_is_on) {
    int is_mpi_initialized = 0;
    PMPI_Initialized(&is_mpi_initialized);
    if (is_mpi_initialized) {
      hpcrun_mpiPaths_fini();
    }
    mpiPaths_thread_is_on = false;
  }

  self->state = UNINIT;
}


static bool
METHOD_FN(supports_event, const char *ev_str)
{
  return hpcrun_ev_is(ev_str, "MPI_CP");
}


static void
METHOD_FN(process_event_list, int lush_metrics)
{
  TMSG(MPIcp, "create MPI_CP metrics");

  char* evStr = METHOD_CALL(self, get_event_str);

  hpcrun_mpiPaths_period = s_periodDefault;
  char evName[32];
  hpcrun_extract_ev_thresh(evStr, sizeof(evName), evName,
			   (long*)&hpcrun_mpiPaths_period, s_periodDefault);

  TMSG(MPIcp, "MPI: %s sampling period: %"PRIu64, evName, hpcrun_mpiPaths_period);

#if 0
  // number of MPI operations
  hpcrun_mpiPaths_mtrcId_op = hpcrun_new_metric();
  hpcrun_set_metric_info(hpcrun_mpiPaths_mtrcId_op, "#mpi");

  // time for MPI operations
  hpcrun_mpiPaths_mtrcId_tOp = hpcrun_new_metric();
  hpcrun_set_metric_info_and_period(hpcrun_mpiPaths_mtrcId_tOp, "Tmpi (us)",
				    MetricFlags_ValFmt_Real, 1, metric_property_time);

  // task arguments...
  hpcrun_mpiPaths_mtrcId_arg1_sum = hpcrun_new_metric();
  hpcrun_set_metric_info(hpcrun_mpiPaths_mtrcId_arg1_sum, "arg:sum");
  
  hpcrun_mpiPaths_mtrcId_arg_n = hpcrun_new_metric();
  hpcrun_set_metric_info(hpcrun_mpiPaths_mtrcId_arg_n, "arg:n");
#endif
}


static void
METHOD_FN(gen_event_set, int lush_metrics)
{
  TMSG(MPIcp, "gen event set (no-op)");
}


static void
METHOD_FN(display_events)
{
  printf("===========================================================================\n");
  printf("Available MPI_CP events\n");
  printf("===========================================================================\n");
  printf("Name\t\tDescription\n");
  printf("---------------------------------------------------------------------------\n");
  printf("MPI_CP\t\t...\n");
  printf("\n");
}


//***************************************************************************
// object
//***************************************************************************

// sync class is "SS_SOFTWARE" so that both synchronous and
// asynchronous sampling is possible

#define ss_name mpi_cp
#define ss_cls SS_SOFTWARE

#include "ss_obj.h"


//***************************************************************************
// interface functions
//***************************************************************************


