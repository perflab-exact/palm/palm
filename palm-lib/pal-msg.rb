#!/usr/bin/env ruby
# -*- mode: ruby -*-

#*BeginPNNLCopyright*********************************************************
#
# $HeadURL$
# $Id$
#
#***********************************************************EndPNNLCopyright*

#****************************************************************************
# $HeadURL$
#
# Nathan Tallent
#****************************************************************************

#****************************************************************************
#
#****************************************************************************

class PALCollectiveTime
  Dbg = false

  # nodes: number of node in execution
  def initialize(palMsgTime, nodes, coresPerNode, fanInOut, factorSync = 1.0)
    @msgTime = palMsgTime
    @nodes = nodes
    @factorIntraNode = [PALMath.log2(coresPerNode), 1.0].max()
    @factorInterNode = [PALMath.log2(@nodes), 1.0].max()
    @factorFanInOut = fanInOut
    @factorSync = factorSync
  end

  # returns time (in micro-seconds) for collective of size 'size' (in bytes)
  def time(size)
    t1 = @msgTime.time(size)
    t2 = @factorIntraNode * @factorInterNode * @factorFanInOut * @factorSync
    t = t1 * t2
    if (Dbg)
      $stdout.write("PALCollectiveTime.time(#{size}): #{t} = #{t1} * #{@factorIntraNode} * #{@factorInterNode} * #{@factorFanInOut} * #{@factorSync})\n")
    end
    t
  end

end


class PALMsgTime
  Dbg = false

  # latency: base message latency, in micro-seconds
  # bandwidthL: list of bandwidths, in increasing order of message size
  def initialize(latency, bandwidthL, factorSync = 1.0)
    @latency = latency
    @bandwidthL = bandwidthL
    @factorSync = factorSync
  end

  # time(): time (in micro-seconds) for message of size 'size' (in bytes)
  def time(size)
    bw = findBW(size)
    t1 = @latency
    t2 = size / bw.bandwidth_us(size)
    t = (t1 + t2) * @factorSync
    if (Dbg)
      $stdout.write("PALMsgTime.time(#{size}): #{t} us = (#{t1} + #{t2}) * #{@factorSync} [bandwidth: #{bw}]\n")
    end
    t
  end
  
  def findBW(size)
    bw = @bandwidthL[-1]

    @bandwidthL.each { |x|
      if (x.contains(size))
        bw = x
        break
      end
    }

    bw
  end
end


class PALMsgBandwidth
  def initialize(size1, bw1, size2, bw2)
    @size1 = size1
    @size2 = size2

    # Parametric equations for a line passing through
    #   (x1 y1 z1), (x2 y2 z2) are
    # x = x1 + (x2 - x1)*t
    # y = y1 + (y2 - y1)*t
    # z = z1 + (z2 - z1)*t
    
    # Line:  y = m(x - x1) + y1
    # Slope: (delta y) / (delta x)
    
    m = (bw2 - bw1) / (size2 - size1)

    @bwFn = lambda { |size|
      m * (size - size1) + bw1
    }
  end

  # bandwidth(): bandwidth in bytes/second
  def bandwidth(size)
    @bwFn.call(size)
  end

  # bandwidth(): bandwidth in bytes/micro-second
  def bandwidth_us(size)
    @bwFn.call(size) / 1e6
  end

  def contains(size)
    (@size1 <= size && size < @size2)
  end

  def to_s()
    "[#{@size1}, #{@size2}) bytes -> [#{bandwidth(@size1)}, #{bandwidth(@size2)}] bytes/s"
  end
end


#****************************************************************************
#
#****************************************************************************

module PALMath
  def self.log2(x)
    Math.log(x) / Math.log(2)
  end
end


#****************************************************************************
#
#****************************************************************************

module MyPAL
  def self.main()
  end
end


#****************************************************************************
#
#****************************************************************************

BEGIN { }

if __FILE__ == $0
  MyPAL.main()
end

END { }
