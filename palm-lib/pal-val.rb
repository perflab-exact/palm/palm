#!/usr/bin/env ruby
# -*- mode: ruby -*-

#*BeginPNNLCopyright*********************************************************
#
# $HeadURL$
# $Id$
#
#***********************************************************EndPNNLCopyright*

#****************************************************************************
# $HeadURL$
#
# Nathan Tallent
#****************************************************************************

#****************************************************************************
#
#****************************************************************************

def _Val(*x)
  val     = x[0] # nil if non-existent
  val_tgt = x[1] # nil if non-existent

  if (val_tgt && val_tgt.instance_of?(Val))
    val_tgt = val_tgt.val
  end

  if (val && val.instance_of?(Val))
    val.nameFn = Val::make_nameFn()
    val.doReport = true
    val # FIXME: use 'val_tgt' as an additional validation point
  else
    Val.new(val, val_tgt, false)
  end
end


class Val
  Dbg = false

  attr_reader :val, :val_tgt
  attr_accessor :doReport, :nameFn
  attr_reader :nameOp, :exprTree
  attr_writer :parent

  NameOp_add  = '+'
  NameOp_sub  = '-'
  NameOp_mult = '*'
  NameOp_div  = '/'

  ExprTree_result = 0
  ExprTree_opnd0  = 1
  ExprTree_opnd1  = 2

  AnnotFn_beg = '['
  AnnotFn_end = ']'
  AnnotConst_beg = '<'
  AnnotConst_end = '>'
  
  Paren_open  = '('
  Paren_close = ')'


  def initialize(val, val_tgt = nil, doReport = nil)
    # @val    : actual value (as a Numeric)
    # @val_tgt: expected value (as a Numeric)
    @val     = val
    @val_tgt = (val_tgt) ? val_tgt : val
    @doReport = doReport

    if (!@val.kind_of?(Numeric))
      raise("non-numeric val: #{@val.inspect}")
    end

    if (!@val_tgt.kind_of?(Numeric))
      raise("non-numeric val_tgt: #{@val_tgt.inspect}")
    end

    # @nameFn: name of function that generated this Val (provenance).
    # @nameOp: name of operation that generated this Val (provenance).
    #   nameX ::= string | nil
    @nameFn = Val::make_nameFn()
    @nameOp = nil

    # @exprTree: The expresssion tree (evaluation history) that
    # generated this Val.  The expresssion tree is a list of lists.
    #
    #   exprTree ::= [ result-Val operand-Val+ ] | Val
    @exprTree = self

    # @parent: the parent Val of the expression tree
    @parent = nil
  end


  def self.make_nameFn()
    nm_fnd = nil

    # Note: Need Ruby 1.9.x for Array.index { |x| ... }
    caller.each { |x|
      nm = (x.match(/`([^']*)'/) and $1)

      # _Val()   :   _Val -> new -> initialize
      # Val.new():           new -> initialize
      # coercion : coerce -> new -> initialize
      
      # coercion means that we should use the default name
      if (nm && (nm.match(/^coerce$/) || nm.match(/^ensureVal$/)))
        break
      end

      if (nm && !(nm.match(/^_Val$/) ||
                  nm.match(/^new$/) ||
                  nm.match(/^initialize$/) ||
                  nm.match(/^main$/)) )
        nm_fnd = nm
        break
      end
    }

    nm_fnd
  end
  #protected :make_nameFn


  # ------------------------------------------------------------
  #  z = x (op) y
  # ------------------------------------------------------------

  def +(_y)
    x = self
    y = ensureVal(_y)
    z = clone()
    z.nameFn = nil
    z.add!(x, y)
  end


  def -(_y)
    x = self
    y = ensureVal(_y)
    z = clone()
    z.nameFn = nil
    z.subtract!(x, y)
  end


  def *(_y)
    x = self
    y = ensureVal(_y)
    z = clone()
    z.nameFn = nil
    z.multiply!(x, y)
  end


  def /(_y)
    x = self
    y = ensureVal(_y)
    z = clone()
    z.nameFn = nil
    z.divide!(x, y)
  end


  # ------------------------------------------------------------
  # 
  # ------------------------------------------------------------

  def add!(x, y)
    @val     = x.val     + y.val
    @val_tgt = x.val_tgt + y.val_tgt
    @nameOp = NameOp_add
    @exprTree = [self, x, y]
    x.parent = self
    y.parent = self
    self
  end
  protected :add!


  def subtract!(x, y)
    @val     = x.val     - y.val
    @val_tgt = x.val_tgt - y.val_tgt
    @nameOp = NameOp_sub
    @exprTree = [self, x, y]
    x.parent = self
    y.parent = self
    self
  end
  protected :subtract!


  def multiply!(x, y)
    @val     = x.val     * y.val
    @val_tgt = x.val_tgt * y.val_tgt
    @nameOp = NameOp_mult
    @exprTree = [self, x, y]
    x.parent = self
    y.parent = self
    self
  end
  protected :multiply!


  def divide!(x, y)
    @val     = x.val     / y.val
    @val_tgt = x.val_tgt / y.val_tgt
    @nameOp = NameOp_div
    @exprTree = [self, x, y]
    x.parent = self
    y.parent = self
    self
  end
  protected :divide!


  # ------------------------------------------------------------
  # 
  # ------------------------------------------------------------

  def to_f()
    @val
  end


  def coerce(x)
    [Val.new(x), self]
  end


  def ensureVal(x)
    if (x.is_a?(Val))
      x
    else
      Val.new(x)
    end  
  end


  # ------------------------------------------------------------
  # name
  # ------------------------------------------------------------

  def name_str(doAnnot)
    nm_str = (@nameFn) ?  @nameFn : nil
    val_str = (@nameOp) ? @nameOp : ('%g' % @val)

    if (nm_str)
      AnnotFn_beg + @nameFn + ': ' + val_str + AnnotFn_end
    else
      (doAnnot) ? (AnnotConst_beg + val_str + AnnotConst_end) : val_str
    end
  end



  # ------------------------------------------------------------
  # expr reports
  # ------------------------------------------------------------

  Fmt_num  = '%12.5g'  # number
  Fmt_numH = '%12s'    # number header

  Fmt_pct  = '%11.5g%%' # percent
  Fmt_pctH = Fmt_numH   # percent header

  Fmt_exprStrH = '%s'

  # expr_report:
  def expr_report(sortkey_fn, deleteif_fn, reportHdr_fn, reportMe_fn,
                  finalVal, prefix)
    pfx1 = prefix + '  '

    str = reportHdr_fn.bind(self).call(finalVal, pfx1) + "\n"

    exprL, isDone = make_exprL(false)

    if (!Dbg)
      exprL.delete_if { |x| deleteif_fn.bind(x).call(1.0, finalVal) }
      
      # N.B.: Use sort_by!, but only available with Ruby 1.9+
      exprL = exprL.sort_by { |x| sortkey_fn.bind(x).call(finalVal) }
    end

    exprL.each_with_index { |x, i|
      str += reportMe_fn.bind(x).call(finalVal, pfx1)
      if (!x.equal?(exprL.last())) then str += "\n" end
    }
    
    str
  end


  # expr_str: 
  def expr_str(maxlen = nil)
    exprL, isDone = make_exprL(true, maxlen)

    doAnnot = (exprL.size() == 1) # non-compound expression

    str = ''
    exprL.each_with_index { |x, i|
      isEnd = (i == exprL.size() - 1)
      notEnd_and_nextNotParenClose = (!isEnd && exprL[i + 1] != Paren_close)

      case x
      when Paren_open, Paren_close
        str += x
        if (notEnd_and_nextNotParenClose && x == Paren_close)
          str += ' '
        end
      when Val
        str += x.name_str(doAnnot)
        if (notEnd_and_nextNotParenClose)
          str += ' '
        end
      else
        raise("Should not be reachable!: #{origin}")
      end
    }
    if (!isDone) then str += '...' end

    str
  end


  # ------------------------------------------------------------
  # contribReport
  # ------------------------------------------------------------

  def contribReport(finalVal = nil, prefix = '')
    sortkey_fn = method(:contribReport_sortkey).unbind()
    deleteif_fn = method(:contribReport_deleteif).unbind()
    reportHdr_fn = method(:contribReport_header).unbind()
    reportMe_fn = method(:contribReport_me).unbind()

    expr_report(sortkey_fn, deleteif_fn, reportHdr_fn, reportMe_fn,
                finalVal, prefix)
  end


  def contribReport_sortkey(finalVal = nil)
    key = [ - @val ]
    
    if (finalVal)
      key.insert(0, - contrib_pct(finalVal))
    end

    key
  end


  def contribReport_deleteif(threshold_pct, finalVal = nil)
    if (contrib_isInteresting() && finalVal)
      k = contribReport_sortkey(finalVal)
      (k[0].abs() < threshold_pct)
    else
      true
    end
  end


  def contribReport_header(finalVal = nil, prefix = '')
    ttl = (finalVal) ? ("#{Fmt_pctH} " % '% Total') : ''
    "#{prefix}%s#{Fmt_numH} #{Fmt_exprStrH}" % [ttl, 'Value', 'Expression'] \
  end
  

  def contribReport_me(finalVal = nil, prefix = '')
    ttl = (finalVal) ? ("#{Fmt_pct} " % [contrib_pct(finalVal)]) : ''
    "#{prefix}%s#{Fmt_num} %s" % [ttl, @val, expr_str(50)]
  end
  

  def contrib_isInteresting()
    isPrimitive = (@nameFn && !@nameOp)
    isConst = (!@nameFn && !@nameOp)
    isAdditiveConst = (isConst && (@parent.nameOp == NameOp_add ||
                                   @parent.nameOp == NameOp_sub))
    isMult = (@nameOp == NameOp_mult)

    (@doReport || isPrimitive || isAdditiveConst ||  isMult)
  end


  def contrib_pct(finalVal)
    @val * 100.0 / Float(finalVal.val)
  end


  # ------------------------------------------------------------
  # errReport
  # ------------------------------------------------------------

  def errReport(finalVal = nil, prefix = '')
    sortkey_fn = method(:errReport_sortkey).unbind()
    deleteif_fn = method(:errReport_deleteif).unbind()
    reportHdr_fn = method(:errReport_header).unbind()
    reportMe_fn = method(:errReport_me).unbind()

    expr_report(sortkey_fn, deleteif_fn, reportHdr_fn, reportMe_fn,
                finalVal, prefix)
  end


  def errReport_sortkey(finalVal = nil)
    key = [- error_pct_abs(), - error_abs() ]
    
    if (finalVal)
      key.insert(1, - error_pct_abs(finalVal))
    end

    key
  end


  def errReport_deleteif(threshold_pct, finalVal = nil)
    if (error_isInteresting())
      k = errReport_sortkey(finalVal)
      if (finalVal)
        (k[0].abs() < threshold_pct && k[1].abs() < threshold_pct)
      else
        (k[0].abs() < threshold_pct)
      end
    else
      true
    end
  end


  def errReport_header(finalVal = nil, prefix = '')
    ttl = (finalVal) ? ("#{Fmt_pctH} " % 'Err% Total') : ''
    "#{prefix}#{Fmt_pctH} %s#{Fmt_numH} #{Fmt_numH} #{Fmt_exprStrH}" %
      ['Err% Local', ttl, 'Error', 'Value', 'Expression']
  end


  def errReport_me(finalVal = nil, prefix = '')
    ttl = (finalVal) ? ("#{Fmt_pct} " % [error_pct(finalVal)]) : ''
    "#{prefix}#{Fmt_pct} %s#{Fmt_num} #{Fmt_num} %s" %
      [error_pct(), ttl, error(), @val, expr_str(50)]
  end


  alias_method :error_isInteresting, :contrib_isInteresting


  # error: negative/positive if value is smaller/larger than target
  def error()
    (@val - @val_tgt)
  end


  def error_abs()
    error().abs()
  end


  def error_pct(finalVal = nil)
    denominator = (finalVal) ? finalVal.val : @val
    error() * 100.0 / Float(denominator)
  end


  def error_pct_abs(finalVal = nil)
    error_pct().abs()
  end


  # ------------------------------------------------------------
  # exprTree primitives
  # ------------------------------------------------------------

  # make_exprL: Returns an expression list, which is a flattened
  #   expression tree.  Whereas the expression tree is a list of
  #   lists, the expression list is a list of the non-list values in
  #   an expression tree.
  #
  #   If 'doParens' is 'true' then the list also include parenthesis
  #   (Paren_open/Paren_close) to indicate order of evaluation.
  #
  #   doParens = true:   exprL ::= [ (exprTree-val | paren)* ]
  #   doParens = false:  exprL ::= [ exprTree-val* ]
  #
  #   N.B.: use breadth-first (not depth-first) recursion
  def make_exprL(doParens, maxlen = nil, debug = nil)
    exprL = [ ]

    queue = [ @exprTree ]
    while (!queue.empty?()) do
      if (debug)
        $stdout.write("==> result/queue: #{Val.exprL_str(exprL)} / #{Val.exprL_str(queue)}\n")
      end
      
      q_elem = queue.delete_at(0)
      
      case q_elem
      when Paren_open, Paren_close
        exprL.push(q_elem)
        if (maxlen && exprL.size() >= maxlen) then break end

      when Val # origin-val
        exprL.push(q_elem)
        if (maxlen && exprL.size() >= maxlen) then break end

      when Array # origin-val
        if (debug)
          $stdout.write("* processing: #{Val.exprTree_str(q_elem)}\n")
        end
        rslt = q_elem[ExprTree_result]

        if (doParens)
          exprL.push(Paren_open)
        end
        exprL.push(rslt)
        if (maxlen && exprL.size() >= maxlen) then break end

        args = q_elem[ExprTree_opnd0..-1]

        queueLcl = args.map { |x| x.exprTree }
        if (doParens)
          queueLcl.push(Paren_close)
        end

        queue = queueLcl + queue
      else
        raise("Should not be reachable!: #{q_elem}")
      end
    end

    isDone = (queue.empty?())
    return exprL, isDone
  end


  # exprTree_str: convert an 'exprTree' to a string
  def self.exprTree_str(exprTree)
    str = ''

    case exprTree
    when Val
      str = exprTree.name_str(false)
    when Array
      str += '['
      exprTree.each { |x|
        if (str.length() > 1)
          str += ' '
        end
        str += x.name_str(false)
      }
      str += ']'
    else
      raise("Should not be reachable!: #{exprTree}")
    end

    str
  end


  # exprL_str: convert an 'exprL' to a string
  def self.exprL_str(exprL)
    str = "["

    exprL.each { |x|
      if (str.length() > 1)
        str += ' '
      end
      
      case x
      when Paren_open, Paren_close
        str += x
      when Val, Array
        str += Val.exprTree_str(x)
      else
        raise("Should not be reachable!: #{x}")
      end
    }

    str += "]"

    str
  end


  # ------------------------------------------------------------
  # 
  # ------------------------------------------------------------

  def to_s()
    to_str(0)
  end


  def to_str(verbose = 0, prefix = '')
    fmt_val = "%g"
    fmt_err = "(%.1f%%, %g)"

    case verbose
    when 0
      "#{fmt_val}" % @val
    when 1
      "#{fmt_val} #{fmt_err}" % [@val, error_pct(), error()]
    when 2
      report = contribReport(self, prefix)
      "#{fmt_val} #{fmt_err}\n\n%s" % [@val, error_pct(), error(), report]
    when 3..10 # Float::INFINITY in Ruby 1.9
      report1 = contribReport(self, prefix)
      report2 = errReport(self, prefix)

      "#{fmt_val} #{fmt_err}\n\n%s\n\n%s" % 
        [@val, error_pct(), error(), report1, report2]
    else
      raise("Should not be reachable!: #{verbose}")
    end
  end

end


#****************************************************************************
#
#****************************************************************************

module MyPAL
  def self.main()
    x1 = Val.new(1, 2) + _Val(9, 7)
    $stdout.write(x1.to_str(2) + "\n")

    x2 = 100 * x1
    $stdout.write(x2.to_str(2) + "\n")

    x3 = x1 + 5 + 4 + 3 + 2 + 1
    $stdout.write(x3.to_s() + "\n")
    $stdout.write(x3.to_str(1) + "\n")
    $stdout.write(x3.to_str(2) + "\n")
  end
end


#****************************************************************************
#
#****************************************************************************

BEGIN { }

if __FILE__ == $0
  MyPAL.main()
end

END { }
