#!/usr/bin/env ruby
# -*- mode: ruby -*-

#*BeginPNNLCopyright*********************************************************
#
# $HeadURL$
# $Id$
#
#***********************************************************EndPNNLCopyright*

#****************************************************************************
# $HeadURL$
#
# Nathan Tallent
#****************************************************************************

require 'pal-msg.rb'

#****************************************************************************
#
#****************************************************************************

module PAL
  class ExecutionPIC
    def initialize(mpi, cores, coresPerNode,
                   factorSyncMsg = 1.0, factorSyncCollective = 1.0)
      @cores = cores
      @coresPerNode = coresPerNode
      @nodes = (Float(cores)/Float(coresPerNode)).ceil

      @factorSyncMsg = factorSyncMsg
      @factorSyncCollective = factorSyncCollective
      

      case mpi
      when :mvapich
        # size, in bytes; bandwidth, in bytes/second
        size_bw0 = [    1, 0.98e6]
        size_bw1 = [  128,  140e6]
        size_bw2 = [32768, 2720e6]
        size_bw3 = [65536, 2850e6]
      when :openmpi
        # size, in bytes; bandwidth, in bytes/second
        size_bw0 = [    1, 0.86e6]
        size_bw1 = [  128,  100e6]
        size_bw2 = [32768, 2770e6]
        size_bw3 = [65536, 2850e6]
      else 
        raise("Bad mpi type: #{mpi}")
      end
      
      bw1 = PALMsgBandwidth.new(size_bw0[0], size_bw0[1],
                                size_bw1[0], size_bw1[1])
      bw2 = PALMsgBandwidth.new(size_bw1[0], size_bw1[1],
                                size_bw2[0], size_bw2[1])
      bw3 = PALMsgBandwidth.new(size_bw2[0], size_bw2[1],
                                size_bw3[0], size_bw3[1])
      
      @bwL = [bw1, bw2, bw3]
      
      latency = 0 # micro-seconds

      @msgTime = PALMsgTime.new(latency, @bwL, @factorSyncMsg)

      @msgTimeCollective = PALMsgTime.new(latency, @bwL)
      @collectiveTime1 =
        PALCollectiveTime.new(@msgTimeCollective, @nodes, @coresPerNode, 1.0,
                              @factorSyncCollective)
      @collectiveTime2 =
        PALCollectiveTime.new(@msgTimeCollective, @nodes, @coresPerNode, 2.0,
                              @factorSyncCollective)
    end
    
    def to_s()
      "PIC Env:\tcores=#{@cores}, nodes=#{@nodes}, c/n=#{@coresPerNode}"
    end
    
    # mpiMsgTime(): time (in micro-seconds)
    def mpiMsgTime(msgSize)
      @msgTime.time(msgSize)
    end
    
    # mpiCollectiveTime(): time (in micro-seconds)
    def mpiCollectiveTime(msgSize)
      @collectiveTime1.time(msgSize)
    end

    # mpiAllCollectiveTime(): time (in micro-seconds)
    def mpiAllCollectiveTime(msgSize)
      @collectiveTime2.time(msgSize)
    end
    
    # mpiMsgBandwidth(): bandwidth in bytes/second
    def mpiMsgBandwidth(msgSize)
      bw = @msgTime.findBW(msgSize)
      bw.bandwidth(msgSize)
    end
  end
end


#****************************************************************************
#
#****************************************************************************

module MyPAL
  def self.main()
    sizeL = []
    for i in 0..20
      sz = 2**i
      sizeL.push(sz)
    end
    
    nodes = 4
    coresPerNode = 16
    machine = PAL::ExecutionPIC.new(:mvapich, nodes, coresPerNode)
    
    $stdout.write("*** MPI msg & collective times (uncontended; #{nodes} nodes) ***\n")
    sizeL.each { |x|
      t1 = machine.mpiMsgTime(x)
      t2 = machine.mpiCollectiveTime(x)
      $stdout.write("  - #{x} bytes: \tmsg #{t1} \tcollective #{t2} us\n")
    }
  end
end

#****************************************************************************
#
#****************************************************************************

BEGIN { }

if __FILE__ == $0
  MyPAL.main()
end

END { }
