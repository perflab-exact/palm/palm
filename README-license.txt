Palm: Performance and Architecture Lab Modeling tool

Copyright ((c)) 2014-17, Battelle Memorial Institute
All Copyright rights reserved.

1. Battelle Memorial Institute (hereinafter Battelle) hereby grants
   permission to any person or entity lawfully obtaining a copy of
   this software and associated documentation files (hereinafter "the
   Software") to redistribute and use the Software in source and
   binary forms, with or without modification.  Such person or entity
   may use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and may permit others to do so,
   subject to the following conditions:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimers.

   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.

   * Other than as used herein, neither the name Battelle Memorial
     Institute or Battelle may be used in any form whatsoever without
     the express written consent of Battelle.

   * Redistributions of the software in any form, and publications
     based on work performed using the software should include the
     following citation as a reference:

     Ryan D. Friese, Nathan R. Tallent, Abhinav Vishnu, Darren
     J. Kerbyson, and Adolfy Hoisie, "Generating Performance Models
     for Irregular Applications," in Proc. of the 31st IEEE
     Intl. Parallel and Distributed Processing Symp., IEEE Computer
     Society

     Nathan R. Tallent and Adolfy Hoisie. "Palm: Easing the Burden of
     Analytical Performance Modeling." In Proc. of the 28th
     International Conference on Supercomputing. New York, NY, USA,
     2014. ACM. doi: 10.1145/2597652.2597683

2. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
   FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL BATTELLE
   OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
   USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
   OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
   SUCH DAMAGE.

