#!/usr/bin/env ruby
# -*- mode: ruby -*-

# NOTE: PIC, Intel compiler, MVAPICH, 32 cores, 16 cores/node, mean realtime

$: << '../palm-lib'
require 'pal-val.rb'
require 'pal-machine-pic.rb'

#        Px   Py
#   16:   4 x  4  
#   32:   8 x  4  134.0 s
#   64:   8 x  8   77.5 s
#  128:  16 x  8   46.3 s
#  256:  16 x 16   37.9 s
#  512:  32 x 16   19.2 s
# 1024:  32 x 32   12.6 s


# Sweep Model, strong scaling, input-50x50x50-mk10-mmi3-strong:
#   n_px: processors in x direction
#   n_py: processors in y direction
#
#   n_solve: number of calls to sweep solver
#   n_octant: 8
#   n_sweep: number of sweeps for one octant
#
#   n_pt_x_gbl: grid points in x direction, global
#   n_pt_y_gbl: grid points in y direction, global
#   n_pt_z_gbl: grid points in z direction, global
#
#   n_angle_blk: 
#
#   n_pt_x: grid points in x direction, per rank
#   n_pt_y: grid points in y direction, per rank
#   n_pt_z: grid points in z direction, per rank
#
#   grind: grind time
#   post: time for post/send
#
#   sweep3d_mdl(): n_solve * (sweep3d() + global_sum() + C)
#
#   sweep3d(): pipe_fill() 
#              + (n_octant * (n_sweep - 1) * pipe_stage())
#              + pipe_drain()
#
#   pipe_fill():  (n_px + n_py - 1) * (grind + 2 * post)
#   pipe_drain(): (2 * n_px + 4 * n_py - 2) * (grind + 2 * post)
#
#   pipe_stage(): (grind + (4 * post))


def main()
  corePairL = [#[4, 4],   #   16
               [8, 4],   #   32
               [8, 8],   #   64
               [16, 8],  #  128
               [16, 16], #  256
               [32, 16], #  512
               [32, 32]  # 1024
              ]
  coresPerNode = 16

  #corePairL.reverse!()

  corePairL.each { |corePair|
    px = corePair[0]
    py = corePair[1]
    cores = px * py
    pe_str = "@n_px = #{px}; @n_py = #{py}"

    factorSyncMsg = 25.0
    factorSyncCollective = 10.0

    exeEnv1 = PAL::ExecutionPIC.new(:mvapich, cores, coresPerNode,
                                    factorSyncMsg, factorSyncCollective)
    model1 = Model.new(exeEnv1)

    exeEnv2 = PAL::ExecutionPIC.new(:mvapich, cores, coresPerNode)
    model2 = Model.new(exeEnv2)
    
#    $stdout.write('=' * 77 + "\n")
#    model1.eval("@doModelExpr1 = false; @doModelExpr2 = false; #{pe_str}")
#    $stdout.write(model1.to_s())

    $stdout.write('-' * 60 + "\n")
    model2.eval("@doModelExpr1 = false; @doModelExpr2 = true; #{pe_str}")
    $stdout.write(model2.to_s())

#    $stdout.write('-' * 60 + "\n")
#    model2.eval("@doModelExpr1 = true; @doModelExpr2 = true; #{pe_str}")
#    $stdout.write(model2.to_s())
  }
end

class Model
  def initialize(exeEnv, paramsGbl = nil)
    @exeEnv = exeEnv
    @paramsGbl = paramsGbl
    @paramsLcl = nil
    @expr = '(amodel() + 105358 + 22036.2 + 1877.34)'
    @val = nil

    @doModelExpr = true
    @doModelExpr1 = true
    @doModelExpr2 = true
  end

  def eval(paramsLcl = nil)
    @paramsLcl = paramsLcl
    @val = Kernel.eval(param_str() + ';' + @expr, binding())
  end

  def val()
    @val
  end

  def param_str()
    if (@paramsGbl && @paramsLcl) then "#{@paramsGbl} ; #{@paramsLcl}"
    elsif (@paramsGbl) then "#{@paramsGbl}"
    elsif (@paramsLcl) then "#{@paramsLcl}" end
  end

  def to_s()
    "#{@exeEnv.to_s()}\n" \
    "Parameters:\t#{param_str()}\n" \
    "Expression:\t#{@expr}\n" \
    "Prediction:\t#{@val.to_f() / 1e6} s\n" \
    "Details:   \t#{@val.to_str(0)}\n"
  end


  #------------------------------------------------------------
  # 
  #------------------------------------------------------------

  def n_px
    _npe_i = 8
    #_npe_i
    @n_px
  end

  def n_py
    _npe_j = 4
    #_npe_j
    @n_py
  end

  def n_pt_x_gbl
    _it_g = 800.0
    _it_g
  end

  def n_pt_y_gbl
    _jt_g = 400.0
    _jt_g
  end

  def n_pt_z_gbl
    _kt = 50
    _kt
  end

  def n_pt_x
    n_pt_x_gbl / n_px
  end

  def n_pt_y
    n_pt_y_gbl / n_py
  end

  def n_pt_z
    _mk = 10
    _mk
  end

  def n_angle_blk
    _mmi = 3
    _mmi
  end

  def n_solve
    _INT_ABS_epsi__ = 84
    _INT_ABS_epsi__
  end

  def n_octant
    _8 = 8
    _8
  end

  def n_sweep
    _mmo___kb = 10
    _mmo___kb
  end


  #------------------------------------------------------------
  # modeling functions
  #------------------------------------------------------------

  def sweep3d_mdl(grnd, pst, gbl_sum)
    _Val(n_solve * (sweep3d(grnd, pst) + gbl_sum))
  end

  def sweep3d(grnd, pst)
    _Val(pipe_fill(grnd, pst) + (n_octant * pipe_octant(grnd, pst)) +
         pipe_drain(grnd, pst))
  end

  def pipe_fill(grnd, pst)
    _Val((n_px + n_py - 1) * (grnd + 2 * pst))
  end

  def pipe_drain(grnd, pst)
    _Val((2 * n_px + 4 * n_py - 2) * (grnd + 2 * pst))
  end

  def pipe_octant(grnd, pst)
    _Val((n_sweep - 1) * pipe_stage(grnd, pst))
  end

  def pipe_stage(grnd, pst)
    _Val((grnd + (4 * pst)))
  end


  #------------------------------------------------------------
  # synthesized modeling functions
  #------------------------------------------------------------

  def amodel(variant = :xN)
    if (variant == :x1 && @doModelExpr1)
      _grind = grind(:x1)
      _post1 = post1(:x1)
      _global_sum = global_sum(:x1)
      sweep3d_mdl(_grind, _post1, _global_sum)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr1))
      amodel(:xN!)
    elsif (variant == :xN && @doModelExpr1)
      _Val(amodel(:x1), amodel(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr1))
      _Val((global_sum() + sweep() + 1.87753e+06 + 271989 + 24338.4 + 7504.47 + 5166.34))
    end
  end

  def sweep(variant = :xN)
    if (variant == :x1 && @doModelExpr2)
      _grind = grind(:x1)
      _post1 = post1(:x1)
      _sweep = sweep(:x1!)
      #p "grind(1): #{_grind.val} / grind(1'): #{grind(:x1!).val}" # tallent
      pipe_fill(_grind, _post1) + _sweep + pipe_drain(_grind, _post1)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr2))
      sweep(:xN!) / n_solve()
    elsif (variant == :xN && @doModelExpr2)
      _Val(n_solve() * sweep(:x1), sweep(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr2))
      _Val((((grind() + post1() + post() + wait_1() + wait_2() + 166272 + 43221.3 + 9863.84 + 3757.19 + 3288.22 + 2817.97 + 2816.97 + 1408.38) + 105213) + 481023))
    end
  end

  def global_sum(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      _nfixed = 1.12423e+06
      allreduce(_nfixed * fp_sz)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      global_sum(:xN!) / n_solve()
    elsif (variant == :xN && @doModelExpr)
      _Val(n_solve() * global_sum(:x1), global_sum(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val(6.25452e+06)
    end
  end

  def grind(variant = :xN)
    _it = 100.0
    _jt = 100.0 # tallent
    if (variant == :x1 && @doModelExpr)
      grind(:x1!) * (n_pt_x / _it) * (n_pt_y / _jt)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      grind(:xN!) / (n_solve() * n_octant() * n_sweep())
    elsif (variant == :xN && @doModelExpr)
      grind(:xN!) * (n_pt_x / _it) * (n_pt_y / _jt) # tallent
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val(((grind_i_loops() + 319395 + 242359 + 149838 + 93944.3 + 88773 + 67166.2 + 62942.8 + 57767.4 + 42739.7 + 37103.6 + 34287.6 + 9865.34 + 1409.47) + 15970.5 + 4228.47 + 2349.44))
    end
  end

  def grind_i_loops(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      _grind_i_loops = grind_i_loops(:x1!)
      _grind_i_loops # * (n_pt_x / _it) * (n_pt_y / _jt) # tallent
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      grind_i_loops(:xN!) / (n_solve() * n_octant() * n_sweep() * lp_idiag() * lp_jkm())
    elsif (variant == :xN && @doModelExpr)
      _Val((n_solve() * n_octant() * n_sweep() * lp_idiag() * lp_jkm()) * grind_i_loops(:x1), grind_i_loops(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val((2.10868e+07 + 9.35535e+06 + 8.55688e+06 + 8.29717e+06 + 8.04408e+06 + 7.69917e+06 + 3.24496e+06 + 2.93275e+06 + 2.84139e+06 + 2.83484e+06 + 2.82615e+06 + 2.31392e+06 + 1.4444e+06 + 1.36645e+06 + 1.10529e+06 + 692854 + 690969 + 490400 + 191162 + 43681.5 + 25360.8 + 14563.1 + 10330.7 + 7983.97))
    end
  end

  def post1(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      _nib = 3000
      send(_nib * fp_sz)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      post1(:xN!) / (n_solve() * n_octant() * n_sweep())
    elsif (variant == :xN && @doModelExpr)
      _Val((n_solve() * n_octant() * n_sweep()) * post1(:x1), post1(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val(9.58206e+06)
    end
  end

  def post(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      _njb = 3000
      send(_njb * fp_sz)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      post(:xN!) / (n_solve() * n_octant() * n_sweep())
    elsif (variant == :xN && @doModelExpr)
      _Val((n_solve() * n_octant() * n_sweep()) * post(:x1), post(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val(1.59648e+06)
    end
  end

  def wait_1(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      _nib = 3000
      recv(_nib * fp_sz)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      wait_1(:xN!) / (n_solve() * n_octant() * n_sweep())
    elsif (variant == :xN && @doModelExpr)
      _Val((n_solve() * n_octant() * n_sweep()) * wait_1(:x1), wait_1(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val(2.33086e+07)
    end
  end

  def wait_2(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      _njb = 3000
      recv(_njb * fp_sz)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      wait_2(:xN!) / (n_solve() * n_octant() * n_sweep())
    elsif (variant == :xN && @doModelExpr)
      _Val((n_solve() * n_octant() * n_sweep()) * wait_2(:x1), wait_2(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val(3.22921e+06)
    end
  end

  def lp_idiag()
    _pt_y = 100.0 # tallent
    _pt_y + n_pt_z + n_angle_blk - 2
  end

  def lp_jkm()
    _ndiag = 27.027
    _ndiag
  end

  #-------------------------------------------------------------
  # PAL Library
  #-------------------------------------------------------------

  def fp_sz
    8
  end

  def send(sz)
    @exeEnv.mpiMsgTime(sz)
  end

  def recv(sz)
    @exeEnv.mpiMsgTime(sz)
  end

  def allreduce(sz)
    @exeEnv.mpiAllCollectiveTime(sz)
  end

end

if (__FILE__ == $0) then main() end
