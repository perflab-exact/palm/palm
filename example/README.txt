=============================================================================

Sweep3D
  - Strong scaling
  - input parameters: input-800x400x50-mk10-mmi3-strong*

Parameters:
  n_px: processors in x direction
  n_py: processors in y direction

  n_solve: number of calls to sweep solver
  n_octant: 8
  n_sweep: number of sweeps for one octant

  n_pt_x_gbl: grid points in x direction, global
  n_pt_y_gbl: grid points in y direction, global
  n_pt_z_gbl: grid points in z direction, global

  n_angle_blk: 

  n_pt_x: grid points in x direction, per rank
  n_pt_y: grid points in y direction, per rank
  n_pt_z: grid points in z direction, per rank

sweep3d_mdl(): n_solve * (sweep3d() + global_sum() + C)

sweep3d():
  [(pipe_steady()) + (pipe_bubbles())] * pipe_stage()

pipe_steady(): n_octant * n_sweep
pipe_bubbles(): 2 * n_px + 4 * n_py - 6

pipe_stage(): (grind + (2 * post))

grind(): grind_C * (n_pt_x / n_pt_x0) * (n_pt_y / n_pt_y0)

-----------------------------------------------------------------------------

*Wrong* Model:

sweep3d(): pipe_fill()
           + (n_octant * (n_sweep - 1) * pipe_stage())
           + pipe_drain()

pipe_fill():  (n_px + n_py - 1) * (grind() + 2 * post)
pipe_drain(): (2 * n_px + 4 * n_py - 2) * (grind() + 2 * post)

pipe_stage(): (grind() + (4 * post))

=============================================================================

Nekbone (v. 1)
  - Weak scaling
  - problem size defined by lx1=10, lp=2048, lelt=64

Parameters:
  n_nodes


nek_cg():
  n_cg_iter * (
    fp_ax_e() +
    fp_add2s1() +
    fp_add2s2_1() +
    fp_add2s2_2() +
    fp_glsc3_1() +
    fp_glsc3_2() +
    fp_glsc3_3() +
  
    26 * send(3.5k) +
    3 * allreduce(n_nodes))

=============================================================================

GTC
  - weak scaling
  - problem size defined by input.gtc-a.*

Parameters:
  n_nodes


n_istep *
  (send(32k) +

   send(96k) +
   2 * send(32k)
    
   4 * send(168k) +
   
   5 * send(32k) +
    
   2 * send(32k) +

   allreduce(n_nodes) +
   2 * allreduce(n_nodes) +
   allreduce(n_nodes) +
   2 * allreduce(n_nodes) +
   allreduce(n_nodes) +
   gather(n_nodes) +
    
   grind())

=============================================================================
