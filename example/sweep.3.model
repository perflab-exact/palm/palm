#!/usr/bin/env ruby
# -*- mode: ruby -*-

# NOTE: PIC, Intel compiler, MVAPICH, 32 cores, 16 cores/node, mean realtime

$: << '../palm-lib'
require 'pal-val.rb'
require 'pal-machine-pic.rb'

#        Px   Py
#   16:   4 x  4  30.6 s
#   32:   8 x  4  34.6 s
#   64:   8 x  8  40.1 s
#  128:  16 x  8  46.8 s
#  256:  16 x 16  58.4 s
#  512:  32 x 16  70.7 s
# 1024:  32 x 32  95.1 s


# Sweep Model, weak scaling, 50x50x50-mk10-mmi3-nrt:
#   n_px: processors in x direction
#   n_py: processors in y direction
#
#   n_solve: number of calls to sweep solver
#   n_octant: 8
#   n_sweep: number of sweeps for one octant
#
#   grind: grind time
#   post: time for post/send
#
#   sweep3d_mdl(): n_solve * (sweep3d() + global_sum() + C)
#
#   sweep3d(): pipe_fill() 
#              + (n_octant * (n_sweep - 1) * pipe_stage())
#              + pipe_drain()
#
#   pipe_fill():  (n_px + n_py - 1) * (grind + 2 * post)
#   pipe_drain(): (2 * n_px + 4 * n_py - 2) * (grind + 2 * post)
#
#   pipe_stage(): (grind + (4 * post))


def main()
  coresL = [16, 32, 64, 128, 256, 512, 1024]
  corePairL = [[4, 4],   #   16
               [8, 4],   #   32
               [8, 8],   #   64
               [16, 8],  #  128
               [16, 16], #  256
               [32, 16], #  512
               [32, 32]  # 1024
              ]
  coresPerNode = 16

  #coresL.reverse!()
  #corePairL.reverse!()

  coresL.each_with_index { |cores, i|
    px = corePairL[i][0]
    py = corePairL[i][1]
    pe_str = "@n_px = #{px}; @n_py = #{py}"

    factorSyncMsg = 25.0
    factorSyncCollective = 10.0

    exeEnv1 = PAL::ExecutionPIC.new(:mvapich, cores, coresPerNode,
                                    factorSyncMsg, factorSyncCollective)
    model1 = Model.new(exeEnv1)

    exeEnv2 = PAL::ExecutionPIC.new(:mvapich, cores, coresPerNode)
    model2 = Model.new(exeEnv2)
    
    $stdout.write('=' * 77 + "\n")
    model1.eval("@doModelExpr1 = false; @doModelExpr2 = false; #{pe_str}")
    $stdout.write(model1.to_s())

    $stdout.write('-' * 60 + "\n")
    model2.eval("@doModelExpr1 = false; @doModelExpr2 = true; #{pe_str}")
    $stdout.write(model2.to_s())

    $stdout.write('-' * 60 + "\n")
    model2.eval("@doModelExpr1 = true; @doModelExpr2 = true; #{pe_str}")
    $stdout.write(model2.to_s())
  }
end

class Model
  def initialize(exeEnv, paramsGbl = nil)
    @exeEnv = exeEnv
    @paramsGbl = paramsGbl
    @paramsLcl = nil
    @expr = '(amodel() + 117475 + 1876.44)'
    @val = nil

    @doModelExpr = true
    @doModelExpr1 = true
    @doModelExpr2 = true
  end

  def eval(paramsLcl = nil)
    @paramsLcl = paramsLcl
    @val = Kernel.eval(param_str() + ';' + @expr, binding())
  end

  def val()
    @val
  end

  def param_str()
    if (@paramsGbl && @paramsLcl) then "#{@paramsGbl} ; #{@paramsLcl}"
    elsif (@paramsGbl) then "#{@paramsGbl}"
    elsif (@paramsLcl) then "#{@paramsLcl}" end
  end

  def to_s()
    "#{@exeEnv.to_s()}\n" \
    "Parameters:\t#{param_str()}\n" \
    "Expression:\t#{@expr}\n" \
    "Prediction:\t#{@val.to_f() / 1e6} s\n" \
    "Details:   \t#{@val.to_str()}\n"
  end


  #------------------------------------------------------------
  # 
  #------------------------------------------------------------

  def n_px
    _npe_i = 8
    #_npe_i
    @n_px
  end

  def n_py
    _npe_j = 4
    #_npe_j
    @n_py
  end

  def n_solve
    _INT_ABS_epsi__ = 84
    _INT_ABS_epsi__
  end

  def n_octant
    _8 = 8
    _8
  end

  def n_sweep
    _mmo___kb = 10
    _mmo___kb
  end


  #------------------------------------------------------------
  # modeling functions
  #------------------------------------------------------------

  def sweep3d_mdl(grnd, pst, gbl_sum)
    _Val(n_solve * (sweep3d(grnd, pst) + gbl_sum))
  end

  def sweep3d(grnd, pst)
    _Val(pipe_fill(grnd, pst) + (n_octant * pipe_octant(grnd, pst)) +
         pipe_drain(grnd, pst))
  end

  def pipe_fill(grnd, pst)
    _Val((n_px + n_py - 1) * (grnd + 2 * pst))
  end

  def pipe_drain(grnd, pst)
    _Val((2 * n_px + 4 * n_py - 2) * (grnd + 2 * pst))
  end

  def pipe_octant(grnd, pst)
    _Val((n_sweep - 1) * pipe_stage(grnd, pst))
  end

  def pipe_stage(grnd, pst)
    _Val((grnd + (4 * pst)))
  end


  #------------------------------------------------------------
  # synthesized modeling functions
  #------------------------------------------------------------

  def amodel(variant = :xN)
    if (variant == :x1 && @doModelExpr1)
      _grind = (grind_1(:x1) + grind_3(:x1))
      _post1 = post1(:x1)
      _global_sum = global_sum(:x1)
      sweep3d_mdl(_grind, _post1, _global_sum)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr1))
      amodel(:xN!)
    elsif (variant == :xN && @doModelExpr1)
      _Val(amodel(:x1), amodel(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr1))
      _Val((global_sum() + sweep() + 415843 + 108321 + 469.219 + 468.906 + 468.844))
    end
  end

  def sweep(variant = :xN)
    if (variant == :x1 && @doModelExpr2)
      _grind = (grind_1(:x1) + grind_3(:x1))
      _post1 = post1(:x1)
      _sweep = sweep(:x1!)
      pipe_fill(_grind, _post1) + _sweep + pipe_drain(_grind, _post1)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr2))
      sweep(:xN!) / n_solve()
    elsif (variant == :xN && @doModelExpr2)
      _Val(n_solve() * sweep(:x1), sweep(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr2))
      _Val((((grind_3() + grind_1() + post1() + post() + wait_1() + wait_2() + 92893.1 + 22521.4 + 2816.31 + 1876.84 + 1407.38 + 937.844 + 469.875 + 469.438 + 469.438 + 469.25 + 469.25 + 469.188 + 468.938 + 468.938 + 468.906 + 468.906) + 8445.91 + 7509.19 + 469.562 + 468.938) + 138578))
    end
  end

  def global_sum(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      _nfixed = 103521
      allreduce(_nfixed * fp_sz)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      global_sum(:xN!) / n_solve()
    elsif (variant == :xN && @doModelExpr)
      _Val(n_solve() * global_sum(:x1), global_sum(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val(1.74746e+06)
    end
  end

  def grind_1(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      grind_1(:x1!)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      grind_1(:xN!) / (n_solve() * n_octant() * n_sweep())
    elsif (variant == :xN && @doModelExpr)
      grind_1(:xN!)
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val((grind_2() + 2.58899e+06 + 2.38165e+06 + 2.21974e+06 + 1.93022e+06 + 1.73283e+06 + 897593 + 727241 + 385685 + 188633 + 186283 + 103695 + 61460.5 + 60066.2 + 46450.3 + 35189.2 + 34723.6 + 34254.9 + 27209.7 + 24867.7 + 20174.4 + 17826.8 + 12671.4 + 10792.7 + 10791.9 + 7505.69 + 5631.91 + 5160.72 + 3751.81 + 1407.09 + 1406.75 + 938.25 + 938.125 + 469.719 + 468.969 + 468.938 + 468.875))
    end
  end

  def grind_2(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      grind_2(:x1!)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      grind_2(:xN!) / (n_solve() * n_octant() * n_sweep())
    elsif (variant == :xN && @doModelExpr)
      grind_2(:xN!)
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val((4.55247e+06 + 1.46959e+06 + 916371 + 762476 + 461724 + 232278 + 176431 + 168938 + 3284.41))
    end
  end

  def grind_3(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      grind_3(:x1!)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      grind_3(:xN!) / (n_solve() * n_octant() * n_sweep())
    elsif (variant == :xN && @doModelExpr)
      grind_3(:xN!)
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val(469.875)
    end
  end

  def post1(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      _nib = 1500
      send(_nib * fp_sz)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      post1(:xN!) / (n_solve() * n_octant() * n_sweep())
    elsif (variant == :xN && @doModelExpr)
      _Val((n_solve() * n_octant() * n_sweep()) * post1(:x1), post1(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val(1.2444e+06)
    end
  end

  def post(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      _njb = 1500
      send(_njb * fp_sz)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      post(:xN!) / (n_solve() * n_octant() * n_sweep())
    elsif (variant == :xN && @doModelExpr)
      _Val((n_solve() * n_octant() * n_sweep()) * post(:x1), post(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val(179225)
    end
  end

  def wait_1(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      _nib = 1500
      recv(_nib * fp_sz)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      wait_1(:xN!) / (n_solve() * n_octant() * n_sweep())
    elsif (variant == :xN && @doModelExpr)
      _Val((n_solve() * n_octant() * n_sweep()) * wait_1(:x1), wait_1(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val(6.77259e+06)
    end
  end

  def wait_2(variant = :xN)
    if (variant == :x1 && @doModelExpr)
      _njb = 1500
      recv(_njb * fp_sz)
    elsif (variant == :x1! || (variant == :x1 && !@doModelExpr))
      wait_2(:xN!) / (n_solve() * n_octant() * n_sweep())
    elsif (variant == :xN && @doModelExpr)
      _Val((n_solve() * n_octant() * n_sweep()) * wait_2(:x1), wait_2(:xN!))
    elsif (variant == :xN! || (variant == :xN && !@doModelExpr))
      _Val(1.03383e+06)
    end
  end


  #-------------------------------------------------------------
  # PAL Library
  #-------------------------------------------------------------

  def fp_sz
    8
  end

  def send(sz)
    @exeEnv.mpiMsgTime(sz)
  end

  def recv(sz)
    @exeEnv.mpiMsgTime(sz)
  end

  def allreduce(sz)
    @exeEnv.mpiAllCollectiveTime(sz)
  end

end

if (__FILE__ == $0) then main() end
