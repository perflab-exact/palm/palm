-*-Mode: markdown;-*-

$Id$

-----------------------------------------------------------------------------
Requirements:
=============================================================================


-----------------------------------------------------------------------------
Building & Installing
=============================================================================

[[new: Palm Externals now includes HPCToolkit-Externals.]]
[[new: include HPCToolkit souce tree?]]

-----------------------------------------------------------------------------
External Packages: HPCToolkit
-----------------------------------------------------------------------------

Patch against:
- HPCToolkit, 2017.06 (2017/06/16 15:13:09)
    4ddaa053890127c30bae087490914119bf425c3d
- HPCToolkit-externals, 2017.06 (2017/06/15 17:08:07)
    032dab135b5212fceafc958dc5cd12ce13a0c5d9


0. cd external

1. Download HPCToolkit

   ```sh
   export HPCTOOLKIT_REV=4ddaa053890127c30bae087490914119bf425c3d
   export HPCTOOLKIT_EXT_REV=032dab135b5212fceafc958dc5cd12ce13a0c5d9

   git clone https://github.com/HPCToolkit/hpctoolkit-externals.git
   cd hpctoolkit-externals && git checkout ${HPCTOOLKIT_EXT_REV}

   git clone https://github.com/HPCToolkit/hpctoolkit.git
   cd hpctoolkit && git checkout ${HPCTOOLKIT_REV}
   ```

2. Build HPCToolkit Externals

   > module swap PrgEnv-intel PrgEnv-gnu

   ```sh
   cd <hpctoolkit-externals>
   mkdir MYBUILD && cd MYBUILD
   ../configure \
     --prefix=`pwd`/../x86_64-linux
   make install
   ```

3. Configure HPCToolkit (but do not build yet!)

   [[must patch Makefile.am...]]

   ```sh
   cd <hpctoolkit>
   mkdir MYBUILD && cd MYBUILD
   ../configure \
     --prefix=`pwd`/../MYINSTALL \
     --with-externals=`pwd`/../../hpctoolkit-externals/x86_64-linux \
     --enable-mpi-wrapper \
     --enable-data-centric-tracing \
     MPICC=mpicc \
     MPICXX=mpicxx \
	 MPI_INC=${MPI_ROOT}/include \
     CFLAGS="-DMY_CPU_MHZ=2800 -I${MPI_ROOT}/include"
   ```

   [[N.B.]] Update `MY_CPU_MHZ` for your system.

   SeaPearl/IvyBridge:    `-DMY_CPU_MHZ=2800`
   PIC/Constance/Haswell: `-DMY_CPU_MHZ=2300`
   PIC/pal/AMD:           `-DMY_CPU_MHZ=2100`

   Recent Intel processors include a constant rate TSC (identified by
   the "constant_tsc" flag in Linux's /proc/cpuinfo). With these
   processors, the TSC ticks at the processor's nominal frequency,
   regardless of the actual CPU clock frequency due to turbo or power
   saving states. Hence TSC ticks are counting the passage of time,
   not the number of CPU clock cycles elapsed.


4. Apply patch

   * Open `external/hpctoolkit-patch-paths/Makefile` and point
     `HPCTOOLKIT` to source tree.
   
   ```sh
   make restore
   ```

5. Build HPCToolkit

   ```sh
   make install
   ```

-----------------------------------------------------------------------------
External Packages: HPCViewer
-----------------------------------------------------------------------------

Patch against:
  - hpcviewer: 5e8ade962c6461199020e1b26c77e566d976da9f (2016/05/24 12:19:38)
    File/Import/EGit: https://github.com/HPCToolkit/hpcviewer.git
      edu.rice.cs.hpc.common
      edu.rice.cs.hpc.data
      edu.rice.cs.hpc.filter
      edu.rice.cs.hpc.remote
      edu.rice.cs.hpc.traceviewer* 
      edu.rice.cs.hpc.viewer*

  - hpcviewer-externals: c84f10f291cc6c39c50fd1adda0b7d982b753da8 (2016/07/07 12:11:30)
    File/Import/EGit: https://github.com/HPCToolkit/hpcviewer-externals.git
      com.graphbuilder
      org.swtchart
      org.swtchart.ext


0. Eclipse 3.7 Indigo RCP

1.  HPCViewer

    cd <hpcviewer>
    # set paths in Makefile
    make restore


2.  Install HPCViewer:

    cd <hpcviewer>
    ./install <hpctoolkit-install-prefix>


-----------------------------------------------------------------------------
Using:
=============================================================================

* Main Parameters:
  - Profiled paths vs. full paths (selected on command line). (For
    testing, if full paths: prune or not)
  - Number of paths (PathSet, constant)
  - Maximum intervals for Path-summary (Path-summary size) (PathSum, constant)
  - Maximum entries for Sample-buffer/Path-buffer
  - For Profiling: sampling period (in us).

 - MPI: MV2_IBA_EAGER_THRESHOLD environment var to ensure PathSet sent eagerly.
 - join rule


* MPI Full Paths:
  make-batch-job ... \
    --paths='-e MPI_CP -t' \

* MPI Critical Path Profile:
  make-batch-job ... \
    --paths='-e MPI_CP -e REALTIME:5000' \

  REALTIME:6500


hpcstruct:
  - paths profiling: "hpcstruct --cfg new": DynInst (SymTabAPI) version. New: reifies inlined procedures and attribute costs to them.
    
  - full paths: "hpcstruct --cfg old": [[paths version has a hack]] Binutils version. The original version identifies inlined code within "real" procedures. My "paths" hack just removes the inlining notations so that task names are good.


* srun -p ivyall -N 1 --exclusive \
  mpiexec -n 2 ~/hpctoolkit/hpctoolkit-paths/MYINSTALL/bin/hpcprof-mpi \
    --metric-db=no \
    -S ../graphClustering-paths.hpcstruct -S ../libgomp.so.1.hpcstruct \
    hpctoolkit-measurements-graphClustering_paths__b__f_zznew640M_bin-paths_e_MPI_CP_t-node01x16omp-000002-137343


* ~/hpctoolkit/hpctraceviewer-graphEdgeSort.sh

-----------------------------------------------------------------------------

  salloc -A PAL -p pal -N 1 -t 0:60:00 --exclusive

  srun -n 8 hpcprof-mpi \
    --metric-db=no \
    -S ../amrGodunov3d.hpcstruct \
    hpctoolkit-measurements-x

-----------------------------------------------------------------------------

  hpctraceviewer

-----------------------------------------------------------------------------

  hpcviewer


=============================================================================

"Main" for Path Analysis:
   `edu.rice.cs.hpc.traceviewer.data/src/edu/rice/cs/hpc/traceviewer/data/timeline/CriticalPathAnalyzer.java`

"Main" called by:
  `edu.rice.cs.hpc.traceviewer/src/edu/rice/cs/hpc/traceviewer/db/local/SpaceTimeDataControllerLocal.java`

[note: TraceDataByRank.java and 'midpoint bug']
