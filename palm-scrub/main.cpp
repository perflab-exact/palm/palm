// -*-Mode: C++;-*-

//*BeginPNNLCopyright********************************************************
//
// $HeadURL$
// $Id$
//
//**********************************************************EndPNNLCopyright*

//***************************************************************************
// $HeadURL$
//
// Nathan Tallent
//***************************************************************************

//***************************************************************************

#include <stdint.h>
#include <fnmatch.h>

#include <iostream>
#include <string>

//***************************************************************************

//#include <BPatch.h>
//#include <BPatch_callbacks.h>
//#include <BPatch_flowGraph.h>

//#include <InstructionSource.h>
#include <ParseCallback.h>
//#include <CodeObject.h>
//#include <CFG.h>

#include <Symtab.h>

//***************************************************************************

std::string hpctoolkitFn_regexp = "hpctoolkit_sample.*";
std::string hpctoolkitFn_glob   = "hpctoolkit_sample*";

uint8_t x86_nop = 0x90;

//***************************************************************************
//
//***************************************************************************

class Callsite
{
public:
  Callsite(Dyninst::Address addr_, unsigned size_,
	   const std::string& callerNm_, Dyninst::Address callerAddr_,
	   Dyninst::Address calleeAddr_)
    : addr(addr_), size(size_),
      callerNm(callerNm_), callerAddr(callerAddr_), calleeAddr(calleeAddr_)
  { }
  
  ~Callsite()
  { }

  Dyninst::Address addr;
  unsigned size;
  const std::string& callerNm;
  Dyninst::Address callerAddr;
  Dyninst::Address calleeAddr;
};


class MyParseCallback
  : public Dyninst::ParseAPI::ParseCallback
{
public:
  MyParseCallback(const std::set<Dyninst::Address>& calleeAddrSet,
		  std::vector<Callsite*>& foundCallsites)
    : Dyninst::ParseAPI::ParseCallback(),
      m_calleeAddrSet(calleeAddrSet), m_foundCallsites(foundCallsites)
  { }

  ~MyParseCallback()
  { }

protected:
  virtual void
  interproc_cf(Dyninst::ParseAPI::Function* func,
	       Dyninst::ParseAPI::Block* block,
	       Dyninst::Address addr,
	       Dyninst::ParseAPI::ParseCallback::interproc_details* details)
  {
    // Dyninst::ParseAPI::ParseCallback::interproc_details::branch_interproc
    if (details->type == Dyninst::ParseAPI::ParseCallback::interproc_details::call) {
      Dyninst::Address calleeAddr = details->data.call.target;
      
      if (m_calleeAddrSet.find(calleeAddr) != m_calleeAddrSet.end()) {
	Dyninst::ParseAPI::Function* caller = func;
	const std::string& callerNm     = caller->name();
	Dyninst::Address   callerAddr   = caller->addr();
	Dyninst::Address   callsiteAddr = addr;
	size_t             callsiteSz   = details->isize;
	
	m_foundCallsites.push_back(new Callsite(callsiteAddr, callsiteSz,
						callerNm, callerAddr,
						calleeAddr));

	if (0) {
	  std::cout << callerNm << " (" << std::hex << callerAddr << "): "
		    << callsiteAddr << " -> " << std::hex << calleeAddr
		    << std::dec << " (" << callsiteSz << " bytes)\n";
	}
      }
    }
  }

private:
  const std::set<Dyninst::Address>& m_calleeAddrSet;
  std::vector<Callsite*>& m_foundCallsites;
};


//***************************************************************************
//
//***************************************************************************


int
main(int argc, const char** argv, const char** envp)
{
  // -------------------------------------------------------
  // Parse command line
  // -------------------------------------------------------
  
  if (argc <= 1) {
    return 1;
  }

  std::string binaryInNm = argv[1];

  std::string binaryOutNm;
  if (argc >= 3) {
    binaryOutNm = argv[2];
  }
  else {
    binaryOutNm = binaryInNm + ".out";
  }

  std::cout << std::showbase;


  // -------------------------------------------------------
  // 
  // -------------------------------------------------------
  
  Dyninst::ParseAPI::SymtabCodeSource* codeSrc =
    new Dyninst::ParseAPI::SymtabCodeSource((char*)binaryInNm.c_str());

  Dyninst::SymtabAPI::Symtab* symtab = codeSrc->getSymtabObject();

  // FIXME: for now, assert an x86 binary


  // -------------------------------------------------------
  // Map callsite target names to addresses
  // -------------------------------------------------------

  std::set<Dyninst::Address> calleeAddrSet;

  std::vector<Dyninst::SymtabAPI::Symbol*> symVec;
  symtab->findSymbol(symVec, hpctoolkitFn_regexp,
		     /*sType*/ Dyninst::SymtabAPI::Symbol::ST_FUNCTION,
		     /*nameType*/ Dyninst::SymtabAPI::anyName,
		     /*isRegex*/ true,
		     /*checkCase*/ true,
		     /*includeUndefined*/ false);


  for (unsigned i = 0; i < symVec.size(); ++i) {
    const Dyninst::SymtabAPI::Symbol* sym = symVec[i];
    std::cout << "Found callsite target in SymTab: " << sym->getPrettyName()
	      << std::hex << " (" << sym->getOffset() << ")\n" << std::dec;
    calleeAddrSet.insert(sym->getOffset());
  }


  std::vector<Dyninst::SymtabAPI::relocationEntry> relocVec;
  symtab->getFuncBindingTable(relocVec);
  
  for (unsigned i = 0; i < relocVec.size(); ++i) {
    const Dyninst::SymtabAPI::relocationEntry& re = relocVec[i];
    const char* re_name = re.name().c_str();
    if (fnmatch(hpctoolkitFn_glob.c_str(), re_name, /*flags*/0) == 0) {
      std::cout << "Found callsite target in RelocTab: " << re.name()
		<< std::hex << " (" << re.target_addr() << ")\n" << std::dec;
      calleeAddrSet.insert(re.target_addr());
    }
  }


  // -------------------------------------------------------
  // Find callsites to remove  { x -> y | y is callee-of-interest }
  // -------------------------------------------------------

  // FIXME: make "callsitesToNop" a set ordered by callsite address so
  // we can get the first and last callsite address and find the
  // corresponding regions.
  std::vector<Callsite*> callsitesToNop;

  MyParseCallback* parseCB = new MyParseCallback(calleeAddrSet, callsitesToNop);

  // N.B.: 'codeObj' assume ownership of 'parseCB'
  Dyninst::ParseAPI::CodeObject* codeObj =
    new Dyninst::ParseAPI::CodeObject(codeSrc, NULL, parseCB, false);
  
  codeObj->parse();


  // -------------------------------------------------------
  // Remove callsites
  // -------------------------------------------------------

  //bool symtab->findRegion(region, Offset addr, unsigned long size);
  //bool symtab->findRegionByEntry(region, Offset offset);
  //region = symtab->findEnclosingRegion(Offset offset);

  Dyninst::SymtabAPI::Region* region = NULL;
  bool status = symtab->findRegion(region, ".text");

  if ( !(status && region) ) {
    return 1; // ERROR
  }

  unsigned long regionDataSz = region->getDiskSize();
  void* regionData_old = region->getPtrToRawData();
  uint8_t* regionData_new = (uint8_t*)malloc(regionDataSz);
  memcpy(regionData_new, regionData_old, regionDataSz);
  
  // N.B.: assumes ownership of 'regionData_new'
  region->setPtrToRawData(regionData_new, regionDataSz);
  

  for (unsigned i = 0; i < callsitesToNop.size(); ++i) {
    const Callsite& callsite = *(callsitesToNop[i]);
    
    std::cout << "Replacing callsite in " << std::hex << callsite.callerNm
	      << " (" << callsite.callerAddr << "): "
	      << callsite.addr << " -> " << callsite.calleeAddr 
	      << " (" << std::dec << callsite.size << " bytes:" << std::hex;

    uint8_t* insn = (uint8_t*)codeSrc->getPtrToInstruction(callsite.addr);

    for (unsigned i = 0; i < callsite.size; ++i) {
      std::cout << " " << (unsigned)insn[i];
      insn[i] = x86_nop;
    }
    std::cout << ")" << std::dec << std::endl;
  }

  symtab->emit(binaryOutNm);

  // -------------------------------------------------------
  // 
  // -------------------------------------------------------

  for (unsigned i = 0; i < callsitesToNop.size(); ++i) {
    delete callsitesToNop[i];
  }

  delete codeObj;
  //delete codeSrc;

  return 0;
}
