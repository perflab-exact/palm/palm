-*-Mode: markdown;-*-

$Id$

-----------------------------------------------------------------------------
New
=============================================================================

- palm-task process: When running with MPI, the .prof file should have mean metrics, not just sum metrics.

- Palm-task: palm-task-run/--footprint requires a Pin-based CFG (from MIAMI-NW)

- Palm-task: footprint only looks at main binary (not libraries)

- Palm-task/MIAMI-nw: new uarch models

----------------------------------------

- palm-task-run: -o/--output: output-path prefix

- palm-task-run: separate memlat and memfp collectors

- palm-task-run: separate memlat and memfp analyzers

- palm-task-run: dangling reference to analyzePerfFilesIACA()

- palm-task-cfgprof: add -o/--output option

- palm-task-run: deprecate IACA tools/modes

- MIAMI_Driver::Initialize(): 
  - We never pass a CFG file
  - We do not use MIAMI's mem latency (KnobMemoryLatency/)

- palm-c/palm-tr: should support task/task parameter annotations


----------------------------------------
Ozgur: Palm/miami task memory models
----------------------------------------

------CFG File Format------

0) [[format template for CFG]]

For making the reader more robust and easy I think we can do couple changes on file format
1-) Using 1 character delimiter (|) with out space
      instead of this
         - binary-name: AllTests.out
      using this
         - binary-name|AllTests.out


     [[change all delimeters to |]]

2-) Using 1 tuple per line
      instead of this
         - func: test1 test1:0x400799
      using this
         - func|test1|0x400799|<end-addr>
		 
		 
3-) If we want to analysis more then 1 routine at a time we need to have number of routines
    (functions) and a different key value based on the index of routine. example:
      -numberRoutines:3
      
      -func1:f1
      ...
      -func2:f2

4-) We need some additional information for routine such as
   -endAddress: [[ok]]
   
   -offset: [[no; same as startaddr]]
    it is used to get start offset of function but not used after that so not needed

   
   -&cfgFlags,&_type of node ,&_flag of node:  if it is exist it would be good 
      but I dont think  we need this 
   -&_type of edge ,&_flag of edge:  if it is exist it would be good 
      but I dont think  we need this 

   

5-) We probably do not need paths at least for Footprint case it will find its own paths with cfg

  [[ignore for now]]


----- Changes in miami ---
CFG reader  needs to be written with new format.
2 more function need to be re created based on need
   CFG::loadFromFile and  Routine::loadCFGFromFile

FP case need to be in a if control with out cfg

TODO:removing MDF file casue having 0 FP  I need to investigate why



*  Option to visualize instruction schedule.

*  Scheduler de-composes costs by CPU and memory latencies (*average
   scalar* values from perf-mem). Compute schedule using costs both
   CPU and memory costs.

Q. Conservative dependence analysis for indirect loads. `pessimistic_memory_dep`

Q. Latency-based scheduling of non-loop paths:
   - Paths that end in a branch always become a loop?
     - DGBuilder.C / lastBranch: seems to create a bogus loop
   - Paths must end in a branch; will not permit fallthru edge?


0. Create unit tests for validation


Compute total data blocks that are not unique

- read in miss ratios; infer footprints for subpaths
  Use MIAMI-v1 to validate footprints...

- Add machine descriptions


0. Use full perf-mem histograms as input: location and cost of miss

   Notes on old-MIAMI
   --------------------

   - The MemReuse tool would compute reuse distance and footprint *per
     level* based on an instruction trace. Both are associated with
     dynamic call chains.
	 
   - Reuse/footprint data: `BlockMRDData::ParseTextMrdFile()`

   - Cache simulator memory latency: `MIAMI_Driver::parse_memory_latencies`
   
   - MIAMI computes misses based on *reuse distance*. It has disabled
     code for annotating with footprint.

   `MIAMI_Driver::Finalize()`
   -> `BlockMRDData::ParseMrdFile()`
   -> `MIAMI_Driver::compute_memory_information()`
      -> `BlockMRDData::ComputeMemoryEventsForLevel`
         -> `ComputeMissCountsForHistogram`
	        -> `MemoryHierarchyLevel::ComputeMissesForReuseDistance`

   `RefLatencyHistMap` `MemoryLatencyHistogram`


   - Current current memory latency info read/used?
   `LoadModule::dyninstAnalyzeRoutine`
   `LoadModule::getMemLoadLatency`

   Exploiting old-MIAMI for palm-MIAMI
   --------------------

   - Reuse `memory_latency_histograms.C` for our perf-mem data
   
   - Rework `BlockMRDData::ComputeMemoryEventsForLevel` to compute
     footprint from perf-mem data and footprint annotations.
	 


2. Create a DataLocality class to abstract data access costs

   Basic idea:
   - use a 'locality model' to predict hit/miss-rates for each memory level
   - use a 'Machine' instance to convert miss-rates to latency costs
  
   How to model locality?
   - Represent data footprint changes along loop iteration space. To
     reason about this, insert loop hierarchy (static loop structure)
     into dynamic instruction paths and then model footprint changes as
     data layout changes.

   - Locality model (which then yields memory costs) could be a
     histogram summarizing locality distribution with iteration space.


3. Scheduler:
   - Determine likely schedules *symbolically*. That is, we need to
     solve for dynamic pipeline overlap *symbolically* and capture the
     paths that are most likely to be critical.


4. Model representation: How to represent the model that is generated?

   - Generated model passes through abstract repr of data locality and
     Capture top k paths.
  
   - Generated model passes through machine descriptoin parameters, not
     just actual costs. Then generated model can re-do.


Annotations for task parameters or data footprint invariants?

----------------------------------------
Ryan
----------------------------------------

*  Enable visualization of instruction schedule

0. perf-mem memory latency information for
    - pflotran/petsc/matmult
	- page-rank/collect-ranks
	- page-rank/compute-page-rank

   [[perf-mem: #hits; #hits/instantiation]]
   [[perf-mem: scale hits by sampling period]]
   [[perf-mem call chains?]]

   [[new format]] address : level hits avglat (non-zero), overall-average

0. palm-miami: scheduler should have a pure latency mode as well as dynamic
   overlap mode (modulo scheduling). [[Important]]

   - Correct handling of non-loop paths:
     - Paths that end in a branch always become a loop?
       - DGBuilder.C / lastBranch: seems to create a bogus loop
     - Paths must end in a branch; will not permit fallthru edge?

   - modulo scheduling can probably replace our old IACA-based
     approach of computing dynamic pipeline overlap, i.e., scheduler
     with CPU + memory information + loop information could compute
     dynamic pipeline overlap (as opposed to invoking many times with
     unrolling).

1. palm-miami: Replace 'cfgprof' instruction paths with our new
   inferred instruction paths. The goal is that miami can perform all
   analysis it did with a 'cfgtool' path.
  
   - Conservative dependence analysis for indirect loads. Unlike
     cfgprof paths, our paths do not have 100% precise
     dependences. Ensure scheduler has a way to conservatively manage
     dependences on indirect loads. (See
     DGBuilder/`optimistic_memory_dep`, `pessimistic_memory_dep`)

   - Remove deprecated 'parallel' path-processing routines, e.g.:
     - `Routine::myConstructPaths()`; old call to `DGBuilder::DGBuilder`
     - `DGBuilder::DGBuilder(Routine* _routine`

2. New 'task-model-gen' driver:
   - generate model in same resolution as with IACA
   - invoke palm-miami once per application instead of once-per-path-per-task
   - generate model in new representation

3. Distribute palm-task's components into a the tool workflow perspective
   - task-run
   - task-gen

4. Update documentation:
   - basic usage notes in "README-install-task.md"
   - document 'perf-mem' (e.g., which version of Linux)


----------------------------------------
Nathan:
----------------------------------------

0.
`--paths='-e MPI_CP -t ' \`           --> `do_paths_full`
  backtraces on task boundary
  save trace records (paths and path costs)

`--paths='-e MPI_CP -e REALTIME:x' \` --> `do_profile`
  backtraces on sample
  no trace records (no -t option)
  record detailed/local path costs in CCT

`--paths='-e MPI_CP -e REALTIME:0' \` --> `do_paths_lite`
  no backtraces
  no permanent trace records (no -t option)
    maintain [[temporary trace records]] (full paths) to attribute costs
  record summary/local path costs in `path_cost_me`


Q. Is `SchedDG::Edge::computeLatency()` really `computeMinLatency()`?
   Scheduler/SchedDG.{h,C}

0. Cleanup:
   - `isaXlate_insn()` (`src/Scheduler/routine.C` `src/Scheduler/DGBuilder.C`)

0. Rename options:
   `--bin_path`  -> --app
   `--lat_path`  -> --mem
   `--blk_path`  -> --cfg-path
   `--dump_file` -> --dump-schedule

0. Cleanup:
   - `NOT_NOW` `COMPUTE_FPGA`, `XXX`, `NO_STD_CHEADERS`
   - `PROFILE_SCHEDULER`, `DECODE_INSTRUCTIONS_IN_PATH`
   - `tallent/FIXME`
   - `ozgurS,ozgurE`

0. Palm READMEs

0. Capture Repr paths 'notes' and 'todo'

0. Code questions:
   - Tarjan intervals vs. instruction-level dependence cycles (?) two
     different algorithms for detecting cycles?


----------------------------------------
TODO:
----------------------------------------

- Use DynInst to decode instead of Xed; support multiple ABIs.
  - Translate DynInst to MIAMI instructions (cf. *DYNINST_DECODE*)
    `src/common/InstructionDecoder-dyninst.{hpp,cpp}`
  ? Translate DynInst CFG to MIAMI CFG?
  

-----------------------------------------------------------------------------
Old
=============================================================================

Directive::encodePgmRefAsCode
    # FIXME:
    #  - need an id/name for the value (ctxt|insn + hash of name)
    #  - must know if type is 4/8 bytes and signed/unsigned integer / fp
    #  - must use appropriate syntax and indentation
    #  - must account for different C and Fortran calling conventions
    #  - must insert correctly (in scope, don't affect line numbering)
    #  - sample using reservior sampling [to avoid adaptive sampling]
    
* Assume program values are integers

-----------------------------------------------------------------------------

make-batch-job:
  - use HPCRUN_IGNORE_THREAD to suppress sampling in mpi-comm-threads
  - palm-paths
  - palm-paths-sample

-----------------------------------------------------------------------------

- palm-c <compiler>

  - Bugs (approximately ordered with most severe first):
    - Recoginze -E flag (stop after preprocessor stage)

    - Automake: For sweep, it seems that for the .c file, a variable
      controls the name of the *.o file (rather than the -o flag). As
      a result, we generate foo.palm.o. Unfortunately, the linker
      expects foo.o and cannot find foo.palm.o. Not sure what the best
      solution is yet. Currently using a workaround.

    - palm-tr generates preprocessor directives (#line). However,
      Fortran .f/.f95 files technically should not have preprocessor
      directives. Palm should probably rename these to .F/.F95 before
      invoking the compiler. Or automatically add -cpp (for gfortran).

    - palm-c autoconf systems can give source paths with .. in them:
      build/../src/x/y. In this case palm-c should be careful with the
      <tmp> sandbox. For example, the '..' could be the first path
      component which would move out of the user's <tmp> sandbox.

    - The following two invocations should be different:
        palm-c -h <compiler> -h  ==> palm help
        palm-c <compiler> -h     ==> compiler help

      See 'FIXME'.

    - Include PAPI if needed (ldd can test for this).

-----------------------------------------------------------------------------

Palm intra-task modeling (Palm-instrumentation)

getAvgLat => getComputeLatAvg
[getComputeOpsAvg]
[getComputeOpLatAvg]

getAvgMemAccesses => Gethsemane
memCycles => cycPerMemOp
[getMemLatAvg]


Future:
- combine "loop" annotations (index variable values) with edge count values (e.g. normalize in the presence of unrolling)
- combine profile info with critical path analysis

-----------------------------------------------------------------------------

Palm inter-task modeling (critical paths)

- Generate a static outline using hpcstruct information
- Simplify model expression

- blocks partition (divide or slice) rather than group.  want a
  directive with "group and label" semantics for subroutines
  - or better: subroutines automatically form a group/label

  - inclusive partitions
  - inclusive groups
  - exclusive groups (loops)

-----------------------------------------------------------------------------

Could Palm nesting information be embedded in inline information (which hpcstruct now reads)?
