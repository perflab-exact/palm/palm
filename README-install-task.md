-*-Mode: markdown;-*-

$Id$

URL: https://gitlab.pnnl.gov/perf-lab/palm/palm.git


-----------------------------------------------------------------------------
Building & Installing
=============================================================================

1. Build externals package (includes Ruby + gems)
   https://gitlab.pnnl.gov/perf-lab/palm/palm-externals.git

  ```sh
  git clone --depth=1 https://gitlab.pnnl.gov/perf-lab/palm/palm-externals.git

  export PALM_EXT_ROOT=<palm-externals>
  export PALM_EXT_HPCTKEXT_ROOT=<...>

  # see ${PALM_EXT_ROOT}/README.md and build HPCToolkit Externals
  ```

2. Build tools
   https://gitlab.pnnl.gov/perf-lab/palm/palm.git

   ```sh
   git clone https://gitlab.pnnl.gov/perf-lab/palm/palm.git

   export PALM_ROOT=<palm>

   cd ${PALM_ROOT}/palm-task
   make all -j
   ```

*. MIAMI-NW analyzer

*. Linux Perf


-----------------------------------------------------------------------------
Using
=============================================================================

----------------------------------------
Assumptions
----------------------------------------

1. Collect list of tasks (functions) to model from MPI critical path.
   (Link mpi-level and instruction-level critical paths with following
   assumption: With SPMD applications, tasks are the same, but the
   task behavior is different due to different parameters. Therefore,
   we assume it is possible to scale task behavior at the basic block
   level with parameters from the MPI-level.)

2. Environment:
  ```sh
  export PALM_EXT_ROOT=<palm-externals>
  export PALM_EXT_HPCTKEXT_ROOT=<hpctoolkit-externals-install>
  export PALM_ROOT=<palm>
  ```


----------------------------------------
Task Modeling Driver
----------------------------------------

1. Obtain control flow graph profile (sample execution or supplied file)
2. Obtain memory access latencies (sample execution or supplied file)
3. Constructs performance models by statically analyzing hot paths
   overlayed with data from 1 and 2. The resulting model is a ruby
   script.

  ```sh
  palm-task-run --model [--arch <arch> --ghz <N>] [model-opts] \
    [--use-cfgprof-file <cfgprof-file>] \
	[--use-memlat-file <memlat-file>] \
    -f <function> --app "<app> <app-args>"
  ```
  
  Application arguments:
  -f: function to model (mangled form); may pass multiple times
  --launch-cmd '<cmd>': app launcher (e.g., 'mpiexec -ppn 20 -n 20')
  
  Modeling arguments:
  --arch <arch>: specify the IACA architecture to model (e.g. IVB, HSW, ...)
  
  --miami: (experimental) use MIAMI as instruction scheduler
  --iaca: use IACA as instruction scheduler

  Extra modeling arguments:
  --path-threshold <N>: model N% most significant paths (0 <= N <= 100)
  [[--no-icp]]: do not perform instruction critical path analysis (hot path analysis is still performed)
  --load-cost-cache <cache-location>: a cache of previously evaluated hot-sub paths. Useful when changing the path threshold, prevents re-calcuating instruction critical paths
  -j, --num-threads <N> - number threads to use when analyzing perf data files


----------------------------------------
Profile control flow graph
----------------------------------------

Profiles control flow graph, capturing basic block and edge
counts. Uses DynInst to insert dynamic instrumentation.

  ```sh
  palm-task-run --cfgprof \
    -f <function> --app "<app> <app-args>"
		
  palm-task-cfgprof -e \
    -f <function> <app> <app-args>
  ```

  Additional arguments (also: palm-task-cfgprof):

  --max-func-level <N>: Create an interprocedural CFG; maximum depth
						for following function calls

----------------------------------------
Profile memory access latencies
----------------------------------------

Profiles memory access latencies using perf mem and Intel's load
latency hardware.
   
  ```sh
  palm-task-memlat --app "<program> <args>"
  ```

  Additional arguments:
  -j, --num-threads <N>: number threads to use when analyzing data files


-----------------------------------------------------------------------------
Task Footprint:
=============================================================================

  ```sh
  palm-task-run --footprint \
    --app "<app> <app-args>"
  ```

1. <miami-install>/bin/cfgprof -- <app> [[palm-task-cfgprof]]

2a. palm-task-memfp
2b. palm-task-memfp-prof [[TODO]]

3. palm-task-genfp --cfgprof <path> --footprint <path> 



Loop-based precise footprint  access diagnostics:
----------------------------------------

1. Collect CFG using miami_nw 
  ```sh
  <miami_nw-install>/bin/cfgprof  -stats  -statistic  -- <executable and args>
  ```

2. Collect memory profile using palm-task
  ```sh
  palm-task-run --footprint -f <function name> --app "<executable and args>"
  ```

3. Run miami_palm
  ```sh
  <miami_palm-install>/bin/miami  -m <mdf file> -c <CFG file> \
    --fp_path ./palm-<appName>/memfp/footprint.prof --func <function name>

  palm-task-run --footprint -f insert_value --launch-cmd=?OMP_NUM_THREADS=1 OMP_PROC_BIND=true KMP_AFFINITY=verbose MV2_CPU_MAPPING='0:6:12:18' srun -n 4 --ntasks-per-node=4" --app ?/people/tallent/app/vite/miniVite.git/miniVite-v1x -f /pic/scratch/tallent/mini-vite/arabic-2005.bin -t 1.0"
  ```

Whole-program, coarse footprint access diagnostics:
----------------------------------------

1. Set the required events
  ```sh
  HPCRUN_FP_EVENTS0="-e REALTIME:7500"

  HPCRUN_FP_EVENTS1="\
    -e skl::MEM_INST_RETIRED:ALL_LOADS@749895 \
    -e skl::MEM_LOAD_RETIRED:FB_HIT@16664 \
    -e skl::MEM_LOAD_RETIRED:L3_MISS@9998 \
    -e skl::L2_RQSTS:PF_MISS@16664"

  HPCRUN_FP_EVENTS2="\
    -e skl::MEM_LOAD_RETIRED:L1_HIT@749895 \
    -e skl::MEM_LOAD_RETIRED:L2_HIT@16664 \
    -e skl::MEM_LOAD_RETIRED:L3_HIT@9998"
  ```

2. Collect memory hit profile:
  ```sh
  ./make-batch-job --time 300 --app= "<executable and args>" --hpcrun="${HPCRUN_FP_EVENTS0}" --mpi-per-node 1 1 --run
  ./make-batch-job --time 300 --app= "<executable and args>" --hpcrun="${HPCRUN_FP_EVENTS1}" --mpi-per-node 1 1 --run
  ./make-batch-job --time 300 --app= "<executable and args>" --hpcrun="${HPCRUN_FP_EVENTS2}" --mpi-per-node 1 1 --run
  ```

3. Collect application structure
  ```sh
  hpcstruct <executable>
  ```

4. Analyze memory hit profile:
  ```sh
  hpcprof-mpi  --metric-db=no --metric=stats -S <executable-name>.hpcstruct \
    <all-data folders collected with 2. step>
  ```

5?. Convert platform-specific metric names to canonical metrics:

  ```
  perl-pie 's/(\d\.)?REALTIME \(usec\):/time-us:/g ; s/(\d\.)?skl::MEM_INST_RETIRED:ALL_(\w+:)/$2/g ; s/(\d\.)?skl::L2_RQSTS:(\w+:)/$2/g ; s/(\d\.)?skl::MEM_LOAD_RETIRED:(\w+:)/$2/g'


  # Alternative method with my replace script:
  /1\.REALTIME \(usec\):/               /time (us):/
  /2\.skl::MEM_INST_RETIRED:(\w+:)/     /\$1/
  /2\.skl::L2_RQSTS:(\w+:)/             /L2-\$1/
  /[2-3]\.skl::MEM_LOAD_RETIRED:(\w+:)/ /\$1/
  ```

6. Create footprint metrics:
  ```sh
  ./hpc_toolkit_formula_modifier <word size in byte> <experiment.xml file created at step 4>
  ```

