<!-- -*-Mode: markdown;-*- -->
<!-- $Id$ -->

Palm: Performance and Architecture Lab Modeling tool
=============================================================================

**Home**:
  - http://hpc.pnnl.gov/palm/

  - https://gitlab.com/perflab-exact/palm/palm

  - [Performance Lab for EXtreme Computing and daTa](https://github.com/perflab-exact)


**About**: Analytical application performance models are critical for
diagnosing performance-limiting resources, optimizing systems, and
designing machines. Creating models, however, is difficult.
Furthermore, models are frequently expressed in forms that are hard to
distribute and validate.

The Performance and Architecture Lab Modeling tool, or Palm, is a
modeling tool designed to make application modeling easier.

Palm provides a source code modeling annotation language. Not only
does the modeling language divide the modeling task into sub problems,
it formally links an application's source code with its model. This
link is important because a model's purpose is to capture application
behavior. Furthermore, this link makes it possible to define rules for
generating models according to source code organization.

Palm generates hierarchical models according to well-defined
rules. Given an application, a set of annotations, and a
representative execution environment, Palm will generate the same
model. A generated model is a an executable program whose constituent
parts directly correspond to the modeled application. Palm generates
models by combining top-down (human-provided) semantic insight with
bottom-up static and dynamic analysis. A model's hierarchy is defined
by static and dynamic source code structure. Because Palm coordinates
models and source code, Palm's models are 'first-class' and
reproducible.

Palm automates common modeling tasks. For instance, Palm incorporates
measurements to focus attention, represent constant behavior, and
validate models.

Palm's workflow is as follows. The workflow's input is source code
annotated with Palm modeling annotations. The most important
annotation models an instance of a block of code. Given annotated
source code, the Palm Compiler produces executables and the Palm
Monitor collects a representative performance profile. The Palm
Generator synthesizes a model based on the static and dynamic mapping
of annotations to program behavior. The model -- an executable program
-- is a hierarchical composition of annotation functions, synthesized
functions, statistics for runtime values, and performance
measurements.


**Contacts**: (_firstname_._lastname_@pnnl.gov)
  - Nathan R. Tallent ([www](https://hpc.pnnl.gov/people/tallent)), ([www](https://www.pnnl.gov/people/nathan-tallent))
  - Ozgur Kilic


**Contributors**:
  - Ozgur Kilic (PNNL)
  - Ryan D. Friese (PNNL)
  - Nathan R. Tallent (PNNL)


References
-----------------------------------------------------------------------------

* Ozgur O. Kilic, Nathan R. Tallent, and Ryan D. Friese, "Rapid memory footprint access diagnostics," in Proc. of the 2020 IEEE Intl. Symp. on Performance Analysis of Systems and Software, IEEE Computer Society, May 2020. <https://10.1109/ISPASS48437.2020.00047>

* Ozgur O. Kilic, Nathan R. Tallent, and Ryan D. Friese, "Rapidly measuring loop footprints," in Proc. of IEEE Intl. Conf. on Cluster Computing (Workshop on Monitoring and Analysis for High Performance Computing Systems Plus Applications), pp. 1--9, IEEE Computer Society, September 2019. https://doi.org/10.1109/CLUSTER.2019.8891025

* Nathan R. Tallent, Darren J. Kerbyson, and Adolfy Hoisie, "Representative paths analysis," in Proc. of the Intl. Conf. for High Performance Computing, Networking, Storage and Analysis (SuperComputing), November 2017. https://doi.org/10.1145/3126908.3126962

* Ryan D. Friese, Nathan R. Tallent, Abhinav Vishnu, Darren J. Kerbyson, and Adolfy Hoisie, "Generating performance models for irregular applications," in Proc. of the 31st IEEE International Parallel and Distributed Processing Symposium, pp. 317--326, IEEE Computer Society, May 2017. https://doi.org/10.1109/IPDPS.2017.61

* M. Halappanavar, M. Schram, L. de La Torre, K. Barker, N. R. Tallent, and D. Kerbyson, "Towards efficient scheduling of data intensive high energy physics workflows," in WORKS '15: Workshop on Workflows in Support of Large-Scale Science, held in conjunction with SuperComputing 15, November 2015. https://doi.org/10.1145/2822332.2822335

* Nathan R. Tallent and Adolfy Hoisie. "Palm: Easing the Burden of Analytical Performance Modeling." In Proc. of the 28th International Conference on Supercomputing. pp. 221–230, New York, NY, USA, 2014. ACM. https://doi.org/10.1145/2597652.2597683


Availability
-----------------------------------------------------------------------------

Palm is available as a prototype.


Acknowledgements
-----------------------------------------------------------------------------

This work was supported by the U.S. Department of Energy's Office of
Advanced Scientific Computing Research:
- Beyond the Standard Model
- Integrated End-to-end Performance Prediction and Diagnosis
- Exascale Computing Project Hardware Evaluation
