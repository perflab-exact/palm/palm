-*-Mode: markdown;-*-

$Id$

-----------------------------------------------------------------------------
Requirements:
=============================================================================

- Require Intel C/C++/Fortran or GCC C/C++ (not GCC gfortran).
  Specifically, compiler must preserve #line directive information.

- Require x86-64 architecture. (The tool to remove value-capture
  instrumentation currently only supports x86-64 binaries.)

-----------------------------------------------------------------------------
Building & Installing
=============================================================================

[[new: Palm Externals now includes HPCToolkit-Externals.]]
[[new: include HPCToolkit souce tree?]]

-----------------------------------------------------------------------------
External Packages: HPCToolkit
-----------------------------------------------------------------------------

Patch against:
- HPCToolkit, 2017.06 (2017/06/16 15:13:09)
    4ddaa053890127c30bae087490914119bf425c3d
- HPCToolkit-externals, 2017.06 (2017/06/15 17:08:07)
    032dab135b5212fceafc958dc5cd12ce13a0c5d9


0. cd external

1. Download HPCToolkit

   ```sh
   export HPCTOOLKIT_REV=4ddaa053890127c30bae087490914119bf425c3d
   export HPCTOOLKIT_EXT_REV=032dab135b5212fceafc958dc5cd12ce13a0c5d9

   git clone https://github.com/HPCToolkit/hpctoolkit-externals.git
   cd hpctoolkit-externals && git checkout ${HPCTOOLKIT_EXT_REV}

   git clone https://github.com/HPCToolkit/hpctoolkit.git
   cd hpctoolkit && git checkout ${HPCTOOLKIT_REV}
   ```

2. Build HPCToolkit Externals

   ```sh
   cd <hpctoolkit-externals>
   mkdir MYBUILD && cd MYBUILD
   ../configure \
     --prefix=`pwd`/../x86_64-linux
   make install
   ```

3. Configure HPCToolkit (but do not build yet!)

   ```sh
   cd <hpctoolkit>
   mkdir MYBUILD && cd MYBUILD
   ../configure \
     --prefix=`pwd`/../MYINSTALL \
     --with-externals=`pwd`/../../hpctoolkit-externals/x86_64-linux
   ```

4. Apply patch

   > Open 'hpctoolkit-patch/Makefile' and point 'HPCTOOLKIT' to source tree.
   
   ```sh
   make restore
   ```

5. Build HPCToolkit

   ```sh
   make install
   ```


-----------------------------------------------------------------------------
Using
=============================================================================

-----------------------------------------------------------------------------
Palm Compiler
-----------------------------------------------------------------------------

Generate two executables.  Make sure to compile with debug information
(e.g., -g).

1. Compile with palm-c wrapper (similar to mpicc):

   Examples:

  ```sh
  palm-c <regular-compiler> <compiler-arguments>
  CC="palm-c icc"
  FC="palm-c ifort"
  ```

2. Process with palm-scrub:

   ```sh
   palm-scrub <app> <app>-clean
   ```

-----------------------------------------------------------------------------
Palm Monitor
-----------------------------------------------------------------------------

Run each executable as follows...

  ```sh
  make-batch-job --app <app> \
    --mpi-per-node 16 \
    --palm \
    --run <input-file>
  ```

-----------------------------------------------------------------------------
Palm Generator
-----------------------------------------------------------------------------

N.B.: Currently assumes hpcprof (not hpcprof-mpi).

  ```sh
  hpcstruct <app>

  hpcprof --metric=stats -S <app>.hpcstruct \
    -R `pwd`/<app>-clean=`pwd`/<app> \
    <run>/hpctoolkit-measurements-<app>-clean \
    <run>/hpctoolkit-measurements-<app>
  ```

-----------------------------------------------------------------------------
Using Models
-----------------------------------------------------------------------------

- PAL Model libraries (e.g., pal-val)
- Your Model libraries (e.g., machine definition)
- Model boilerplate (e.g, strong or weak scaling)


[def annotations not fully implemented: (a) not placed in model; (b) not summarized without calling context.]
