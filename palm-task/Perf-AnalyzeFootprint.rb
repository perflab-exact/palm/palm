#!/usr/bin/env ruby
# -*- mode: ruby -*-

#*BeginPNNLCopyright*********************************************************
#
# $HeadURL$
# $Id$
#
#***********************************************************EndPNNLCopyright*

require 'fileutils'
require "parallel"
require 'open3'

#****************************************************************************
#
#****************************************************************************

$sample_countL = [0, 0, 0, 0, 0, 0, 0] # seven metrics

#****************************************************************************

module MyStat
  def self.sum(a)
    a.inject(0){ |accum, i| accum + i }
  end

  def self.mean(a)
    sum(a) / a.length.to_f
  end

  def self.sample_variance(a)
    m = mean(a)
    sum = a.inject(0){ |accum, i| accum + (i - m) ** 2 }
    sum / (a.length - 1).to_f
  end

  def self.standard_deviation(a)
    Math.sqrt(sample_variance(a))
  end

  def self.coeff_variation(a)
    stddev = standard_deviation(a)
    m = mean(a)
    stddev / m
  end
end

#****************************************************************************

def fpAnalyzePerfFiles_dla(nprocs, perfDir, period)
  # TODO: 'period' is currently ignored

  #-----------------------------------------------------------
  # Analyze individual 'perf' files
  #-----------------------------------------------------------
  files = Dir.glob("#{perfDir}memfp*.perf") # dir ends with "/"
  profL = Parallel.map(files, in_processes: nprocs) do |perfFile|
    #{ |tid| createGraphThread(func,demang_func,binary,tid) }
    fpAnalyzePerfOneFile_dla(perfFile)
  end

  #-----------------------------------------------------------
  # Combine indiviual profiles: 'prof_all' is the sum-reduction of 'profL'
  #-----------------------------------------------------------
  prof_all = {}
  
  for prof in profL
    for dso in prof.keys
      if !prof_all.key? dso
        prof_all[dso] = {}
      end
      for func in prof[dso].keys
        if !prof_all[dso].key? func
          prof_all[dso][func] = {}
        end
        for offset in prof[dso][func].keys
          if !prof_all[dso][func].key? offset
            prof_all[dso][func][offset] = $sample_countL.dup()
          end
          for i in (0...$sample_countL.length)
            prof_all[dso][func][offset][i] += prof[dso][func][offset][i]
          end
        end 
      end
    end
  end
  
  #-----------------------------------------------------------
  # Write combined profile (metrics values use a sparse format)
  #-----------------------------------------------------------
  outFile = open("#{perfDir}/footprint.prof",'w')

  outFile.write("#{$sample_countL.length}\n") # metrics

  for dso in prof_all.keys.sort
    outFile.write("dso:#{dso}\n")
    for func in prof_all[dso].keys.sort
      outFile.write("func:#{func}\n")
      for offset in prof_all[dso][func].keys.sort
        n_nz_count = 0
        nz_count_str = ""
        for event_i in (0...$sample_countL.length)
          count = prof_all[dso][func][offset][event_i]
          if count > 0
            n_nz_count += 1
            nz_count_str << " #{event_i} #{count}"
          end
        end
        outFile.write("#{offset} #{n_nz_count}#{nz_count_str}\n")
      end
      outFile.write("\n")
    end
  end
  outFile.close()
end


#****************************************************************************
#
#****************************************************************************

def fpAnalyzePerfOneFile_dla(perfFile)
  #-----------------------------------------------------------
  # 
  #-----------------------------------------------------------
  cmd = "perf script -F event,ip,sym,symoff,dso -i #{perfFile}"

  puts "*** Analyze footprint: #{cmd}"

  stdin, stdout, stderr, wait_thr = Open3.popen3(cmd)
  cmdPid = wait_thr[:pid]

  #-----------------------------------------------------------
  # Analyze/parse "perf script" output
  #-----------------------------------------------------------
  periodL = $sample_countL.dup()

  prof = {}

  stdout.each do |line|
    line.strip!

    if (line.include? "unknown") # TODO
      next
    end
    
    #-------------------------------------
    # parse line
    #-------------------------------------
    lineA = line.split(/[[:blank:]]+/) #(' ')

    event  = lineA[0]
    ip     = lineA[1]
    funcA  = lineA[2].split('+')
    dso    = lineA[3]
    
    func   = funcA[0]
    offset = funcA[1]

    dso = dso[1..-2] # remove parens

    #puts "#{event} #{dso} #{ip} #{func} #{offset}"
    
    re_period = %r'period=(\d+)'
    m = event.match(re_period)
    period = m[1].to_i()

    #if (dso.include? "[unknown]")
    #  next
    #end

    #-------------------------------------
    # event information
    #-------------------------------------

    event_i = nil

    case event
    when /all_loads/
      event_i = 0
    when /l1_hit/
      event_i = 1
    when /l2_hit/
      event_i = 2
    when /l3_hit/
      event_i = 3
    when /fb_hit/
      event_i = 4
    when /l3_miss/
      event_i = 5
    when /l2_rqsts.pf_miss/
      event_i = 6
    end

    #puts "#{event_i}: #{period} #{dso} #{ip} #{func} #{offset}"

    #-------------------------------------
    # update 'prof'
    #-------------------------------------

    periodL[event_i] = period

    # Ensure dso dictionary exists
    if !prof.key? dso
      prof[dso] = {}
    end

    # Ensure func dictionary (per dso) exists
    if !prof[dso].key? func
      prof[dso][func] = {}
    end

    # Ensure event data (per dso/func) exists
    if !prof[dso][func].key? offset
      prof[dso][func][offset] = $sample_countL.dup()
    end
    
    prof[dso][func][offset][event_i] += 1 # sample count
  end

  stdin.close()
  stdout.close()
  stderr.close()

  #-----------------------------------------------------------
  # Write profile (metrics values use a sparse format)
  #-----------------------------------------------------------
  outFnm = File.dirname(perfFile) + "/" +
           File.basename(perfFile, ".perf") + ".prof"
  outFile = open(outFnm,'w')

  for dso in prof.keys.sort
    outFile.write("dso:#{dso}\n")
    for func in prof[dso].keys.sort
      outFile.write("func:#{func}\n")
      for offset in prof[dso][func].keys.sort
        n_nz_count = 0
        nz_count_str = ""
        for event_i in (0...$sample_countL.length)
          count = prof[dso][func][offset][event_i]
          if count > 0
            n_nz_count += 1
            prof[dso][func][offset][event_i] = count * periodL[event_i]
            nz_count_str << " #{event_i} #{count}"
          end
        end
        outFile.write("#{offset} #{n_nz_count}#{nz_count_str}\n")
      end
      outFile.write("\n")
    end
  end

  outFile.close()

  return prof
end


def fpAnalyzePerfOneFile_ldlat(perfFile)
  #-----------------------------------------------------------
  # 
  #-----------------------------------------------------------
  cmd = "perf script -F event,addr,bpf-output,ip,sym,symoff,dso -i #{perfFile}"

  puts "*** Analyze ldlat: #{cmd}"

  stdin, stdout, stderr, wait_thr = Open3.popen3(cmd)
  cmdPid = wait_thr[:pid]

  #-----------------------------------------------------------
  # Analyze/parse "perf script" output
  #-----------------------------------------------------------
  prof = {}

  stdout.each do |line|
    line.strip!
    
    if (line.include? "unknown") # TODO
      next
    end
    
    #-------------------------------------
    # parse line
    #-------------------------------------
    lineA = line.split(/:[[:blank:]]+/)

    event    = lineA[1]
    lineCore = lineA[2]
    lineCore.strip!
    lineCoreA = lineCore.split(/[[:blank:]][[:blank:]]+/)

    #puts("#{lineCore}")
    addr    = lineCoreA[0]
    hitInfo = lineCoreA[1]
    lat     = lineCoreA[2]
    ipInfo  = lineCoreA[3]
    
    re_period = %r'period=(\d+)'
    m = event.match(re_period)
    period = m[1].to_i()

    #-------------------------------------
    # instruction attribution
    #-------------------------------------
    ipA    = ipInfo.split(' ')
    ip     = ipA[0]
    funcA  = ipA[1].split('+')
    dso    = ipA[2]

    dso = dso[1..-2] # remove parens

    func   = funcA[0]
    offset = funcA[1]
    
    #-------------------------------------
    # hit information
    #-------------------------------------
    hitInfo.sub!(/[[:digit:]]+[[:blank:]]/,"")
    hitA = hitInfo.split('|')
    hitLvl = hitA[0]

    hitLvl.sub!(/ hit/,"")

    #puts("#{period} / #{addr} #{lat} #{hitLvl} / #{ip} #{func} #{offset}")

    #-------------------------------------
    # update 'prof'
    #-------------------------------------

    # Ensure dso dictionary exists
    if !prof.key? dso
      prof[dso] = {}
    end

    # Ensure function dictionary (per dso) exists
    if !prof[dso].key? func
      prof[dso][func] = {}
    end

    # Ensure event data (per dso/func) exists
    if !prof[dso][func].key? offset
      prof[dso][func][offset] = { :hit => {}, :lat => {}, :addr => {} }      
    end

    # Sample data
    if !prof[dso][func][offset][:hit].key? hitLvl
      prof[dso][func][offset][:hit][hitLvl] = 0
    end
    prof[dso][func][offset][:hit][hitLvl] += 1

    lat_val = lat.to_i
    if !prof[dso][func][offset][:lat].key? lat_val
      prof[dso][func][offset][:lat][lat_val] = 0
    end
    prof[dso][func][offset][:lat][lat_val] += 1

    if !prof[dso][func][offset][:addr].key? addr
      prof[dso][func][offset][:addr][addr] = 0
    end
    prof[dso][func][offset][:addr][addr] += 1

  end

  stdin.close()
  stdout.close()
  stderr.close()

  #-----------------------------------------------------------
  # Write per-process output file
  #-----------------------------------------------------------
  outFnm = File.dirname(perfFile) + "/" +
           File.basename(perfFile, ".perf") + ".prof"

  outFile = open(outFnm,'w')

  # TODO: with ruby 2.5, hash has an in-place sort
  #prof.sort!
  #prof.each do |dso, funcH|
  #  puts "#{dso}"
  #end
  
  for dso in prof.keys.sort
    outFile.write("dso:#{dso}\n")
    for func in prof[dso].keys.sort
      outFile.write("func:#{func}\n")
      for offset in prof[dso][func].keys.sort
        outFile.write("#{offset}\n ")

        prof[dso][func][offset][:hit].sort.map do |hit, count|
          outFile.write(" #{hit}: #{count}")
        end
        outFile.write("\n ")

        prof[dso][func][offset][:lat].sort.map do |lat, count|
          outFile.write(" (#{lat}: #{count})")
        end
        outFile.write("\n ")

        addrA = prof[dso][func][offset][:addr].sort
        addr_beg = addrA[0][0]
        addr_prev = addr_beg.to_i(16)

        offsetA = []
        
        outFile.write(" #{addr_beg}")
        addrA.map do |addr, count|
          addr_val = addr.to_i(16)
          
          offset = (addr_val - addr_prev)
          offsetA << offset

          if count == 1
            outFile.write(" +%.2g" % offset)
          else
            outFile.write(" +%.2g: #{count}" % offset)
          end

          addr_prev = addr_val
        end

        if (offsetA.length > 1)
          offset_mean = MyStat.mean(offsetA)
          offset_stddev = MyStat.standard_deviation(offsetA)
          offset_cv = offset_stddev / offset_mean
          outFile.write(" [%.3g %.2f]" % [offset_mean, offset_cv])
        end
        
        outFile.write("\n")
      end
      outFile.write("\n")
    end
  end

  outFile.close()

  return prof
end


#****************************************************************************
#
#****************************************************************************

if (__FILE__ == $0)
  perf_file_or_dir = ARGV[0]

  if (perf_file_or_dir.match(/.perf$/))
    perfFnm = perf_file_or_dir
    fpAnalyzePerfOneFile_dla(perfFnm)
    #fpAnalyzePerfOneFile_ldlat(perfFnm)
  else
    perfDir = perf_file_or_dir
    nprocs = `nproc`.to_i
    period = 49993
    fpAnalyzePerfFiles_dla(nprocs, perfDir, period)
  end
end

#****************************************************************************
