#!/bin/python
import sys
import os

def main():   
   print("Hello!")  
   if len(sys.argv) !=  3:
      print "Usage: ./hpc_toolkit_formula_modifier.py <WordSize>  <XMLPath>"
      sys.exit()
   word = 4
   word = int(sys.argv[1])
   L1_cache = 32800
   L2_cache = 1080000
   L3_cache = 21300000
   index = 0
#Commnet out explicit one or not
   explicit = False

   #Read Metrics
   inMetrics = False 
   MetricDict = {}
   file =  open(sys.argv[2], "r+") 
   for line in file:   
      if "<MetricTable>" in line:
         inMetrics = True
      if inMetrics:
#         items = line.split()
#         if "<Metric" == items[0]:
#            key = "empty"
#            value = "empty"
#            for item in items:
#               s1 = items[1].split("=")
#               s2 = items[2].split("=")
#               s3 = items[3]
#               value = s1[1].strip("\"")
#               key = s2[1]+s3
#               key = key.strip("\"")
##               print "Value=",value," Key=",key
#               MetricDict[key] = value
#               index = int(value)
##         print line
         items = line.split("=")

         if "<Metric" == items[0].split()[0]:
            key = ""
            bigKey = "empty"
            value = "empty"
            print items
            value = items[1].split("\" ")[0]
            value = value.strip("\"")
            print "VALUE: " + value
            bigKey = items[2].split("\" ")[0]
            bigKey = bigKey.strip("\"")
            print "Big Key: "+ bigKey
            bigKey = bigKey[2:]
            print "Big Key 2: "+ bigKey
            shortKeys = bigKey.split(":")
            print shortKeys
            inShortKey = False
            for sk in shortKeys:
               if any( k in sk for k in ["HIT","MISS","ALL_LOADS","TIME"]):
                  inShortKey = True
               if inShortKey:
                  key += sk +":"
            print "Short Key: "+key[:-1]
            key = key[:-1]
            MetricDict[key] = value
            index = int(value)

      if "</MetricTable>" in line:
         print MetricDict
         inMetrics = False
         break
   
   print MetricDict
   
   #Write new formulas
   file.close()
   f1 =  open(sys.argv[2], "r") 
   lines = f1.readlines()
   f2 =  open(sys.argv[2], "w") 
   index +=1
   for line in lines:
      if "</MetricTable>" in line:
         #1 ~pf_wpl (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"~pf_wpl (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"max(${}-${}-${}-${}-${}, ${})/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (I)'],MetricDict['L1_HIT:Mean (I)'],MetricDict['FB_HIT:Mean (I)'],MetricDict['L2_HIT:Mean (I)'],MetricDict['L3_HIT:Mean (I)'],MetricDict['L3_MISS:Mean (I)'],MetricDict['L3_MISS:Mean (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["npf_wpl (I)"]=str(index)
         if explicit:
            #1 ~pf_wpl (E)
            f2.write("    <Metric i=\""+str(index)+"\" n=\"~pf_wpl (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"max(${}-${}-${}-${}-${}, ${})/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (E)'],MetricDict['L1_HIT:Mean (E)'],MetricDict['FB_HIT:Mean (E)'],MetricDict['L2_HIT:Mean (E)'],MetricDict['L3_HIT:Mean (E)'],MetricDict['L3_MISS:Mean (E)'],MetricDict['L3_MISS:Mean (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["npf_wpl (E)"]=str(index+1)
            index+=2
         else:
            index+=1
   
         #2 pf_wpl>= (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"pf_wpl_min (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}+${}+${}+${}+(${}*${})+${}-${})/${} \"/>\n".format(MetricDict['L1_HIT:Mean (I)'],MetricDict['FB_HIT:Mean (I)'],MetricDict['L2_HIT:Mean (I)'],MetricDict['L3_HIT:Mean (I)'],MetricDict['L3_MISS:Mean (I)'],MetricDict['npf_wpl (I)'],MetricDict['PF_MISS:Mean (I)'],MetricDict['ALL_LOADS:Mean (I)'],MetricDict['PF_MISS:Mean (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["pf_wpl_min (I)"]=str(index)
         if explicit:
            #2 pf_wpl>= (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"pf_wpl_min (E)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}+${}+${}+${}+(${}*${})+${}-${})/${} \"/>\n".format(MetricDict['L1_HIT:Mean (E)'],MetricDict['FB_HIT:Mean (E)'],MetricDict['L2_HIT:Mean (E)'],MetricDict['L3_HIT:Mean (E)'],MetricDict['L3_MISS:Mean (E)'],MetricDict['npf_wpl (E)'],MetricDict['PF_MISS:Mean (E)'],MetricDict['ALL_LOADS:Mean (E)'],MetricDict['PF_MISS:Mean (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["pf_wpl_min (E)"]=str(index+1)
            index+=2
         else:
            index+=1
         
#         #3 pf% (I)
#         f2.write("    <Metric i=\""+str(index)+"\" n=\"pf% (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"1\">\n")
#         f2.write("        <MetricFormula t=\"finalize\" frm=\"((${}*${})/(${}+${}+(${}*${})+(${}*${})))*100 \"/>\n".format(MetricDict['PF_MISS:Mean (I)'],MetricDict['pf_wpl_min (I)'],MetricDict['L2_HIT:Mean (I)'],MetricDict['L3_HIT:Mean (I)'],MetricDict['PF_MISS:Mean (I)'],MetricDict['pf_wpl_min (I)'],MetricDict['L3_MISS:Mean (I)'],MetricDict['npf_wpl (I)']))
#         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
#         f2.write("    </Metric>\n")
#         MetricDict["pf_percent (I)"]=str(index)      
#         if explicit:
#            #3 pf% (E)
#            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"pf% (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"1\">\n")
#            f2.write("        <MetricFormula t=\"finalize\" frm=\"((${}*${})/(${}+${}+(${}*${})+(${}*${})))*100 \"/>\n".format(MetricDict['PF_MISS:Mean (E)'],MetricDict['pf_wpl_min (E)'],MetricDict['L2_HIT:Mean (E)'],MetricDict['L3_HIT:Mean (E)'],MetricDict['PF_MISS:Mean (E)'],MetricDict['pf_wpl_min (E)'],MetricDict['L3_MISS:Mean (E)'],MetricDict['npf_wpl (E)']))
#            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
#            f2.write("    </Metric>\n")
#            MetricDict["pf_percent (E)"]=str(index+1)     
#            index+=2
#         else:
#            index+=1
#         
#         #4 L1|strided (I)
#         f2.write("    <Metric i=\""+str(index)+"\" n=\"L1|strided (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
#         f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}*${}) \"/>\n".format(MetricDict['pf_percent (I)'],MetricDict['L1_HIT:Mean (I)']))
#         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
#         f2.write("    </Metric>\n")
#         MetricDict["L1_strided (I)"]=str(index)      
#         if explicit:
#            #4 L1|strided (E)
#            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"L1|strided (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
#            f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}*${}) \"/>\n".format(MetricDict['pf_percent (E)'],MetricDict['L1_HIT:Mean (E)']))
#            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
#            f2.write("    </Metric>\n")
#            MetricDict["L1_strided (E)"]=str(index+1)      
#            index+=2
#         else:
#            index+=1
#
#         #5 pf_wpl<=(pf%) (I)
#         f2.write("    <Metric i=\""+str(index)+"\" n=\"pf_wpl_max1 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
#         f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}/${}) \"/>\n".format(MetricDict['L1_strided (I)'],MetricDict['PF_MISS:Mean (I)']))
#         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
#         f2.write("    </Metric>\n")
#         MetricDict["pf_wpl_max1 (I)"]=str(index)      
#         if explicit:
#            #5 pf_wpl<=(pf%) (E)
#            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"pf_wpl_max1 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
#            f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}/${}) \"/>\n".format(MetricDict['L1_strided (E)'],MetricDict['PF_MISS:Mean (E)']))
#            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
#            f2.write("    </Metric>\n")
#            MetricDict["pf_wpl_max1 (E)"]=str(index+1)      
#            index+=2
#         else:
#            index+=1
#
#         #5 pf_wpl<=(L1) (I)
#         f2.write("    <Metric i=\""+str(index)+"\" n=\"pf_wpl_max2 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
#         f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}/${}) \"/>\n".format(MetricDict['L1_HIT:Mean (I)'],MetricDict['PF_MISS:Mean (I)']))
#         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
#         f2.write("    </Metric>\n")
#         MetricDict["pf_wpl_max2 (I)"]=str(index)      
#         if explicit:
#            #5 pf_wpl<=(L1) (E)
#            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"pf_wpl_max2 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
#            f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}/${}) \"/>\n".format(MetricDict['L1_HIT:Mean (E)'],MetricDict['PF_MISS:Mean (E)']))
#            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
#            f2.write("    </Metric>\n")
#            MetricDict["pf_wpl_max2 (E)"]=str(index+1)      
#            index+=2
#         else:
#            index+=1

         #5 pf_wpl<=(min(line, L1/pf_miss) (I) 
         f2.write("    <Metric i=\""+str(index)+"\" n=\"pf_wpl_max3 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"min(64/{},${}/${}) \"/>\n".format(str(word),MetricDict['L1_HIT:Mean (I)'],MetricDict['PF_MISS:Mean (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["pf_wpl_max3 (I)"]=str(index)      
         if explicit:
            #5 pf_wpl<=(L1) (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"pf_wpl_max3 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            #f2.write("        <MetricFormula t=\"finalize\" frm=\"(64/{}) \"/>\n".format(str(word)))
            f2.write("        <MetricFormula t=\"finalize\" frm=\"min(64/{},${}/${}) \"/>\n".format(str(word),MetricDict['L1_HIT:Mean (E)'],MetricDict['PF_MISS:Mean (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["pf_wpl_max3 (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #6 pf_wpl* (I)
         #6 pf_wpl* (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"pf_wpl (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${} \"/>\n".format(MetricDict['pf_wpl_max3 (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["pf_wpl (I)"]=str(index)      
         if explicit:
            #6 pf_wpl* (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"pf_wpl (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${} \"/>\n".format(MetricDict['pf_wpl_max3 (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["pf_wpl (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #4 L1 (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"L1 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}-${}*${})+${} \"/>\n".format(MetricDict['L1_HIT:Mean (I)'],MetricDict['PF_MISS:Mean (I)'],MetricDict['pf_wpl (I)'],MetricDict['FB_HIT:Mean (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["L1 (I)"]=str(index)      
         if explicit:
            #4 L1 (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"L1 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}-${}*${})+${} \"/>\n".format(MetricDict['L1_HIT:Mean (E)'],MetricDict['PF_MISS:Mean (E)'],MetricDict['pf_wpl (E)'],MetricDict['FB_HIT:Mean (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["L1 (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

#         #6 pf_wpl* (I)
#         #6 pf_wpl* (I)
#         f2.write("    <Metric i=\""+str(index)+"\" n=\"pf_wpl (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
#         f2.write("        <MetricFormula t=\"finalize\" frm=\"min(${},${},${}) \"/>\n".format(MetricDict['pf_wpl_max1 (I)'],MetricDict['pf_wpl_max2 (I)'],MetricDict['pf_wpl_max3 (I)']))
#         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
#         f2.write("    </Metric>\n")
#         MetricDict["pf_wpl (I)"]=str(index)      
#         if explicit:
#            #6 pf_wpl* (E)
#            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"pf_wpl (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
#            f2.write("        <MetricFormula t=\"finalize\" frm=\"min(${},${},${}) \"/>\n".format(MetricDict['pf_wpl_max1 (E)'],MetricDict['pf_wpl_max2 (E)'],MetricDict['pf_wpl_max3 (E)']))
#            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
#            f2.write("    </Metric>\n")
#            MetricDict["pf_wpl (E)"]=str(index+1)      
#            index+=2
#         else:
#            index+=1

         #7 Mem (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"Mem (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"0\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"max(${}-sum((${}-(${}*${})),${},${},${}), 0) \"/>\n".format(MetricDict['ALL_LOADS:Mean (I)'],MetricDict['L1_HIT:Mean (I)'],MetricDict['PF_MISS:Mean (I)'],MetricDict['pf_wpl (I)'],MetricDict['FB_HIT:Mean (I)'],MetricDict['L2_HIT:Mean (I)'],MetricDict['L3_HIT:Mean (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["Mem (I)"]=str(index)      
         if explicit:
            #7 Mem (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"Mem (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"max(${}-sum((${}-(${}*${})),${},${},${}),0)\"/>\n".format(MetricDict['ALL_LOADS:Mean (E)'],MetricDict['L1_HIT:Mean (E)'],MetricDict['PF_MISS:Mean (E)'],MetricDict['pf_wpl (E)'],MetricDict['FB_HIT:Mean (E)'],MetricDict['L2_HIT:Mean (E)'],MetricDict['L3_HIT:Mean (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["Mem (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #7 Mem|~pf (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"Mem|~pf (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"0\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"max(${}-${}-${}-${}-${},${}) \"/>\n".format(MetricDict['ALL_LOADS:Mean (I)'],MetricDict['L1_HIT:Mean (I)'],MetricDict['FB_HIT:Mean (I)'],MetricDict['L2_HIT:Mean (I)'],MetricDict['L3_HIT:Mean (I)'],MetricDict['L3_MISS:Mean (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["Mem_div_npf (I)"]=str(index)      
         if explicit:
            #7 Mem|~pf (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"Mem|~pf (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"max(${}-${}-${}-${}-${},${}) \"/>\n".format(MetricDict['ALL_LOADS:Mean (E)'],MetricDict['L1_HIT:Mean (E)'],MetricDict['FB_HIT:Mean (E)'],MetricDict['L2_HIT:Mean (E)'],MetricDict['L3_HIT:Mean (E)'],MetricDict['L3_MISS:Mean (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["Mem_div_npf (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #7 Mem|pf (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"Mem|pf (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"0\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"max((${}-${}),0) \"/>\n".format(MetricDict['Mem (I)'],MetricDict['Mem_div_npf (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["Mem_div_pf (I)"]=str(index)      
         if explicit:
            #7 Mem|pf (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"Mem|pf (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"max((${}-${}),0) \"/>\n".format(MetricDict['Mem (E)'],MetricDict['Mem_div_npf (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["Mem_div_pf (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #8 L1_hits Survival (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"surv|L1 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"0\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}+${}+${} \"/>\n".format(MetricDict['L2_HIT:Mean (I)'],MetricDict['L3_HIT:Mean (I)'],MetricDict['Mem (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["L1_hit_surv (I)"]=str(index)      
         if explicit:
            #8 L1_hits Survival (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"surv|L1 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}+${}+${} \"/>\n".format(MetricDict['L2_HIT:Mean (E)'],MetricDict['L3_HIT:Mean (E)'],MetricDict['Mem (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["L1_hit_surv (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #8 L2_hits Survival (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"surv|L2 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"0\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}+${} \"/>\n".format(MetricDict['L3_HIT:Mean (I)'],MetricDict['Mem (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["L2_hit_surv (I)"]=str(index)      
         if explicit:
            #8 L1_hits Survival (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"surv|L2 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}+${} \"/>\n".format(MetricDict['L3_HIT:Mean (E)'],MetricDict['Mem (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["L2_hit_surv (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #8 L3_hits Survival (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"surv|L3 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"0\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${} \"/>\n".format(MetricDict['Mem (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["L3_hit_surv (I)"]=str(index)      
         if explicit:
            #8 L3_hits Survival (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"surv|L3 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${} \"/>\n".format(MetricDict['Mem (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["L3_hit_surv (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #8 L3_hits/npf Survival (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"~pf surv|L3 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"0\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}\"/>\n".format(MetricDict['Mem_div_npf (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["L3_hit_npf_surv (I)"]=str(index)      
         if explicit:
            #8 L3_hits/npf Survival (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"~pf surv|L3 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}\"/>\n".format(MetricDict['Mem_div_npf (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["L3_hit_npf_surv (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #9 ct|L1 (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"ct|L1 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"0\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (I)'],MetricDict['L1_hit_surv (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["ct-L1 (I)"]=str(index)      
         if explicit:
            #9 ct|L1 (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"ct|L1 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (E)'],MetricDict['L1_hit_surv (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["ct-L1 (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #9 ct|L2 (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"ct|L2 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"0\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (I)'],MetricDict['L2_hit_surv (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["ct-L2 (I)"]=str(index)      
         if explicit:
            #9 ct|L2 (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"ct|L2 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (E)'],MetricDict['L2_hit_surv (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["ct-L2 (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #9 ct|L3 (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"ct|L3 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"0\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (I)'],MetricDict['L3_hit_surv (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["ct-L3 (I)"]=str(index)      
         if explicit:
            #9 ct|L3 (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"ct|L3 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (E)'],MetricDict['L3_hit_surv (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["ct-L3 (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #9 ~pf ct|L3 (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"~pf ct|L3 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"0\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"max(${}/${},0) \"/>\n".format(MetricDict['ALL_LOADS:Mean (I)'],MetricDict['L3_hit_npf_surv (I)']))
#         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (I)'],MetricDict['Mem_div_npf (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["ct-L3_npf (I)"]=str(index)      
         if explicit:
            #9 ~pf ct|L3 (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"~pf ct|L3 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"max(${}/${},0) \"/>\n".format(MetricDict['ALL_LOADS:Mean (E)'],MetricDict['L3_hit_npf_surv (E)']))
#            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (E)'],MetricDict['Mem_div_npf (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["ct-L3_npf (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #10 fp ~pf (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"fp ~pf (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}*{})/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (I)'],str(word),MetricDict['ct-L3_npf (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["fp-L3_npf (I)"]=str(index)      
         if explicit:
            #10 fp ~pf (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"fp ~pf (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}*{})/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (E)'],str(word),MetricDict['ct-L3_npf (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["fp-L3_npf (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #10 fp|L1 (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"fp|L1 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}*{}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (I)'],str(word),MetricDict['ct-L1 (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["fp-L1 (I)"]=str(index)      
         if explicit:
            #10 fp|L1 (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"fp|L1 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}*{}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (E)'],str(word),MetricDict['ct-L1 (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["fp-L1 (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #10 fp|L2 (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"fp|L2 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}*{}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (I)'],str(word),MetricDict['ct-L2 (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["fp-L2 (I)"]=str(index)      
         if explicit:
            #10 fp|L2 (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"fp|L2 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}*{}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (E)'],str(word),MetricDict['ct-L2 (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["fp-L2 (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #10 fp|L3 (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"fp|L3 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}*{}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (I)'],str(word),MetricDict['ct-L3 (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["fp-L3 (I)"]=str(index)      
         if explicit:
            #10 fp|L3 (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"fp|L3 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}*{}/${} \"/>\n".format(MetricDict['ALL_LOADS:Mean (E)'],str(word),MetricDict['ct-L3 (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["fp-L3 (E)"]=str(index)      
            index+=2
         else:
            index+=1

         #10 fp (I) TODO FIXME this is not fill  I need to add L1 L2 L3 cache sizes
         f2.write("    <Metric i=\""+str(index)+"\" n=\"fp (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"min({},{}*${})+min({},{}*${})+ min({},{}*${})+{}*${}\"/>\n".format(str(L1_cache),str(word),MetricDict['L1 (I)'],str(L2_cache),str(word),MetricDict['L2_HIT:Mean (I)'],str(L3_cache),str(word),MetricDict['L3_HIT:Mean (I)'],str(word),MetricDict['Mem (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["fp (I)"]=str(index)      
         if explicit:
            #10 fp (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"fp (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"min({},{}*${})+min({},{}*${})+ min({},{}*${})+{}*${}\"/>\n".format(str(L1_cache),str(word),MetricDict['L1 (E)'],str(L2_cache),str(word),MetricDict['L2_HIT:Mean (E)'],str(L3_cache),str(word),MetricDict['L3_HIT:Mean (E)'],str(word),MetricDict['Mem (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["fp (E)"]=str(index)      
            index+=2
         else:
            index+=1


         #10 fp ~pf% (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"fp ~pf% (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"1\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}/${})*100 \"/>\n".format(MetricDict['fp-L3_npf (I)'],MetricDict['fp-L3 (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["fp-npf_percent (I)"]=str(index)      
         if explicit:
            #10 fp ~pf% (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"fp ~pf% (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"1\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}/${})*100 \"/>\n".format(MetricDict['fp-L3_npf (E)'],MetricDict['fp-L3 (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["fp-npf_percent (E)"]=str(index)      
            index+=2
         else:
            index+=1

         #11 rate|L1 (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"rate|L1 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['fp-L1 (I)'],MetricDict['ALL_LOADS:Mean (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["fp-L1_rate (I)"]=str(index)      
         if explicit:
            #11 rate|L1 (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"rate|L1 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['fp-L1 (E)'],MetricDict['ALL_LOADS:Mean (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["fp-L1_rate (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #11 rate|L2 (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"rate|L2 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['fp-L2 (I)'],MetricDict['ALL_LOADS:Mean (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["fp-L2_rate (I)"]=str(index)      
         if explicit:
            #10 rate|L2 (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"rate|L2 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['fp-L2 (E)'],MetricDict['ALL_LOADS:Mean (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["fp-L2_rate (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #11 rate|L3 (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"rate|L3 (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['fp-L3 (I)'],MetricDict['ALL_LOADS:Mean (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["fp-L3_rate (I)"]=str(index)      
         if explicit:
            #11 rate|L3 (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"rate|L3 (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['fp-L3 (E)'],MetricDict['ALL_LOADS:Mean (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["fp-L3_rate (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #11 ~pf rate (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"rate ~pf (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"0\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['fp-L3_npf (I)'],MetricDict['ALL_LOADS:Mean (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["fp-L3_npf_rate (I)"]=str(index)      
         if explicit:
            #11 ~pf rate (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"rate ~pf (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"0\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"${}/${} \"/>\n".format(MetricDict['fp-L3_npf (E)'],MetricDict['ALL_LOADS:Mean (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["fp-L3_npf_rate (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

         #12 ~pf rate% (I)
         f2.write("    <Metric i=\""+str(index)+"\" n=\"rate ~pf% (I)\" v=\"derived-incr\" t=\"inclusive\" partner=\""+str(index+1)+"\" show=\"1\" show-percent=\"1\">\n")
         f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}/${})*100 \"/>\n".format(MetricDict['fp-L3_npf_rate (I)'],MetricDict['fp-L3_rate (I)']))
         f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
         f2.write("    </Metric>\n")
         MetricDict["fp-npf_rate (I)"]=str(index)      
         if explicit:
            #12 ~pf rate% (E)
            f2.write("    <Metric i=\""+str(index+1)+"\" n=\"rate ~pf% (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\""+str(index)+"\" show=\"0\" show-percent=\"1\">\n")
            f2.write("        <MetricFormula t=\"finalize\" frm=\"(${}/${})*100 \"/>\n".format(MetricDict['fp-L3_npf_rate (E)'],MetricDict['fp-L3_rate (E)']))
            f2.write("        <Info><NV n=\"units\" v=\"events\"/></Info>\n")
            f2.write("    </Metric>\n")
            MetricDict["fp-npf_rate (E)"]=str(index+1)      
            index+=2
         else:
            index+=1

      
      f2.write(line)

#Test print
#   print MetricDict.get('Mem (I)')
#   print "\t<Metric i=\""+MetricDict['Mem (E)']+"\" n=\"Mem (E)\" v=\"derived-incr\" t=\"exclusive\" partner=\"100\" show=\"1\" show-percent=\"0\">"
#   print "\t\t<MetricFormula t=\"finalize\" frm=\"$62 + $42\"/>"
#   print "\t\t<Info><NV n=\"units\" v=\"events\"/></Info>"
#   print "\t </Metric>"
#   # MetricDict['']
   
   
#Clean up
   f1.close
   f2.close
   



if __name__== "__main__":   main()
