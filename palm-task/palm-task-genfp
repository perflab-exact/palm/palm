#!/bin/bash
# -*- mode: sh -*-

#*BeginPNNLCopyright*********************************************************
#
# $HeadURL$
# $Id$
#
#***********************************************************EndPNNLCopyright*

# set -x

scriptPath0="${BASH_SOURCE[0]}" # works when script is sourced (unlike $0)
scriptPath=$(readlink -f "${scriptPath0}")
scriptCmd=${scriptPath##*/} # cf. $(basename ...)
scriptDir=${scriptPath%/*}  # cf. $(dirname ...)

opt_cfgPath=''
opt_fpPath=''
opt_funcName=''
opt_outDir=''


#****************************************************************************
# Parse arguments
#****************************************************************************

die()
{
    cat <<EOF 1>&2
${scriptCmd}: $*
Use '${scriptCmd} -h' for usage.
EOF
    exit 1
}

usage()
{
    cat <<EOF
Usage: ${scriptCmd} --footprint <path> --cfgprof <path> --func <name> -o/--output <output folder name>
EOF
    exit 0
}

#-----------------------------------------------------------
# optional arguments
#-----------------------------------------------------------
while [[ $# -gt 0 ]] ; do

    arg="$1"
    shift # past argument
    
    case "${arg}" in
	-h | --help )
	    usage
	    ;;
    
	--cfgprof )
	    opt_cfgPath="$1"
	    shift # past value
	    ;;

	-o | --output )
	    opt_outDir="$1"
	    shift # past value
	    ;;
      	
        --footprint )
	    opt_fpPath="$1"
	    shift # past value
	    ;;
	
        --func )
	    opt_funcName="$1"
	    shift # past value
	    ;;
	
	* ) # unknown option
	    die "unknown option '$1'"
	    ;;
    esac
done

#-----------------------------------------------------------
# required args
#-----------------------------------------------------------

if [[ -z ${opt_cfgPath} ]] ; then
    die "no CFGProf path"
fi

if [[ -z ${opt_fpPath} ]] ; then
    die "no footprint path"
fi


#****************************************************************************
# 
#****************************************************************************

# find application name
my_appNm="${1##*/}" # cf. $(basename ...)


#-----------------------------------------------------------
# output directory and file
#-----------------------------------------------------------

my_outDir="./palm-${my_appNm}-footprint"
[[ -n ${opt_outDir} ]] && my_outDir="${opt_outDir}"

mkdir -p "${my_outDir}"

my_outFnm="${my_outDir}/${opt_funcName}"


#****************************************************************************
# 
#****************************************************************************

${scriptDir}/../../../palm-miami/MIAMI-palm/install/bin/miami \
    -c ${opt_cfgPath} \
    -m ${scriptDir}/test/x86_SandyBridge_EP.mdf \
    --fp_path ${opt_fpPath} \
    --func ${opt_funcName} \
    &> ${my_outFnm}
cat ${my_outFnm} | grep TotalFP
