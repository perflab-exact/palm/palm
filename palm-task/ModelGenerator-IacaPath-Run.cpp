// -*-Mode: C++;-*-

//*BeginPNNLCopyright********************************************************
//
// $HeadURL$
// $Id$
//
//**********************************************************EndPNNLCopyright*

//***************************************************************************
// $HeadURL$
//
// Nathan Tallent, Ryan Friese
//***************************************************************************

//***************************************************************************

#include <stdint.h>
#include <fnmatch.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <iterator>
#include <stack>

//***************************************************************************

#include <BPatch.h>
#include <BPatch_object.h>
#include <BPatch_addressSpace.h>
#include <BPatch_process.h>
#include <BPatch_binaryEdit.h>
#include <BPatch_point.h>
#include <BPatch_function.h>
#include <BPatch_flowGraph.h>
#include <BPatch_statement.h>
//#include <BPatch_callbacks.h>
//#include <BPatch_flowGraph.h>

#include <InstructionDecoder.h>
#include <InstructionSource.h>
//#include <ParseCallback.h>
#include <CodeObject.h>
#include <CFG.h>

#include <Symtab.h>
#include <Function.h>
//#include <PatchCFG.h>

#include "ModelGenerator-IacaPath.hpp"

//***************************************************************************
//
//***************************************************************************

std::fstream ofs;

int main(int argc, const char** argv, const char** envp){
	// -------------------------------------------------------
	// Parse command line
	// -------------------------------------------------------
	if (argc <= 2) {
		return 1;
	}

	ofs.open("iacaInsertDat.out");

	IacaInserter* inserter = new IacaInserter();
	std::string binaryInNm="";
	std::string binaryOutNm="";
	for(int i =1; i<argc;i++){
		std::cout<<argv[i]<<" ";
		if (strcmp("-b",argv[i])==0 | strcmp("--binary",argv[i])==0){ //binary name
			binaryInNm = argv[++i];
			if (binaryOutNm ==""){
				binaryOutNm = binaryInNm + ".out";
			}
		}
		else if (strcmp("-o",argv[i])==0 | strcmp("--binary-output-name",argv[i])==0){// instrumented binary name
			binaryOutNm = argv[++i];
		}
		else if(strcmp("-f",argv[i])==0 | strcmp("--function",argv[i])==0){ 
			std::string func = argv[++i];
			std::vector<std::string> fargs;
			std::stringstream ss(func);
			std::string item;
			while(getline(ss,item,';')){
				std::cout<<item<<std::endl;
				fargs.push_back(item);
			}
			inserter->addFunction(fargs);
		}
		else if(strcmp("-p",argv[i])==0 | strcmp("--path",argv[i])==0){
			inserter->parsePath(argv[++i]);
		}
		else if(strcmp("--u",argv[i])==0 | strcmp("--unroll",argv[i])==0){
			inserter->setUnrolling(atoi(argv[++i]));
		}
	}
	const char* progName = binaryInNm.c_str();
	Dyninst::ParseAPI::SymtabCodeSource* codeSrc = new Dyninst::ParseAPI::SymtabCodeSource((char*)binaryInNm.c_str());
	Dyninst::SymtabAPI::Symtab* symtab = codeSrc->getSymtabObject();

	BPatch bpatch;
	std::cout<<"palm-inst: "<<progName<<std::endl;
	BPatch_addressSpace* app = bpatch.openBinary(progName,false);
	std::cout<<"palm-inst: binary loaded "<<std::endl;

	Dyninst::ParseAPI::CodeObject* codeObj = new Dyninst::ParseAPI::CodeObject(codeSrc);
	std::cout <<"performing insert"<<std::endl;

	inserter->performInsert(symtab,codeSrc,app);	

	symtab->emit(binaryOutNm);

	ofs.close();
	return 0;
}
