#include <BPatch.h>
#include <BPatch_flowGraph.h>
#include <BPatch_object.h>
#include <BPatch_point.h>
#include <BPatch_statement.h>
#include <Function.h>
#include <Symtab.h>

#include <fstream>
#include <sstream>
#include <tuple>

#include "DyninstUtil.hpp"

extern std::ostream ofs;

/*
* using mangled function names is prefered as they map to a single function
* demangled names potentially can return multiple functions
*/
std::vector<BPatch_function *> findFunction(BPatch_addressSpace *app, std::string functionStr, std::string funcFile, int funcLine, bool regex) {
  ofs << "pid: " << ::getpid() << " "
      << "searching for function: " << functionStr << " " << funcFile << " " << funcLine << std::endl;
  if (regex) {
    ofs << functionStr << " " << funcFile << " " << funcLine << std::endl;
    std::vector<BPatch_function *> tfunctions;
    BPatch_image *appImage = app->getImage();
    std::vector<BPatch_module *> mods;
    appImage->getModules(mods);
    ofs << mods.size() << std::endl;
    std::string regexFunc = ".*" + functionStr + ".*";
    for (int m = 0; m < mods.size(); m++) {
      std::vector<BPatch_function *> ttfunctions;
      mods[m]->findFunction(regexFunc.c_str(), ttfunctions, false, true, false, false);
      for (int i = 0; i < ttfunctions.size(); i++) {
        if (mods[m]->libraryName() != NULL) {
          ofs << "lib-path: " << functionStr << " " << mods[m]->getObject()->pathName() << std::endl;
        }
      }
    }
    appImage->findFunction(regexFunc.c_str(), tfunctions, false, true, false);

    for (int i = 0; i < tfunctions.size(); i++) {
      ofs << tfunctions[i]->getName() << std::endl;
      std::vector<BPatch_statement> lines;
      bool srcLines = appImage->getSourceLines((unsigned long int)tfunctions[i]->getBaseAddr(), lines);
      if (srcLines) {
        std::string fn = lines[0].fileName();
        int ln = lines[0].lineNumber();
        ofs << functionStr << " " << funcFile << " " << fn << " " << funcLine << " " << ln << std::endl;
        if ((fn.find(funcFile) != std::string::npos) && ln == funcLine) {
          ofs << lines.size() << " " << lines[0].fileName() << " " << lines[0].lineNumber() << std::endl;
          break;
        }
      }
    }
    return tfunctions;
  } else {
    std::vector<BPatch_function *> tfunctions;
    BPatch_image *appImage = app->getImage();
    //string regexFunc = ".*"+functionStr+".*";
    appImage->findFunction(functionStr.c_str(), tfunctions, false, true, false);
    // ofs<<"num found: "<<tfunctions.size();
    // if (tfunctions.size() == 0){
    // 	std::vector<BPatch_module*> mods;
    // 	appImage->getModules(mods);
    // 	for(int m=0;m<mods.size();m++){
    // 		if(mods[m]->libraryName()!=NULL){
    // 			ofs<<mods[m]->getObject()->pathName()<<std::endl;
    // 		}
    // 	}
    // 	tfunctions=searchAllFunctions(appImage,functionStr);
    // }
    ofs << " num found: " << tfunctions.size() << std::endl;
    //printAllFunctions(appImage,functionStr);

    for (int f = 0; f < tfunctions.size(); f++) { //search for inlined functions (possibly do special analysis in inlined function found)
      ofs << tfunctions[f]->getName() << tfunctions[f]->getMangledName() << " inst: " << tfunctions[f]->isInstrumentable() << " " << (unsigned int *)tfunctions[f]->getBaseAddr() << std::endl;
      BPatch_flowGraph *fg = tfunctions[f]->getCFG();
      std::set<BPatch_basicBlock *> blks;
      fg->getAllBasicBlocks(blks);
      ofs << "num basic blocks: " << blks.size() << std::endl;
      Dyninst::SymtabAPI::Symtab *symtab = Dyninst::SymtabAPI::convert(tfunctions[f]->getModule()->getObject());
      ofs << (symtab == NULL) << std::endl;
      if (symtab != NULL) {
        Dyninst::SymtabAPI::Function *tf;
        if (symtab->findFuncByEntryOffset(tf, (Dyninst::Offset)tfunctions[f]->getBaseAddr())) {
          Dyninst::SymtabAPI::InlineCollection inlines = tf->getInlines();
          ofs << "inlined: " << inlines.size() << std::endl;
          for (Dyninst::SymtabAPI::InlineCollection::iterator it = inlines.begin(); it != inlines.end(); ++it) {
            Dyninst::SymtabAPI::FunctionBase *inFunc = (*it);
            ofs << "\t" << inFunc->getName() << std::endl;
            std::pair<std::string, Dyninst::Offset> callSite = dynamic_cast<Dyninst::SymtabAPI::InlinedFunction *>(inFunc)->getCallsite();
            ofs << callSite.first << " " << callSite.second << std::endl;
          }
        }
      }
    }
    return tfunctions;
  }
}

// create mapping from dyninst block numbering into our own block numbering
void createBasicBlockMap(BPatch_function *function, graph &g) {
  BPatch_flowGraph *fg = function->getCFG();
  std::set<BPatch_basicBlock *> blks;
  fg->getAllBasicBlocks(blks);
  ofs << "num basic blocks: " << blks.size() << std::endl;
  if (g.basicBlockNoMap.size() == 0) {
    int blkCnt = 0;
    for (auto it = blks.begin(); it != blks.end(); it++) {
      ofs << blkCnt << " " << std::hex << (*it)->getStartAddress() << "," << (*it)->getEndAddress() << "," << std::dec << (*it)->size() << std::endl;
      g.basicBlockNoMap[(*it)->blockNo()] = blkCnt++;
      //g.basicBlockNoMap[(*it)->blockNo()]=(*it)->getStartAddress();
      g.blks[g.basicBlockNoMap[(*it)->blockNo()]] = (*it);
    }
  }
}

std::vector<std::string> getCalledFunctions(BPatch_function *function, graph &g) {
  ofs << "getCalledFUnctions" << std::endl;
  BPatch_flowGraph *fg = function->getCFG();
  std::set<BPatch_basicBlock *> blks;
  fg->getAllBasicBlocks(blks);
  std::vector<std::string> calledFuncs;
  for (auto it = blks.begin(); it != blks.end(); it++) {
    BPatch_basicBlock *blk = *it;
    std::vector<BPatch_point *> tpnts;
    blk->getAllPoints(tpnts);
    for (int p = 0; p < tpnts.size(); p++) {
      if (tpnts[p]->getCalledFunction() != NULL) {
        BPatch_function *pntFunc = tpnts[p]->getCalledFunction();
        if (g.calledFunctionMap.find(std::to_string((long long)g.basicBlockNoMap[blk->blockNo()])) == g.calledFunctionMap.end()) {
          Dyninst::Address s, e;
          pntFunc->getAddressRange(s, e);
          g.calledFunctionMap[std::to_string((long long)g.basicBlockNoMap[blk->blockNo()])] = std::make_tuple<std::string, Dyninst::Address, bool>(pntFunc->getMangledName(), (Dyninst::Address)pntFunc->getBaseAddr(), pntFunc->isInstrumentable());
          //ofs<<"func: "<<function->getName()<<" blk: "<<g.basicBlockNoMap[blk->blockNo()]<<" function found! "<<pntFunc->getMangledName()<<std::endl;
          calledFuncs.push_back(pntFunc->getMangledName());
        }
      }
      if (tpnts[p]->isDynamic()) {
        //ofs<<"func: "<<function->getName()<<" blk: "<<g.basicBlockNoMap[blk->blockNo()]<<" is dynamic"<<std::endl;
        g.calledFunctionMap[std::to_string((long long)g.basicBlockNoMap[blk->blockNo()])] = std::make_tuple<std::string, Dyninst::Address, bool>("dynamic", 0, false);
        //tpnts[p]->monitorCalls();
      }
    }
  }
  return calledFuncs;
}

void printAllFunctions(BPatch_image *appImage) {
  std::vector<BPatch_function *> funcs = *(appImage->getProcedures(true));
  for (int f = 0; f < funcs.size(); f++) {
    ofs << funcs[f]->getName() << " " << funcs[f]->isInstrumentable() << std::endl;
  }
}

std::vector<BPatch_function *> searchAllFunctions(BPatch_image *appImage, std::string functionStr) {
  std::vector<BPatch_function *> funcs = *(appImage->getProcedures(true));
  std::vector<BPatch_function *> tfunctions;
  for (int f = 0; f < funcs.size(); f++) {
    if (funcs[f]->getName() == functionStr || funcs[f]->getMangledName() == functionStr) {
      ofs << funcs[f]->getName() << " " << funcs[f]->isInstrumentable() << std::endl;
      tfunctions.push_back(funcs[f]);
      break;
    }
  }
  return tfunctions;
}

std::ostream &serialize(std::ostream &os, graph &g) {
  if (g.executed) {
    os << "func|" << g.name << "|" << g.baseMangledName << "|" << std::hex << "0x" << g.startAddr << "|"
       << "0x" << g.endAddr << std::dec << std::endl;
    os << "lib-path|" << g.libPath << std::endl;
    os << "blocks|" << g.blks.size() << std::endl;
    std::set<int> blks;
    for (const auto &kv : g.basicBlockNoMap) {
      blks.insert(kv.second);
    }
    for (const auto &b : blks) {
      std::string blk = std::to_string((long long)b);
      os << std::dec << b << "|";
      std::stringstream temp;
      for (const auto &kv : g.ncounts[blk]) {
        temp << std::dec << kv.second << ",";
      }
      os << temp.str().erase(temp.str().size() - 1, 1);
      os << "|";
      //   if (g.lineNos.count(blk) != 0) {
      //     os << g.lineNos[blk];
      //   }
      //   os << "|";
      if (g.calledFunctionMap.count(blk) != 0) {
        os << std::get<0>(g.calledFunctionMap[blk]) << "," << std::hex << "0x" << std::get<1>(g.calledFunctionMap[blk]) << std::dec << "," << std::get<2>(g.calledFunctionMap[blk]);
      }
      os << "|";
      uint64_t start = g.blks[b]->getStartAddress();
      uint64_t end = g.blks[b]->getEndAddress();
      os << "0x" << std::hex << start << "|0x" << end << std::dec;
      os << std::endl;
    }
    os << "edges|" << g.edges.size() << std::endl;
    for (const auto &edge : g.edges) {
      os << edge << "|";
      std::stringstream temp;
      for (const auto &kv : g.ncounts[edge]) {
        temp << kv.second << ",";
      }
      os << temp.str().erase(temp.str().size() - 1, 1);
      os << std::endl;
    }
    os << "loops|" << g.loops.size() << std::endl;
    for (const auto &kv : g.loops) {
      os << kv.first + "|" << kv.second << "|" << g.loopEntries[kv.first] << std::endl;
    }
    os << std::endl;

    return os;
  }
  return os;
}
