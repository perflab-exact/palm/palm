#ifndef IACAINSERTER_H
#define IACAINSERTER_H

#include "DyninstUtil.hpp"

class IacaInserter
{
public:
	IacaInserter();
	~IacaInserter();
	void parsePath();
	void parsePath(std::string path);
	void performInsert(Dyninst::SymtabAPI::Symtab* symtab, Dyninst::ParseAPI::SymtabCodeSource* codeSrc,BPatch_addressSpace* app);
	void createBasicBLockPath(BPatch_function* function, std::map<std::string,std::vector<Dyninst::ParseAPI::Block*> >& paths, graph& g, std::vector<int> path, std::string spath);
	void insertIACAAroundPath(std::map<std::string,std::vector<uint8_t> >& bufs,Dyninst::ParseAPI::SymtabCodeSource* codeSrc, std::map<std::string,std::vector<Dyninst::ParseAPI::Block*> >& paths);
	void insertIACAStart(std::vector<uint8_t>& buf);
	void insertIACAEnd(std::vector<uint8_t>& buf);


	void addFunction(std::vector<std::string> function);
	void setFunctionNames(std::vector<std::string> functionNms);
	std::vector<std::string>& getFunctionNames();


	void setFunctionFiles(std::map<std::string,std::string> funcFiles);
	std::map<std::string,std::string>& getFunctionFiles();

	void setFunctionLines(std::map<std::string,int> funcLines);
	std::map<std::string,int>& getFunctionLines();

	void setPath(std::string spath);
	std::string getPath();

	void setUnrolling(int unrolling);
	int getUnrolling();

	void setGraph(graph g);
	graph getGraph();

private:
	std::vector<std::string> _functionNms;
	std::map<std::string,std::string> _funcFiles;
	std::map<std::string, int> _funcLines;
	std::string _spath;
	std::vector<int> _path;
	int _unrolling;
	graph _g;
};

#endif /* IACAINSERTER_H */