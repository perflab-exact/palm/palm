#ifndef INSTRUMENTOR_H
#define INSTRUMENTOR_H

#include <vector>
#include <map>
#include <queue>
#include <unordered_set>

#include <BPatch_flowGraph.h>

#include "DyninstUtil.hpp"

struct graph;
class Instrumentor{
public:
	Instrumentor();
	~Instrumentor();

	int constructCallGraph(BPatch_addressSpace* app, std::string appProcPid);

	int instrumentFunctions(BPatch_addressSpace* app, std::string appProcPid);
	int instrumentFunctions(BPatch_addressSpace* app, std::string appProcPid,int funcLevel);
	int instrumentDynamicFunction(BPatch_point *at_point, BPatch_function *called_function);
	void instrumentDynamicFunction2(BPatch_process *proc);
	void instrumentFunction(BPatch_addressSpace* app, std::string name, BPatch_function *func, std::string appProcPid,int funcLevel);
	void instrumentFunction(BPatch_addressSpace* app, std::string name, BPatch_function *func, std::vector<std::string>& newFuncs, std::string appProcPid,int funcLevel);

	void monitorFunctionCall(BPatch_addressSpace* app, BPatch_point* pnt, graph& g);
	void monitorDynamicCall(BPatch_addressSpace* app, BPatch_point* pnt, graph& g);
	void monitorDynamicCallOMP(BPatch_addressSpace* app, BPatch_point* pnt, graph& g);


	
	void startCFG(BPatch_addressSpace* app,std::string functionStr,std::string appProcPid, BPatch_function* function,std::map<std::string,bool>&functionNmsMap,graph& g,bool useOmp);
	void traverseCFG(BPatch_addressSpace* app,std::string function,std::string appProcPid, BPatch_basicBlock* blk,std::map<BPatch_basicBlock *,bool>& seen,graph& g, bool useOmp);
	void startCFGEdges(BPatch_addressSpace* app,std::string functionStr,std::string appProcPid, BPatch_function* function,std::map<std::string,bool>&functionNmsMap,graph& g,bool useOmp);
	void traverseCFGEdges(BPatch_addressSpace* app,std::string function,std::string appProcPid,BPatch_basicBlock* blk,std::map<std::string,bool>& seen,graph& g,BPatch_edge* edge, bool useOmp);
	bool createAndInsertCounterSnippet(BPatch_addressSpace* app, std::vector<BPatch_point*>* points, std::string funcName,std::string blkOrEdge,std::string appProcPid,graph &g);
	int gatherCounts(BPatch_addressSpace* app, std::string appProcPid);
	bool createAndInsertCounterSnippetOMP(BPatch_addressSpace* app, std::vector<BPatch_point*>* points, std::string funcName,std::string blkOrEdge,std::string appProcPid,graph &g);
	int gatherCountsOMP(BPatch_addressSpace* app, std::string appProcPid);
	void getLoopBasicBlocks(BPatch_addressSpace* app, std::string name, BPatch_function* function,graph& g);
	void traverseLoopTree(BPatch_loopTreeNode* ltn,std::vector<std::pair<std::string, BPatch_basicBlockLoop*> >&loops);


	void addFunction(std::vector<std::string> function);
	void addFunction(std::string function);

	// void setFunctionNames(std::vector<std::string> functionNms);
	// std::vector<std::string>& getFunctionNames();

	void setFunctionNames(std::unordered_set<std::string> functionNms);
	std::unordered_set<std::string>& getFunctionNames();

	void setFunctionNamesMap(std::map<std::string,bool> functionNmsMap);
	std::map<std::string,bool>& getFunctionNamesMap();

	void setFunctionFiles(std::map<std::string,std::string> funcFiles);
	std::map<std::string,std::string>& getFunctionFiles();

	void setFunctionLines(std::map<std::string,int> funcLines);
	std::map<std::string,int>& getFunctionLines();

	void setGraphs(std::map<std::string,graph*> graphs);
	std::map<std::string,graph*>& getGraphs();

	void setMangledNameMap(std::map<std::string,std::vector<std::string> > mangledNameMap);
	std::map<std::string,std::vector<std::string> >& getMangledNameMap();

	void setFCnt(int fCnt);
	int getFCnt();

	void setDoEdges(bool doEdges);
	bool getDoEdges();

	void setUseOmp(bool useOmp);
	bool getUseOmp();

	void setCG(bool cg);
	bool getCG();

	void setDynamicCalls(bool dc);
	bool getDynamicCalls();

	void setRunTimeCalls(bool rtc);
	bool getRunTimeCalls();


	void setMaxFuncLevel(int maxFuncLevel);
	int getMaxFuncLevel();
	

private:
	std::queue<std::string> _funcsToProcess;
	std::unordered_set<std::string> _funcsVisited;
	std::unordered_set<std::string> _functionNms;
	//std::vector<std::string> _functionNms;
	std::map<std::string,bool> _functionNmsMap;
	std::map<std::string,std::string> _funcFiles;
	std::map<std::string,int> _funcLines;
	std::map<std::string,graph*> _graphs;
	std::map<std::string,std::vector<std::string> > _mangledNameMap;
	
	int _fCnt;
	bool _doEdges;
	bool _useOmp;
	bool _cg;
	bool _dynamicCalls;
	bool _runTimeCalls;
	int _maxFuncLevel;
	BPatch_type* _long_long;
	BPatch_type* _unsigned_long;
	BPatch_type* _int;
	BPatch_variableExpr* _lock;
	BPatch_variableExpr* _dynAddrs;
	
};

#endif /* INSRUMENTOR_H */