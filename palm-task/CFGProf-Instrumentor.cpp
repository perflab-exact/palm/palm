#include <BPatch.h>
#include <BPatch_object.h>
#include <BPatch_point.h>
#include <BPatch_statement.h>
#include <CodeObject.h>
#include <PatchMgr.h>
#include <boost/thread.hpp>
#include <dyninstRTExport.h>

#include <fstream>

#include "CFGProf-Instrumentor.hpp"

extern std::ostream ofs;

Instrumentor::Instrumentor() {
  _fCnt = 0;
  _doEdges = true;
  _useOmp = false;
  _dynamicCalls = true;
  _cg = false;
  _maxFuncLevel = 1;
  _long_long = NULL;
  _dynAddrs = NULL;
  _int = NULL;
  _unsigned_long = NULL;
  _lock = NULL;
  _runTimeCalls = true;
}

Instrumentor::~Instrumentor() {
  for (std::map<std::string, graph *>::iterator git = _graphs.begin(); git != _graphs.end(); ++git) {
    graph *g = (*git).second;
    delete g;
  }
}

int Instrumentor::instrumentFunctions(BPatch_addressSpace *app, std::string appProcPid) {
  BPatch_image *appImage = app->getImage();
  if (_long_long == NULL) {
    _long_long = appImage->findType("long long");
  }
  if (_int == NULL) {
    _int = appImage->findType("int");
  }
  if (_unsigned_long == NULL) {
    _unsigned_long = appImage->findType("unsigned long");
  }
  // if (_lock == NULL){

  // 	BPatch_type* voidPtr = BPatch::bpatch->createTypedef("dyntid_t",BPatch::bpatch->createPointer("voidp",appImage->findType("void")));
  // 	std::vector<char *> fieldNames;
  // 	fieldNames.push_back("mutex");
  // 	fieldNames.push_back("tid");
  // 	std::vector<BPatch_type*> fieldTypes;
  // 	fieldTypes.push_back(appImage->findType("volatile float"));
  // 	fieldTypes.push_back(voidPtr);
  // 	ofs<<fieldTypes[0]<<" "<<fieldTypes[1]<<std::endl;
  // 	BPatch_type* lock = BPatch::bpatch->createStruct("dyninst_lock_t_struct",fieldNames,fieldTypes);
  // 	ofs<<lock<<std::endl;
  // 	BPatch_type* lockType = BPatch::bpatch->createTypedef("dyninst_lock_t",lock);
  // 	ofs<<lockType<<std::endl;
  // 	std::string lockStr = "palm_lock";
  // 	_lock = app->malloc(*lockType,lockStr.c_str());
  // 	dyninst_lock_t lck = {0, DYNINST_INITIAL_LOCK_PID};
  // 	_lock->writeValue(&lck);
  // }
  return instrumentFunctions(app, appProcPid, 0);
}

int Instrumentor::constructCallGraph(BPatch_addressSpace *app, std::string appProcPid) {
  std::vector<std::string> unprocessed;
  while (!_funcsToProcess.empty()) {
    std::string name = _funcsToProcess.front();
    _funcsToProcess.pop();
    if (_funcsVisited.count(name) == 0) {
      std::vector<BPatch_function *> funcs = findFunction(app, name, _funcFiles[name], _funcLines[name], false);
      if (funcs.size() > 0) {
        for (int i = 0; i < funcs.size(); i++) {
          BPatch_function *func = funcs[i];
          if (_funcsVisited.count(func->getMangledName()) == 0) {
            //_functionNms.push_back(func->getMangledName());
            _functionNms.insert(func->getMangledName());
            graph *g = NULL;
            if (_graphs.count(func->getMangledName()) == 0) {
              g = new graph;
              _graphs[func->getMangledName()] = g;
              g->name = func->getMangledName();
            } else {
              g = _graphs[func->getMangledName()];
            }
            g->mangledName = func->getMangledName();
            ofs << func->getMangledName() << std::endl;
            createBasicBlockMap(func, *g);
            std::vector<std::string> calledFuncs = getCalledFunctions(func, *g);
            for (int f = 0; f < calledFuncs.size(); f++) {
              _funcsToProcess.push(calledFuncs[f]);
            }
            _funcsVisited.insert(func->getMangledName());
          }
        }
        _funcsVisited.insert(name);
      } else {
        unprocessed.push_back(name);
      }
    }
  }
  for (int f = 0; f < unprocessed.size(); f++) {
    _funcsToProcess.push(unprocessed[f]);
  }
  return 0;
}

int Instrumentor::instrumentFunctions(BPatch_addressSpace *app, std::string appProcPid, int funcLevel) {
  bool doInst = true;
  ofs << "here " << _functionNms.size() << std::endl;
  app->beginInsertionSet();
  ofs << doInst << " " << funcLevel << " " << _maxFuncLevel << std::endl;
  while (doInst && funcLevel < _maxFuncLevel) {
    doInst = false;
    std::vector<std::string> newFuncs;
    for (auto it = _functionNms.begin(); it != _functionNms.end(); ++it) {
      std::string name = (*it);
      if (_functionNmsMap.count(appProcPid + "_" + name) == 0) {
        std::vector<BPatch_function *> funcs = findFunction(app, name, _funcFiles[name], _funcLines[name], false);
        for (int i = 0; i < funcs.size(); i++) {
          BPatch_function *func = funcs[i];
          instrumentFunction(app, name, func, newFuncs, appProcPid, funcLevel);
        }
      }
    }
    funcLevel++;
    if (funcLevel < _maxFuncLevel) {
      for (int f = 0; f < newFuncs.size(); f++) {
        //if (std::find(_functionNms.begin(),_functionNms.end(),newFuncs[f]) == _functionNms.end()){
        if (_functionNms.count(newFuncs[f]) == 0) {
          //_functionNms.push_back(newFuncs[f]);
          _functionNms.insert(newFuncs[f]);
          doInst = true;
        }
      }
    }
  }
  bool inserted = app->finalizeInsertionSet(true);
  ofs << "inserted: " << inserted << std::endl;
  ofs.flush();
  return inserted;
}

int Instrumentor::instrumentDynamicFunction(BPatch_point *at_point, BPatch_function *called_function) {
  //int ret = at_point->stopMonitoring();
  //at_point->removeSnippet(_graphs[at_point->getFunction()->getMangledName()]->dynamicCalls[_graphs[at_point->getFunction()->getMangledName()]->basicBlockNoMap[at_point->getBlock()->blockNo()]]);

  // Dyninst::PatchAPI::PatchMgrPtr pm = Dyninst::PatchAPI::convert(at_point->getFunction()->getAddSpace());
  // graph *g = _graphs[at_point->getFunction()->getMangledName()+"_"+std::to_string((long long)at_point->getFunction()->getBaseAddr())];
  // void* t = g->dynamicCalls[g->basicBlockNoMap[at_point->getBlock()->blockNo()]];
  // Dyninst::PatchAPI::InstancePtr *ip = static_cast<Dyninst::PatchAPI::InstancePtr*>(t);
  // //at_point->stopMonitoring();
  // (*ip)->destroy();
  //pm.get()->removeSnippet(*ip);
  //
  std::stringstream ss;
  ss << called_function->getMangledName() << "_" << std::hex << called_function->getBaseAddr() << std::dec;
  //if (_graphs.count(called_function->getMangledName()+"_"+std::to_string((long long)called_function->getBaseAddr()) )==0){
  if (_graphs.count(ss.str()) == 0) {
    // if (called_function->getModule()->libraryName()!=NULL){
    // 	ofs<<"lib-path: "<<called_function->getName()<<" "<<called_function->getModule()->getObject()->pathName()<<std::endl;
    // }
    //graph *g = _graphs[at_point->getFunction()->getMangledName()+"_"+std::to_string((long long)at_point->getFunction()->getBaseAddr())];
    graph *g = _graphs[ss.str()];
    // Dyninst::PatchAPI::InstancePtr *ip = g->dynamicCalls[g.basicBlockNoMap[at_point->getBlock->blockNo()]];
    // ofs<<ip->format()<<std::endl;
    int seen = true;
    //if (std::find(_functionNms.begin(),_functionNms.end(),called_function->getMangledName()) == _functionNms.end()){
    if (_functionNms.count(called_function->getMangledName()) == 0) {
      //_functionNms.push_back(called_function->getMangledName());
      _functionNms.insert(called_function->getMangledName());
      seen = false;
    }
    if (!seen) {
      ofs << "pid: " << ::getpid() << " dynamic_call!!! g: " << g << " calling func: " << at_point->getFunction()->getName() << " " << at_point->getFunction()->getMangledName() << " called func: " << called_function->getName() << std::endl;
    }
    //g->calledFunctionMap[std::to_string((long long)g->basicBlockNoMap[at_point->getBlock()->blockNo()])] = called_function->getMangledName();

    BPatch_addressSpace *app = called_function->getAddSpace();
    BPatch_process *proc = dynamic_cast<BPatch_process *>(app);
    std::string appProcPid = std::to_string((long long)proc->getPid());
    if (called_function->isInstrumentable() && g->level + 1 < _maxFuncLevel) {
      app->beginInsertionSet();
      instrumentFunction(app, called_function->getMangledName(), called_function, appProcPid, g->level + 1);
      app->finalizeInsertionSet(true);
      instrumentFunctions(app, appProcPid, g->level + 1);
    }
    // if (!seen){
    // 	//searchAllFunctions(app->getImage(),called_function->getMangledName());
    // 	for (std::map<std::string, std::vector<std::string> >::iterator it = _mangledNameMap.begin();it != _mangledNameMap.end();++it){
    // 		for(int n =0; n < (*it).second.size();n++){
    // 			ofs<<"mangled-name-map: "<<(*it).first<<" "<<(*it).second[n]<<std::endl;
    // 		}
    // 	}
    // }
  }
  return 0;
}

void Instrumentor::instrumentDynamicFunction2(BPatch_process *proc) {
  Dyninst::Address tVar[3];
  ofs << "instrument dynamic function" << std::endl;
  _dynAddrs->readValue(&tVar, sizeof(Dyninst::Address) * 3);
  ofs << "caller: " << std::hex << tVar[0] << " callee: " << std::hex << tVar[1] << " pnt:  " << std::hex << tVar[2] << std::endl;

  std::vector<BPatch_function *> callerFuncs;
  Dyninst::Address taddr = (Dyninst::Address)tVar[0];
  proc->getImage()->findFunction(taddr, callerFuncs);
  ofs << "dyn funcs, caller: " << std::hex << tVar[0] << std::dec << " " << callerFuncs.size() << std::endl;

  std::vector<BPatch_function *> calleeFuncs;
  taddr = (Dyninst::Address)tVar[1];
  proc->getImage()->findFunction(taddr, calleeFuncs);
  ofs << "dyn funcs, callee: " << std::hex << tVar[1] << std::dec << " " << calleeFuncs.size() << std::endl;
  if (callerFuncs.size() > 0 and calleeFuncs.size() > 0) {
    ofs << "going to instrument" << std::endl;
    BPatch_function *caller_function = callerFuncs[0];
    BPatch_function *callee_function = calleeFuncs[0];
    ofs << "calling func: " << caller_function->getName() << " called func: " << callee_function->getName() << " check if exists: " << (_graphs.count(callee_function->getMangledName() + ":" + std::to_string((long long)callee_function->getBaseAddr())) == 0) << std::endl;
    std::stringstream ss;
    ss << callee_function->getMangledName() << ":" << std::hex << callee_function->getBaseAddr() << std::dec;
    //if (_graphs.count(callee_function->getMangledName()+":"+std::to_string((long long)callee_function->getBaseAddr()) )==0){
    if (_graphs.count(ss.str()) == 0) {
      ss.str("");
      ss.clear();
      ss << caller_function->getMangledName() << ":" << std::hex << caller_function->getBaseAddr() << std::dec;
      //graph *g = _graphs[caller_function->getMangledName()+":"+std::to_string((long long)caller_function->getBaseAddr())];
      graph *g = _graphs[ss.str()];
      int seen = true;
      if (_functionNms.count(callee_function->getMangledName()) == 0) {
        _functionNms.insert(callee_function->getMangledName());
        seen = false;
      }
      if (!seen) {
        ofs << "pid: " << ::getpid() << " dynamic_call!!! g: " << g << " calling func: " << caller_function->getName() << " " << caller_function->getMangledName() << " atPoint " << tVar[2] << " " << g->dynamicCallPnts[tVar[2]] << " called func: " << callee_function->getName() << " " << callee_function->getMangledName() << " instrumentable: " << callee_function->isInstrumentable() << " " << (g->level + 1 < _maxFuncLevel) << std::endl;
      }
      g->calledFunctionMap[std::to_string((long long)g->basicBlockNoMap[g->dynamicCallPnts[tVar[2]]->getBlock()->blockNo()])] = std::make_tuple<std::string, Dyninst::Address, bool>(callee_function->getMangledName(), (Dyninst::Address)callee_function->getBaseAddr(), callee_function->isInstrumentable());

      BPatch_addressSpace *app = callee_function->getAddSpace();
      std::string appProcPid = std::to_string((long long)proc->getPid());
      ofs << "instrumentable: " << callee_function->isInstrumentable() << " < max level: " << (g->level + 1 < _maxFuncLevel) << std::endl;
      if (callee_function->isInstrumentable() && g->level + 1 < _maxFuncLevel) {
        app->beginInsertionSet();
        instrumentFunction(app, callee_function->getMangledName(), callee_function, appProcPid, g->level + 1);
        app->finalizeInsertionSet(true);
        instrumentFunctions(app, appProcPid, g->level + 1);
      }
      // if (!seen){
      // 	//searchAllFunctions(app->getImage(),called_function->getMangledName());
      // 	for (std::map<std::string, std::vector<std::string> >::iterator it = _mangledNameMap.begin();it != _mangledNameMap.end();++it){
      // 		for(int n =0; n < (*it).second.size();n++){
      // 			ofs<<"mangled-name-map: "<<(*it).first<<" "<<(*it).second[n]<<std::endl;
      // 		}
      // 	}
      // }

    } else {
      ofs << "called func: " << callee_function->getName() << " already instrumented... updating block" << std::endl;
      ss.str("");
      ss.clear();
      ss << caller_function->getMangledName() << ":" << std::hex << caller_function->getBaseAddr() << std::dec;
      //graph *g = _graphs[caller_function->getMangledName()+":"+std::to_string((long long)caller_function->getBaseAddr())];
      graph *g = _graphs[ss.str()];
      g->calledFunctionMap[std::to_string((long long)g->basicBlockNoMap[g->dynamicCallPnts[tVar[2]]->getBlock()->blockNo()])] = std::make_tuple<std::string, Dyninst::Address, bool>(callee_function->getMangledName(), (Dyninst::Address)callee_function->getBaseAddr(), callee_function->isInstrumentable());
    }
  }
  ofs << "done with dynamic instrumentation" << std::endl;
}

void Instrumentor::instrumentFunction(BPatch_addressSpace *app, std::string name, BPatch_function *func, std::string appProcPid, int funcLevel) {
  std::vector<std::string> newFuncs;
  instrumentFunction(app, name, func, newFuncs, appProcPid, funcLevel);
  if (funcLevel + 1 < _maxFuncLevel) {
    for (int f = 0; f < newFuncs.size(); f++) {
      //if (std::find(_functionNms.begin(),_functionNms.end(),newFuncs[f]) == _functionNms.end()){
      if (_functionNms.count(newFuncs[f]) == 0) {
        //_functionNms.push_back(newFuncs[f]);
        _functionNms.insert(newFuncs[f]);
        //doInst = true;
      }
    }
  }
}

void Instrumentor::instrumentFunction(BPatch_addressSpace *app, std::string name, BPatch_function *func, std::vector<std::string> &newFuncs, std::string appProcPid, int funcLevel) {
  ofs << "instrumenting: " << name << " " << func->getMangledName() << " funcLevel: " << funcLevel << " isInst: " << func->isInstrumentable() << std::endl;
  if (func != 0) {
    std::stringstream ss;
    ss << func->getMangledName() << ":" << std::hex << func->getBaseAddr() << std::dec;
    //std::string mangledName=func->getMangledName()+":"+std::to_string((long long)func->getBaseAddr());
    std::string mangledName = ss.str();

    if (_functionNmsMap.count(appProcPid + "_" + mangledName) == 0) {
      graph *g = NULL;
      if (_graphs.count(mangledName) == 0) {
        g = new graph;
        _graphs[mangledName] = g;
        g->name = func->getMangledName();

      } else {
        g = _graphs[mangledName];
      }
      g->mangledName = mangledName;
      g->baseMangledName = func->getMangledName();
      func->getAddressRange(g->startAddr, g->endAddr);
      g->cnt = 0;
      g->level = funcLevel;
      if (func->getModule()->libraryName() != NULL) {
        //g->libPath = "lib-path: "+mangledName+" "+func->getModule()->getObject()->pathName();
        g->libPath = func->getModule()->getObject()->pathName();
        ofs << "lib-path: " << mangledName << " " << func->getModule()->getObject()->pathName() << std::endl;
      }

      ofs << "pid: " << ::getpid() << " " << appProcPid << " "
          << "instrumenting: " << mangledName << std::endl;
      std::set<BPatch_function *> sharedFuncs;
      bool sharedF = func->getSharedFuncs(sharedFuncs);
      ofs << "shared found? " << sharedF << std::endl;
      for (std::set<BPatch_function *>::iterator sf = sharedFuncs.begin(); sf != sharedFuncs.end(); ++sf) {
        ofs << "\t shared func" << (*sf)->getName() << std::endl;
      }
      createBasicBlockMap(func, *g);
      if (_doEdges) {
        startCFG(app, mangledName, appProcPid, func, _functionNmsMap, *g, _useOmp);
        startCFGEdges(app, mangledName, appProcPid, func, _functionNmsMap, *g, _useOmp);
        getLoopBasicBlocks(app, mangledName, func, *g);
      } else {
        startCFG(app, mangledName, appProcPid, func, _functionNmsMap, *g, _useOmp);
        getLoopBasicBlocks(app, mangledName, func, *g);
      }
      _functionNmsMap[appProcPid + "_" + mangledName] = true;
      _fCnt += 1;
      ofs << appProcPid + "_" + name << std::endl;
      ofs << "pid: " << ::getpid() << " "
          << "funcs instrumented: " << _fCnt << std::endl;
      _functionNmsMap[appProcPid + "_" + name] = true;
      _mangledNameMap[func->getName()].push_back(mangledName);
      for (auto fit = g->calledFunctionMap.begin(); fit != g->calledFunctionMap.end(); ++fit) {
        newFuncs.push_back(std::get<0>((*fit).second));
      }
    }
    // else{
    // 	ofs<<"look for called functions 0"<<std::endl;
    // 	graph* g = _graphs[mangledName];
    // 	getCalledFunctions(func,*g);
    // }
  }
  ofs << "finished instrumenting: " << func->getMangledName() << std::endl;
}

void Instrumentor::monitorFunctionCall(BPatch_addressSpace *app, BPatch_point *pnt, graph &g) {
  BPatch_process *proc = dynamic_cast<BPatch_process *>(app);
  BPatch_image *appImage = app->getImage();
  std::vector<BPatch_function *> funcs;
  appImage->findFunction("DYNINSTuserMessage", funcs);
  if (!funcs.size()) {
    std::cerr << "cannot find DYNINSTasyncDynFuncCall - should be in dyninst runtime library" << std::endl;
  }
  if (_dynAddrs == NULL) {
    std::stringstream arrayTypeName;
    arrayTypeName << "unsigned long"
                  << "[" << 3 << "]";
    if (_unsigned_long == NULL) {
      _unsigned_long = appImage->findType("unsigned long");
    }
    BPatch_type *array = BPatch::bpatch->createArray(arrayTypeName.str().c_str(), _unsigned_long, 0, 2);
    _dynAddrs = app->malloc(*array);
    _dynAddrs = app->createVariable("dynInfo", (Dyninst::Address)_dynAddrs->getBaseAddr(), array);
  }

  if (_int == NULL) {
    _int = appImage->findType("int");
  }

  std::string visitedStr = "visited_" + std::to_string((unsigned long)pnt->getAddress());
  ofs << "visitedStr: " << visitedStr << std::endl;
  BPatch_variableExpr *visited = app->malloc(*_int, visitedStr.c_str());

  BPatch_constExpr zero(0);
  BPatch_arithExpr caller(BPatch_ref, *_dynAddrs, zero);
  BPatch_arithExpr getCallerAddr(BPatch_assign, caller, BPatch_constExpr(pnt->getFunction()->getBaseAddr()));

  BPatch_constExpr one(1);
  BPatch_arithExpr setVistited(BPatch_assign, *visited, one);
  BPatch_arithExpr callee(BPatch_ref, *_dynAddrs, one);
  BPatch_arithExpr getCalleeAddr(BPatch_assign, callee, BPatch_constExpr(pnt->getCalledFunction()->getBaseAddr()));

  BPatch_constExpr two(2);
  BPatch_arithExpr atPoint(BPatch_ref, *_dynAddrs, two);
  BPatch_arithExpr getPointAddr(BPatch_assign, atPoint, BPatch_constExpr(pnt->getAddress()));

  std::vector<BPatch_snippet *> dynCallArgs;
  BPatch_constExpr msg("");
  dynCallArgs.push_back(&msg);

  BPatch_constExpr dynSize(1);
  dynCallArgs.push_back(&dynSize);
  BPatch_funcCallExpr monFunc(*funcs[0], dynCallArgs);

  std::vector<BPatch_snippet *> items;
  items.push_back(&setVistited);
  items.push_back(&getCallerAddr);
  items.push_back(&getCalleeAddr);
  items.push_back(&getPointAddr);
  items.push_back(&monFunc);

  BPatch_sequence getAddrs(items);

  BPatch_ifExpr ifNotVisited(BPatch_boolExpr(BPatch_eq, *visited, zero), getAddrs);
  if (!app->insertSnippet(ifNotVisited, *pnt)) {
    ofs << "here2" << std::endl;
  }

  ofs << &g << " " << pnt->getFunction()->getName() << " " << pnt->getAddress() << " " << (Dyninst::Address)pnt->getAddress() << " " << pnt << std::endl;
  g.dynamicCallPnts[(Dyninst::Address)pnt->getAddress()] = pnt;
}

void Instrumentor::monitorDynamicCall(BPatch_addressSpace *app, BPatch_point *pnt, graph &g) {
  BPatch_process *proc = dynamic_cast<BPatch_process *>(app);
  BPatch_image *appImage = app->getImage();
  std::vector<BPatch_function *> funcs;
  appImage->findFunction("DYNINSTuserMessage", funcs);
  if (!funcs.size()) {
    std::cerr << "cannot find DYNINSTasyncDynFuncCall - should be in dyninst runtime library" << std::endl;
  }
  if (_dynAddrs == NULL) {
    std::stringstream arrayTypeName;
    arrayTypeName << "unsigned long"
                  << "[" << 3 << "]";
    if (_unsigned_long == NULL) {
      _unsigned_long = appImage->findType("unsigned long");
    }
    BPatch_type *array = BPatch::bpatch->createArray(arrayTypeName.str().c_str(), _unsigned_long, 0, 2);
    _dynAddrs = app->malloc(*array);
    _dynAddrs = app->createVariable("dynInfo", (Dyninst::Address)_dynAddrs->getBaseAddr(), array);
  }

  if (_int == NULL) {
    _int = appImage->findType("int");
  }

  std::string visitedStr = "visited_" + std::to_string((unsigned long)pnt->getAddress());
  ofs << "visitedStr: " << visitedStr << std::endl;
  BPatch_variableExpr *visited = app->malloc(*_int, visitedStr.c_str());

  BPatch_constExpr zero(0);
  BPatch_arithExpr caller(BPatch_ref, *_dynAddrs, zero);
  BPatch_arithExpr getCallerAddr(BPatch_assign, caller, BPatch_constExpr(pnt->getFunction()->getBaseAddr()));

  BPatch_constExpr one(1);
  BPatch_arithExpr setVistited(BPatch_assign, *visited, one);
  BPatch_arithExpr callee(BPatch_ref, *_dynAddrs, one);
  BPatch_arithExpr getCalleeAddr(BPatch_assign, callee, BPatch_dynamicTargetExpr());

  BPatch_constExpr two(2);
  BPatch_arithExpr atPoint(BPatch_ref, *_dynAddrs, two);
  BPatch_arithExpr getPointAddr(BPatch_assign, atPoint, BPatch_constExpr(pnt->getAddress()));

  std::vector<BPatch_snippet *> dynCallArgs;
  BPatch_constExpr msg("");
  dynCallArgs.push_back(&msg);

  BPatch_constExpr dynSize(1);
  dynCallArgs.push_back(&dynSize);
  BPatch_funcCallExpr monFunc(*funcs[0], dynCallArgs);

  std::vector<BPatch_snippet *> items;
  items.push_back(&setVistited);
  items.push_back(&getCallerAddr);
  items.push_back(&getCalleeAddr);
  items.push_back(&getPointAddr);
  items.push_back(&monFunc);

  BPatch_sequence getAddrs(items);

  BPatch_ifExpr ifNotVisited(BPatch_boolExpr(BPatch_eq, *visited, zero), getAddrs);
  if (!app->insertSnippet(ifNotVisited, *pnt)) {
    ofs << "here1" << std::endl;
  }

  ofs << &g << " " << pnt->getFunction()->getName() << " " << pnt->getAddress() << " " << (Dyninst::Address)pnt->getAddress() << " " << pnt << std::endl;
  g.dynamicCallPnts[(Dyninst::Address)pnt->getAddress()] = pnt;
}

void Instrumentor::monitorDynamicCallOMP(BPatch_addressSpace *app, BPatch_point *pnt, graph &g) {
  BPatch_process *proc = dynamic_cast<BPatch_process *>(app);
  ofs << "mon" << std::endl;
  BPatch_image *appImage = app->getImage();
  std::vector<BPatch_function *> funcs;
  //appImage->findFunction("DYNINSTasyncDynFuncCall", funcs);DYNINSTuserMessage
  appImage->findFunction("DYNINSTuserMessage", funcs);
  if (!funcs.size()) {
    std::cerr << "cannot find DYNINSTasyncDynFuncCall - should be in dyninst runtime library" << std::endl;
  }

  if (_dynAddrs == NULL) {
    std::stringstream arrayTypeName;
    arrayTypeName << "unsigned long"
                  << "[" << 3 << "]";
    if (_unsigned_long == NULL) {
      _unsigned_long = appImage->findType("unsigned long");
    }
    BPatch_type *array = BPatch::bpatch->createArray(arrayTypeName.str().c_str(), _unsigned_long, 0, 2);
    _dynAddrs = app->malloc(*array);
    _dynAddrs = app->createVariable("dynInfo", (Dyninst::Address)_dynAddrs->getBaseAddr(), array);
  }

  if (_int == NULL) {
    _int = appImage->findType("int");
  }

  std::string visitedStr = "visited_" + std::to_string((unsigned long)pnt->getAddress());
  BPatch_variableExpr *visited = app->malloc(*_int, visitedStr.c_str());

  BPatch_constExpr zero(0);
  BPatch_arithExpr caller(BPatch_ref, *_dynAddrs, zero);
  BPatch_arithExpr getCallerAddr(BPatch_assign, caller, BPatch_constExpr(pnt->getFunction()->getBaseAddr()));

  BPatch_constExpr one(1);
  BPatch_arithExpr setVistited(BPatch_assign, *visited, one);
  BPatch_arithExpr callee(BPatch_ref, *_dynAddrs, one);
  BPatch_arithExpr getCalleeAddr(BPatch_assign, callee, BPatch_dynamicTargetExpr());

  BPatch_constExpr two(2);
  BPatch_arithExpr atPoint(BPatch_ref, *_dynAddrs, two);
  BPatch_arithExpr getPointAddr(BPatch_assign, atPoint, BPatch_constExpr(pnt->getAddress()));

  std::vector<BPatch_snippet *> dynCallArgs;
  BPatch_constExpr msg("");
  dynCallArgs.push_back(&msg);

  BPatch_constExpr dynSize(1);
  dynCallArgs.push_back(&dynSize);
  BPatch_funcCallExpr monFunc(*funcs[0], dynCallArgs);

  std::vector<BPatch_function *> tidFuncs;
  std::vector<BPatch_snippet *> targs;

  appImage->findFunction("omp_get_thread_num", tidFuncs, false, true, false);
  if (tidFuncs.size() == 0) { //assume openmp isn't being used, so instrument as a single threaded function
    ofs << "Warning omp_get_thread_num not found" << std::endl;
  }
  BPatch_funcCallExpr tid(*tidFuncs[0], targs);

  std::vector<BPatch_snippet *> items;
  items.push_back(&getCallerAddr);
  items.push_back(&getCalleeAddr);
  items.push_back(&getPointAddr);
  items.push_back(&monFunc);
  items.push_back(&setVistited);

  BPatch_sequence getAddrs(items);
  BPatch_whileExpr whileNotZeroAndUnvisited(BPatch_boolExpr(BPatch_and, BPatch_boolExpr(BPatch_ne, tid, zero), BPatch_boolExpr(BPatch_eq, *visited, zero)), BPatch_nullExpr());

  BPatch_ifExpr ifNotVisited(BPatch_boolExpr(BPatch_eq, *visited, zero), getAddrs);
  std::vector<BPatch_snippet *> items2;
  items2.push_back(&whileNotZeroAndUnvisited);

  items2.push_back(&ifNotVisited);
  BPatch_sequence instDynCall(items2);
  if (!app->insertSnippet(instDynCall, *pnt)) {
    ofs << "here1" << std::endl;
  }

  ofs << &g << " " << pnt->getFunction()->getName() << " " << pnt->getAddress() << " " << (Dyninst::Address)pnt->getAddress() << " " << pnt << std::endl;
  g.dynamicCallPnts[(Dyninst::Address)pnt->getAddress()] = pnt;
}

void Instrumentor::startCFG(BPatch_addressSpace *app, std::string functionStr, std::string appProcPid, BPatch_function *function, std::map<std::string, bool> &functionNmsMap, graph &g, bool useOmp) {
  if (function != 0 && !functionNmsMap[functionStr]) {
    std::map<BPatch_basicBlock *, bool> seen;

    BPatch_flowGraph *fg = function->getCFG();
    std::set<BPatch_basicBlock *> blks;
    fg->getAllBasicBlocks(blks);
    ofs << "num basic blocks: " << blks.size() << std::endl;

    std::vector<BPatch_basicBlock *> entryBlk;
    std::vector<BPatch_basicBlock *> exitBlk;
    fg->getExitBasicBlock(exitBlk);
    fg->getEntryBasicBlock(entryBlk);

    //ofs<<entryBlk.size()<<" "<<entryBlk[0]->blockNo()<<" "<<function->findPoint(BPatch_entry)<<" "<<exitBlk.size()<<" "<<function->findPoint(BPatch_exit)<<std::endl;
    if (entryBlk.size() > 0) { //and exitBlk.size()>0){
      ofs << "here 0" << std::endl;
      g.entry = g.basicBlockNoMap[entryBlk[0]->blockNo()];
      if (exitBlk.size() > 0) {
        ofs << "here 1" << std::endl;
        g.exit = g.basicBlockNoMap[exitBlk[0]->blockNo()];
      }
      for (int b = 0; b < entryBlk.size(); b++) {
        ofs << "here 2 " << b << std::endl;
        traverseCFG(app, functionStr, appProcPid, entryBlk[b], seen, g, useOmp);
      }
    }
  }
}

void Instrumentor::traverseCFG(BPatch_addressSpace *app, std::string function, std::string appProcPid, BPatch_basicBlock *blk, std::map<BPatch_basicBlock *, bool> &seen, graph &g, bool useOmp) {
  seen[blk] = true;
  BPatch_point *entry = blk->findEntryPoint();
  std::vector<BPatch_point *> points;
  points.push_back(entry);
  std::string blkStr = std::to_string((long long)g.basicBlockNoMap[blk->blockNo()]);
  std::string name = function + "_b_" + blkStr;
  //ofs<<name<<std::endl;
  std::string gname = std::to_string((long long)g.basicBlockNoMap[blk->blockNo()]);

  if (g.elements.count(gname) == 0) {
    g.elements[gname] = g.basicBlockNoMap[blk->blockNo()];
    g.declarations[gname] = std::to_string((long long)g.elements[gname]) + " [label=\"" + gname + "\"]//";
  }

  std::vector<BPatch_statement> lines;
  std::vector<BPatch_statement> lines2;
  bool srcLines = app->getImage()->getSourceLines((unsigned long int)blk->getStartAddress(), lines);
  bool srcLines2 = app->getImage()->getSourceLines((unsigned long int)blk->getEndAddress(), lines2);
  std::string lineNosStr = "";
  // if (srcLines){
  // 	for(int i=0;i<lines.size();i++){
  // 		std::string tmp =lines[i].fileName();
  // 		ofs<<tmp<<std::endl;
  // 		std::string in = tmp.substr(tmp.find_last_of("/")+1);
  // 		in+=":"+std::to_string((long long)lines[i].lineNumber());
  // 		lineNosStr+=in+",";
  // 	}
  // }
  // if (srcLines2){
  // 	for(int i=0;i<lines2.size();i++){
  // 		std::string tmp =lines2[i].fileName();
  // 		std::string in = tmp.substr(tmp.find_last_of("/")+1);
  // 		in+=":"+std::to_string((long long)lines2[i].lineNumber());
  // 		lineNosStr+=in+",";
  // 	}
  // }
  g.lineNos[gname] = lineNosStr;

  std::vector<BPatch_point *> tpnts;
  blk->getAllPoints(tpnts);
  for (int p = 0; p < tpnts.size(); p++) {
    if (tpnts[p]->getCalledFunction() != NULL) {
      BPatch_function *pntFunc = tpnts[p]->getCalledFunction();
      if (_runTimeCalls) {
        monitorFunctionCall(app, tpnts[p], g);
        g.calledFunctionMap[std::to_string((long long)g.basicBlockNoMap[blk->blockNo()])] = std::make_tuple<std::string, Dyninst::Address, bool>("function", 0, false);
      } else {
        g.calledFunctionMap[std::to_string((long long)g.basicBlockNoMap[blk->blockNo()])] = std::make_tuple<std::string, Dyninst::Address, bool>(pntFunc->getMangledName(), (Dyninst::Address)pntFunc->getBaseAddr(), pntFunc->isInstrumentable());
      }
      ofs << "blk: " << g.basicBlockNoMap[blk->blockNo()] << " function found! " << pntFunc->getMangledName() << std::endl;
    }
    // else if (tpnts[p]->getCalledFunctionName() != ""){
    // 	ofs<<"blk: "<<g.basicBlockNoMap[blk->blockNo()]<<" function (name) found! "<<tpnts[p]->getCalledFunctionName()<<std::endl;
    // }
    if (tpnts[p]->isDynamic()) {
      g.calledFunctionMap[std::to_string((long long)g.basicBlockNoMap[blk->blockNo()])] = std::make_tuple<std::string, Dyninst::Address, bool>("dynamic", 0, false);
      ofs << "blk: " << g.basicBlockNoMap[blk->blockNo()] << " is dynamic" << std::endl;
      //tpnts[p]->monitorCalls();
      if (_dynamicCalls) {
        if (useOmp) {
          monitorDynamicCallOMP(app, tpnts[p], g);
        } else {
          monitorDynamicCall(app, tpnts[p], g);
        }
      }
    }
  }

  if (useOmp) {
    createAndInsertCounterSnippetOMP(app, &points, name, blkStr, appProcPid, g);
  } else {
    createAndInsertCounterSnippet(app, &points, name, blkStr, appProcPid, g);
  }

  std::vector<BPatch_basicBlock *> targets;
  blk->getTargets(targets);
  for (int t = 0; t < targets.size(); t++) {
    std::string ename = std::to_string((long long)g.basicBlockNoMap[targets[t]->blockNo()]);
    if (g.elements.count(ename) == 0) {

      g.elements[ename] = g.basicBlockNoMap[targets[t]->blockNo()];
      g.declarations[ename] = std::to_string((long long)g.elements[ename]) + " [label=\"" + ename + "\"]//";
    }
    g.links.insert(std::to_string((long long)g.elements[gname]) + " -> " + std::to_string((long long)g.elements[ename]));

    if (seen.count(targets[t]) == 0) {
      traverseCFG(app, function, appProcPid, targets[t], seen, g, useOmp);
    }
  }
}

void Instrumentor::startCFGEdges(BPatch_addressSpace *app, std::string functionStr, std::string appProcPid, BPatch_function *function, std::map<std::string, bool> &functionNmsMap, graph &g, bool useOmp) {

  if (function != 0 && !functionNmsMap[functionStr]) {
    std::map<std::string, bool> seen;
    BPatch_flowGraph *fg = function->getCFG();
    std::vector<BPatch_basicBlock *> entryBlk;
    std::vector<BPatch_basicBlock *> exitBlk;
    fg->getExitBasicBlock(exitBlk);
    fg->getEntryBasicBlock(entryBlk);
    if (entryBlk.size() > 0) { //&& exitBlk.size() > 0){
      g.entry = g.basicBlockNoMap[entryBlk[0]->blockNo()];
      if (exitBlk.size() > 0) {
        g.exit = g.basicBlockNoMap[exitBlk[0]->blockNo()];
      }
      std::vector<BPatch_edge *> edges;
      entryBlk[0]->getOutgoingEdges(edges);
      std::string gname = std::to_string((long long)g.basicBlockNoMap[entryBlk[0]->blockNo()]);

      if (g.elements.count(gname) == 0) {
        g.elements[gname] = g.basicBlockNoMap[entryBlk[0]->blockNo()];
        g.declarations[gname] = std::to_string((long long)g.elements[gname]) + " [label=\"" + gname + "\"]//";
      }
      for (int e = 0; e < edges.size(); e++) {
        traverseCFGEdges(app, functionStr, appProcPid, entryBlk[0], seen, g, edges[e], useOmp);
      }
    }
  }
}

void Instrumentor::traverseCFGEdges(BPatch_addressSpace *app, std::string function, std::string appProcPid, BPatch_basicBlock *blk, std::map<std::string, bool> &seen, graph &g, BPatch_edge *edge, bool useOmp) {
  BPatch_basicBlock *src = edge->getSource();
  BPatch_basicBlock *trg = edge->getTarget();

  std::string s = std::to_string((long long)g.basicBlockNoMap[src->blockNo()]) + " -> " + std::to_string((long long)g.basicBlockNoMap[trg->blockNo()]);
  //ofs<<s<<std::endl;
  seen[s] = true;

  BPatch_point *entry = edge->getPoint();

  std::vector<BPatch_point *> points;
  points.push_back(entry);
  std::stringstream ss;
  ss << std::dec << g.basicBlockNoMap[src->blockNo()] << "_" << g.basicBlockNoMap[trg->blockNo()];
  //std::string edgeStr = std::to_string((long long)g.basicBlockNoMap[src->blockNo()])+"_"+std::to_string((long long)g.basicBlockNoMap[trg->blockNo()]);
  std::string edgeStr = ss.str();
  std::string name = function + "_e_" + edgeStr;
  g.edges.insert(edgeStr);

  std::string gname = std::to_string((long long)g.basicBlockNoMap[trg->blockNo()]);
  if (g.elements.count(gname) == 0) {
    g.elements[gname] = g.basicBlockNoMap[trg->blockNo()];
    g.declarations[gname] = std::to_string((long long)g.elements[gname]) + " [label=\"" + gname + "\"]//";
  }

  if (useOmp) {
    createAndInsertCounterSnippetOMP(app, &points, name, edgeStr, appProcPid, g);
  } else {
    createAndInsertCounterSnippet(app, &points, name, edgeStr, appProcPid, g);
  }

  std::vector<BPatch_edge *> edges;
  trg->getOutgoingEdges(edges);
  for (int e = 0; e < edges.size(); e++) {
    std::string ename = std::to_string((long long)g.basicBlockNoMap[edges[e]->getTarget()->blockNo()]);
    if (g.elements.count(ename) == 0) {
      g.elements[ename] = g.basicBlockNoMap[edges[e]->getTarget()->blockNo()]; //++g.cnt;
      g.declarations[ename] = std::to_string((long long)g.elements[ename]) + " [label=\"" + ename + "\"]//";
    }

    std::string te = gname + " -> " + ename;
    if (seen.count(te) == 0) {
      traverseCFGEdges(app, function, appProcPid, edges[e]->getTarget(), seen, g, edges[e], useOmp);
    }
  }
  //}
}

bool Instrumentor::createAndInsertCounterSnippet(BPatch_addressSpace *app, std::vector<BPatch_point *> *points, std::string funcName, std::string blkOrEdge, std::string appProcPid, graph &g) {
  BPatch_image *appImage = app->getImage();
  std::string cntVar = appProcPid + "_" + funcName + "_counter";

  if (_long_long == NULL) {
    _long_long = appImage->findType("long long");
  }

  BPatch_variableExpr *counter = app->malloc(*_long_long, cntVar.c_str());

  //BPatch_variableExpr* counter = app->malloc(*(appImage->findType("long long")),cntVar.c_str());

  BPatch_arithExpr addOne(BPatch_assign, *counter, BPatch_arithExpr(BPatch_plus, *counter, BPatch_constExpr((long)1)));
  if (!app->insertSnippet(addOne, *points)) {

    return false;
  }
  g.varMap[appProcPid].push_back(std::make_pair(blkOrEdge, counter));
  return true;
}

int Instrumentor::gatherCounts(BPatch_addressSpace *app, std::string appProcPid) {
  std::map<std::string, bool> processed;
  for (auto it = _functionNms.begin(); it != _functionNms.end(); ++it) {
    std::string name = (*it);
    ofs << name << std::endl;
    if (_functionNmsMap.count(appProcPid + "_" + name) > 0) {
      std::vector<BPatch_function *> funcs = findFunction(app, name, _funcFiles[name], _funcLines[name], false);
      for (int i = 0; i < funcs.size(); i++) {
        BPatch_function *func = funcs[i];
        for (int n = 0; n < _mangledNameMap[func->getName()].size(); n++) {
          if (processed.count(_mangledNameMap[func->getName()][n]) == 0) {
            graph *g = _graphs[_mangledNameMap[func->getName()][n]];
            std::vector<std::pair<std::string, BPatch_variableExpr *>> data = g->varMap[appProcPid];
            g->executed = false;
            for (auto jit = data.begin(); jit != data.end(); jit++) {
              long long tVar;

              BPatch_variableExpr *var = (*jit).second;
              var->readValue(&tVar, sizeof(tVar));
              if (g->counts.count((*jit).first + "_t_" + std::to_string((long long)0)) == 0) {
                g->counts[(*jit).first + "_t_" + std::to_string((long long)0)] = 0;
              }
              g->counts[(*jit).first + "_t_" + std::to_string((long long)0)] += tVar;
              ofs << (*jit).first + "_t_" + std::to_string((long long)0) << std::endl;
              if (g->ncounts.count((*jit).first) == 0) {
                g->ncounts[(*jit).first] = std::map<int, long long>();
              }
              if (g->ncounts[(*jit).first].count(0) == 0) {
                g->ncounts[(*jit).first][0] = 0;
              }
              g->ncounts[(*jit).first][0] += tVar;
              if (tVar > 0) {
                g->executed = true;
              }
            }
            processed[_mangledNameMap[func->getName()][n]] = true;
            ofs << _mangledNameMap[func->getName()][n] << std::endl;
          }
        }
      }
    }
  }
  ofs << "done gathering counts " << processed.size() << std::endl;
  return 0;
}

/*
* When analyzing OMP code, only let thread 0 increment the counter for the basic blocks
* TODO: create implmentation where each thread has its own list of counters,
* essentially each basic block would have an array of counters, where each counter is indexed by the thread id
*/
bool Instrumentor::createAndInsertCounterSnippetOMP(BPatch_addressSpace *app, std::vector<BPatch_point *> *points, std::string funcName, std::string blkOrEdge, std::string appProcPid, graph &g) {

  BPatch_image *appImage = app->getImage();

  unsigned int nThreads = boost::thread::hardware_concurrency();
  BPatch_process *appProc = dynamic_cast<BPatch_process *>(app);
  if (!appProc->isMultithreadCapable()) {
    nThreads = 1;
  }

  std::string cntVar = appProcPid + "_" + funcName + "_counter";

  std::vector<BPatch_function *> tfunctions;
  std::vector<BPatch_snippet *> args;

  appImage->findFunction("omp_get_thread_num", tfunctions, false, true, false);
  if (tfunctions.size() == 0) { //assume openmp isn't being used, so instrument as a single threaded function
    ofs << "WARNING: \"omp_get_thread_num\" not found, instrumenting in single threaded mode" << std::endl;
    return createAndInsertCounterSnippet(app, points, funcName, blkOrEdge, appProcPid, g);
  }
  std::stringstream arrayTypeName;
  arrayTypeName << "long long"
                << "[" << nThreads << "]";
  if (_long_long == NULL) {
    _long_long = appImage->findType("long long");
  }
  BPatch_type *array = BPatch::bpatch->createArray(arrayTypeName.str().c_str(), _long_long, 0, nThreads - 1);
  BPatch_variableExpr *counter = app->malloc(*array);
  counter = app->createVariable(cntVar, (Dyninst::Address)counter->getBaseAddr(), array);
  //BPatch_variableExpr* counter = app->malloc(*(appImage->findType("long long")),cntVar.c_str());

  BPatch_funcCallExpr tid(*tfunctions[0], args);
  BPatch_arithExpr tRef(BPatch_ref, *counter, tid);
  BPatch_arithExpr addOne(BPatch_assign, tRef, BPatch_arithExpr(BPatch_plus, tRef, BPatch_constExpr((long)1)));
  //BPatch_ifExpr ifThrdZero(BPatch_boolExpr(BPatch_eq,tid,BPatch_constExpr(0)),addOne); //only allow thread 0 to increment counter

  //if (!app->insertSnippet(ifThrdZero,*points)){
  if (!app->insertSnippet(addOne, *points)) {
    return false;
  }
  g.varMap[appProcPid].push_back(std::make_pair(blkOrEdge, counter));
  return true;
}

int Instrumentor::gatherCountsOMP(BPatch_addressSpace *app, std::string appProcPid) {
  unsigned int nThreads = boost::thread::hardware_concurrency();
  BPatch_process *appProc = dynamic_cast<BPatch_process *>(app);
  if (!appProc->isMultithreadCapable()) {
    nThreads = 1;
  }
  ofs << "num threads: " << nThreads << std::endl;
  std::map<std::string, bool> processed;
  for (auto it = _functionNms.begin(); it != _functionNms.end(); ++it) {
    std::string name = (*it);
    if (_functionNmsMap.count(appProcPid + "_" + name) > 0) {
      std::vector<BPatch_function *> funcs = findFunction(app, name, _funcFiles[name], _funcLines[name], false);
      for (int i = 0; i < funcs.size(); i++) {
        BPatch_function *func = funcs[i];
        for (int n = 0; n < _mangledNameMap[func->getName()].size(); n++) {
          if (processed.count(_mangledNameMap[func->getName()][n]) == 0) {
            graph *g = _graphs[_mangledNameMap[func->getName()][n]];
            std::vector<std::pair<std::string, BPatch_variableExpr *>> data = g->varMap[appProcPid];
            g->executed = false;
            for (auto jit = data.begin(); jit != data.end(); jit++) {

              long long tVar[nThreads];
              BPatch_variableExpr *var = (*jit).second;
              var->readValue(&tVar, sizeof(long long) * nThreads);
              for (int t = 0; t < nThreads; t++) {
                if (g->counts.count((*jit).first + "_t_" + std::to_string((long long)t)) == 0) {
                  g->counts[(*jit).first + "_t_" + std::to_string((long long)t)] = 0;
                }
                g->counts[(*jit).first + "_t_" + std::to_string((long long)t)] += tVar[t];
                if (g->ncounts.count((*jit).first) == 0) {
                  g->ncounts[(*jit).first] = std::map<int, long long>();
                }
                if (g->ncounts[(*jit).first].count(t) == 0) {
                  g->ncounts[(*jit).first][t] = 0;
                }
                g->ncounts[(*jit).first][t] += tVar[t];
                if (tVar > 0) {
                  g->executed = true;
                }
              }
            }
            processed[_mangledNameMap[func->getName()][n]] = true;
          }
        }
      }
    }
  }
  return 0;
}

void Instrumentor::getLoopBasicBlocks(BPatch_addressSpace *app, std::string name, BPatch_function *function, graph &g) {

  if (function != 0) {
    char mn[100];
    function->getModule()->getName(mn, 100);
    BPatch_flowGraph *cfg = function->getCFG();
    BPatch_loopTreeNode *ltn = cfg->getLoopTree();
    std::vector<std::pair<std::string, BPatch_basicBlockLoop *>> loops;
    for (int l = 0; l < ltn->children.size(); l++) {
      traverseLoopTree(ltn->children[l], loops);
    }
    for (int l = 0; l < loops.size(); l++) {
      std::vector<BPatch_basicBlock *> blks;
      std::vector<BPatch_basicBlock *> eblks;
      loops[l].second->getLoopBasicBlocks(blks);
      loops[l].second->getLoopBasicBlocksExclusive(eblks);
      std::string loop = "";
      std::stringstream loop_ss;
      for (int i = 0; i < blks.size(); i++) {
        loop += std::to_string((long long)g.basicBlockNoMap[blks[i]->blockNo()]) + ",";
        loop_ss << std::dec << g.basicBlockNoMap[blks[i]->blockNo()] << ",";
      }
      loop = loop_ss.str().erase(loop_ss.str().size() - 1, 1);
      ofs << loops[l].first << " all: "
          << " " << loop << std::endl;
      std::string eloop = "";
      for (int i = 0; i < eblks.size(); i++) {
        eloop += std::to_string((long long)g.basicBlockNoMap[eblks[i]->blockNo()]) + " -> ";
      }
      ofs << loops[l].first << " exclusive: "
          << " " << eloop << std::endl;
      g.loops[loops[l].first] = loop;
      std::vector<BPatch_basicBlock *> entries;
      loops[l].second->getLoopEntries(entries);
      std::string loopEnt = "";
      std::stringstream loopEnt_ss;
      for (int i = 0; i < entries.size(); i++) {
        loopEnt += std::to_string((long long)g.basicBlockNoMap[entries[i]->blockNo()]) + ",";
        loopEnt_ss << std::dec << g.basicBlockNoMap[entries[i]->blockNo()] << ",";
      }
      loopEnt = loopEnt_ss.str().erase(loopEnt_ss.str().size() - 1, 1);
      ofs << loops[l].first << " entries: " << loopEnt << std::endl;
      g.loopEntries[loops[l].first] = loopEnt;
      std::string bckEdgs = "";
      std::vector<BPatch_edge *> backEdges;
      loops[l].second->getBackEdges(backEdges);
      for (int i = 0; i < backEdges.size(); i++) {
        bckEdgs += std::to_string((long long)g.basicBlockNoMap[backEdges[i]->getSource()->blockNo()]) + "->" + std::to_string((long long)g.basicBlockNoMap[backEdges[i]->getTarget()->blockNo()]) + ", ";
      }
      ofs << loops[l].first << " backedges: " << bckEdgs << std::endl;
    }
  }
}

void Instrumentor::traverseLoopTree(BPatch_loopTreeNode *ltn, std::vector<std::pair<std::string, BPatch_basicBlockLoop *>> &loops) {
  std::string name = ltn->name();
  loops.push_back(std::pair<std::string, BPatch_basicBlockLoop *>(name, ltn->loop));
  ofs << "palm-inst: " << ltn << " " << ltn->name() << " " << ltn->children.size() << std::endl;
  for (int l = 0; l < ltn->children.size(); l++) {
    traverseLoopTree(ltn->children[l], loops);
  }
}

void Instrumentor::addFunction(std::vector<std::string> function) {
  _functionNms.insert(function[0]);
  //_functionNms.push_back(function[0]);
  _funcsToProcess.push(function[0]);
  if (function.size() > 2) {
    _funcFiles[function[0]] = function[1];
    _funcLines[function[0]] = atoi(function[2].c_str());
  }
}

void Instrumentor::addFunction(std::string function) {
  _functionNms.insert(function);
  //_functionNms.push_back(function);
}

// void Instrumentor::setFunctionNames(std::vector<std::string> functionNms){
// 	_functionNms=functionNms;
// }
// std::vector<std::string>& Instrumentor::getFunctionNames(){
// 	return _functionNms;
// }

void Instrumentor::setFunctionNames(std::unordered_set<std::string> functionNms) {
  _functionNms = functionNms;
}
std::unordered_set<std::string> &Instrumentor::getFunctionNames() {
  return _functionNms;
}

void Instrumentor::setFunctionNamesMap(std::map<std::string, bool> functionNmsMap) {
  _functionNmsMap = functionNmsMap;
}
std::map<std::string, bool> &Instrumentor::getFunctionNamesMap() {
  return _functionNmsMap;
}

void Instrumentor::setFunctionFiles(std::map<std::string, std::string> funcFiles) {
  _funcFiles = funcFiles;
}
std::map<std::string, std::string> &Instrumentor::getFunctionFiles() {
  return _funcFiles;
}
void Instrumentor::setFunctionLines(std::map<std::string, int> funcLines) {
  _funcLines = funcLines;
}
std::map<std::string, int> &Instrumentor::getFunctionLines() {
  return _funcLines;
}

void Instrumentor::setGraphs(std::map<std::string, graph *> graphs) {
  _graphs = graphs;
}
std::map<std::string, graph *> &Instrumentor::getGraphs() {
  return _graphs;
}

void Instrumentor::setMangledNameMap(std::map<std::string, std::vector<std::string>> mangledNameMap) {
  _mangledNameMap = mangledNameMap;
}
std::map<std::string, std::vector<std::string>> &Instrumentor::getMangledNameMap() {
  return _mangledNameMap;
}

void Instrumentor::setFCnt(int fCnt) {
  _fCnt = fCnt;
}
int Instrumentor::getFCnt() {
  return _fCnt;
}

void Instrumentor::setDoEdges(bool doEdges) {
  _doEdges = doEdges;
}
bool Instrumentor::getDoEdges() {
  return _doEdges;
}

void Instrumentor::setUseOmp(bool useOmp) {
  _useOmp = useOmp;
}
bool Instrumentor::getUseOmp() {
  return _useOmp;
}

void Instrumentor::setCG(bool cg) {
  _cg = cg;
}
bool Instrumentor::getCG() {
  return _cg;
}

void Instrumentor::setDynamicCalls(bool dc) {
  _dynamicCalls = dc;
}

bool Instrumentor::getDynamicCalls() {
  return _dynamicCalls;
}

void Instrumentor::setRunTimeCalls(bool rtc) {
  _runTimeCalls = rtc;
}

bool Instrumentor::getRunTimeCalls() {
  return _runTimeCalls;
}

void Instrumentor::setMaxFuncLevel(int maxFuncLevel) {
  _maxFuncLevel = maxFuncLevel;
}
int Instrumentor::getMaxFuncLevel() {
  return _maxFuncLevel;
}
