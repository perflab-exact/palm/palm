#!/usr/bin/env ruby
# -*- mode: ruby -*-

class CFGProfParse #OutputAnalyzer
  def initialize(output,verbose=false)
    @output=output
    @executable=""
    @executablePath=""
    @nThreads = -1
    @funcData = {}
    @nameMap = {}
    @uninstrumentable = []
    @verbose = verbose
    analyzeOutput()
  end

  def analyzeOutput()
    getExecutableInfo() #lines 1 and 2
    getNumThreads() #line 3
    readData()
  end

  #first two lines of output
  def getExecutableInfo()
    if @executable == "" and @executablePath == ""
      matches = @output[0].match(/binary-name: (.*)/)
      if matches
        @executable = matches.captures[0]
      else
        puts "Error: invalid output file (no executable name found, or on wrong line)"
        exit
      end
      matches = @output[1].match(/binary-path: (.*)/)

      if matches
        @executablePath = matches.captures[0]
      else
        puts "Error: invalid output file (no executable path found, or on wrong line)"
        exit
      end
      if @verbose
        puts "executable: #{@executable}"
        puts "executablePath: #{@executablePath}"
      end
    end
    return @executable,@executablePath
  end

  #-----3rd line of output
  def getNumThreads()
    if @nThreads == -1
      # puts "OZGUR: #{@output[2]}"
      matches = @output[2].match(/nThreads: (.*)/)
      if matches
        @nThreads = matches.captures[0]
      else
        puts "Error: Invalid output file (no num threads present, or on the wrong line)"
        exit
      end
    end
    return @nThreads
  end


  def readData()
    getExecutableInfo()
    line = 2
    demangCnt = {}
    while line < @output.length
      matches = @output[line].match(/func: (.*) (.*)/) #first - demangled name, second mangled name
      if matches
        mangled=matches.captures[1].split(":")[0]
        demangled=makeNameNice(matches.captures[0])

        if demangCnt[demangled] == nil
          demangCnt[demangled] = 0
        end
        if @nameMap[mangled] == nil
          @nameMap[mangled]=demangled+"_#{demangCnt[demangled]}"
          demangCnt[demangled] += 1
          @funcData[mangled]={"dot"=>[],"lineno"=>{},"calledFuncs"=>{},"lib-path"=>"","loops"=>[],"loopEntries"=>{}}
          for tid in 0..(@nThreads.to_i-1)
            @funcData[mangled][tid]={"blkCnts"=>{},"edgeCnts"=>{}}
          end
          line = getLibInfo(line,mangled)
          line = getBlockData(line,mangled)
          line = getEdgeData(line,mangled)
          line = getLoopData(line,mangled)
        else
          line = updateBlockData(line,mangled)
          line = updateEdgeData(line,mangled)
        end
      end

      line += 1
    end
  end


  def fileError(str)
    puts "file error #{str}"
  end


  def getLibInfo(line,mangled)
    #------lib-path-------------------------
    line += 1
    matches = @output[line].match(/lib-path: (.*)/)
    if matches
      path = matches.captures[0]
      @funcData[mangled]["lib-path"]=path
    else
      fileError("lib-path")
    end
    return line
  end

  def getBlockData(line,mangled)
    #------block data--------------
    line+=1
    matches = @output[line].match(/blocks: (.*)/)
    if matches
      line+=1
      endLine = line+matches.captures[0].to_i
      while line < endLine
        theLine = @output[line]
        theLine.encode!('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
        blkNum,counts,lineInfo,calledFunc = theLine.split(";")
        counts = counts[1..-2].split(",")
        for tid in 0..(@nThreads.to_i-1)
          @funcData[mangled][tid]["blkCnts"][blkNum]=counts[tid].to_i#+1
        end
        @funcData[mangled]["lineno"][blkNum] = lineInfo
        calledFunc = calledFunc.split(":")
        if calledFunc != []
          if calledFunc[2] == "0"
            @uninstrumentable << calledFunc[0]
          end
          @funcData[mangled]["calledFuncs"][blkNum] = calledFunc[0]
        end
        @funcData[mangled]["dot"] << "#{blkNum} [label=\"#{blkNum}\"]//"
        line += 1
      end
    else
      fileError("block-data")
    end
    return line
  end

  def updateBlockData(line,mangled)
    line += 2
    matches = @output[line].match(/blocks: (.*)/)
    if matches
      line+=1
      endLine = line+matches.captures[0].to_i
      while line < endLine
        blkNum,counts,lineInfo,calledFunc = @output[line].split(";")
        counts = counts[1..-2].split(",")
        for tid in 0..(@nThreads.to_i-1)
          if @funcData[mangled][tid]["blkCnts"][blkNum] == nil
            @funcData[mangled][tid]["blkCnts"][blkNum] = 0
          end
          @funcData[mangled][tid]["blkCnts"][blkNum]+=counts[tid].to_i
        end
        line += 1
      end
    else
      fileError("block-data")
    end
    return line
  end

  def getEdgeData(line,mangled)
    #------Edge data--------------
    matches = @output[line].match(/edges: (.*)/)
    if matches
      line+=1
      endLine = line+matches.captures[0].to_i
      while line < endLine
        edge,counts= @output[line].split(";")
        edge = edge.gsub(/_/," -> ")
        counts = counts[1..-2].split(",")
        for tid in 0..(@nThreads.to_i-1)
          @funcData[mangled][tid]["edgeCnts"][edge]=counts[tid].to_i#+1
        end
        @funcData[mangled]["dot"] << edge
        line += 1
      end
    else
      fileError("edge-data")
    end
    return line
  end

  def updateEdgeData(line,mangled)
    #------Edge data--------------
    matches = @output[line].match(/edges: (.*)/)
    if matches
      line+=1
      endLine = line+matches.captures[0].to_i
      while line < endLine
        edge,counts= @output[line].split(";")
        edge = edge.gsub(/_/," -> ")
        counts = counts[1..-2].split(",")
        for tid in 0..(@nThreads.to_i-1)
          if @funcData[mangled][tid]["edgeCnts"][edge] == nil
            @funcData[mangled][tid]["edgeCnts"][edge] = 0
          end
          @funcData[mangled][tid]["edgeCnts"][edge]+=counts[tid].to_i
        end
        line += 1
      end
    else
      fileError("edge-data")
    end
    return line
  end

  def getLoopData(line,mangled)
    #------Loop data--------------
    matches = @output[line].match(/loops: (.*)/)
    if matches
      line+=1
      endLine = line+matches.captures[0].to_i
      loops = []
      loopEntries = {}
      while line < endLine
        loopName,blks,entries= @output[line].split(";")
        loopName = loopName.split("_")[1]
        blks = blks.gsub(/\s+/, "").split("->")
        entries = entries.split(",")
        @funcData[mangled]["loops"]<<[loopName,blks]
        @funcData[mangled]["loopEntries"][loopName]=entries
        line += 1
      end
    else
      fileError("loop-data")
    end
    return line
  end

  def makeNameNice(name)
    niceName  = name.gsub(/:+/,"_").gsub(/\!+/,"_not_").gsub(/\.+/,"_").gsub(/<+/,"_").gsub(/>+/,"_").gsub(/,+/,"_").gsub(/ +/,"").gsub(/\=+/,"_eq_").gsub(/\[+/,"").gsub(/\]+/,"").gsub(/\~+/,"_de_").gsub(/\-+/,"_minus_").gsub(/\++/,"_plus_").gsub(/\&+/,"_").gsub(/\*+/,"_mul_").gsub(/\\+/,"_div_")

    niceName = niceName.gsub(/_+/,"_").sub(/^_/,"")
    if niceName.size > 244 #filename has to be less than 256 characters long (244 because we add an 11 character extension later on)
      niceName = niceName[-230..-1]
    end
    niceName = niceName.gsub(/_+/,"_").sub(/^_/,"")
    return niceName.downcase
  end

	#-----------
  # mangled function name is key into map
  #
  def getNameMap()
    if @nameMap == nil
      @nameMap = {}
      demangCnt = {} #keep track of functions that have same demangled function name
      inNameMap = 0 #0 =name map section not seen yet, 1 = in name map section, 2 = name map section seen and no longer in it
      for o in @output
        if o.include? "mangled-name-map:"
          inNameMap=1
          matches = o.match(/mangled-name-map: (.*) (.*)/) #first - demangled name, second mangled name
          if matches
            mangled=matches.captures[1]
            demangled=makeNameNice(matches.captures[0])

            if demangCnt[demangled] == nil
              demangCnt[demangled] = 0
            end

            @nameMap[mangled]=demangled+"_#{demangCnt[demangled]}"
            demangCnt[demangled] += 1
          end
        else
          if inNameMap == 1
            inNameMap = 2
          end
        end
        if inNameMap == 2
          break
        end
      end
    end
    return @nameMap
  end

  def createNameMap()
    getNameMap()
  end


  def callTree(func,layer,data)
    if @funcData[func]==nil
      data[func]=layer
    elsif @funcData[func]["calledFuncs"].size == 0
      data[func]=layer
    else
      data[func]=layer
      for calledFunc in @funcData[func]["calledFuncs"]
        if data[calledFunc[1]] == nil #and @nameMap[calledFunc[1]] != nil
          data = callTree(calledFunc[1],layer+1,data)
        end
      end
      #data[func]=layer
    end
    return data
  end

  def getOrderedFuncs()
    cnts = {}
    for func in @funcData.keys
      #puts func
      if @nameMap[func] != nil
        if cnts[func] == nil
          cnts[func]=0
        end
        for calledFunc in @funcData[func]["calledFuncs"]
          #puts "\t #{calledFunc}"
          if @nameMap[calledFunc[1]] != nil
            if cnts[calledFunc[1]] == nil
              cnts[calledFunc[1]]=0
            end
            cnts[calledFunc[1]]+=1
          end
        end
      end
    end
    data = {}
    for func in cnts.keys
      if cnts[func] == 0
        data = callTree(func,0,data)
      end
    end
    return data.sort { |a, b| b[1] <=> a[1] }.to_h
  end

  def analyzeBlkOutput(func,tid=0)
    blkCnts = @funcData[func][tid.to_i]["blkCnts"].to_a
    dot = @funcData[func]["dot"]
    lineNos = @funcData[func]["lineno"]
    calledFuncs = @funcData[func]["calledFuncs"]

    sum = 0
    dels = []
    blks = []
    for blk,val in blkCnts
      if val == 0 # if block was never called, remove from cfg
        dels << blk
      else
        blks << blk
      end
      sum = sum+val
    end
    blkCnts.sort! {|left, right| right[0].to_i <=> left[0].to_i}
    return blkCnts,dot,sum,dels,blks,lineNos,calledFuncs
  end

  def analyzeBlkOutputAvg(func)
    blkCnts = @funcData[func][0]["blkCnts"]
    for tid in 1..(@nThreads.to_i-1)
      tmpBlkCnts = @funcData[func][tid.to_i]["blkCnts"]
      for blk in tmpBlkCnts.keys
        if blkCnts.keys.include? blk
          blkCnts[blk]+=tmpBlkCnts[blk]
        else
          blkCnts[blk]=tmpBlkCnts[blk]
        end
      end
    end
    blkCnts = blkCnts.to_a

    dot = @funcData[func]["dot"]
    lineNos = @funcData[func]["lineno"]
    calledFuncs = @funcData[func]["calledFuncs"]

    sum = 0
    dels = []
    blks = []
    for blk,val in blkCnts
      if val == 0 # if block was never called, remove from cfg
        dels << blk
      else
        blks << blk
      end
      sum = sum+val
    end
    blkCnts.sort! {|left, right| right[0].to_i <=> left[0].to_i}
    return blkCnts,dot,sum,dels,blks,lineNos,calledFuncs
  end

  def analyzeLinkOutput(func,tid=0)
    edgeCnts = @funcData[func][tid.to_i]["edgeCnts"].to_a
    edgeCnts.sort!  { |x,y| y[0].split(" -> ")[0].to_i <=> x[0].split(" -> ")[0].to_i }
    return edgeCnts
  end

  def analyzeLinkOutputAvg(func)
    edgeCnts = @funcData[func][0]["edgeCnts"]
    for tid in 1..(@nThreads.to_i-1)
      tmpEdgeCnts = @funcData[func][tid.to_i]["edgeCnts"]
      for edge in tmpEdgeCnts.keys
        if edgeCnts.keys.include? edge
          edgeCnts[edge]+=tmpEdgeCnts[edge]
        else
          edgeCnts[Edge]=tmpEdgeCnts[edge]
        end
      end
    end
    edgeCnts = edgeCnts.to_a
    edgeCnts.sort!  { |x,y| y[0].split(" -> ")[0].to_i <=> x[0].split(" -> ")[0].to_i }
    return edgeCnts
  end

  def analyzePathOutput(func)
    path = @funcData[func]["lib-path"]
    #puts "#{path} #{@executablePath}"
    if path == ""
      return @executablePath
    else
      return path
    end
  end

  def analyzeLoopOutput(func)
    loops  = @funcData[func]["loops"]
    loopEntries = @funcData[func]["loopEntries"]
    return loops,loopEntries
  end


  def getCalledFuncs(func)
    return @funcData[func]["calledFuncs"]
  end

  def getLatencies(fname,funcs)
    latencies = {}
    if File.exist?(fname)
      file = File.open(fname,"r")
      for l in file
        data = l.split(":") #function,costs,avgcost
        func = data[0]
        costs = data[1]
        latencies[func]=costs
      end
      file.close
    end
    for func in funcs
      if !latencies.has_key? func[0]
        latencies[func[0]] = [[5, 1.0], [10, 0.0], [30.0, 0.0], [100, 0.0], [300, 0.0]]
      end
    end
    for func in funcs
      puts "#{func[0]} #{latencies[func[0]]}"
    end
    return latencies
  end

  def getUninstrumentable()
    return @uninstrumentable
  end
end
