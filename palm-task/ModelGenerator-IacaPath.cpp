
#include <iostream>
#include <sstream>
#include <fstream>

#include <BPatch.h>
#include <BPatch_object.h>
#include <BPatch_addressSpace.h>
#include <BPatch_process.h>
#include <BPatch_binaryEdit.h>
#include <BPatch_point.h>
#include <BPatch_function.h>
#include <BPatch_flowGraph.h>
#include <BPatch_statement.h>
//#include <BPatch_callbacks.h>
//#include <BPatch_flowGraph.h>

#include <InstructionDecoder.h>
#include <InstructionSource.h>
//#include <ParseCallback.h>
#include <CodeObject.h>
#include <CFG.h>

#include <Symtab.h>
#include <Function.h>
//#include <PatchCFG.h>

#include "ModelGenerator-IacaPath.hpp"
#include "DyninstUtil.hpp"

extern std::ostream ofs;

IacaInserter::IacaInserter(){
	_spath = "";
	_unrolling = 1;
}
IacaInserter::~IacaInserter(){

}

void IacaInserter::parsePath(){
	if (_spath != ""){
		std::stringstream ss(_spath);
		std::vector<int> blks;
		std::string delim = "->";
		size_t start = 0;
		size_t end = 0;
		while((end = _spath.find(delim,start))!=std::string::npos){
			blks.push_back(std::stoi(_spath.substr(start,end)));
			start=end+delim.length();
		}
		blks.push_back(std::stoi(_spath.substr(start,end)));
		_path = blks;
	}
}

void IacaInserter::parsePath(std::string spath){
	_spath = spath;
	parsePath();	
}

void IacaInserter::performInsert(Dyninst::SymtabAPI::Symtab* symtab, Dyninst::ParseAPI::SymtabCodeSource* codeSrc,BPatch_addressSpace* app){
	Dyninst::Offset free = symtab->getFreeOffset(10000);
	for(int i =0; i< _functionNms.size();i++){
		std::map<std::string,std::vector<uint8_t> > testBufs;
		std::map<std::string,std::vector<Dyninst::ParseAPI::Block*> >paths;
		std::cout<<"palm-inst: finding "<<_functionNms[i]<<std::endl;
		std::string funcName = _functionNms[i];
		std::vector<BPatch_function*> funcs = findFunction(app,funcName,_funcFiles[funcName],_funcLines[funcName],false);
		if (funcs.size() == 0){
			funcs = searchAllFunctions(app->getImage(),funcName);
		}
		if (funcs.size() > 0){
			BPatch_function* func = funcs[0]; //assume a mangled name was passed to 'findFunction' so only a single function should have been returned
			if (func!=0){
				std::vector<uint8_t> bufVec;
				std::cout<<"palm-inst: "<<func->getName()<<" "<<std::endl;
				createBasicBlockMap(func,_g);
				createBasicBLockPath(func, paths, _g, _path, _spath);
				std::cout<<"palm-inst: "<<"************************************************************"<<std::endl;
				insertIACAAroundPath(testBufs,codeSrc,paths);
				std::cout<<std::hex<<free<<std::endl;
			}
		}

		std::map<std::string,std::vector<uint8_t> >::iterator it;

		for(it=testBufs.begin();it!=testBufs.end();++it){
			std::vector<uint8_t> bufVec = (*it).second;

			uint8_t* buf = (uint8_t*)malloc((unsigned int)bufVec.size());

			std::cout<<"bufVec size "<<bufVec.size()<<std::endl;
			for(int b=0;b<(unsigned int)bufVec.size();b++){
				buf[b]=bufVec[b];
				std::cout<<(unsigned int)buf[b]<<" ";
			}
			std::cout<<std::endl;
			std::string name=".palm_"+(*it).first;
			std::cout<<"palm-inst: inserting: "<<name<<std::endl;
			std::size_t found = (*it).first.find("loop: ");
			if(found!=std::string::npos){
				std::cout<<(*it).first<<std::endl;
			}
			bool temp = symtab->addRegion(free,buf,(unsigned int)bufVec.size(),name,Dyninst::SymtabAPI::Region::RT_TEXT,true,sizeof(uint8_t),false);
			if(!temp){
				std::cout<<"adding region failed"<<std::endl;
			}

			free+=bufVec.size()+8;
		}
	}
}

void IacaInserter::createBasicBLockPath(BPatch_function* function, std::map<std::string,std::vector<Dyninst::ParseAPI::Block*> >& paths, graph& g, std::vector<int> path, std::string spath){
	std::cout<<"in use path "<<path.size()<<std::endl;
	BPatch_flowGraph *fg =function->getCFG();
	std::set<BPatch_basicBlock *>allBlks;
	fg->getAllBasicBlocks(allBlks);
	std::vector<Dyninst::ParseAPI::Block*> blkPath;
	for (int i = 0; i < path.size(); i++){
		//BPatch_basicBlock * blk = NULL;
		BPatch_basicBlock* blk = g.blks[path[i]];
		Dyninst::ParseAPI::Block* pblk = Dyninst::ParseAPI::convert(blk);
		blkPath.push_back(pblk);
		//spath+=std::to_string((long long)g.basicBlockNoMap[blk->blockNo()])+" -> ";
	}
	std::string tpath="cp: "+spath;
	std::cout<<tpath<<":"<<blkPath.size()<<std::endl;
	paths[tpath]=blkPath;
}


void IacaInserter::insertIACAAroundPath(std::map<std::string,std::vector<uint8_t> >& bufs,Dyninst::ParseAPI::SymtabCodeSource* codeSrc, std::map<std::string,std::vector<Dyninst::ParseAPI::Block*> >& paths){
	std::map<std::string,std::vector<Dyninst::ParseAPI::Block*> >::iterator pit=paths.begin();
	while(pit!=paths.end()){
		std::cout<<"palm-inst: "<<(*pit).first<<" "<<(*pit).second.size()<<" !!!!!!!!!!!!!!!!!"<<std::endl;
		std::vector<uint8_t> buf;
		for(int l = 0; l<_unrolling;l++){
			insertIACAStart(buf);
			for (int k =0; k <= l; k++){
				for(int i = 0;i<(*pit).second.size();i++){
					Dyninst::ParseAPI::Block* blk= (*pit).second[i];
					uint8_t* addr =(uint8_t*)codeSrc->getPtrToInstruction(blk->start());
					std::cout<<"palm-inst: "<<(unsigned int*)codeSrc->getPtrToInstruction(blk->start())<<" ------ "<<(unsigned int*)codeSrc->getPtrToInstruction(blk->end())<<std::endl;
					int j =0;
					while(addr+j!=(uint8_t*)codeSrc->getPtrToInstruction(blk->end())){
						buf.push_back(addr[j]);
						j++;
					}
				}
			}
			insertIACAEnd(buf);
			std::cout<<std::endl;
		}
		bufs[(*pit).first]=buf;
		++pit;
	}
}

void IacaInserter::insertIACAStart(std::vector<uint8_t>& buf){
	uint8_t beg[8] = { 0xbb,0x6f,0x00,0x00,0x00,0x64,0x67,0x90};
	for(int i =0;i<8;i++){
		buf.push_back(beg[i]);
	}
}

void IacaInserter::insertIACAEnd(std::vector<uint8_t>& buf){
	uint8_t end[8] = { 0xbb,0xde,0x00,0x00,0x00,0x64,0x67,0x90};
	for(int i =0;i<8;i++){
		buf.push_back(end[i]);
	}
}

void IacaInserter::addFunction(std::vector<std::string> function){
	_functionNms.push_back(function[0]);
	if (function.size()>2){
		_funcFiles[function[0]]=function[1];
		_funcLines[function[0]]=atoi(function[2].c_str());
	}
}

void IacaInserter::setFunctionNames(std::vector<std::string> functionNms){
	_functionNms=functionNms;
}
std::vector<std::string>& IacaInserter::getFunctionNames(){
	return _functionNms;
}

void IacaInserter::setFunctionFiles(std::map<std::string,std::string> funcFiles){
	_funcFiles=funcFiles;
}
std::map<std::string,std::string>& IacaInserter::getFunctionFiles(){
	return _funcFiles;
}

void IacaInserter::setFunctionLines(std::map<std::string,int> funcLines){
	_funcLines=funcLines;
}
std::map<std::string,int>& IacaInserter::getFunctionLines(){
	return _funcLines;
}

void IacaInserter::setPath(std::string spath){
	_spath = spath;
}
std::string IacaInserter::getPath(){
	return _spath;
}

void IacaInserter::setUnrolling(int unrolling){
	_unrolling = unrolling;
}
int IacaInserter::getUnrolling(){
	return _unrolling;
}

void IacaInserter::setGraph(graph g){
	_g = g;
}
graph IacaInserter::getGraph(){
	return _g;
}

