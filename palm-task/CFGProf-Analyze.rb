#!/usr/bin/env ruby
# -*- mode: ruby -*-

#*BeginPNNLCopyright*********************************************************
#
# $HeadURL$
# $Id$
#
#***********************************************************EndPNNLCopyright*

#****************************************************************************
# $HeadURL$
#
# Ryan Friese
#****************************************************************************

require 'yaml'
require 'set'

$verbose = false
class Graph
  class Node
    def initialize(name,value,graph,verbose=false) #used
      @name = name
      #@baseValue = value # number of times this node was executed
      @value = value
      @sources = [] # nodes that are parents of this node
      @targets = [] # nodes that are targets of this node
      @edges = [] # all edges into or out of the node
      @baseGraph = graph #the full control flow graph
      @subGraphs = [] #all graphs (loops) this node exists in

      @entryEdges = []
      @entryCnts = 0

      @exitEdges = []
      @exitCnts = 0
    end

    def copyNew(num=1) #used
      nnode = Node.new("#{@name}.#{num}",0,@baseGraph)
      @baseGraph.addNode(nnode)
      for sg in @subGraphs
        nnode.addSubGraph(sg)
        sg.addNode(nnode)
      end
      return nnode
    end

    def removeAllSubGraphs() # used
      @subGraphs = []
    end

    def addSourceAndEdge(node,weight) #used
      if (getEdge(node,self) == nil)
        edge = Edge.new(node,self,weight)
        addEdge(edge)
        node.addEdge(edge)
        @baseGraph.addEdge(edge)
        setValue(getValue+weight)
      end
    end

    def addSource(node) # used
      if !@sources.include? node
        @sources << node
      end
    end

    def addInternalSources(node,loop) # used
      internalSources = []
      for s in node.getSources
        if loop.getNodes[s.getName] != nil and !(node.getInnerSubGraph == s.getInnerSubGraph and node.getInnerSubGraph!=loop)
          internalSources << s
        end
      end
      for s in internalSources
        edge = Edge.new(s,self,node.getEdge(s,node).getWeight)
        addEdge(edge,true)
        s.addEdge(edge)
        @baseGraph.addEdge(edge)
      end
    end



    def transferBackedges(node,loop)
      for t in node.getTargets
        if loop.getNodes[t.getName] != nil and loop.getEntrys.include? t
            edge = Edge.new(self,t,node.getEdge(node,t).getWeight)
            addEdge(edge,true)
            t.addEdge(edge)
            @baseGraph.addEdge(edge)

            @baseGraph.removeEdge(getEdge(node,t))
            for g in @subGraphs
              g.removeEdge(getEdge(node,t))
            end
            t.removeSource(node)
            node.removeTarget(t)
        end
      end
    end

    def addBackedges(node,loop)
      for t in node.getTargets
        if loop.getNodes[t.getName] != nil and loop.getEntrys.include? t
            edge = Edge.new(self,t,node.getEdge(node,t).getWeight)
            addEdge(edge,true)
            t.addEdge(edge)
            @baseGraph.addEdge(edge)
        end
      end
    end

    def addExternalSources(node,loop) # used
      externalSources = []
      for s in node.getSources
        if loop.getNodes[s.getName] == nil
          externalSources << s
        end
      end
      for s in externalSources
        edge = Edge.new(s,self,node.getEdge(s,node).getWeight)
        addEdge(edge,true)
        s.addEdge(edge)
        @baseGraph.addEdge(edge)
      end
    end

    def removeInternalSources(child)# used
      internalSources = []
      for s in getSources
        if child.getNodes[s.getName] != nil and !(getInnerSubGraph == s.getInnerSubGraph and getInnerSubGraph!=child)
          internalSources << s
        end
      end
      for s in internalSources
        @baseGraph.removeEdge(getEdge(s,self))
        for g in @subGraphs
          g.removeEdge(getEdge(s,self))
        end
        removeSource(s)
        s.removeTarget(self)
      end
    end

    def removeExternalSources(child) # used
      externalSources = []
      for s in getSources
        if child.getNodes[s.getName] == nil
          externalSources << s
        end
      end
      for s in externalSources
        @baseGraph.removeEdge(getEdge(s,self))
        for g in @subGraphs
          g.removeEdge(getEdge(s,self))
        end
        removeSource(s)
        s.removeTarget(self)
      end
    end

    def removeSource(node) # used
      if node != nil
        delEdge = nil
        for e in @entryEdges
          if e.getSource == node and e.getTarget == self
            delEdge = e
            @entryCnts -=e.getWeight
            @value -= e.getWeight
            break
          end
        end
        if @entryEdges.include? delEdge
          @entryEdges.delete(delEdge)
        end
        if @edges.include? delEdge
          @edges.delete(delEdge)
        end
        @baseGraph.removeEdge(delEdge)
        for g in @subGraphs
          g.removeEdge(delEdge)
        end
        if @sources.include? node
          @sources.delete(node)
          node.removeTarget(self)
        end
      end
    end

    def removeSources(sources)
      for s in sources
        removeSource(s)
      end
    end

    def addTarget(node) # used
      if !@targets.include? node
        @targets << node
      end
    end


    def addExternalTargets(node,loop) # used
      externalTargets = []
      for t in node.getTargets
        if loop.getNodes[t.getName] == nil
          externalTargets << t
        end
      end
      for t in externalTargets
        edge = Edge.new(self,t,node.getEdge(node,t).getWeight)
        add = addEdge(edge)
        add = add && t.addEdge(edge)
        if add
          t.setValue(t.getValue+edge.getWeight)
        end
        @baseGraph.addEdge(edge)
      end
    end

    def removeTarget(node) # used
      if node != nil
        delEdge = nil
        for e in @exitEdges
          if e.getTarget == node and e.getSource == self
            delEdge = e
            @exitCnts -=e.getWeight
            break
          end
        end
        if @exitEdges.include? delEdge
          @exitEdges.delete(delEdge)
        end
        if @edges.include? delEdge
          @edges.delete(delEdge)
        end
        @baseGraph.removeEdge(delEdge)
        for g in @subGraphs
          g.removeEdge(delEdge)
        end
        if @targets.include? node
          @targets.delete(node)
          node.removeSource(self)
        end
      end
    end

    def removeTargets(targets)
      for t in targets
        removeTarget(t)
      end
    end

    def normalize() # used
      origVal = getValue
      exitCnt = 0
      for ee in getExitEdges
        exitCnt += ee.getWeight
      end
      for ee in getExitEdges
        ee.getTarget.setValue(ee.getTarget.getValue-ee.getWeight)
        ee.setWeight(origVal * ee.getWeight/exitCnt.to_f)
        ee.getTarget.setValue(ee.getTarget.getValue+ee.getWeight)
      end
    end

    def addEdge(edge,addWeight=false) # used
      add=true
      for e in @edges
        if e.to_s == edge.to_s #edge may represent an already present edge (@edges.include? will not work in this case because the actual edge object will be different)
          add=false
          break
        end
      end
      if add
        @edges << edge
        if self == edge.getSource
          addTarget(edge.getTarget)
          add = true
          for e in @exitEdges
            if e.to_s == edge.to_s
              add = false
              break
            end
          end
          if add
            @exitEdges << edge
            @exitCnts += edge.getWeight
          end
        end
        if self == edge.getTarget # maybe change back to elsif
          addSource(edge.getSource)
          add = true
          for e in @entryEdges
            if e.to_s == edge.to_s
              add = false
              break
            end
          end
          if add
            @entryEdges << edge
            @entryCnts += edge.getWeight
            if addWeight
              @value += edge.getWeight
            end
          end
        end
      end
      return add
    end

    def getSourceEdge(node) # used
      return getEdge(node,self)
    end

    def getTargetEdge(node) # used
      return getEdge(self,node)
    end

    def getEdge(src,trg) # used
      edge = nil
      for e in @edges
        if e.getSource == src and e.getTarget == trg
          edge = e
          break
        end
      end
      return edge
    end

    def getEntryEdges() # used
      return @entryEdges
    end

    def getEntryCnts()
      return @entryCnts
    end

    def getExitEdges() # used
      return @exitEdges
    end

    def getAllExitCnts() # used
      return @exitCnts
    end

    # only get the exit counts from this node to other nodes in `graph`
    def getExitCnts(graph) # used
      count = 0
      if graph != nil and graph.getNodes[@name] != nil
        for edge in @exitEdges
          if graph.getNodes[edge.getTarget.getName] != nil
            count += edge.getWeight
          end
        end
      end
      return count
    end

    def getEntryEdgeCount(source) # used
      entryEdgeCnt=0
      for e in @entryEdges
        if source.is_a? Graph
          if source.getNodes[e.getSource.getName]
            entryEdgeCnt+=e.getWeight
          end
        else
          if e.getSource == source
            entryEdgeCnt+=e.getWeight
          end
        end
      end
      return entryEdgeCnt
    end

    def getSources # used
      return @sources
    end

    def getTargets # used
      return @targets
    end

    def getEdges # used
      return @edges
    end

    def getName # used
      return @name
    end

    def getDotName # used
      return "b_#{@name.gsub(/\.+/,"_")}"
    end

    def setValue(value) # used
      @value = value
    end

    def getValue # used
      return @value
    end

    def getSubGraphs # used
      return @subGraphs
    end

    def getInnerSubGraph # used
      if @subGraphs.length ==1
        return @subGraphs[0]
      elsif @subGraphs.length > 1
        inner = 0
        for sub in @subGraphs
          if sub.getParentGraph == @baseGraph
            inner = sub
            subStack = []
            subStack.push(sub)
            while subStack.length > 0
              psub = subStack.pop
              for csub in psub.getSubGraphs
                if csub.getNodes[self.getName] != nil
                  subStack.push(csub)
                  inner=csub
                end
              end
            end
            break
          end
        end
        return inner
      end
      return @baseGraph
    end

    def addSubGraph(sub) # used
      @subGraphs << sub
    end

    def removeSubGraph(sub) # used
      @subGraphs.delete(sub)
    end

    def print # used
      s =  "("+@name.to_s+", "+@value.to_s+") ["
      for src in @sources
        s+=src.getName+" "
      end
      s+="] ["
      for trg in @targets
        s+=trg.getName+" "
      end
      s+="]"
      s+="#{@edges}"
      s+=" loops: "+@subGraphs.length.to_s
    end

    def <=> (other) # used
      self.getName.to_f <=> other.getName.to_f
    end

    def to_s # used
      return @name.to_s
    end

    def inspect # used
      return @name.to_s
    end

  end #end Node Class

  class Edge
    def initialize(src,trg,weight) # used
      @source = src
      @target = trg
      @weight = weight # number of times this edge was taken
    end

    def getSource # used
      return @source
    end

    def getTarget # used
      return @target
    end

    def getWeight # used
      return @weight
    end

    def setWeight(weight)
      @weight = weight
    end

    def print # used
      puts @source.getName+" -> "+@target.getName
    end

    def getDot # used
      return "#{@source.getDotName} -> #{@target.getDotName}"
    end

    def to_s # used
      return @source.getName+" -> "+@target.getName
    end

  end #end egde class


  #----------------------------------------------------------------Graph Class ------------------------------------------------------------------------------
  # this class is used to represent both the full control flow graph, and any subgraphs (loops)
  def self.unserialize(fname) # used
    g = YAML.load_file(fname)
    g.initEvalPaths
    return g
  end

  def initialize(func,nice_func,name,binary="",verbose) # used
    @nodes = {}
    @nodeCopyCounts = {}
    @loopCopyCounts = {}
    @edges = {}

    @entrys = []
    @entryEdges = []
    @entryCnts = -1
    @exits = []
    @exitEdges = []
    @exitCnts = -1

    @backEdges = []
    @backEdgeCnts = -1

    @instances = -1
    @loops = {}

    @baseGraph = nil
    @parentGraph = nil
    @subGraphs = []


    @binary = binary
    @func = func
    @nice_func = nice_func
    @blockPaths = {} #basic block array => variable name
    @evalPaths = Hash.new { |hash, key| hash[key] = Set.new }  # path => variable name (the same path may be associated with multiple variable names)
    @varProbs = {} # variable name => probability
    @varDefs = {} # variable name => path
    @loopItsVars = {}
    @evalStr = ""
    @maxVal = 0
    @name = name

    @calced = false
    @threadId = 0
    $verbose = verbose
  end

  def marshal_dump # used
    evalPathsDump=[]
    for path in @evalPaths
      evalPathsDump << path
    end
    data=[@nodes,@edges,@entrys,@entryEdges,@entryCnts,@exits,@exitEdges,@exitCnts,
      @backEdges,@backEdgeCnts,@instances,@loops,@parentGraph,@subGraphs,
      @binary,@func,@nice_func,@blockPaths,@varProbs,@varDefs,@loopItsVars,@evalStr,
      @maxVal,@name,@calced,@threadId,evalPathsDump,@nodeCopyCounts,@loopCopyCounts,@baseGraph]
    return data
  end

  def marshal_load(data) # used
    @nodes,@edges,@entrys,@entryEdges,@entryCnts,@exits,@exitEdges,@exitCnts,
      @backEdges,@backEdgeCnts,@instances,@loops,@parentGraph,@subGraphs,
      @binary,@func,@nice_func,@blockPaths,@varProbs,@varDefs,@loopItsVars,
      @evalStr,@maxVal,@name,@calced,@threadId,evalPathsDump,@nodeCopyCounts,@loopCopyCounts,@baseGraph = data
    @evalPaths = Hash.new { |hash, key| hash[key] = Set.new }

    for elem in evalPathsDump
      path = elem
      @evalPaths[path[0]]=path[1]
    end
  end

  def removeAllSubGraphs() # used
    @subGraphs = []
  end

  def setCalced(calced) # used
    @calced = calced
  end

  def setName(name) # used
    @name = name
  end


  def initEvalPaths # used
    @evalPaths = Hash.new { |hash, key| hash[key] = Set.new }
  end

  def to_s # used
    return getVarName
  end

  def inspect  # used
    return getVarName
  end

  def getDotData() # used
    calledBlkLabels = []
    blkCnts = []
    for node in @nodes.keys.sort { |x, y| y.gsub(/cp+/,"").to_i <=> x.gsub(/cp+/,"").to_i }
      calledBlkLabels << "#{@nodes[node].getDotName} [label=\"#{@nodes[node].getDotName}\"]//"
      blkCnts << ["#{@nodes[node].getDotName}",@nodes[node].getValue]
    end
    calledLinks = []
    edgeCnts = []
    for e in @edges.keys
      calledLinks << "#{@edges[e].getDot}"
      edgeCnts << ["#{@edges[e].getDot}",@edges[e].getWeight]
    end
    return calledBlkLabels,calledLinks,blkCnts,edgeCnts
  end

  def writeDotFile(fname) # used
    dirName="palm-#{getBinary()}-cfg-dots"
    #puts "writing #{dirName}/#{fname}.dot"
    FileUtils.mkdir_p("#{dirName}")
    wd = Dir.pwd
    Dir.chdir(dirName)
    path = "#{fname}.dot"
    file = File.open(path,"w")
    nodes = []
    nodes << "digraph #{@nice_func}{"
    for node in @nodes.keys.sort { |x, y| x.gsub(/cp+/,"").to_i <=> y.gsub(/cp+/,"").to_i }
      nodes << "#{@nodes[node].getDotName} [label=\"#{@nodes[node].getDotName}\\ncalls: #{@nodes[node].getValue}\\n\"]//"
    end
    links = []
    for e in @edges.keys.reverse
      links << "#{@edges[e].getDot} [label=\"#{@edges[e].getWeight}\\n\"]"#,weight=\"#{@edges[e].getWeight}\"]//"
    end
    links << "}"
    file.puts(nodes)
    file.puts(links)
    file.close()
    dotCmd = "dot -Tsvg "+fname+".dot -o "+fname+".svg"
    begin
      `#{dotCmd}`
    rescue Exception => e
      puts "#{e}"
    end
    Dir.chdir(wd)
  end

  def getBlockPaths # used
    return @blockPaths
  end

  def getEvalPaths # used
    return @evalPaths
  end

  def getVarProbs # used
    return @varProbs
  end

  def getVarDefs # used
    return @varDefs
  end

  def getLoopItsVars # used
    return @loopItsVars
  end

  def getEvalStr # used
    return @evalStr
  end

  def getName
    return @name
  end

  def getVarName # used
    varName = @name
    if @parentGraph != nil
      varName = "loop#{varName}"
    end
    return varName.gsub('.','_')
  end

  def getBinary
    return @binary.to_s
  end

  def setBinary(binary)
    @binary = binary
  end

  def getThreadId # used
    return @threadId
  end

  def setThreadId(tid)
    @threadId = tid
  end

  def getFunc
    return @func
  end

  def getNice_func
    return @nice_func
  end

  def addNode(node) # used
    @nodes[node.getName] = node
  end

  def getNode(node) # used
    return @nodes[node.to_s.strip]
  end

  def addEdge(edge) # used
    if @edges[edge.to_s] == nil
      @edges[edge.to_s]=edge
      return true
    end
    return false
  end

  def removeEdge(edge) # used
    # reset variables dealing with edges so that they wil be recalculated next time they are accessed.
    @entrys = []
    @entryEdges = []
    @entryCnts = - 1
    @exits = []
    @exitEdges = []
    @exitCnts = -1
    @backEdgeCnts = -1
    @backEdges = []
    # @instances = -1
    @edges.delete(edge.to_s)
  end

  def removeNode(node) # used
    @nodes.delete(node.to_s)
  end

  #get all the edges in the graph
  def getAllEdges()
    return @edges
  end

  #get only the entry and exit edges into the graph (only applies to subgraphs)
  def getEdges() # used
    if getParentGraph == nil
      puts "WARNING: should not be here"
    end
    return getEntryEdges() + getExitEdges()
  end

  def setBaseGraph(graph) # used
    @baseGraph = graph
  end

  def getBaseGraph() # used
    if @baseGraph == nil
      return self
    else
      return @baseGraph
    end
  end

  def setParentGraph(graph) # used
    @parentGraph = graph
  end

  def getParentGraph # used
    return @parentGraph
  end

  def addSubGraph(graph) # used
    @subGraphs << graph
  end

  def getSubGraphs # used
    return @subGraphs
  end

  def getSubGraph(graphName) # used
    grp=nil
    for g in @subGraphs
      if "#{g}" == "#{graphName}"
        grp = g
      end
    end
    return grp
  end

  def getEntrys # used
    @entryCnts = 0
    @entrys = []
    @entryEdges = []
    @nodes.each do |name,node|
      if node.getSources.length == 0 and !@entrys.include? node #typically only occurs in full graph
          @entrys << node
          @entryCnts += node.getValue
      else #used to find entrys to subgraphs (loops)
        for source in node.getSources
         # puts "#{node} #{source}"
          if @nodes[source.getName] == nil # source not in subgraph (thus is an entry source)
           # puts "found entry #{source.getName} - > #{node}"
            if !@entrys.include? node
              @entrys << node
            end
            edge = node.getSourceEdge(source)
            @entryCnts += edge.getWeight
            @entryEdges << edge
          end
        end
      end
    end
    return @entrys
  end

  def getNodeEntryCnts(node) # used
    count = 0
    if !getEntrys.include? node
      puts "WARNING: this is not an entry node"
    else
      if node.getSources.length == 0 #typcially only occurs in a full graph
        count = node.getValue
      else
        for source in node.getSources
          if @nodes[source.getName] == nil # source not in subgraph
            edge = node.getSourceEdge(source)
            count += edge.getWeight
          end
        end
      end
    end
    return count
  end

  def getEntryEdges # used
    getEntrys()
    return @entryEdges
  end

  def getEntryCnts # used
    getEntrys()
    return @entryCnts
  end

  def getEntryEdgeCount(source) # used
    entryEdgeCnt = 0
    for e in getEdges
      if source.is_a? Graph
        if source.getNodes[e.getSource.getName]
          entryEdgeCnt += e.getWeight
        end
      else
        if e.getSource == source
          entryEdgeCnt += e.getWeight
        end
      end
    end
    return entryEdgeCnt
  end

  def getExits # used
    @exitCnts = 0
    @exits = []
    @exitEdges = []
    @nodes.each do |name,node|
      if node.getTargets.length==0
        @exits << node
        @exitCnts = node.getValue
      else
        for target in node.getTargets
          if @nodes[target.getName] == nil # target not in subgraph (thus is an exit source)

            @exits << node
            edge = node.getTargetEdge(target)
            @exitCnts += edge.getWeight
            @exitEdges << edge
          end
        end
      end
    end
    return @exits
  end

  def getExitEdges # used
    getExits()
    return @exitEdges
  end

  def getAllExitCnts() # used
    getExits()
    return @exitCnts
  end


  # only get the exit counts from self to nodes in `graph`
  def getExitCnts(graph) # used
    count = 0
    if graph.getSubGraphs.include? self
      for edge in @exitEdges
        if graph.getNodes[edge.getTarget.getName] != nil
          count += edge.getWeight
        end
      end
    end
    return count
  end

  # only get exit counts from node to other nodes not in self
  def getNodeExitCnts(node) # used
    count = 0
    if getNodes[node.getName] != nil
      for edge in node.getExitEdges
        if getNodes[edge.getTarget.getName] == nil
          count += edge.getWeight
        end
      end
    end
    return count
  end

  def getBackEdges() #assume backedges must point to a loop entry (header) # used
    if @backEdges == [] and @backEdgeCnts == -1
      @backEdgeCnts = 0
      for entry in getEntrys
        for edge in entry.getEdges
          if edge.getTarget == entry and @nodes[edge.getSource.getName] != nil #backedge
            if !@backEdges.include? edge and !(edge.getSource.getInnerSubGraph == edge.getTarget.getInnerSubGraph and edge.getTarget.getInnerSubGraph != @baseGraph)
              @backEdges << edge
              @backEdgeCnts += edge.getWeight
            end
          end
        end
      end
    end
    return @backEdges
  end

  def getBackEdgeCnts() # used
    if @backEdgeCnts == -1
       getBackEdges()
    end
    return @backEdgeCnts
  end

  # if full graph this is the entry counts
  # if this is a subgraph(loop) this is (backedge counts + exit counts)/entry counts
  def getInstances() # used
    if @instances == -1
      if getBackEdges.length == 0
        @instances = getEntryCnts().to_f
      else
        @instances = (getBackEdgeCnts() + getAllExitCnts()).to_f/getEntryCnts().to_f
      end
    end
    return @instances
  end

  def setInstances(instances)
    @instances = instances
  end

  def compare(graph) # used
    same = true
    if (@nodes.length == graph.getNodes.length) and (getInstances == graph.getInstances) and
      (getEntrys.length == graph.getEntrys.length) and (getEntryCnts == graph.getEntryCnts) and
      (getExits.length == graph.getExits.length) and (getAllExitCnts == graph.getAllExitCnts) and
      ((getEntrys & graph.getEntrys).length == getEntrys.length) and ((getEntrys & graph.getEntrys).length == graph.getEntrys.length) #do this for the other conditionals
      return true
    else
      return false
    end
  end

  def normalize(order) # used
    entryCnt = 0
    for en in getEntryEdges
      entryCnt += en.getWeight
    end
    en.getTarget.setValue(0)
    for en in getEntryEdges
      en.getTarget.setValue(en.getTarget.getValue+entryCnt*getInstances * en.getWeight/entryCnt.to_f)
    end
    seen = []
    for n in order
      seen << n
      if n.is_a? Node
        node = getNode(n)
        origVal = node.getValue
        inExitCnt = 0
        outExitCnt = 0
        for ee in node.getExitEdges
          if getNode(ee.getTarget) != nil
            inExitCnt += ee.getWeight
          else
            outExitCnt += ee.getWeight
          end
        end
        exitCnt = inExitCnt + outExitCnt
        for ee in node.getExitEdges
          if getNode(ee.getTarget) != nil #internal node
            if !seen.include? ee.getTarget
              ee.getTarget.setValue(ee.getTarget.getValue-ee.getWeight)
              ee.setWeight(origVal * ee.getWeight/(outExitCnt.to_f+inExitCnt.to_f))
              ee.getTarget.setValue(ee.getTarget.getValue+ee.getWeight)
            else
              ee.setWeight(origVal-(origVal*(outExitCnt/exitCnt.to_f)) * ee.getWeight/inExitCnt.to_f)
            end
          end
        end
      else
        n[0].normalize(n[1])
      end
    end
    exitCnt = 0
    inExitCnt = 0
    outExitCnt = 0
    for ee in getExitEdges
      exitCnt += ee.getWeight
      if getNode(ee.getTarget) != nil
        inExitCnt += ee.getWeight
      else
        outExitCnt += ee.getWeight
      end
    end
    for ee in getExitEdges
      ee.getTarget.setValue(ee.getTarget.getValue-ee.getWeight)
      ee.setWeight(entryCnt * ee.getWeight/exitCnt.to_f)
      ee.getTarget.setValue(ee.getTarget.getValue+ee.getWeight)
    end
  end

  # def unrollNew(loop) # used
  #   unrolled = false
  #   nodesCopied={}
  #   if loop.getEntrys.length == 1 #and loop.getExits.length == 1
  #     for node in loop.getNodes.keys
  #       loop.getNodes[node].addSubGraph(loop)
  #     end
  #     if loop.getEntrys[0] == loop.getExits[0]  #entry and exit are the same node, so only need to create a copy of the entry/exit node
  #       origNode = loop.getEntrys[0]
  #       if nodesCopied[origNode] == nil
  #         if @nodeCopyCounts[origNode.getName] == nil
  #           @nodeCopyCounts[origNode.getName] = 1
  #         end
  #         newNode = origNode.copyNew(@nodeCopyCounts[origNode.getName])
  #         nodesCopied[origNode]=newNode
  #         @nodeCopyCounts[origNode.getName]+=1
  #         loop.addNode(newNode)
  #         addNode(newNode)
  #       else
  #         newNode = getNode("#{origNode.getName}.#{@nodesCopied[origNode]}")
  #       end
  #       newNode.addInternalSources(origNode,loop)
  #       origNode.removeInternalSources(loop)
  #       newNode.addExternalTargets(origNode,loop)
  #     else
  #       paths = {}
  #       for entry in loop.getEntrys
  #         if entry.getValue.to_f/loop.getInstances.to_f > 0.00001
  #           loop.getPaths(entry,nil,{},paths,{},[],{},{},{},{},false,1.0,true,1.0,1.0)
  #         end
  #       end
  #       loopPaths = []
  #       exitPaths = []
  #       for path in paths
  #         if path[1][0] != -1
  #           exitPaths << path[0]
  #         end
  #         if path[1][1] != -1
  #           loopPaths << path[0]
  #         end
  #       end
  #       c1 =0
  #       for ep in exitPaths
  #         c2 = 0
  #         for lp in loopPaths
  #           seenNodes = []
  #           c3 = 0
  #           #writeDotFile("#{@nice_func}_#{loop.getName}_#{c1}_#{c2}_#{c3}")
  #           c3+=1
  #           origPrev = nil
  #           newPrev = nil
  #           for index in 0..lp.length-1
  #             origNode = getNode(lp[index])
  #             if origNode == nil
  #               origNode = getSubGraph(lp[index])
  #             end
  #             if ep.include? lp[index]
  #               if !origNode.is_a? Graph
  #                 if nodesCopied[origNode] == nil
  #                   if @nodeCopyCounts[origNode.getName] == nil
  #                     @nodeCopyCounts[origNode.getName] = 1
  #                   end
  #                   #writeDotFile("#{@nice_func}_#{loop.getName}_#{c1}_#{c2}_#{c3}_pcp")
  #                   newNode = origNode.copyNew(@nodeCopyCounts[origNode.getName])
  #                   nodesCopied[origNode]=newNode
  #                   @nodeCopyCounts[origNode.getName]+=1
  #                   loop.addNode(newNode)
  #                   addNode(newNode)
  #                   #writeDotFile("#{@nice_func}_#{loop.getName}_#{c1}_#{c2}_#{c3}")
  #                   c3+=1
  #                   seenNodes << origNode
  #                   seenNodes << newNode
  #                   if index == 0
  #                     #change backedges from origNode to new node
  #                     newNode.addInternalSources(origNode,loop)
  #                     origNode.removeInternalSources(loop)
  #                     backEdgeNode = getNode(lp[-1])
  #                   else
  #                     if newPrev.is_a? Graph
  #                       for s in origNode.getSources
  #                         if origPrev.getNode(s) != nil
  #                           newPrev = nodesCopied[s]
  #                           origPrev = s
  #                           break
  #                         end
  #                       end
  #                     end
  #                     newNode.addSourceAndEdge(newPrev,origNode.getEdge(origPrev,origNode).getWeight)
  #                     if origNode == getNode(ep[-1])
  #                       newNode.addExternalTargets(origNode,loop)
  #                       break
  #                     end
  #                   end
  #                   origPrev = origNode
  #                   newPrev = newNode
  #                 else
  #                   newNode = nodesCopied[origNode]
  #                   if newPrev !=nil
  #                     if newPrev.is_a? Graph
  #                       for s in origNode.getSources
  #                         if origPrev.getNode(s) != nil
  #                           newPrev = nodesCopied[s]
  #                           origPrev = s
  #                           break
  #                         end
  #                       end
  #                     end
  #                     newNode.addSourceAndEdge(newPrev,origNode.getEdge(origPrev,origNode).getWeight)
  #                   end
  #                   if origNode == getNode(ep[-1])
  #                     newNode.addExternalTargets(origNode,loop)
  #                     break
  #                   end
  #                   origPrev = origNode
  #                   newPrev = newNode
  #                 end
  #               else
  #                 if nodesCopied[origNode] == nil
  #                   nodesAdded,loopsAdded = createLoopCopy(origNode)
  #                   nodesCopied[origNode]=loopsAdded[0]
  #                   origNode.setParentGraph(self)
  #                   loop.addSubGraph(loopsAdded[0])
  #                   for n in nodesAdded.values
  #                     getNode(n).addSubGraph(loop)
  #                     loop.addNode(getNode(n))
  #                   end
  #                   nodesCopied.merge!(nodesAdded)

  #                   if index == 0
  #                     for e in origNode.getEntryEdges
  #                       if nodesAdded[e.getTarget] != nil
  #                         nodesAdded[e.getTarget].addInternalSources(e.getTarget,loop)
  #                         #change backedges from origNode to new node
  #                         e.getTarget.removeInternalSources(loop)
  #                       end
  #                     end
  #                   else
  #                     if newPrev !=nil
  #                       for t in origPrev.getTargets
  #                         if nodesAdded[t] != nil
  #                           nodesAdded[t].addSourceAndEdge(newPrev,t.getEdge(origPrev,t).getWeight)
  #                         end
  #                       end
  #                     end

  #                     if origNode == ep[-1]
  #                       for e in origNode.getExitEdges
  #                         if nodesAdded[e.getSource] != nil
  #                           nodesAdded[e.getSource].addExternalTargets(e.getSource,loop)
  #                         end
  #                       end
  #                       break
  #                     end
  #                   end

  #                   origPrev = origNode
  #                   newPrev = loopsAdded[0]
  #                 else
  #                   newNode = nodesCopied[origNode]
  #                   if origNode == ep[-1]
  #                     for e in origNode.getExitEdges
  #                       if nodesAdded[e.getSource] != nil
  #                         nodesAdded[e.getSource].addExternalTargets(e.getSource,loop)
  #                       end
  #                     end
  #                     break
  #                   elsif newPrev !=nil
  #                     if newPrev.is_a? Graph
  #                       for s in origNode.getSources
  #                         if origPrev.getNode(s) != nil
  #                           newPrev = nodesCopied[s]
  #                           origPrev = s
  #                           break
  #                         end
  #                       end
  #                     end
  #                     for en in origNode.getEntryEdges
  #                       if origPrev == en.getSource
  #                         nodesCopied[en.getTarget].addSourceAndEdge(newPrev,en.getWeight)
  #                         break
  #                       end
  #                     end
  #                   end
  #                   origPrev = origNode
  #                   newPrev = newNode
  #                 end
  #               end
  #             else
  #               origPrev = origNode
  #               newPrev = nil
  #             end
  #           end
  #           c2+=1
  #         end
  #         c1+=1
  #       end
  #     end
  #     writeDotFile("#{@nice_func}_#{loop.getName}_before_done")

  #     for node in nodesCopied.keys
  #       if node.is_a? Node
  #         node.removeSubGraph(loop)
  #         nodesCopied[node].removeSubGraph(loop)
  #       end
  #     end
  #     for node in loop.getNodes.values
  #       if node.is_a? Node
  #         node.removeSubGraph(loop)
  #       end
  #     end
  #     unrolled = true
  #   end
  #   writeDotFile("#{@nice_func}_loop#{loop.getName}_unrolled")
  #   return unrolled
  # end

  # #TODO: node unrolling for branch free loops is working, need to make sure (i.e. implement) edge counts and node counts are correct
  # def unrollFull(loop,its) # used
  #   if loop.getEntrys.length == 1 #and loop.getExits.length == 1
  #     origEntry = loop.getEntrys[0]
  #     origNodes = loop.getNodes.values
  #     torigEdges = {}
  #     for origNode in origNodes
  #       torigEdges[origNode] = origNode.getExitEdges.clone
  #     end
  #     nodesCopied={}
  #     #while its > 0
  #     for i in 0..its-1
  #       for origNode in origNodes
  #         #loop.getNodes[origNode].addSubGraph(loop)
  #         origNode.addSubGraph(loop)
  #         if @nodeCopyCounts[origNode.getName] == nil
  #           @nodeCopyCounts[origNode.getName] = 1
  #         end
  #         newNode = origNode.copyNew(@nodeCopyCounts[origNode.getName])
  #         newNode.setValue(0)#origNode.getValue)
  #         if nodesCopied[origNode] == nil
  #           nodesCopied[origNode]=[]
  #         end
  #         nodesCopied[origNode]<<newNode
  #         @nodeCopyCounts[origNode.getName]+=1
  #         loop.addNode(newNode)
  #         addNode(newNode)
  #       end
  #       newEdges={}
  #       for origNode in origNodes
  #         origEdges = torigEdges[origNode]
  #         for edge in origEdges
  #           if newEdges[edge] == nil
  #           src = edge.getSource
  #           trg = edge.getTarget
  #             if loop.getNode(src) != nil and loop.getNode(trg) != nil
  #               if origEntry == trg #backedge
  #                 if i == its-1 and false# last unrolling so create new backedge
  #                   nedge = Edge.new(nodesCopied[src][i],trg,edge.getWeight/(its))
  #                   newEdges[edge]=nedge
  #                   nodesCopied[src][i].addEdge(nedge,true)
  #                   trg.addEdge(nedge,true)
  #                   if @baseGraph != nil
  #                     @baseGraph.addEdge(nedge)
  #                     @baseGraph.removeEdge(edge)
  #                   else
  #                     addEdge(nedge)
  #                     removeEdge(edge)
  #                   end
  #                   for g in @subGraphs
  #                     g.removeEdge(edge)
  #                   end
  #                   trg.removeSource(src)
  #                   src.removeTarget(trg)
  #                 end
  #                 #else
  #                   temp = src
  #                   if i > 0
  #                     temp = nodesCopied[src][i-1]
  #                   end
  #                   nedge = Edge.new(temp,nodesCopied[origEntry][i],edge.getWeight/(its))
  #                   newEdges[edge]=nedge
  #                   temp.addEdge(nedge,true)
  #                   nodesCopied[trg][i].addEdge(nedge,true)
  #                   if @baseGraph != nil
  #                     @baseGraph.addEdge(nedge)
  #                     @baseGraph.removeEdge(edge)
  #                   else
  #                     addEdge(nedge)
  #                     removeEdge(edge)
  #                   end
  #                   for g in @subGraphs
  #                     g.removeEdge(edge)
  #                   end
  #                   trg.removeSource(temp)
  #                   temp.removeTarget(trg)
  #                 #end
  #               else
  #                 nedge = Edge.new(nodesCopied[src][i],nodesCopied[trg][i],edge.getWeight/(its))
  #                 newEdges[edge]=nedge
  #                 nodesCopied[src][i].addEdge(nedge,true)
  #                 nodesCopied[trg][i].addEdge(nedge,true)
  #                 #loop.addEdge(edge)
  #                 if @baseGraph != nil
  #                   @baseGraph.addEdge(nedge)
  #                 else
  #                   addEdge(nedge)
  #                 end
  #               end
  #             elsif loop.getNode(src) != nil and loop.getNode(trg) == nil #exit edgeCnts
  #               if i == its-1 #final unrolling iteration
  #                 nedge = Edge.new(nodesCopied[src][i],trg,edge.getWeight)
  #                 newEdges[edge]=nedge
  #                 nodesCopied[src][i].addEdge(nedge,true)
  #                 trg.addEdge(nedge,true)
  #                 if @baseGraph != nil
  #                   @baseGraph.addEdge(nedge)
  #                   @baseGraph.removeEdge(edge)
  #                 else
  #                   addEdge(nedge)
  #                   removeEdge(edge)
  #                 end
  #                 for g in @subGraphs
  #                   g.removeEdge(edge)
  #                 end
  #                 trg.removeSource(src)
  #                 src.removeTarget(trg)
  #               end
  #             end
  #           end
  #         end
  #         writeDotFile("#{@nice_func}_#{loop.getName}_unrolled_it_#{i}")
  #       end
  #     end
  #     writeDotFile("#{@nice_func}_#{loop.getName}_before_done")

  #     for node in nodesCopied.keys
  #       if node.is_a? Node
  #         node.removeSubGraph(loop)
  #         for cnode in nodesCopied[node]
  #           cnode.removeSubGraph(loop)
  #         end
  #       end
  #     end
  #     for node in loop.getNodes.values
  #       if node.is_a? Node
  #         node.removeSubGraph(loop)
  #       end
  #     end
  #     unrolled = true
  #   end
  #   writeDotFile("#{@nice_func}_loop#{loop.getName}_unrolled")
  #   return unrolled
  # end

  # def unrollLoop(its,loop) # used
  #   unrolled = true
  #   if its == 1
  #     unrolled = unrollNew(loop)
  #   elsif its > 1
  #     unrolled = unrollFull(loop,its)
  #   end
  #   return unrolled
  # end

  def tempBfs(loop,roots,finish=nil) # used
    q = []
    for root in roots
      q << root
    end
    ex_q = []
    order = []
    seen = {}
    oldLen = q.length
    cnt=0
    while q.length > 0
      cur = q.shift
      if loop.getNode(cur) != nil
        if cur.getInnerSubGraph == loop
          parentsSeen =true
          for e in cur.getEntryEdges
            if cur != root and loop.getNode(e.getSource) != nil
              if !seen.has_key? getNode(e.getSource)
                parentsSeen=false
              end
            end
          end
          if parentsSeen and !seen.has_key? getNode(cur)
            order << cur
            seen[getNode(cur)] = true
            cnt =0
            for e in cur.getExitEdges
              if !q.include? e.getTarget
                q << e.getTarget
              end
            end
          elsif !seen.has_key? getNode(cur) and !q.include? cur
            q << cur
          end
        else
          parentsSeen = true
          lp2 = nil
          for cl in loop.getSubGraphs
            if cl.getNode(cur) != nil and cl != loop
              lp2 = cl
            end
          end
          if lp2 != nil
            for en in lp2.getEntryEdges
              if lp2.getNode(en.getSource) == nil and !loop.getBackEdges.include? en and !loop.getEntryEdges.include? en
                if !seen.has_key? getNode(en.getSource) and getNode(en.getSource) != cur
                  parentsSeen=false
                end
              end
            end
            if parentsSeen and !seen.has_key? lp2
              lp2_order,lp2_q = tempBfs(lp2,[cur])
              if !order.include? [lp2,lp2_order]
                order << [lp2,lp2_order]
              end
              q += lp2_q
              seen[lp2] = true
              cnt =0
              for e in lp2.getExitEdges
                seen[getNode(e.getSource)] = true
                if !q.include? e.getTarget
                  q << e.getTarget
                end
              end
            elsif !seen.has_key? lp2 and !q.include? cur
              q << cur
            end
          end
        end
        if oldLen == q.length
          cnt +=1
        else
          cnt =0
          oldLen = q.length
        end
        if cnt > 20
          exit
        end
      else # if loop.getNode(cur) != nil
        ex_q << cur
      end # if loop.getNode(cur) != nil
    end #while q.length > 0
    return order,ex_q
  end

  def createLoopCopy(loop) # used
    if @loopCopyCounts[loop.getName] == nil
      @loopCopyCounts[loop.getName] = 1
    end
    nloop = Graph.new(@func,@nice_func,"#{loop.getName}__#{@loopCopyCounts[loop.getName]}",$verbose)
    nloop.setBaseGraph(loop.getBaseGraph)
    nloop.setInstances(loop.getInstances)
    nloops = [nloop]
    addSubGraph(nloop)
    nloop.setParentGraph(self)
    @loopCopyCounts[loop.getName]+=1
    nLoopStr = []
    nodesCopied = {}
    for n in loop.getNodes.keys
      origNode = loop.getNodes[n]
      if !nodesCopied.include? origNode
        if origNode.getInnerSubGraph == loop
          if @nodeCopyCounts[origNode.getName] == nil
            @nodeCopyCounts[origNode.getName] = 1
          end
          newNode = Node.new("#{origNode.getName}.#{@nodeCopyCounts[origNode.getName]}",0,self)
          nLoopStr << "#{newNode}"
          nloop.addNode(newNode)
          newNode.addSubGraph(nloop)
          nodesCopied[origNode]=newNode
          @nodeCopyCounts[origNode.getName]+=1
          addNode(newNode)
        else
          subLoops = loop.getSubGraphs.clone()
          for cl in subLoops
            if cl.getNode(origNode) != nil and cl != loop
              nodesAdded,loopsAdded = createLoopCopy(cl)
              loopsAdded[0].setParentGraph(nloop)
              nloop.addSubGraph(loopsAdded[0])
              for nl in loopsAdded
                nloops << nl
              end
              for n in nodesAdded.values
                getNode(n).addSubGraph(nloop)
                nloop.addNode(getNode(n))
                nLoopStr << "#{getNode(n)}"
              end
              nodesCopied.merge!(nodesAdded)
            end
          end
        end
      end
    end

    for n in loop.getNodes.keys
      origNode = loop.getNodes[n]
      newNode = nodesCopied[origNode]
      for s in origNode.getSources
        if loop.getNode(s) != nil
          newSource = nodesCopied[s]
          newNode.addSourceAndEdge(newSource,origNode.getEdge(s,origNode).getWeight)
        end
      end
    end
    @addedLoops[nLoopStr.sort] = nloop

    return nodesCopied,nloops
  end

  def testLoops(loops,allLoops) # used
    # first combine loops that have both common nodes and exclusive nodes
    nloops = loops
    for l1 in loops
      for l2 in loops
        if l1 != l2
          temp = (l1 & l2)
          if !temp.empty? and temp != l1 and temp != l2
            nloop = (l1 | l2).sort
            if !allLoops.has_key? nloop
              nloops << nloop
            end
            nloops.delete(l1)
            nloops.delete(l2)
          end
        end
      end
    end
    # now remove any subloops
    loops = nloops
    for l1 in loops
      for l2 in loops
        if l1 != l2
          if (l1 - l2).empty? #l1 is subset of l2
            nloops.delete(l1)
          elsif (l2 - l1).empty? #l2 is subset of l1
            nloops.delete(l2)
          end
        end
      end
    end
    return nloops
  end

  def solveForLoops2(entrys=nil,exits=[],nodes=nil,allLoops={},loopNm="",tab="") # used
    loopCnt = 1
    loops = []
    if nodes==nil
      nodes = {}
      for n in getNodes.values
        nodes[n] = true
      end
    end

    touchedNodes = {}

    if entrys == nil
      entrys = getEntrys
    end
    for en in entrys
      node = getNode(en)
      bes = []
      for s in node.getSources
        if nodes.has_key? s
          if !bes.include? [s,node]
            bes << [s,node]
          end
        end
      end
      tloops,ssc = tarjan(entrys,nodes,bes)
      for c in ssc
        for n in c
          touchedNodes[n] = true
        end
      end
      for l in tloops
        if !allLoops.has_key? l
          loops << l
        end
      end

      if bes.length == 0
        bes <<[nil,nil]
      end
      for be in bes
        tloops,ssc = tarjan(entrys,nodes,[be])
        for c in ssc
          for n in c
            touchedNodes[n] = true
          end
        end
        for l in tloops
          if !allLoops.has_key? l and !loops.include? l
            loops << l
          end
        end
      end
    end
    loops = testLoops(loops,allLoops)
    for loop in loops
      entrys = []
      exits = []
      inLoop = {}
      nodes = {}
      for node in loop
        inLoop[node]=true
        nodes[node]=true
        for s in node.getSources
          if !inLoop.has_key? s
            if loop.include? s
              inLoop[s]=true
            else
              inLoop[s]=false
            end
          end
          if inLoop[s] == false
            if !entrys.include? node
              entrys << node
            end
          end
        end
        for t in node.getTargets
          if !inLoop.has_key? t
            if loop.include? t
              inLoop[t]=true
            else
              inLoop[t]=false
            end
          end
          if inLoop[t] == false
            if !exits.include? node
              exits << node
            end
          end
        end
      end
      if !allLoops.has_key? loop.sort
        allLoops[loop.sort] = ["#{loopNm}#{loopCnt}",entrys]
        loopCnt += 1
      end
      if loop.length > 1
        solveForLoops2(entrys,exits,nodes,allLoops,"#{loopNm}#{loopCnt-1}.","")
      end
    end
    tloops=[]
    tentries = {}
    if loopNm == ""
      for l in allLoops
        loopStr = []
        for n in l[0]
          if !loopStr.include? "#{n}"
            loopStr << "#{n}"
          end
        end
        tloops << [l[1][0],loopStr]
        entrysStr = []
        for en in l[1][1]
          if !entrysStr.include? "#{en}"
            entrysStr << "#{en}"
          end
        end
        tentries[l[1][0]]=entrysStr
      end
    end
    return tloops,tentries,touchedNodes.keys.sort
  end

  def tarjan(entrys,nodes,be) # used
    index = 0
    stack = []
    onStack = {}
    nindex = {}
    nlow = {}
    ssc = []
    for en in entrys
      node = getNode(en)
      if !nindex.has_key? node
        strongconnect(be,node,nodes,node,index,nindex,nlow,stack,onStack,ssc)
      end
    end
    loops = []
    for loop in ssc
      if loop.length == 1
        if loop[0].getTargets.include? loop[0]
          loops << loop.sort
        end
      elsif loop.length > 1
        loops << loop.sort
      end
    end
    return loops, ssc
  end

  def strongconnect(be,entry,nodes,node,index,nindex,nlow,stack,onStack,ssc,tab="") # used
    nindex[node] = index
    nlow[node] = index
    index += 1
    stack.push(node)
    onStack[node] = true
    for t in node.getTargets
      if nodes.has_key? t and !be.include? [node,t]
        if !nindex.has_key? t
          strongconnect(be,entry,nodes,t,index,nindex,nlow,stack,onStack,ssc,tab+" ")
          nlow[node] = [nlow[node],nlow[t]].min
        elsif onStack[t] == true
          nlow[node] = [nlow[node],nlow[t]].min
        end
      end
    end
    if nindex[node] == nlow[node]
      nssc = []
      tempNode = stack.pop
      if tempNode != nil
        onStack[tempNode] = false
        nssc << tempNode
      end
      while tempNode != node and tempNode != nil
        tempNode = stack.pop
        if tempNode != nil
          onStack[tempNode] = false
          nssc << tempNode
        end
      end
      ssc << nssc
    end
  end

  def findPaths(loop, start, finish, node,  paths, path, seen) # used
    if !seen.has_key? node and node != finish
      seen[node]=true
      path << node
      for t in node.getTargets
        if loop.getNode(t) != nil
          findPaths(loop,start,finish,t,paths,path.clone,seen.clone)
        end
      end
    else
      if node == finish
        path << node
        paths << path
      end
    end
  end

  def makeCopyOfPath(path,loop,nodesCopied) # used
    origPrev = nil
    newPrev = nil
    for i in 0..path.length-2
      node = path[i]
      origNode = getNode(node)
      if origNode == nil
        origNode = getSubGraph(node)
      end
      if !origNode.is_a? Graph
        if nodesCopied[origNode] == nil
          if @nodeCopyCounts[origNode.getName] == nil
            @nodeCopyCounts[origNode.getName] = 1
          end
          newNode = origNode.copyNew(@nodeCopyCounts[origNode.getName])
          nodesCopied[origNode]=newNode
          @nodeCopyCounts[origNode.getName]+=1
          addNode(newNode)
          if i == 0
            newNode.addExternalSources(origNode,loop)
            origNode.removeExternalSources(loop)
          else
            if newPrev.is_a? Graph
              for s in origNode.getSources
                if origPrev.getNode(s) != nil
                  newPrev = nodesCopied[s]
                  origPrev = s
                  break
                end
              end
            end
            exitCnt = origPrev.getExitCnts(loop)
            newNode.addSourceAndEdge(newPrev,newPrev.getValue * (origNode.getEdge(origPrev,origNode).getWeight/exitCnt.to_f))
            origPrev.getEdge(origPrev,origNode).setWeight(origPrev.getEdge(origPrev,origNode).getWeight - newNode.getEdge(newPrev,newNode).getWeight)
            origNode.setValue(origNode.getValue-newNode.getEdge(newPrev,newNode).getWeight)
          end
          if i == path.length-2
            lastNode = getNode(path[i+1])
            exitCnt =0
            for e in origNode.getExitEdges
              if loop.getNode(e.getTarget) != nil
                exitCnt += e.getWeight
              end
            end
            lastNode.addSourceAndEdge(newNode,newNode.getValue*(lastNode.getEdge(origNode,lastNode).getWeight/exitCnt.to_f))
            lastNode.getEdge(origNode,lastNode).setWeight(lastNode.getEdge(origNode,lastNode).getWeight-lastNode.getEdge(newNode,lastNode).getWeight)
            lastNode.setValue(lastNode.getValue-lastNode.getEdge(newNode,lastNode).getWeight)
          end
          origPrev = origNode
          newPrev = newNode
        else # if nodesCopied[origNode] == nil
          newNode = nodesCopied[origNode]
          if i == 0
            newNode.addExternalSources(origNode,loop)
            origNode.removeExternalSources(loop)
          else
            if newPrev !=nil
              if newPrev.is_a? Graph
                for s in origNode.getSources
                  if origPrev.getNode(s) != nil
                    newPrev = nodesCopied[s]
                    origPrev = s
                    break
                  end
                end
              end
              exitCnt = origPrev.getExitCnts(loop)

              if newNode.getEdge(newPrev,newNode) != nil
                newEdgeOrigValue = newNode.getEdge(newPrev,newNode).getWeight
                newNode.getEdge(newPrev,newNode).setWeight(newPrev.getValue * (origNode.getEdge(origPrev,origNode).getWeight/exitCnt.to_f))
                newNode.setValue(newNode.getValue - newEdgeOrigValue + newNode.getEdge(newPrev,newNode).getWeight)
                origPrev.getEdge(origPrev,origNode).setWeight(origPrev.getEdge(origPrev,origNode).getWeight - (newNode.getEdge(newPrev,newNode).getWeight-newEdgeOrigValue))
                origNode.setValue(origNode.getValue-(newNode.getEdge(newPrev,newNode).getWeight-newEdgeOrigValue))
              else
                newNode.addSourceAndEdge(newPrev,newPrev.getValue * (origNode.getEdge(origPrev,origNode).getWeight/exitCnt.to_f))
                origPrev.getEdge(origPrev,origNode).setWeight(origPrev.getEdge(origPrev,origNode).getWeight - newNode.getEdge(newPrev,newNode).getWeight)
                origNode.setValue(origNode.getValue-newNode.getEdge(newPrev,newNode).getWeight)
              end
            end
          end
          if i == path.length-2
            lastNode = getNode(path[i+1])
            exitCnt =0
            for e in origNode.getExitEdges
              if loop.getNode(e.getTarget) != nil
                exitCnt += e.getWeight
              end
            end
            newEdgeOrigValue = lastNode.getEdge(newNode,lastNode).getWeight
            lastNode.getEdge(newNode,lastNode).setWeight(newNode.getValue*(lastNode.getEdge(origNode,lastNode).getWeight/exitCnt.to_f))
            lastNode.getEdge(origNode,lastNode).setWeight(lastNode.getEdge(origNode,lastNode).getWeight-(lastNode.getEdge(newNode,lastNode).getWeight-newEdgeOrigValue))
          end
          origPrev = origNode
          newPrev = newNode
        end # if nodesCopied[origNode] == nil
      else # if !origNode.is_a Graph
        if nodesCopied[origNode] == nil
          nodesAdded,loopsAdded = createLoopCopy(origNode)
          nodesCopied[origNode]=loopsAdded[0]
          origNode.setParentGraph(self)
          nodesCopied.merge!(nodesAdded)
          if i == 0
            for e in origNode.getEntryEdges
              if nodesAdded[e.getTarget] != nil
                nodesAdded[e.getTarget].addExternalSources(e.getTarget,loop)
                e.getTarget.removeExternalSources(loop)
              end
            end
          else
            if newPrev != nil
              if newPrev.is_a? Graph
                for s in origNode.getSources
                  if origPrev.getNode(s) != nil
                    newPrev = nodesCopied[s]
                    origPrev = s
                    break
                  end
                end
              end
              for t in origPrev.getTargets
                if nodesAdded[t] != nil
                  nodesAdded[t].addSourceAndEdge(newPrev, t.getEdge(origPrev,t).getWeight)
                end
              end
            end
          end
          if i == path.length-2
            lastNode = getNode(path[i+1])
            for s in lastNode.getSources
              if nodesAdded[s] != nil
                exitCnt =0
                for e in s.getExitEdges
                  if loop.getNode(e.getTarget) != nil
                    exitCnt += e.getWeight
                  end
                end
                lastNode.addSourceAndEdge(nodesAdded[s],nodesAdded[s].getValue*(lastNode.getEdge(s,lastNode).getWeight/exitCnt.to_f))
                lastNode.getEdge(s,lastNode).setWeight(lastNode.getEdge(s,lastNode).getWeight-lastNode.getEdge(nodesAdded[s],lastNode).getWeight)
              end
            end
          end
          origPrev = origNode
          newPrev = loopsAdded[0]
        else # if nodesCopied[origNode] == nil
          newNode = nodesCopied[origNode]
          if i == 0
            for e in origNode.getEntryEdges
              if nodesAdded[e.getTarget] != nil
                nodesAdded[e.getTarget].addExternalSources(e.getTarget,loop)
                e.getTarget.removeExternalSources(loop)
              end
            end
          else
          if newPrev !=nil
            if newPrev.is_a? Graph
              for s in origNode.getSources
                if origPrev.getNode(s) != nil
                  newPrev = nodesCopied[s]
                  origPrev = s
                  break
                end
              end
            end
            for en in origNode.getEntryEdges
              if origPrev == en.getSource
                nodesCopied[en.getTarget].addSourceAndEdge(newPrev,en.getWeight)
                break
              end
            end
          end
          end
          if i == path.length-2
            lastNode = getNode(path[i+1])
            for s in lastNode.getSources
              if nodesAdded[s] != nil
                exitCnt =0
                for e in s.getExitEdges
                  if loop.getNode(e.getTarget) != nil
                    exitCnt += e.getWeight
                  end
                end
                lastNode.addSourceAndEdge(nodesAdded[s],nodesAdded[s].getValue*(lastNode.getEdge(s,lastNode).getWeight/exitCnt.to_f))
                lastNode.getEdge(s,lastNode).setWeight(lastNode.getEdge(s,lastNode).getWeight-lastNode.getEdge(nodesAdded[s],lastNode).getWeight)
              end
            end
          end
          origPrev = origNode
          newPrev = newNode
        end # if nodesCopied[origNode] == nil
      end # if !origNode.is_a Graph
    end # for node in path
  end

  # essentially create an outside path for each of the duplicatable entrys to the chosen entry
  def makeReducible(loop)  # used
    dups = []
    exitEntrys = []
    for en in loop.getEntrys
      valid = true
      for t in en.getTargets
        if loop.getNode(t) == nil
          valid = false
          break
        end
      end
      if valid
        dups << en
      else
        exitEntrys << en
      end
    end

    if exitEntrys.length == 0 or exitEntrys.length > 1
      exit
    end
    paths = []
    for dup in dups
      findPaths(loop,dup,exitEntrys[0],dup,paths,[],{})
    end
    nodesCopied = {}
    cnt=0
    for path in paths
      makeCopyOfPath(path,loop,nodesCopied)
      writeDotFile("#{@nice_func}_loop_#{loop.getName}_reduced_p#{cnt}")
      cnt += 1
    end
    writeDotFile("#{@nice_func}_loop_#{loop.getName}_reduced")
  end

  def performNormalization() # used
    order = []

    order = tempBfs(self,getEntrys)[0]

    for node in order
      if node.is_a? Node
        getNode(node).normalize()
      else
        node[0].normalize(node[1])
      end
    end
    writeDotFile("#{@nice_func}_after_norm")
    #exit
  end

  def analyzeLoops(threshold,unroll=false) # used
    writeDotFile("#{@nice_func}_before")
    @addedLoops = {}

    done = false
    reSolve = true
    first = true

    while !done
      done = true
      if reSolve == true
        loops,loopEntries,touchedNodes = solveForLoops2()
        reSolve = false
      end
      if $verbose
        puts "palm-loops: #{@nice_func}"
        puts "palm-loops: #{loops}"
        #puts "tjan-loops: #{tloops}"
        puts "palm-loops: #{loopEntries}"
        #puts "tjan-loops: #{tloopEntries}"
        puts "palm-loops: "
      end
      for loopData in loops.sort.reverse # need to rename any already solved for loops before processing new loops
        loopStr = loopData[1].sort
        if @addedLoops.has_key? loopStr
          if $verbose
            puts "palm-loops: already found: #{loopData[0]} #{loopStr} (#{@addedLoops[loopStr]}) "
          end
          @addedLoops[loopStr].setName(loopData[0])
        end
      end
      if $verbose
        for sub in getSubGraphs
          puts "sg: #{sub} #{sub.getNodes}"
        end
        puts "palm-nodes: #{touchedNodes}"
      end

      for loopData in loops.sort.reverse
        loopStr = loopData[1].sort
        if !@addedLoops.has_key? loopStr
          loop = Graph.new(@func,@nice_func,loopData[0],$verbose)
          loop.setBaseGraph(self)
          loop.setParentGraph(self)

          for l in loopStr
            if $verbose
              puts "#{l}"
            end
            n = @nodes[l]
            if $verbose
              puts "#{n}"
            end
            if n != nil
              loop.addNode(n)
            end
          end
          if $verbose
            puts "here #{loop.marshal_dump}"
            puts "loop entrys length: #{loop.getEntrys.length}"
          end
          if loop.getEntrys.length > 0
            if loop.getEntrys.length>1 and not first
              if $verbose
                puts "*********** Warning: Irreducible loop detected (multiple entry points) ****************"
              end
              makeReducible(loop)
              reSolve = true
              if loop.getNodes.length > 2
                #exit
              end
            end

            lCnts={}
            valid=true
            for child in @subGraphs
              # if child.getEntrys.length < 1 or child.getExits.length <1
              #   valid=false
              #   break
              # end
              if loop != child
                if loop.getName.split(".")[0] == child.getName.split(".")[0] and child.getName.start_with? loop.getName
                  c = child.getName.count(".")-loop.getName.count(".") #the child and parent loops typically have a difference of one
                  #puts "#{child.getName} #{loop.getName} #{c}"
                  if lCnts[c]==nil
                    #puts "should not be here"
                    #exit
                    lCnts[c]=[child]
                  else
                    lCnts[c] << child
                  end
                end
              end
            end
            if valid
              min = 10000000
              for k in lCnts.keys.sort # ascending order sort
                if k <= min # the closest loops occur first, but there is no guarantee on what the minimum 'k' is for a given graph/execution
                  if $verbose
                    puts "#{lCnts[k]} sets parent #{loop}"
                    puts "#{loop} adds #{lCnts[k]}"
                  end
                  for cl in lCnts[k]
                    loop.addSubGraph(cl)
                    cl.setParentGraph(loop)
                  end
                  min = k
                end
              end
            end
            loop.getInstances()
            if $verbose
              puts "instances #{loop.getInstances()}"
            end
            #unrolled = false

            # if loop.getInstances() < 1 and not first# unroll
            #   puts "here?"
            #   doUnroll = unroll
            #   stk = []
            #   for sg in loop.getSubGraphs
            #     stk.push(sg)
            #   end
            #   while stk.length > 0
            #     sg = stk.pop()
            #     if sg.getEntrys.length>1  or false#can not handle unrolling loops that contain an irreducible innerloop
            #       doUnroll = false
            #       break
            #     else
            #       for cg in sg.getSubGraphs
            #         stk.push(cg)
            #       end
            #     end
            #   end
            #   puts "unroll: #{doUnroll}"
            #   if doUnroll

            #     unrolled = unrollLoop(loop.getInstances().to_i-1,loop)
            #     #unrolled = unrollLoop(2,loop)
            #     #reSolve = true
            #   end
            # end
            #if !unrolled
              inSubGraphs = false#check to see if an identical subgraph already exists
              for sub in @subGraphs
                if sub.compare(loop) == true
                  inSubGraphs = true
                end
              end
              if $verbose
                puts inSubGraphs
              end
              if !inSubGraphs
                # maybe add a check here to handle loops that iterate less than 2 times
                addSubGraph(loop)
                for l in loopStr
                  n = @nodes[l]
                  if n != nil and loop.getNodes[l] != nil
                    n.addSubGraph(loop)
                  end
                end
                writeDotFile("#{@nice_func}_loop_#{loop.getName}_added")
              end
            # else
            #   reSolve = true
            # end
          end
          @addedLoops[loopStr] = loop
          done = false
          if not first
            break
          end
        end # if addedLoops.has_key? loopStr.sort
      end # for loopData in loops.sort.reverse
      if first
        pruneCFG(threshold)
        for node in getNodes.values
          node.removeAllSubGraphs
        end
        removeAllSubGraphs
        first = false
        done = false
        reSolve = true
        @addedLoops = {}
      end
    end # !done
    performNormalization()
    for loop in @subGraphs
      loop.setCalced(false)
    end
    if $verbose
      puts "nodes #{@nodes}"
      puts "edges #{@edges}"
    end
    return true
  end

  def maxVal # used
    return @maxVal
  end

  def pruneCFG(threshold) # used
    writeDotFile("#{@nice_func}_before_prune")
    origEntrys = getEntrys
    if $verbose
      puts "#{self} --- pruneCFG: #{threshold} oe #{origEntrys} "
    end
    nodes = getNodes.values
    for node in nodes
      val = node.getValue.to_f
      if $verbose
        puts "n: #{node} #{val}"
      end
      if node.getExitEdges.length > 0
        exitEdges = node.getExitEdges.clone
        for e in exitEdges
          if $verbose
            puts "#{e.getTarget.getInnerSubGraph} #{self}"
          end
          if e.getSource.getInnerSubGraph == e.getTarget.getInnerSubGraph
            if $verbose
              puts "e: #{e} #{e.getWeight} #{e.getWeight.to_f/val} #{1.0-threshold.to_f}"
            end
            if e.getWeight.to_f/val < 1.0-threshold.to_f
              e.getSource.removeTarget(e.getTarget)
            end
          else
            if $verbose
              puts "source and target have different innersubgraphs"
            end
            # for cl in getSubGraphs
            #   if cl.getNode(e.getTarget) != nil
            #     #cl.pruneCFG(threshold)
            #   end
            # end
          end
        end
        if node.getEdges.length == 0
          if $verbose
            puts "node #{node} is pruned"
          end
          removeNode(node)
          for sg in node.getSubGraphs
            sg.removeNode(node)
          end

        elsif node.getExitEdges.length == 0
          puts "deleted too many edges"
          #exit
        end
      end
    end
    if $verbose
      puts "finished first prunning pass"
    end
    updated = true
    while updated
      updated = false
      loops,loopEntries,touchedNodes = solveForLoops2()
      nodes = getNodes.values
      for node in nodes
        if $verbose
          puts "n: #{node} #{node.getEntryEdges} #{!origEntrys.include? node}"
        end
        if (node.getEntryEdges.length == 0 and !origEntrys.include? node)  or !touchedNodes.include? node
          if $verbose
            puts "#{node} #{node.getEdges}"
          end
          exitEdges = node.getExitEdges.clone
          for e in exitEdges
            if $verbose
              puts "edge to remove: #{e} #{exitEdges}"
            end
            e.getSource.removeTarget(e.getTarget)
            updated = true
            if $verbose
              puts "#{exitEdges}"
            end
          end
          if $verbose
            puts "#{node} #{node.getEdges}"
            puts "pruning node: #{node}"
          end
          removeNode(node)
          for sg in node.getSubGraphs
            sg.removeNode(node)
          end
        end
      end
    end


    writeDotFile("#{@nice_func}_after_prune")

  end

  def generateNodes(blkCnts,labels) # used
    if blkCnts
      for b in blkCnts
        for l in labels
          if l.index("\""+b[0]+"\"") and b[1] > 0  #only add blocks that were called  #change back
            node = Node.new(b[0],b[1],self)
            if @maxVal < node.getValue
              @maxVal = node.getValue
            end
            addNode(node)
          end
        end
      end
    end
  end


   # edgeCnts: [["src -> trg" , # times called],...] (contains all edges in function)
   # links: ["src -> trg",...] (contains edges that were actually encountered during execution)
   # delEdges: [["src -> trg" , # times called],...] (contains edges that were never encountered)
  def generateEdges(edgeCnts,links,delEdges) # used
    if edgeCnts.length > 0
      for e in edgeCnts
        for l in links
          if l.index(e[0]) and e[1] > 0 # add edge if it was encountered during execution #change back
            matches = e[0].match(/(.*) -> (.*)/)
            src = getNode(matches.captures[0])
            trg = getNode(matches.captures[1])
            edge = Edge.new(src,trg,e[1])
            src.addEdge(edge)
            if src != trg
              trg.addEdge(edge)
            end
            addEdge(edge)
          end
        end
      end
    end
    #TODO: need to document what the following code is doing
    #possibly remove below code due to new pruneCFG functionality?
    newEdges = []
    if delEdges.length > 0
      entrys = getEntrys
      exits = getExits
      for n in exits
        trgs=[]
        srcs=[]
        for e in delEdges
          matches = e[0].match(/(.*) -> (.*)/)
          if matches.captures[0] == n.getName and getNode(matches.captures[1]) != nil
            tval = 0
            for te in getNode(matches.captures[0]).getEdges
              if te.getSource == getNode(matches.captures[0])
                tval += te.getWeight
              end
            end
            if getNode(matches.captures[1]).getValue > 0 and getNode(matches.captures[0]).getValue != tval
              trgs << matches.captures[1]
            end
          elsif matches.captures[1] == n.getName and getNode(matches.captures[0]) != nil
            tval = 0
            for te in getNode(matches.captures[0]).getEdges
              if te.getSource == getNode(matches.captures[0])
                tval += te.getWeight
              end
            end
            if getNode(matches.captures[0]).getValue > 0 and getNode(matches.captures[0]).getValue != tval
              srcs << matches.captures[0]
            end
          end
        end
        for e in delEdges
          matches = e[0].match(/(.*) -> (.*)/)
          tval = 0
          if getNode(matches.captures[0]) != nil
            for te in getNode(matches.captures[0]).getEdges
              if te.getSource == getNode(matches.captures[0])
                tval += te.getWeight
              end
            end
            if matches.captures[0] == n.getName and getNode(matches.captures[1]) != nil and getNode(matches.captures[0]).getValue != tval                ##### switch statements #####
              if entrys.include? getNode(matches.captures[1])
                src = getNode(matches.captures[0])
                trg = getNode(matches.captures[1])
                edge = Edge.new(src,trg,trg.getValue)
                src.addEdge(edge)
                if src != trg
                  trg.addEdge(edge)
                end
                if addEdge(edge)
                  newEdges << [e[0],trg.getValue]
                end
              elsif trgs.length == 1
                src = getNode(matches.captures[0])
                trg = getNode(matches.captures[1])
                edge = Edge.new(src,trg,src.getValue)
                src.addEdge(edge)
                if src != trg
                  trg.addEdge(edge)
                end
                if addEdge(edge)
                  newEdges << [e[0],src.getValue]
                end
              end
            elsif matches.captures[1] == n.getName and getNode(matches.captures[0]) != nil and getNode(matches.captures[0]).getValue != tval
              if entrys.include? getNode(matches.captures[0])
                src = getNode(matches.captures[0])
                trg = getNode(matches.captures[1])
                edge = Edge.new(src,trg,trg.getValue)
                src.addEdge(edge)
                if src != trg
                  trg.addEdge(edge)
                end
                if addEdge(edge)
                  newEdges << [e[0],trg.getValue]
                end
              elsif srcs.length == 1
                src = getNode(matches.captures[0])
                trg = getNode(matches.captures[1])
                edge = Edge.new(src,trg,trg.getValue)
                src.addEdge(edge)
                if src != trg
                  trg.addEdge(edge)
                end
                if addEdge(edge)
                  newEdges << [e[0],trg.getValue]
                end
              end
            end
          end
        end
      end
    end
    #reset entry counts, to account for new and/or deleted edges
    @entryCnts = -1
    @exitCnts = -1
    @backEdgeCnts = -1
    @getInstances = -1
    return newEdges
  end

  def printOut
    @nodes.each do |key,node|
      node.print
    end
  end

  def getNodes # used
    return @nodes
  end

  def serialize(fname) # used
    file = File.open(fname,"w")
    file.puts(YAML::dump(self))
    file.close()
  end

  def getPaths(node,p_edge,seen,paths,ks,path,blockPaths,varProbs,varDefs,evalPaths,full,probThresh,forceCalc,prevk=1.0,k=1.0,tab="",highProb={0=>0}) # used
    if seen[node]==nil
      seen[node]=0
    end

    ps = ""
    ks[node.getName]=prevk*k
    for p in path
      ps +="(#{p.getName},#{ks[p.getName]})->"
    end

    if (@nodes[node.getName] != nil and node.getTargets.length != 0) and seen[node] < 1 #node is in this (sub)graph
      if node.getInnerSubGraph == self
        path << node
        seen[node] += 1
        for target in node.getTargets
          edge = node.getTargetEdge(target)
          if prevk*k > 1.0-probThresh or (prevk*k < 1.0-probThresh and prevk*k > highProb[0])
            getPaths(target,edge,seen.clone,paths,ks.clone,path.clone,blockPaths,varProbs,varDefs,evalPaths,full,probThresh,forceCalc,prevk*k,(edge.getWeight.to_f/(node.getValue.to_f-getNodeExitCnts(node))),"#{tab}\t",highProb)
          end
        end
      else
        for sub in @subGraphs
          if sub.getNodes[node.getName] != nil and sub.getParentGraph == self
            if seen[sub]==nil
              seen[sub]=0
            end
            if seen[sub] < 1
              seen[sub]+=1
              tblockPaths,tvarProbs,tvarDefs,tevalPaths,tloopItsVars,tevalStr=sub.calculatePaths(probThresh,forceCalc)
              for nk in sub.getNodes.keys
                seen[sub.getNode(nk)] = 1
              end

              blockPaths.merge!(tblockPaths)
              varProbs.merge!(tvarProbs)
              varDefs.merge!(tvarDefs)
              evalPaths.merge!(tevalPaths){|key,o,n| o + n}
              path<<sub
              for exit in sub.getExits
                for target in exit.getTargets
                  edge = exit.getTargetEdge(target)
                  if prevk*k > 1.0-probThresh or (prevk*k < 1.0-probThresh and prevk*k > highProb[0])
                    getPaths(target,edge,seen.clone,paths,ks.clone,path.clone,blockPaths,varProbs,varDefs,evalPaths,full,probThresh,forceCalc,prevk*k,(edge.getWeight.to_f/sub.getAllExitCnts.to_f),"#{tab}\t",highProb)
                  end
                end
              end
            end
          end
        end
      end
    else
      if (p_edge == nil) or !(p_edge.getSource.getInnerSubGraph == p_edge.getTarget.getInnerSubGraph and node.getInnerSubGraph != self) #source and target are part of an inner loop so not an actual backedge in this loop
        if node.getTargets.length == 0 and getParentGraph == nil
          ps += node.getName
          path << node
        end
        if paths[path] == nil
          paths[path] = [-1,-1] # [exitpath,looppath]
        end

        if @nodes.length == 1
          paths[path][0] = 1.0
          highProb[0] = 1.0
        else
          if (@nodes[node.getName] == nil or node.getTargets.length == 0)
            if getParentGraph == nil
              paths[path][0]=prevk*k
              if highProb[0] < prevk*k
                highProb[0] = prevk*k
              end
            else
              paths[path][0]=prevk*(p_edge.getWeight.to_f/getAllExitCnts.to_f)
              if highProb[0] < prevk*(p_edge.getWeight.to_f/getAllExitCnts.to_f)
                highProb[0] = prevk*(p_edge.getWeight.to_f/getAllExitCnts.to_f)
              end
            end
          end
          if seen[node] >= 1
            paths[path][1]=prevk*k
            if highProb[0] < prevk*k
              highProb[0] = prevk*k
            end
          end
        end
      end
    end
  end

  def createBlockPathVar(path,blockPaths,varProbs,varDefs,cnts,name="seg") # used
    var = "#{getVarName}_#{name}_#{cnts[name]}"
    pathArr = path.split("->")
    if blockPaths[pathArr] == nil
      var = "#{getVarName}_#{name}_#{cnts[name]}"
      blockPaths[pathArr] = var
      varDefs[var]=pathArr
      varProbs[var]=1.0
      cnts[name]+=1
    end
    return blockPaths[pathArr]
  end

  def createPathVar(path,evalPaths,varProbs,varDefs,cnts,name="path") # used
    var = "#{getVarName}_#{name}_#{cnts[name]}"
    set = Set.new()
    evalPaths[path[0]] << var
    varDefs[var]=path[0]
    varProbs[var]=path[1]
    cnts[name]+=1
    return var
  end

  def constructPathStr(pathData,evalStr,exitStr,blockPaths,evalPaths,varProbs,varDefs,cnts,probThresh) # used
    pathStr= ""
    varPathStr = "("
    loopSeen = false
    entryPath = ""
    exitPath = ""
    fullPath = ""

    path = pathData[0]

    #------------------------------Root Node---------------------------------------
    node = path[0]

    if node.is_a? Graph
      varPathStr +="#{node.getVarName} + "
      fullPath+="loop "+node.getName+"->"
      exitPath = ""
      loopSeen = true
      #puts "WARNING!!! first node is a loop, need to implement rule"
      #exit
    else
      entryPath+=node.getName+"->"
      fullPath+=node.getName+"->"
      exitPath+=node.getName+"->"
    end

    #---------------------------------------------------------------------------------
    #---------------------------intermediate nodes------------------------------------
    prevNode = node
    prevNodeExitCnts = prevNode.getExitCnts(self)
    prevLoopSeen = false

    #=================================================================================
    for n in 1..path.length-1 #handle intermediate nodes
      node = path[n]
      if node.is_a? Graph
        loopStr = ""
        if not loopSeen and entryPath != ""
          tVarPathStr = createBlockPathVar(entryPath,blockPaths,varProbs,varDefs,cnts,"entry")
          loopStr += tVarPathStr + " + "
          loopSeen=true
        elsif exitPath != ""
          tVarPathStr = createBlockPathVar(exitPath,blockPaths,varProbs,varDefs,cnts,"mid")
          loopStr += tVarPathStr + " + "
        end
        if loopStr != ""
          loopStr = loopStr.gsub(/(.*) \+ /,'\1')
          varPathStr += loopStr+" + "
        end

        fullPath+="loop "+node.getName+"->"

        exitPath = "" #reset exit path since this may be the last loop before end of function
        varPathStr +="#{node.getVarName} + "
      else
        pathStr+=node.getName + "->"
        if not loopSeen
          entryPath+=node.getName+"->"
        end
        fullPath+=node.getName+"->"
        exitPath+=node.getName+"->"
      end

      entryEdgeCnt = node.getEntryEdgeCount(prevNode)
      exitEdgeCnt = node.getExitCnts(self)

      prevNode = node
      prevNodeExitCnts=exitEdgeCnt
      prevLoopSeen = loopSeen
    end# end of "for n in 1..path.length-1"
    #===================================================================================

    fullPath = fullPath.gsub(/(.*)->/,'\1')
    if fullPath!=""
      if !loopSeen # only called if no other loops exist in this (sub)graph
        tVarPathStr =  createBlockPathVar(fullPath,blockPaths,varProbs,varDefs,cnts,"seg")
        tVarPathStr = varPathStr+tVarPathStr
        tVarPathStr += ")"
        if tVarPathStr != "()" and evalPaths[tVarPathStr].size == 0# == nil
          if pathData[1][1] != -1 # this represents a path through a graph terminating in a back edge (should only be called by loop sub-graphs)
            tEvalStr = createPathVar([tVarPathStr,pathData[1][1]],evalPaths,varProbs,varDefs,cnts,"path")
            evalStr +=  tEvalStr + " + "
          end
          if pathData[1][0] != -1 # this represents a path through a graph terminating in an exit node
            tEvalStr = createPathVar([tVarPathStr,pathData[1][0]],evalPaths,varProbs,varDefs,cnts,"out")
            exitStr +=  tEvalStr + " + "
          end
        end
        varPathStr="("
      end
    end

    loopExitStr = ""
    exitPath= exitPath.gsub(/(.*)->/,'\1')

    if exitPath != ""
      if loopSeen
        tVarPathStr = createBlockPathVar(exitPath,blockPaths,varProbs,varDefs,cnts,"exit")
        loopExitStr +=tVarPathStr + " + "
      end
    end


    loopExitStr = loopExitStr.gsub(/(.*) \+ /,'\1')
    if loopExitStr != ""
      varPathStr += loopExitStr + ")"
    else
      varPathStr = varPathStr.gsub(/(.*) \+ /,'\1')+")"
    end
    if varPathStr != "()"
      if evalPaths[varPathStr].size == 0# == nil
        if pathData[1][1] != -1 # this represents a path through a graph terminating in a back edge (should only be called by loop sub-graphs)
          tEvalStr = createPathVar([varPathStr,pathData[1][1]],evalPaths,varProbs,varDefs,cnts,"path")
          evalStr +=  tEvalStr + " + "
        end
        if pathData[1][0] != -1
          tEvalStr = createPathVar([varPathStr,pathData[1][0]],evalPaths,varProbs,varDefs,cnts,"out")
          exitStr +=  tEvalStr + " + "
        end
      end
    end
    return evalStr,exitStr

    #-------------------------------------------------------------------------------------
  end

  def getVarsToKeep(varProbs,varDefs,loopItsVars,probThresh) # used
    pathSumK = 0.0
    outSumK = 0.0
    varsToKeep = []
    probThresh = 1.0
    for v in varProbs.sort { |x,y| y[1] <=> x[1] }
      if v[1] != nil and ((v[0].start_with? "#{getVarName}_path" and pathSumK < probThresh) or (v[0].start_with? "#{getVarName}_out" and outSumK < probThresh))
        varQueue = [v[0]]
        while varQueue.length > 0
          var = varQueue.pop
          varsToKeep << var
          path = varDefs[var]
          if !path.is_a? Array and !path.is_a? Float and !path.is_a? Integer and path != nil
            if !path.include? "_its*"
              path = path[1..-2].split(" + ") # remove parenthesis and split on +
              for p in path
                varQueue << p
              end
            else
              path = path.split("*")
              if getParentGraph != nil # need to keep iteration variables (base graph returns them in a seperate data structure so no need keep them)
                varQueue << path[0]
              end
              loopItsVars[path[0]] = varDefs[path[0]]
              path = path[1].split(" + ") # remove parenthesis and split on +
              for p in path
                if p.include? "("
                  p = p.gsub("(","")
                end
                if p.include? ")"
                  p = p.gsub(")","")
                end
                varQueue << p
              end
            end
          end
        end

        if v[0].start_with? "#{getVarName}_path"
          pathSumK+=v[1]
        elsif v[0].start_with? "#{getVarName}_out"
          outSumK+=v[1]
        end
      end
    end
    return varsToKeep,pathSumK,outSumK
  end

  def calculatePaths(probThresh = 0.999, forceCalc = false) # used
    if forceCalc or @calced == false
      @blockPaths = {}
      @evalPaths = Hash.new { |hash, key| hash[key] = Set.new }
      @varProbs = {}
      @varDefs = {}
      @calced = false

    end
    if @calced
      return @blockPaths,@varProbs,@varDefs,@evalPaths,@loopItsVars,@evalStr
    end

    blockPaths = {} #basic block array => variable name
    evalPaths = Hash.new { |hash, key| hash[key] = Set.new }  # path => variable name(s)
    varProbs = {} # variable name => probability
    varDefs = {} # variable name => path

    # variables and paths produced from any child loops
    tblockPaths = {} #basic block array => variable name
    tevalPaths = Hash.new { |hash, key| hash[key] = Set.new }  # path => variable name
    tvarProbs = {} # variable name => probability
    tvarDefs = {}

    cnts = {"entry" => 0, "mid" => 0, "exit" => 0, "seg" => 0, "path" => 0, "out" => 0}

    paths = {}
    for entry in getEntrys
      if entry.getValue.to_f/getInstances.to_f > 0.00001
        getPaths(entry,nil,{},paths,{},[],tblockPaths,tvarProbs,tvarDefs,tevalPaths,false,probThresh,forceCalc,1.0,getNodeEntryCnts(entry).to_f/getEntryCnts.to_f)
      end
    end

    evalStr = ""
    exitStr = ""
    entryPaths={}
    exitPaths = {}
    fullPaths = {}
    progCnt = 0
    if getParentGraph == nil
      #puts "#{getVarName}: num Paths: #{paths.length}"
    end
    progStr ="["
    for pi in (0..100)
      progStr+=" "
    end
    progStr +="] 0.0\%\r"
    print progStr
    $stdout.flush
    for path in paths
      tstr=""
      for p in path[0]
        tstr+=p.getName+"->"
      end


      evalStr,exitStr = constructPathStr(path,evalStr,exitStr,blockPaths,evalPaths,varProbs,varDefs,cnts,probThresh)

      progCnt+=1
      progStr ="["
      percDone = (progCnt.to_f/paths.length.to_f)*100.0
      for pi in (0..(percDone.to_i/1))
        progStr+="\#"
      end
      for pi in ((percDone.to_i/1)..100)
        progStr+=" "
      end
      progStr +="] #{progCnt}/#{paths.size} #{percDone.round(2)}\%\r"
      #print progStr
      $stdout.flush
    end

    blockPaths = blockPaths.merge(tblockPaths)
    varProbs = varProbs.merge(tvarProbs)
    varDefs = varDefs.merge(tvarDefs)
    evalPaths = evalPaths.merge(tevalPaths){|key,o,n| o + n}

    loopItsVars = {}
    varsToKeep,pathSumK,outSumK = getVarsToKeep(varProbs,varDefs,loopItsVars,probThresh)
    #puts "pathSumK #{pathSumK}"
    #puts "outSumK #{outSumK}"

    evalStr = "("
    exitStr = ""
    for var in varsToKeep.reverse
      @varDefs[var] = varDefs[var]
      if varProbs[var] != nil
        @varProbs[var] = varProbs[var]
      end
      if varDefs[var].is_a? Array
        @blockPaths[varDefs[var]]=var
      elsif var.start_with? "#{getVarName}_path"
        @evalPaths[@varDefs[var]] << var
        @varProbs[var] = varProbs[var]/pathSumK
        evalStr+="#{var} + "
      elsif var.start_with? "#{getVarName}_out"
        @evalPaths[@varDefs[var]] << var
        @varProbs[var]=varProbs[var]/outSumK
        exitStr+="#{var} + "
      elsif var.include? "_its"
        @evalPaths[var] << var
      else
        @evalPaths[@varDefs[var]] << var
      end
    end

    exitStr = exitStr.gsub(/(.*) \+ /,'\1')
    if evalStr != "("
      evalStr = evalStr.gsub(/(.*) \+ /,'\1)')

      if exitStr != ""
        evalStr += " + "+exitStr
      end
    else
      if exitStr != ""
        evalStr = exitStr
      end
    end
    if getParentGraph != nil
      evalStr = "#{getVarName}_its*#{evalStr}"
      @evalPaths[evalStr] << "#{getVarName}"
      @varDefs["#{getVarName}"]=evalStr
      @varDefs["#{getVarName}_its"]=getInstances()
      @evalPaths["#{getVarName}_its"] << "#{getVarName}_its"
    end
    @evalStr=evalStr
    @loopItsVars=loopItsVars
    #puts
    @calced = true

    return @blockPaths,@varProbs,@varDefs,@evalPaths,@loopItsVars,@evalStr
  end
end


