#!/usr/bin/env ruby
# -*- mode: ruby -*-

require 'pathname'
require 'parallel'
require 'graphviz'

require_relative 'ModelGenerator'
require_relative 'CFGProf-Analyze'
require_relative 'CFGProf-Parse'

#****************************************************************************

$verbose = false

# FIXME: comment out as this is also in 'palm-task-run'

# def runCmd(cmd,verbose,timeout=0)
#   output = []
#   if verbose
#     puts cmd.to_s
#   end
#   cmdPid = -1
#   if timeout > 0
#     begin
#       Timeout.timeout(timeout) do
#         #puts "runcmd: (#{Dir.pwd}) #{cmd}"
#         stdin, stdout, stderr, wait_thr = Open3.popen3(cmd)
#         cmdPid = wait_thr[:pid]
#         stdout.each do |line|
#         if line.include? "palm-inst:"
#           if verbose
#             p line.chomp
#           end
#         else
#           if verbose
#             p line.chomp
#           end
#         end
#         output << line.chomp.to_s
#       end
#       stdin.close()
#       stdout.close()
#       stderr.close()
#       if verbose
#         puts "#{wait_thr.value}"
#       end
#     end
#     rescue Timeout::Error, Exception => e
#       puts "Error #{e}"
#       if wait_thr != nil
#         Process.kill 9, wait_thr[:pid]#cmdOut.pid
#         Process.wait wait_thr[:pid]#cmdOut.pid
#         if verbose
#           puts "#{wait_thr.value} #{$?.to_i}"
#         end
#       end
#     end
#   else
#     begin
#       stdin, stdout, stderr, wait_thr = Open3.popen3(cmd)
#       cmdPid = wait_thr[:pid]
#       stdout.each do |line|
#         if line.include? "palm-inst:"
#           if verbose
#             p line.chomp
#           end
#         else
#           if verbose
#             p line.chomp
#           end
#         end
#         output << line.chomp.to_s
#       end
#       stdin.close()
#       stdout.close()
#       stderr.close()
#       if verbose
#         puts "#{wait_thr.value}"
#       end
#     rescue Timeout::Error, Exception => e
#       if wait_thr != nil
#         Process.kill 9, wait_thr[:pid]
#         Process.wait wait_thr[:pid]
#         if verbose
#           puts "#{wait_thr.value} #{$?.to_i}"
#         end
#       end
#     end
#   end
#   return output,cmdPid
# end


#****************************************************************************

def createModel(cfgprof_fnm,latency_file,fp_file,args)
  $verbose = args[:arg_verbose]
  if cfgprof_fnm

    puts "*** Analyze CFG profile: #{cfgprof_fnm}"
    output,cmdPid = readCFGProfile(cfgprof_fnm)
    cfgprofParser = CFGProfParse.new(output,$verbose)

    modGen = ModelGenerator.new($verbose)
    nprocs = args[:arg_num_threads].to_i
    modGen.setNprocs(nprocs)
    modGen.setInclusive(args[:arg_inclusive_model])
    modGen.setGhz(args[:arg_ghz].to_f*10**9)
    modGen.setAllDotFiles(args[:arg_print_all_dot_files])
    modGen.setPathThreshold(args[:arg_path_threshold].to_f)
    modGen.setIaca(false)
    modGen.setOutputAnalyzer(cfgprofParser)

    loadCache = args[:arg_load_cost_cache]
    average = args[:arg_average]
    arch = args[:arg_architecture]
    miami = args[:arg_miami]
    funcs=[]
    nameMap = modGen.getNameMap()
    executable,executablePath = cfgprofParser.getExecutableInfo()
    nThreads = cfgprofParser.getNumThreads()

    workDir = "palm-#{executable}/cfgprof-#{cmdPid}"
    FileUtils.mkdir_p(workDir)
    modGen.setExecutable(executable)
    modGen.setExecutablePath(executablePath)
    modGen.setWorkDir(workDir)

    orderedFuncs = cfgprofParser.getOrderedFuncs
    if $verbose
      puts "#{orderedFuncs}"
      puts "#{orderedFuncs.keys}"
    end
    if funcs == []
      for mangled in orderedFuncs.keys
        if $verbose
          puts "#{mangled}"
        end
        if args[:arg_functions] == []
          funcs << [mangled,0]
        else
          if args[:arg_functions].include? mangled or args[:arg_func_level] != nil
            funcs << [mangled,0]
          end
        end
      end
    end
    if latency_file == ""
      latency_file = "palm-#{executable}/latency/max_insn.lat"
    end
    if $verbose
      puts "latfile: #{latency_file}"
    end
    modGen.setLatPath(latency_file)
    modGen.setFPPath(fp_file)
   
    funcCnt = 0
    finishedFuncs = {}
    unfinishedFuncs = {}
    uncapturedFuncs = {}
    unanalyzedFuncs = {}

    if $verbose
      puts "funcs: #{funcs}"
    end

    if funcs == []
      puts "Please ensure you use the mangled name of functions you wish to model (-f argument)"
    end

    for func,execTime in funcs
      funcCnt+=1
      if $verbose
        puts func
        puts execTime
        puts func+", "+execTime.to_s
      end
      if nameMap[func] != nil

        puts "*** Model #{func} (#{funcCnt}/#{funcs.length})"

        binaryPath = cfgprofParser.analyzePathOutput(func) #binary the function is located in (can be an executable, library, ect)\\
        binary = Pathname.new(binaryPath).basename
        if $verbose
          puts "binaryPath: #{binaryPath}"
        end

        if modGen.copyBinaryToWorkDir(binary,binaryPath)
          # create graphs for each thread of func in parallel
          if $verbose
            puts "creating graph"
          end
          graphs = modGen.createGraph(func,nprocs,average,args[:arg_unroll]) # createGraph is a parallel function
          #---------
          # generate IACA commands for each graph
          results = Parallel.map(0..(graphs.length-1), in_processes: nprocs) do |g|
            graph = graphs[g]
            if graph != nil
              if graphs.length == 1
                tid = ""
              else
                tid = graph.getThreadId
              end
              if !miami
                if $verbose
                  puts "generating IACA cmds"
                end
                cmds,calledDict = modGen.generateGraphIACAcmds(func,graph)
              else
                if $verbose
                  puts "generating MIAMI cmds"
                end
                cmds,calledDict = modGen.generateGraphMIAMIcmds(func,graph)
              end
              [cmds,calledDict,graph] #need to return updated graph from process
            else
              false
            end
          end
          #----------

          # gather commands into a single array and calledFunctions into a single hash as there will be a lot of overlap across the thread models
          # combining these prevents duplicate calls to IACA/MIAMI
          valid = true
          cmds = []
          calledDict={}
          graphs = []
          for res in results
            #puts "#{res}"
            if res == false
              valid = false
            else
              cmds = cmds | res[0]
              calledDict = calledDict.merge(res[1])
              graphs << res[2]
            end
          end
          #----------

          loopItsParamStrDict={}
          if valid and graphs.length > 0
            if args[:arg_icp]
              #puts cmds
              #sleep()
              if !miami
                valsCache = evalIacaCmds(arch,workDir,graphs[0],cmds,nprocs,loadCache) #note we pass through the first thread graph just to access info about the function name, binary name, ect., valscache can be used by all thread models for this function
              else
                valsCache = evalMIAMICmds(arch,workDir,graphs[0],cmds,nprocs,func,loadCache) #note we pass through the first thread graph just to access info about the function name, binary name, ect., valscache can be used by all thread models for this function
              end
            else
              valsCache = evalNOICPCmds(arch,workDir,graphs[0],cmds,nprocs,loadCache)
            end

            for vals in valsCache
              v = vals[1]
              #puts "vals: #{vals} #{v}"
              if v[0][0] == 0 and v[0][1] == 0 and miami and args[:arg_icp]#false#!noIaca
                # puts "no value for command: #{vals}"
                unanalyzedFuncs[func] = true
              end
            end

            # create the executable models for each thread (in parallel)
            results = Parallel.map(0..(graphs.length-1), in_processes: nprocs) do |g|
              graph = graphs[g]
              if graph != nil
                if graphs.length == 1
                  tid = ""
                  tid = graph.getThreadId
                else
                  tid = graph.getThreadId
                end
                if $verbose
                  puts "creating #{func}_t_#{tid} model"
                end
                loopItsParamStr,unrollArgsStr = modGen.createFunctionModel(graph,cmds,valsCache,miami,tid)#,calledDict)
                [tid,loopItsParamStr] # return the thread id and associated loop iteration data (loop iterations are likely to be different for each thread)
              end
            end
            for tid,loopItsParamStr in results
              loopItsParamStrDict[tid]=loopItsParamStr
            end
            #--------
          else
            valid = false
            nameMap.delete(func)
          end

          if valid and graphs.length > 0

            #if graphs.length > 1 # if threads were used in application, need to create a meta model containing the thread models
              instances = 0
              instancesArray = {}
              for g in graphs
                instancesArray[g.getThreadId] = g.getInstances
                if g.getInstances > instances and g.getThreadId.to_i != nThreads.to_i
                  instances = g.getInstances
                end
              end
              modGen.createThreadedModel(func,loopItsParamStrDict,instancesArray,args[:arg_ghz],miami)
              if $verbose
                puts "updating #{executable} model threaded"
                puts "instances count: #{instancesArray}"
              end
              modGen.updateModelStrs(func,instances,loopItsParamStrDict[graphs[0].getThreadId],calledDict,uncapturedFuncs,miami)
            #else
            #  puts "updating #{executable} model"
            #  modGen.updateModelStrs(func,graphs[0].getInstances,loopItsParamStrDict[""],calledDict)
            #end
          end
        else
          puts "binary not found for #{func}"
          nameMap.delete(func)
        end

      end
    end

    modGen.analyzeDependencies()
    modGen.writeOutModel("#{workDir}/#{executable}.model",miami)
    #puts "uncaptured funcs: "

    uninstrumentable = cfgprofParser.getUninstrumentable()
    fstr = ""
    ustr = ""
    for f in uncapturedFuncs.keys
      if !uninstrumentable.include? f
        fstr += "-f #{f} "
      else
        ustr += "#{f} "
      end
    end

    #puts "#{fstr}"
    puts "uninstrumentable functions: "
    puts "#{ustr}"
    puts "unanalyzed functions: "
    puts "#{unanalyzedFuncs.keys}"
    #puts "finished"
  end
end


def readCFGProfile(cfgprofFnm)
  stem = "cfgprof-"
  if cfgprofFnm.include? stem
    temp = cfgprofFnm[cfgprofFnm.index(stem) + (stem.length())..-1]
    cmdPid = temp[0..temp.index("/")-1]
  end
  output = []
  ofile = File.open(cfgprofFnm,"r")
  for l in ofile
    output << l
  end
  ofile.close
  return output,cmdPid
end


#****************************************************************************

def evalIacaCmds(arch,workDir,graph,cmds,nprocs,loadCache=false)
  tempCache = {}
  loadedCache = {}
  if loadCache and File.exist? "#{workDir}/#{graph.getNice_func}/#{graph.getNice_func}_miami_cache.yaml"
    loadedCache = YAML.load_file("#{workDir}/#{graph.getNice_func}/#{graph.getNice_func}_miami_cache.yaml")
  end
  nCmds = []
  origSize = cmds.size
  if $verbose
    #puts "#{tempCache.size} #{loadedCache.size} #{origSize}"
    puts "#{workDir}/#{graph.getNice_func}_miami_cache.yaml"
    puts "#{loadedCache.keys}"
  end
  for cmd in cmds
    if $verbose
      puts "#{cmd[1]} #{loadedCache.keys.include? cmd[1]}"
    end
    if !loadedCache.keys.include? cmd[1]
      nCmds << cmd
    elsif loadedCache[cmd[1]][0][0] ==0 and loadedCache[cmd[1]][0][1]==0
      nCmds << cmd
    end
  end
  cmds = nCmds
  #puts "#{nCmds.size}"
  if $verbose
    puts "nproc "+nprocs.to_s+" num cmds: "+cmds.size.to_s
  end
  if cmds.size > 0
    results = Parallel.map(0..(cmds.size-1),in_processes: nprocs) do |i|
      cmd = cmds[i][0]
      blks = cmds[i][1]
      innerLoop = cmds[i][2]
      vals = performIaca(arch,workDir,graph,cmd,innerLoop)
      [i,blks,vals]
    end
    for res in results
      if $verbose
        puts "#{res}"
      end
      tempCache[res[1]]=res[2]
    end

    if File.exist? "#{workDir}/#{graph.getNice_func}/#{graph.getNice_func}_miami_cache.yaml"
      for key in loadedCache.keys
        if $verbose
          puts "#{key} #{loadedCache[key][0][0]} #{loadedCache[key][0][1]}"
        end
        if !(loadedCache[key][0][0] == 0 and loadedCache[key][0][1] ==0)
          tempCache[key] = loadedCache[key]
        end
      end
    end
    if $verbose
      puts "#{tempCache}"
    end

    FileUtils.rm(Dir.glob(workDir+"/"+graph.getBinary+"_*"))
    file = File.open("#{workDir}/#{graph.getNice_func}/#{graph.getNice_func}_miami_cache.yaml","w")
    file.puts(YAML::dump(tempCache))
    file.close()
  else
    for cmd in cmds
      tempCache[cmd[1]]=[[0,0,0,0]]
    end
    if loadedCache.size > 0
      tempCache = loadedCache
    end
  end
  return tempCache
end


def performIaca(arch,workDir,graph,cmd,innerLoop)
  verbose = true
  vals = []
  lat = 0
  mem = 0
  lat2 = 0
  mem2 = 0
  tempBin ="#{workDir}/#{graph.getBinary}_#{Thread.current.object_id}"
  res = FileUtils.cp("#{workDir}/#{graph.getBinary}",tempBin)
  tempCmd = cmd.sub(graph.getBinary,tempBin)
  output,pid = runCmd(tempCmd,verbose,600)
  insts = analyzeInsertOutput(output)
  puts "#{insts}"

  iacaPath = "#{ENV["PALM_TASK_ROOT"]}/../../../palm-externals/trunk/hpctoolkit-externals/x86_64-linux/iaca-lin64/bin/iaca.sh"

  matches = tempCmd.match(/palm-mkpath-iaca -p (.*) --unroll (.*)/)
  if matches
    maxMark = eval(matches.captures[1])
  else
    maxMark = 1
  end
  if insts.size > 0
    for i in insts
      for mark in 1..maxMark
        lat = 0
        mem = 0
        lat2 = 0
        mem2 = 0
        cmd = "#{iacaPath} -arch #{arch} -64 -analysis LATENCY -mark #{mark} -graph #{workDir}/iaca_#{Thread.current.object_id} #{tempBin}.out"
        #cmd = "iaca -arch HSW -64 -analysis LATENCY -mark #{mark} -graph #{workDir}/iaca_#{Thread.current.object_id} #{tempBin}.out"

        output,pid = runCmd(cmd,verbose)
        if (!output.include? "Initialization failed" or !output.include? "COULD NOT FIND START_MARKER NUMBER") and File.exist?("#{workDir}/iaca_#{Thread.current.object_id}#{mark}.dot")
          puts "here"
          lat = analyzeIacaOutput(output,"Latency")
          mem = analyzeIacaDot("#{workDir}/iaca_#{Thread.current.object_id}#{mark}.dot",false)
          FileUtils.rm("#{workDir}/iaca_#{Thread.current.object_id}#{mark}.dot")
        else
          puts "dont want to be here"
          lat=0
          mem=0
        end
        puts "#{lat} #{mem}"

        # cmd = "iaca -arch IVB -64 -analysis THROUGHPUT -mark #{mark} -graph #{workDir}/#{graph.getNice_func}_iaca_#{Thread.current.object_id} #{tempBin}.out"
        # output,pid = runCmd(cmd,false)
        # if !output.include? "Initialization failed" and File.exist?("#{workDir}/#{graph.getNice_func}_iaca_#{Thread.current.object_id}#{mark}.dot")
        #   lat2 = analyzeIacaOutput(output,"Throughput")
        #   mem2 = analyzeIacaDot("#{workDir}/#{graph.getNice_func}_iaca_#{Thread.current.object_id}#{mark}.dot",true)
        #   FileUtils.rm("#{workDir}/#{graph.getNice_func}_iaca_#{Thread.current.object_id}#{mark}.dot")
        # else
          lat2=0
          mem2=0
        #end
        if innerLoop
          vals << [lat2,mem2,lat,mem]
        else
          vals << [lat,mem,lat2,mem2]
        end
      end
    end
    return vals
  else
    return [[0,0,0,0]]
  end
  
end


def analyzeInsertOutput(output)
  inserts = []
  for o in output
    matches = o.match(/palm-inst: inserting: .palm_(.*)/)
    if matches
      inserts << matches.captures[0].split(":")
    end
  end
  return inserts
end



def analyzeIacaOutput(output,type="Latency")
  latency = 0
  for o in output
    if type == "Latency"
      matches = o.match(/Latency: (.*) Cycles/)
    elsif type == "Throughput"
      matches = o.match(/Throughput: (.*) Cycles/)
    end
    if matches
      latency = matches.captures[0]
    end
  end
  return latency
end


def analyzeIacaDot(fname,throughput=false)
  g = GraphViz.parse(fname)
  criticalPath = []
  parents = {}
  zero = nil
  g.each_node do |ni|
    ni = g.get_node(ni)
    if ni["label"].to_s.start_with? "\"0. "
      zero = ni
    end
      if !parents.has_key? ni
        parents[ni] = []
      end
      for n in ni.neighbors
        if !parents.has_key? n
          parents[n] = []
        end
        if !parents[n].include? ni
          parents[n] << ni
        end
      end
    if ni["color"].to_s == "\"pink\""
      criticalPath.push(ni)
    end
  end

  criticalPaths = {}
  if zero != nil
    parents[zero]=[]
  end
  for n in parents.keys
    if parents[n].size == 0 and !criticalPaths.has_key? n
      if n["color"].to_s == "\"pink\""
        stack = [[n,[n],{},""]]
      else
        stack = [[n,[],{},""]]
      end
      while stack.size() > 0
        cur = stack.pop
        if !cur[2].has_key? cur[0]["label"].to_s
          cur[2][cur[0]["label"].to_s]=true
          if cur[0].neighbors.size > 0
            for c in cur[0].neighbors
              if c["color"].to_s == "\"pink\""
                stack.push([c,cur[1].clone.push(c),cur[2].clone,cur[3]+"\t"])
              else
                stack.push([c,cur[1],cur[2].clone.clone,cur[3]+"\t"])
              end
            end
          else
            if cur[1].size > 0
              if !criticalPaths.has_key? n
                criticalPaths[n] = []
              end
              if !criticalPaths[n].include? cur[1]
                criticalPaths[n] << cur[1]
              end
            end
          end
        end
      end
    end
  end

  maxInstCnts = {
          /[a-z]*mov[a-z]*/ => [0,[]],
          /.*ptr.*/ => [0,[]],
          /[a-z]*add[a-z]*/ => [0,[]],
          /[a-z]*mul[a-z]*/ => [0,[]],
          /[a-z]*/ => [0,[]]
  }
  for r in criticalPaths.keys
    for cp in criticalPaths[r]
      instCnts = {
          /[a-z]*mov[a-z]*/ => [0,[]],
          /.*ptr.*/ => [0,[]],
          /[a-z]*add[a-z]*/ => [0,[]],
          /[a-z]*mul[a-z]*/ => [0,[]],
          /[a-z]*/ => [0,[]]
      }
      instCntsKeys = [/[a-z]*mov[a-z]*/,/.*ptr.*/]
      for n in cp
        temp = n["label"].to_s.gsub(/"/,"").sub(/[0-9]*\. /,"")
        inst = temp[/.*/]
        for k in instCntsKeys
          if inst =~ k
            if k == /[a-z]*mov[a-z]*/
              temp = inst.split(",")
              if temp[-1].include? "ptr"
                instCnts[k][0]+=1
                instCnts[k][1].push(inst)
              end
            else
              instCnts[k][0]+=1
              instCnts[k][1].push(inst)
            end
            if k == /[a-z]*mov[a-z]*/ or k == /.*ptr.*/
              break
            end

          end
        end
      end
      for k in maxInstCnts.keys
        if instCnts[k][0]>maxInstCnts[k][0]
          maxInstCnts[k]=instCnts[k]
        end
      end
      #puts
    end
    #puts
  end
  instCnts = {
          /[a-z]*mov[a-z]*/ => [0,[]],
          /.*ptr.*/ => [0,[]],
          /[a-z]*add[a-z]*/ => [0,[]],
          /[a-z]*mul[a-z]*/ => [0,[]],
          /[a-z]*/ => [0,[]]
  }
  instCntsKeys = [/[a-z]*mov[a-z]*/,/.*ptr.*/]
  criticalPath.each do |n|
    temp = n["label"].to_s.gsub(/"/,"").sub(/[0-9]*\. /,"")
    inst = temp[/.*/]
    for k in instCntsKeys
      if inst =~ k
        if k == /[a-z]*mov[a-z]*/
          temp = inst.split(",")
          if temp[-1].include? "ptr"
            instCnts[k][0]+=1
            instCnts[k][1].push(inst)
          end
        else
          instCnts[k][0]+=1
          instCnts[k][1].push(inst)
        end
        if k == /[a-z]*mov[a-z]*/ or k == /.*ptr.*/
          break
        end
      end
    end
    #puts
  end
  #puts "old #{instCnts[/[a-z]*mov[a-z]*/][0]} + #{instCnts[/.*ptr.*/][0]}"
  #puts "new #{maxInstCnts[/[a-z]*mov[a-z]*/][0]} + #{maxInstCnts[/.*ptr.*/][0]}"
  #if not throughput and (instCnts[/[a-z]*mov[a-z]*/][0] + instCnts[/.*ptr.*/][0]) != (maxInstCnts[/[a-z]*mov[a-z]*/][0] + maxInstCnts[/.*ptr.*/][0])
    #exit
  #end
  if throughput
    return instCnts[/[a-z]*mov[a-z]*/][0] + instCnts[/.*ptr.*/][0]
  else
    return maxInstCnts[/[a-z]*mov[a-z]*/][0] + maxInstCnts[/.*ptr.*/][0]
  end
end


#****************************************************************************

def evalMIAMICmds(arch,workDir,graph,cmds,nprocs,func, loadCache=false)
  tempCache = {}
  loadedCache = {}
  if loadCache and File.exist? "#{workDir}/#{graph.getNice_func}/#{graph.getNice_func}_miami_cache.yaml"
    loadedCache = YAML.load_file("#{workDir}/#{graph.getNice_func}/#{graph.getNice_func}_miami_cache.yaml")
  end
  nCmds = []
  origSize = cmds.size
  if $verbose
    #puts "#{tempCache.size} #{loadedCache.size} #{origSize}"
    puts "#{workDir}/#{graph.getNice_func}_miami_cache.yaml"
    puts "#{loadedCache.keys}"
  end
  for cmd in cmds
    if $verbose
      puts "#{cmd[1]} #{loadedCache.keys.include? cmd[1]}"
    end
    if !loadedCache.keys.include? cmd[1]
      nCmds << cmd
    elsif loadedCache[cmd[1]][0][0] ==0 and loadedCache[cmd[1]][0][1]==0
      nCmds << cmd
    end
  end
  cmds = nCmds
  #puts "#{nCmds.size}"
  if $verbose
    puts "nproc "+nprocs.to_s+" num cmds: "+cmds.size.to_s
  end
  if cmds.size > 0
    results = Parallel.map(0..(cmds.size-1),in_processes: nprocs) do |i|
      cmd = cmds[i][0]
      blks = cmds[i][1]
      innerLoop = cmds[i][2]
      vals = performMiami(arch,workDir,graph,cmd,blks,innerLoop,func)
      [i,blks,vals]
    end
    for res in results
      if $verbose
        puts "#{res}"
      end
      tempCache[res[1]]=res[2]
    end

    if File.exist? "#{workDir}/#{graph.getNice_func}/#{graph.getNice_func}_miami_cache.yaml"
      for key in loadedCache.keys
        if $verbose
          puts "#{key} #{loadedCache[key][0][0]} #{loadedCache[key][0][1]}"
        end
        if !(loadedCache[key][0][0] == 0 and loadedCache[key][0][1] ==0)
          tempCache[key] = loadedCache[key]
        end
      end
    end
    if $verbose
      puts "#{tempCache}"
    end

    FileUtils.rm(Dir.glob(workDir+"/"+graph.getBinary+"_*"))
    file = File.open("#{workDir}/#{graph.getNice_func}/#{graph.getNice_func}_miami_cache.yaml","w")
    file.puts(YAML::dump(tempCache))
    file.close()
  else
    for cmd in cmds
      tempCache[cmd[1]]=[[0,0,0,0]]
    end
    if loadedCache.size > 0
      tempCache = loadedCache
    end
  end
  return tempCache
end


def performMiami(arch,workDir,graph,cmd,blks,innerLoop,func)
  blkPathStr = ""
  blkPathName = ""
  for blk in blks
    blkPathStr+="#{blk}->"
    blkPathName+="#{blk}-"
  end
  iterations = 1
  if innerLoop
    iterations = 10
  end
  vals = []
  lat = 0
  mem = 0
  lat2 = 0
  mem2 = 0
  verbose = $verbose
  #puts "pm #{verbose}"
  binary_nm = graph.getBinary()
  tempBin ="#{workDir}/#{binary_nm}" # FIXME: copy only necessary for IACA?
  for i in (1..iterations)
    tempCmd = cmd.sub(graph.getBinary,tempBin)
    tmpBlkPathStr = blkPathStr
    tmpBlkPathName = blkPathName
    for j in (1...i)
      tmpBlkPathStr += blkPathStr
      tmpBlkPathName += blkPathName
    end
    tempCmd += "#{blkPathStr}\""
    #tempCmd += " 2>&1 | grep numuops && rm *Intel_Sandy_Bridge_EP-*.xml"
    tempCmd += " &> #{binary_nm}_#{func}_#{tmpBlkPathName}.miami | grep numuops && rm *Intel_Sandy_Bridge_EP-*.xml"

    puts "*** Model: #{tempCmd}"
    output,pid = runCmd(tempCmd,verbose,600)
    if (output.length > 0)
      lat,mem = analyzeMiamiOutput(output)
      cnt = 0
      while lat == 0 and cnt < 5
        output,pid = runCmd(tempCmd,verbose,600)
        lat,mem = analyzeMiamiOutput(output)
        cnt+=1
      end
      if cnt==5
        exit
      end
      #mem = 0
    else
      if cmd.include? "testing"
        sleep(5)
      end
      lat=0
      mem=0
    end
    lat2=0
    mem2=0
    vals << [lat,mem,lat2,mem2]
  end
  return vals
end


def analyzeMiamiOutput(output)
  if $verbose
    puts "#{output}"
  end
  latency = 0
  memLat = 0
  for o in output
      matches = o.match(/lat: (.*) numuops: (.*) memLat: (.*)/)
    if matches
      latency = matches.captures[0]
      memLat = matches.captures[2]
    end
  end
  return latency,memLat
end


#****************************************************************************

def evalNOICPCmds(arch,workDir,graph,cmds,nprocs,loadCache=false)
  tempCache = {}
  loadedCache = {}
  if loadCache and File.exist? "#{workDir}/#{graph.getNice_func}/#{graph.getNice_func}_miami_cache.yaml"
    loadedCache = YAML.load_file("#{workDir}/#{graph.getNice_func}/#{graph.getNice_func}_miami_cache.yaml")
  end
  nCmds = []
  origSize = cmds.size
  if $verbose
    #puts "#{tempCache.size} #{loadedCache.size} #{origSize}"
    puts "#{workDir}/#{graph.getNice_func}_miami_cache.yaml"
    puts "#{loadedCache.keys}"
  end
  for cmd in cmds
    if $verbose
      puts "#{cmd[1]} #{loadedCache.keys.include? cmd[1]}"
    end
    if !loadedCache.keys.include? cmd[1]
      nCmds << cmd
    elsif loadedCache[cmd[1]][0][0] ==0 and loadedCache[cmd[1]][0][1]==0
      nCmds << cmd
    end
  end
  cmds = nCmds
  #puts "#{nCmds.size}"
  if $verbose
    puts "nproc "+nprocs.to_s+" num cmds: "+cmds.size.to_s
  end
  if cmds.size > 0
    results = Parallel.map(0..(cmds.size-1),in_processes: nprocs) do |i|
      cmd = cmds[i][0]
      blks = cmds[i][1]
      innerLoop = cmds[i][2]
      vals = [[0,0,0,0]]
      [i,blks,vals]
    end
    for res in results
      if $verbose
        puts "#{res}"
      end
      tempCache[res[1]]=res[2]
    end

    if File.exist? "#{workDir}/#{graph.getNice_func}/#{graph.getNice_func}_noicp_cache.yaml"
      for key in loadedCache.keys
        if $verbose
          puts "#{key} #{loadedCache[key][0][0]} #{loadedCache[key][0][1]}"
        end
        if !(loadedCache[key][0][0] == 0 and loadedCache[key][0][1] ==0)
          tempCache[key] = loadedCache[key]
        end
      end
    end
    if $verbose
      puts "#{tempCache}"
    end

    FileUtils.rm(Dir.glob(workDir+"/"+graph.getBinary+"_*"))
    file = File.open("#{workDir}/#{graph.getNice_func}/#{graph.getNice_func}_noicp_cache.yaml","w")
    file.puts(YAML::dump(tempCache))
    file.close()
  else
    for cmd in cmds
      tempCache[cmd[1]]=[[0,0,0,0]]
    end
    if loadedCache.size > 0
      tempCache = loadedCache
    end
  end
  return tempCache
end

#****************************************************************************

