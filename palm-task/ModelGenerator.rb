require 'tempfile'
#require "thread/pool"
require "parallel"

require_relative 'CFGProf-Parse'

class ModelGenerator
  def initialize(verbose=false)
    @executable = ""
    @executablePath = ""
    #@output = nil
    @iaca = true
    @nprocs = `nproc`.to_i
    @allDotFiles = false
    @pathThreshold = 0.99
    @inclusive = false
    @ghz = 1.0

    @workDir = "./"

    @finishedFuncs = {}
    @unfinishedFuncs = {}

    @oa = nil #output analyzer
    @latencies = nil
    @lat_path = ""
    @fp_path = ""
    @nameMap = nil



    @modelStrs=""
    @ghzStr=""
    createGhzStr(@ghz)
    @itsStr = ""
    @funcStr = ""
    @verbose = verbose
  end

  def createGhzStr(ghz)
    @ghzStr =  "ghz = #{ghz}\n"
    @ghzStr +=  "inclusive = false\n"
    @ghzStr +=  "average = false\n"
    @ghzStr +=  "useMax = false\n"
    @ghzStr += "i=0\n"
    @ghzStr += "until i == ARGV.length\n"
    @ghzStr += "\tif ARGV[i]==\"--ghz\"\n"
    @ghzStr += "\t\tghz = ARGV[i+1].to_f*10**9\n"
    @ghzStr += "\tend\n"
    @ghzStr += "\tif ARGV[i]==\"--inclusive\"\n"
    @ghzStr += "\t\tinclusive = true\n"
    @ghzStr += "\tend\n"
    @ghzStr += "\tif ARGV[i]==\"--average\"\n"
    @ghzStr += "\t\taverage = true\n"
    @ghzStr += "\tend\n"
    @ghzStr += "\tif ARGV[i]==\"--max\"\n"
    @ghzStr += "\t\tuseMax = true\n"
    @ghzStr += "\tend\n"
    @ghzStr += "\ti+=1\n"
    @ghzStr += "end\n"
    @ghzStr += "funcs={}\n"
  end

  def setExecutable(exec)
    @executable = exec
  end

  def setExecutablePath(execPath)
    @executablePath = execPath
  end

  def setOutputAnalyzer(oa)
    #@output = output
    @oa=oa
  end

  def setIaca(iaca)
    @iaca = iaca
  end

  def setNprocs(nprocs)
    @nprocs = nprocs
  end

  def setAllDotFiles(allDotFiles)
    @allDotFiles = allDotFiles
  end

  def setPathThreshold(pathThreshold)
    @pathThreshold = pathThreshold
  end

  def setInclusive(inclusive)
    @inclusive = inclusive
  end

  def setGhz(ghz)
    @ghz = ghz
    createGhzStr(@ghz)
  end

  def setWorkDir(workDir)
    @workDir = workDir
  end

  def setLatencies(latencies)
    @latencies = latencies
  end
   
  def setFPPath(fp_path)
    @fp_path = fp_path
  end

  def setLatPath(lat_path)
    @lat_path = lat_path
  end

  def makeNameNice(name)
    return @oa.makeNameNice(name)
  end

  def getNameMap
    if @nameMap == nil
      @nameMap = @oa.getNameMap
    end
    return @nameMap
  end

  def generateDotNodes(func,blkCnts,labels,sum,graph=nil)
    nodes = "digraph #{func}{\n"
    if @verbose
      puts "creating dot nodes "+blkCnts.length.to_s
    end
    if blkCnts
      for b in blkCnts.reverse
        for l in labels
          if l.index("\""+b[0]+"\"")
            bs = "\""+b[0]
            lab = "\\n# calls:"+b[1].round(3).to_s
            if b.length == 3
              perc = 100.*b[2]
              lab+="\\n"+"#{perc.round(3)}"+"\%"
            end
            l.insert(l.index(bs)+bs.length,lab)
            l.insert(l.index("\"")+1,"blk: ")
            if sum!=0
              greyVal = Integer(100-(b[1]/Float(sum))*100)
            else
              greyVal = 100
            end
            loopStr = "\\nloops: "
            if graph!=nil
              bname = b[0][2..-1].gsub(/_+/,".")
              for lp in graph.getNode(bname).getSubGraphs
                loopStr += lp.getName+", "
              end
            end
            if loopStr != "\\nloops: "
              l.insert(l.index("\"]"),loopStr)
            end
            if greyVal < 50
              l.insert(l.index("]"),",style=filled,fillcolor=\"grey"+greyVal.to_s+"\",fontcolor=\"white\"")
            else
              l.insert(l.index("]"),",style=filled,fillcolor=\"grey"+greyVal.to_s+"\"")
            end
            nodes += l+"\n"

          end
        end
      end
    end
    return nodes
  end

  def generateDotLinks(edgeCnts,links,g=nil)
    linkStr = ""
    if @verbose
      puts "creating dot links "+edgeCnts.length.to_s
    end
    weightDiv = 1.0
    if g != nil
      weightDiv = Float(g.maxVal)
    end
    if edgeCnts.length > 0
      for e in edgeCnts.reverse
        for l in links
          if l == e[0]
            bs = e[0]
            lab = " label=\""+e[1].round(3).to_s
            if g != nil
              weight = (e[1]/weightDiv)*100
            else
              weight = 0
            end
            l.insert(l.index(bs)+bs.length," ["+lab+"\",weight=\""+weight.to_s+"\"]")
            linkStr += l+"\n"
          end
        end
      end
    else
      for l in links
        linkStr += l+"\n"
      end
    end
    linkStr += "}\n"
  end

  def writeDotFile(dirName,fname,nodes,links)
    if @verbose
      puts "writing #{dirName}/#{fname}.dot"
    end 
    FileUtils.mkdir_p(dirName)
    wd = Dir.pwd
    Dir.chdir(dirName)
    path = "#{fname}.dot"
    file = File.open(path,"w")
    file.puts(nodes)
    file.puts(links)
    file.close()
    Dir.chdir(wd)
  end

  def addLineNos(dirName,fname,lineNos)
    wd = Dir.pwd
    Dir.chdir(dirName)
    file = File.open(fname+".dot","r")
    tfile = Tempfile.new('temp')
    for line in file
      matches = line.match(/(.*) \[label=\"blk:/)
      if matches
        blk = matches.captures[0]
        if lineNos[blk] != nil
          line.insert(line.index("\",style"),"\\n #{lineNos[blk]}")
        end
      end
      tfile.puts(line)
    end
    file.close()
    tfile.close()
    FileUtils.mv(tfile.path,fname+".dot")
    dotCmd = "dot -Tsvg "+fname+".dot -o "+fname+".svg"
    runCmd(dotCmd,@verbose)
    Dir.chdir(wd)
  end

  def addCalledFuncs(dirName,fname,calledFuncs)
    addLineNos(dirName,fname,calledFuncs)
  end

  def createDotFile(demang_func,name,labels,links,blkCnts,edgeCnts,sum,lineNos,calledFuncs,g=nil)
    dotLabels = labels.map do |e| e.dup end
    dotLinks = links.map do |e| e.dup end
    nodeStr = generateDotNodes(demang_func,blkCnts,dotLabels,sum,g)
    linkStr = generateDotLinks(edgeCnts,dotLinks,g)
    writeDotFile("#{@workDir}/#{demang_func}","#{demang_func}_#{name}",nodeStr,linkStr)
    addLineNos("#{@workDir}/#{demang_func}","#{demang_func}_#{name}",lineNos)
    addCalledFuncs("#{@workDir}/#{demang_func}","#{demang_func}_#{name}",calledFuncs)
  end

  def removeUncalledBlks(labels,links,dels,edgeCnts)
    labelsCalled = labels.map do |e| e.dup end
    linksCalled = links.map do |e| e.dup end
    delEdges=[]

    for d in dels
      labelsCalled = labelsCalled.reject{|o| o.match(/\"#{d}\"/)}
      linksCalled = linksCalled.reject{|o| o.match(/^#{d} -/)}
      linksCalled = linksCalled.reject{|o| o.match(/> #{d}$/)}
    end
    for e in edgeCnts
      if e[1]==0
        delEdges << e
        linksCalled = linksCalled.reject{|o| o.match(/^#{e[0]}$/)}
      end
    end
    return labelsCalled,linksCalled,delEdges
  end

  def copyBinaryToWorkDir(binary,binaryPath)
    if @verbose
      puts "#{binary} #{binaryPath}"
    end
    binSearchPath = ["./"] # probably get rid of this later
    if !File.exist?(File.join(@workDir,binary)) # test if binary is already in local dir
      if File.exist?(binaryPath) # search for binary from bin path provided by palm-instrument-cfg
        FileUtils.cp(binaryPath,@workDir)
        return true #binary was found
      else #search user provided bin path (possibly remove)
        for p in binSearchPath
          bp = File.join(p,binary)
          if File.exist?(bp)
            if @verbose
              puts "copying #{binary} from #{bp}"
            end
            FileUtils.cp(bp,@workDir)
            return true #binary was found
          end
        end
      end
    else
      return true #binary was found
    end
    return false #binary was not found
  end

  def createGraphThread(func,demang_func,binary,tid,avg=false,unroll=false)
    if @verbose
      puts "here in createGraphThread"
    end
    if avg
      blkCnts,dot,sum,dels,blks,lineNos,calledFuncs = @oa.analyzeBlkOutputAvg(func)
    else
      blkCnts,dot,sum,dels,blks,lineNos,calledFuncs = @oa.analyzeBlkOutput(func,tid)
    end
    g = nil
    if blks.length > 0
      if avg
        edgeCnts = @oa.analyzeLinkOutputAvg(func)
      else
        edgeCnts = @oa.analyzeLinkOutput(func,tid)
      end

      blkLabels = dot.select{|o| o.match(/[0-9]* \[label/)}
      links = dot.reject{|o| o.match(/[0-9]* \[label/)}

      if @allDotFiles
        createDotFile(demang_func,"all_#{tid}",blkLabels,links,blkCnts,edgeCnts,sum,lineNos,calledFuncs)
      end

      calledBlkLabels,calledLinks,delEdges = removeUncalledBlks(blkLabels,links,dels,edgeCnts)

      g = Graph.new(func,demang_func,"base",binary,@verbose) # check why we pass both func and demang_func
      g.setThreadId(tid)
      if @verbose
        puts "generateNodes"
      end
      g.generateNodes(blkCnts,calledBlkLabels)
      if @verbose
        puts "generateEdges"
      end
      newEdges = g.generateEdges(edgeCnts,calledLinks,delEdges)
      for e in newEdges
        calledLinks << e[0]
        for e2 in edgeCnts
          if e2[0] == e[0]
            e2[1] = e[1]
            break
          end
        end
      end

      tloops,tloopEntries = @oa.analyzeLoopOutput(func) #this is deprecated now?
      #puts " dyn-loops: #{tloops}"
      #puts " dyn-loops: #{tloopEntries}"

      valid = g.analyzeLoops(@pathThreshold,unroll)

      calledBlkLabels,calledLinks,blkCnts,edgeCnts = g.getDotData
      #puts "#{calledBlkLabels}"
      #puts "#{calledLinks}"
      #puts "#{blkCnts}"
      #puts "#{edgeCnts}"
      createDotFile(demang_func,"called_#{tid}",calledBlkLabels,calledLinks,blkCnts,edgeCnts,sum,lineNos,calledFuncs,g)
      if (!valid)
        puts "loops are invalid"
        exit
        g=nil #?
      end
    else
      if tid == 0
        puts "no blks found for #{func}"
      end
    end
    serialized = ""
    if g != nil
      serialized=Marshal.dump(g)
    end
    return serialized
  end

  def createGraph(func,nprocs,avg,unroll=false)
    demang_func = @nameMap[func]
    binaryPath = @oa.analyzePathOutput(func) #binary the function is located in (can be an executable, library, ect)
    binary = Pathname.new(binaryPath).basename
    #puts "binaryPath: #{binaryPath}"
    graphs = []

    # TODO is parallelization possible (useful) here?
    if @oa.getNumThreads.to_i > 1 and false  
      graphsTemp = Parallel.map(0..(@oa.getNumThreads.to_i-1), in_processes: nprocs) do |tid| #{ |tid| createGraphThread(func,demang_func,binary,tid) }
        createGraphThread(func,demang_func,binary,tid,false,unroll)
      end
    else
      if @verbose
        puts "single thread"
      end
      graphsTemp = [createGraphThread(func,demang_func,binary,0,false,unroll)] #even if avg is 'true', we still want to calculate individual thread graphs
    end
    valid = false
    for g in graphsTemp
      g
      if g!=""
        valid = true
        graphs << Marshal.load(g)
      end
    end

    if valid
      if avg
        g = createGraphThread(func,demang_func,binary,@oa.getNumThreads.to_i,true,unroll)
        if g != ""
          graphs << Marshal.load(g)
        end
      end
    else
      puts "invalid! deleteing func!"
      @nameMap.delete(func)
    end
    #puts "#{graphs}"
    return graphs
  end


  def createFunctionModel(graph,cmds,valsCache,miami,tid="")#,calledDict)
    if @verbose
      puts "Create Function Model"
    end
    if tid != ""
      demang_func = @nameMap["#{graph.getFunc}"]+"_#{tid}"
    else
      demang_func = @nameMap["#{graph.getFunc}"]
    end
    loopItsParamStr=""
    for loopIts in graph.getLoopItsVars
      loopItsParamStr+=", #{loopIts[0]}: #{loopIts[1]}"
    end
    if @verbose
      puts "#{demang_func}"
    end
    if @executable != ""

      mfile = File.open("#{@workDir}/#{@nameMap["#{graph.getFunc}"]}/#{demang_func}.rb","w")
      mfile.puts("#!/usr/bin/env ruby")
      mfile.puts("class #{demang_func.capitalize}")
      if miami
        defStr = "\tdef evalModel(funcs#{loopItsParamStr}"
      else
        defStr = "\tdef evalModel(cycPerMemOp,funcs#{loopItsParamStr}"
      end
      unrollArgsStr = ""
      bodyStr = ""

      #puts "#{graph.getBlockPaths.keys}"
      cmds = cmds.select {|k,v| graph.getBlockPaths.keys.include? v} #[graph.getBlockPaths.keys]
      for cmd in cmds.sort_by {|k,v| graph.getBlockPaths[v]}
        #puts "#{graph.getBlockPaths}"
        #puts "#{cmd}"
        unrolledCnt = 1
        unrolledDictStr = "#{graph.getBlockPaths[cmd[1]]}_vals = {"
        blkStr = ""

        for v in valsCache[cmd[1]]
          varName = graph.getBlockPaths[cmd[1]]
          blkPath = "#{cmd[1][0]}"
          for b in 1..cmd[1].size
            blkPath += "->#{cmd[1][b]}"
          end
          if unrolledCnt == 1
            if miami
              blkStr = "\t\t#{varName} = (#{v[0]})#{graph.getVarProbs[varName]}/#{unrolledCnt.to_f} \#cycles due to memop => #{v[1]}, blks: #{blkPath}"
            else
              blkStr = "\t\t#{varName} = (#{v[0]}+#{v[1]}*cycPerMemOp)#{graph.getVarProbs[varName]}/#{unrolledCnt.to_f} \##{varName} = (#{v[2]}+#{v[3]}*cycPerMemOp)#{graph.getVarProbs[varName]} \#blks: #{blkPath} \#unrolled=#{unrolledCnt}"
            end
          else
            if miami
              blkStr = "\t\t#{varName} = (#{varName}_vals[#{varName}_unroll]/#{varName}_unroll) \#cycles due to memop => #{v[1]}, blks: #{blkPath}"
            else
              blkStr = "\t\t#{varName} = (#{varName}_vals[#{varName}_unroll]/#{varName}_unroll+#{v[1]}*cycPerMemOp) \#blks: #{blkPath}"
            end
            
          end

          unrolledDictStr += " #{unrolledCnt} => #{v[0]},"
          unrolledCnt+=1
        end
        if unrolledCnt > 2
          unrollArgsStr += ", #{graph.getBlockPaths[cmd[1]]}_unroll: 1"
          unrolledDictStr = unrolledDictStr.chomp(",")+"} \##{blkPath}"
          bodyStr += "\t\t#{unrolledDictStr}\n"
        end
        bodyStr += "#{blkStr}\n"
      end
      defStr += unrollArgsStr +")"
      mfile.puts(defStr)
      mfile.puts(bodyStr)

      for p in graph.getEvalPaths
        for eval in graph.getEvalPaths[p[0]]
          val = "#{graph.getVarDefs[eval]}"
          # if eval.include? "func_call_"
          #   val.insert(-3,"_#{graph.getThreadId}")
          # end
          #puts "\t\t#{eval}=#{val}#{graph.getVarProbs[eval]}"
          mfile.puts("\t\t#{eval}=#{val}#{graph.getVarProbs[eval]}")
        end
      end

      if graph.getEvalStr != ""
        mfile.puts("\t\treturn #{graph.getEvalStr}")
      else
        mfile.puts("\t\treturn 0")
      end
      mfile.puts("\tend")
      mfile.close
      return loopItsParamStr,unrollArgsStr#,calledDict
    end
    return "",""
  end

  def createMIAMICmd(graph,blockPath,calledFuncs,callCnt,innerLoop)
    blockPaths = graph.getBlockPaths()
    cmds = []
    i = 0
    pathCnt = 0
    pathName = blockPaths[blockPath]
    evalStr = ""
    
    miamiRoot = "#{ENV["PALM_TASK_ROOT"]}/../../../palm-miami/MIAMI-palm/install"

    miamiPath = "#{miamiRoot}/bin/miami"
    miamiMdf = "#{miamiRoot}/share/machines/x86_SandyBridge_EP.mdf"

    cmd = "#{miamiPath} --func #{graph.getFunc} --bin_path #{graph.getBinary} -m #{miamiMdf} --lat_path #{@lat_path} --dla_path #{@fp_path} --blk_path \""

    blkPathStr = ""
    bp = []
    tevalPaths = Hash.new { |hash, key| hash[key] =  Set.new }
    calledFuncsNameMap={}
    while i < blockPath.size()
      b = blockPath[i]
      b = b.gsub(/cp+/,"")
      if calledFuncs[b] != nil # function call encountered in this block, split path
        innerLoop = false

        if @nameMap[calledFuncs[b]]
          calledFn = "#{@nameMap[calledFuncs[b]]}_#{graph.getThreadId}"
        else
          calledFn = "#{makeNameNice(calledFuncs[b])}"
        end
        calledFuncsNameMap[calledFuncs[b]]=calledFn

        bp << b
        if graph.getBlockPaths[bp] == nil or not (/_[0-9]+_[0-9]+$/ =~ graph.getBlockPaths[bp]) #fixme (function is last block of path)
          graph.getBlockPaths[bp]="#{pathName}_#{pathCnt}"
          graph.getVarDefs["#{pathName}_#{pathCnt}"] = bp
          pathCnt+=1
        end
        if evalStr == ""
          evalStr = "(#{graph.getBlockPaths[bp]}"
        else
          evalStr += " + #{graph.getBlockPaths[bp]}"
        end
        cmds << [cmd,bp,false]
        #cmd = "miami -func #{graph.getFunc} -bin_path #{graph.getBinary} -m ./x86_SandyBridge_EP.mdf -lat_path #{@lat_path}  -blk_path \""
        cmd = "miami -func #{graph.getFunc} -bin_path #{graph.getBinary} -m #{ENV["PALM_TASK_RO     OT"]}/x86_SandyBridge_EP.mdf -lat_path #{@lat_path} --dla_path #{@fp_path} -blk_path \""
        bp = []

        if tevalPaths["funcs[\"#{calledFn}\"]"].size == 0               #have not encountered this called function, in the current path yet
          if graph.getEvalPaths["funcs[\"#{calledFn}\"]"].size != 0              #have encountered this called function in a different path
            tevalPaths["funcs[\"#{calledFn}\"]"]=graph.getEvalPaths["funcs[\"#{calledFn}\"]"]
            graph.getEvalPaths.delete("funcs[\"#{calledFn}\"]")
          else                                                          #have not encountered this called function in a different path
            tevalPaths["funcs[\"#{calledFn}\"]"] << "func_call_#{callCnt}"
            if calledFn.downcase.include? "mpi"
              graph.getVarDefs["func_call_#{callCnt}"] = "0\#funcs[\"#{calledFn}\"]"
            else
              graph.getVarDefs["func_call_#{callCnt}"] = "funcs[\"#{calledFn}\"]"
            end
            callCnt+=1
          end
        end
        for eval in tevalPaths["funcs[\"#{calledFn}\"]"]
          if evalStr == ""
            evalStr = "(#{eval}"
          else
            evalStr += " + #{eval}"
          end
        end
      else # no function call encountered add block to current command
        bp << b
      end
      i+=1
    end # end while

    if bp != []
      if pathName.include? "loop" and cmds.size == 0 and innerLoop
        #cmd += " --unroll 10"
      end
      cmds << [cmd,bp,innerLoop]
    end
    if bp != blockPath
      if graph.getBlockPaths[bp] == nil and bp != []
        graph.getBlockPaths[bp]="#{pathName}_#{pathCnt}"
        graph.getVarDefs["#{pathName}_#{pathCnt}"] = bp
        pathCnt+=1
      end
      if bp != []
        if evalStr == ""
          evalStr = "(#{graph.getBlockPaths[bp]}"
        else
          evalStr += " + #{graph.getBlockPaths[bp]}"
        end
      end

      evalStr+=")"
      graph.getVarDefs[pathName]=evalStr
      tevalPaths[evalStr] << pathName
      tevalPaths.merge!(graph.getEvalPaths){|key,o,n| o + n}
      graph.getEvalPaths.replace(tevalPaths)
      if not (/_[0-9]+_[0-9]+$/ =~ graph.getBlockPaths[blockPath])
        graph.getBlockPaths.delete(blockPath)
      end
    end

    #puts "miami cmd: #{cmds}"

    return cmds,callCnt,calledFuncsNameMap
  end

  #------------------------------------------
  # if another function(s) is called in this path, split path into sub-paths before and after the called function
  def createIACACmd(graph,blockPath,calledFuncs,callCnt,innerLoop)
    blockPaths = graph.getBlockPaths()
    cmds = []
    i = 0
    pathCnt = 0
    pathName = blockPaths[blockPath]
    evalStr = ""
    palmIacaPath= "#{ENV["PALM_TASK_ROOT"]}/palm-mkpath-iaca"
    cmd = "#{palmIacaPath} -p \""
    bp = []
    tevalPaths = Hash.new { |hash, key| hash[key] =  Set.new }
    calledFuncsNameMap={}
    while i < blockPath.size()
      b = blockPath[i]
      b = b.gsub(/cp+/,"")
      if calledFuncs[b] != nil # function call encountered in this block, split path

        if @nameMap[calledFuncs[b]]
          calledFn = "#{@nameMap[calledFuncs[b]]}_#{graph.getThreadId}"
        else
          calledFn = "#{makeNameNice(calledFuncs[b])}"
        end
        calledFuncsNameMap[calledFuncs[b]]=calledFn
        if cmd == "#{palmIacaPath} -p \""
          cmd+=b
        else
          cmd+="->"+b
        end
        bp << b
        if graph.getBlockPaths[bp] == nil or not (/_[0-9]+_[0-9]+$/ =~ graph.getBlockPaths[bp]) #fixme (function is last block of path)
          graph.getBlockPaths[bp]="#{pathName}_#{pathCnt}"
          graph.getVarDefs["#{pathName}_#{pathCnt}"] = bp
          pathCnt+=1
        end
        if evalStr == ""
          evalStr = "(#{graph.getBlockPaths[bp]}"
        else
          evalStr += " + #{graph.getBlockPaths[bp]}"
        end
        cmd += "\" -f #{graph.getFunc} -b #{graph.getBinary}"
        cmds << [cmd,bp,false]
        cmd = "#{palmIacaPath} -p \""
        bp = []

        if tevalPaths["funcs[\"#{calledFn}\"]"].size == 0               #have not encountered this called function, in the current path yet
          if graph.getEvalPaths["funcs[\"#{calledFn}\"]"].size != 0              #have encountered this called function in a different path
            tevalPaths["funcs[\"#{calledFn}\"]"]=graph.getEvalPaths["funcs[\"#{calledFn}\"]"]
            graph.getEvalPaths.delete("funcs[\"#{calledFn}\"]")
          else                                                          #have not encountered this called function in a different path
            tevalPaths["funcs[\"#{calledFn}\"]"] << "func_call_#{callCnt}"
            graph.getVarDefs["func_call_#{callCnt}"] = "funcs[\"#{calledFn}\"]"
            callCnt+=1
          end
        end
        for eval in tevalPaths["funcs[\"#{calledFn}\"]"]
          if evalStr == ""
            evalStr = "(#{eval}"
          else
            evalStr += " + #{eval}"
          end
        end
      else # no function call encountered add block to current command
        if cmd == "#{palmIacaPath} -p \""
          cmd+=b
        else
          cmd+="->"+b
        end
        bp << b
      end
      i+=1
    end # end while

    if cmd != "#{palmIacaPath} -p \""
      cmd += "\" -f #{graph.getFunc} -b #{graph.getBinary}"
      if pathName.include? "loop" and cmds.size == 0 and innerLoop
        cmd += " --unroll 10"
      end
      cmds << [cmd,bp,false]
    end
    if bp != blockPath
      if graph.getBlockPaths[bp] == nil and bp != []
        graph.getBlockPaths[bp]="#{pathName}_#{pathCnt}"
        graph.getVarDefs["#{pathName}_#{pathCnt}"] = bp
        pathCnt+=1
      end
      if bp != []
        if evalStr == ""
          evalStr = "(#{graph.getBlockPaths[bp]}"
        else
          evalStr += " + #{graph.getBlockPaths[bp]}"
        end
      end

      evalStr+=")"
      graph.getVarDefs[pathName]=evalStr
      tevalPaths[evalStr] << pathName
      tevalPaths.merge!(graph.getEvalPaths){|key,o,n| o + n}
      graph.getEvalPaths.replace(tevalPaths)
      if not (/_[0-9]+_[0-9]+$/ =~ graph.getBlockPaths[blockPath])
        graph.getBlockPaths.delete(blockPath)
      end
    end
    return cmds,callCnt,calledFuncsNameMap
  end

  def generateGraphIACAcmds(func,graph)
    cmds = []
    if graph.getEntrys.length > 0
      if @verbose
        puts "calculating paths"
      end
      graph.calculatePaths(@pathThreshold,true)

      callCnt = 0
      calledDict = {}

      for bp in graph.getBlockPaths.keys.sort
        innerLoop = false
        if graph.getBlockPaths[bp].include? "loop"
          loopStr = ""
          if  graph.getBlockPaths[bp].include? "entry"
            loopStr = graph.getBlockPaths[bp].gsub(/_entry(.*)/,"")
          elsif  graph.getBlockPaths[bp].include? "seg"
            loopStr =  graph.getBlockPaths[bp].gsub(/_seg(.*)/,"")
          elsif  graph.getBlockPaths[bp].include? "exit"
            loopStr =  graph.getBlockPaths[bp].gsub(/_exit(.*)/,"")
          end
          for l in graph.getSubGraphs
            #puts "innerLoopCheck #{l} #{l.getSubGraphs} #{loopStr} #{l.getVarName}"
            if "#{l.getVarName}"==loopStr
              if l.getSubGraphs == []
                innerLoop = true
              end
            end
          end
        end
        tcmds,callCnt,calledFuncsNameMap = createIACACmd(graph,bp,@oa.getCalledFuncs(func),callCnt,innerLoop)
        for calledFunc in calledFuncsNameMap.keys
          if calledDict[calledFunc] == nil
            calledDict[calledFunc]=calledFuncsNameMap[calledFunc]
          end
        end
        cmds += tcmds
      end
      for v in graph.getVarProbs.keys
        graph.getVarProbs[v]="*#{graph.getVarProbs[v]}"
      end
    end
    return cmds.uniq,calledDict
  end

  def generateGraphMIAMIcmds(func,graph)
    cmds = []
    if graph.getEntrys.length > 0
      if @verbose
        puts "calculating paths"
      end
      graph.calculatePaths(@pathThreshold,true)

      callCnt = 0
      calledDict = {}

      for bp in graph.getBlockPaths.keys.sort
        innerLoop = false
        if graph.getBlockPaths[bp].include? "loop"
          loopStr = ""
          if  graph.getBlockPaths[bp].include? "entry"
            loopStr = graph.getBlockPaths[bp].gsub(/_entry(.*)/,"")
          elsif  graph.getBlockPaths[bp].include? "seg"
            loopStr =  graph.getBlockPaths[bp].gsub(/_seg(.*)/,"")
          elsif  graph.getBlockPaths[bp].include? "exit"
            loopStr =  graph.getBlockPaths[bp].gsub(/_exit(.*)/,"")
          end
          for l in graph.getSubGraphs
            if @verbose
              puts "innerLoopCheck #{l} #{l.getSubGraphs} #{loopStr} #{l.getVarName}"
            end
            if "#{l.getVarName}"==loopStr
              if l.getSubGraphs == []
                innerLoop = true
              end
            end
          end
        end
        tcmds,callCnt,calledFuncsNameMap = createMIAMICmd(graph,bp,@oa.getCalledFuncs(func),callCnt,innerLoop)
        for calledFunc in calledFuncsNameMap.keys
          if calledDict[calledFunc] == nil
            calledDict[calledFunc]=calledFuncsNameMap[calledFunc]
          end
        end
        cmds += tcmds
      end

      for v in graph.getVarProbs.keys
        graph.getVarProbs[v]="*#{graph.getVarProbs[v]}"
      end

    end
    return cmds.uniq,calledDict
  end

  def createThreadedModel(func,loopItsParamStrDict,instances,ghz,miami)
    # puts "#{func}"
    # puts "#{loopItsParamStrDict}"
    # puts "#{instances}"
    demang_func = @nameMap[func]
    loadStr = ""
    modelStr = "class #{demang_func.capitalize}\n"
    if miami
      modelStr +="\tdef evalModel(funcs,inclusive,average,useMax#{loopItsParamStrDict[loopItsParamStrDict.keys[0]]})\n"
    else
      modelStr +="\tdef evalModel(cycPerMemOp,funcs,inclusive,average,useMax#{loopItsParamStrDict[loopItsParamStrDict.keys[0]]})\n"
    end
    
    modelStr +="\t\tvals=[]\n"
    modelStr += "\t\tif !average\n"
    tids = []
    for tid in 0..(@oa.getNumThreads.to_i-1)
      if instances.has_key? tid
        tids << tid
        loadStr += "require_relative '#{demang_func}_#{tid}.rb'\n"
        modelStr += "\t\t\t#{demang_func}_#{tid}=#{demang_func.capitalize}_#{tid}.new()\n"
        if miami
          modelStr += "\t\t\tvals << [#{demang_func}_#{tid}.evalModel(funcs#{loopItsParamStrDict[tid]}),#{instances[tid]},#{demang_func}_#{tid}.evalModel(funcs#{loopItsParamStrDict[tid]})*#{instances[tid]},#{tid}]\n"
        else
          modelStr += "\t\t\tvals << [#{demang_func}_#{tid}.evalModel(cycPerMemOp,funcs#{loopItsParamStrDict[tid]}),#{instances[tid]},#{demang_func}_#{tid}.evalModel(cycPerMemOp,funcs#{loopItsParamStrDict[tid]})*#{instances[tid]},#{tid}]\n"
        end
        
        modelStr += "\t\t\tif inclusive\n"
        if miami
          modelStr += "\t\t\t\tfuncs[\"#{demang_func}_#{tid}\"]=#{demang_func}_#{tid}.evalModel(funcs#{loopItsParamStrDict[tid]})\n"
        else
          modelStr += "\t\t\t\tfuncs[\"#{demang_func}_#{tid}\"]=#{demang_func}_#{tid}.evalModel(cycPerMemOp,funcs#{loopItsParamStrDict[tid]})\n"
        end
        modelStr += "\t\t\telse\n"
        modelStr += "\t\t\t\tfuncs[\"#{demang_func}_#{tid}\"]=0\n"
        modelStr += "\t\t\tend\n"
      end
    end
    tid = @oa.getNumThreads.to_i
    if instances.has_key? tid
      tids << tid
      loadStr += "require_relative '#{demang_func}_#{tid}.rb'\n"
      modelStr += "\t\telse\n"
      modelStr += "\t\t\t#{demang_func}_#{tid}=#{demang_func.capitalize}_#{tid}.new()\n"
      modelStr += "\t\t\tvals << [#{demang_func}_#{tid}.evalModel(cycPerMemOp,funcs#{loopItsParamStrDict[tid]}),#{instances[tid]},#{demang_func}_#{tid}.evalModel(cycPerMemOp,funcs#{loopItsParamStrDict[tid]})*#{instances[tid]},#{tid}]\n"
      modelStr += "\t\t\tif inclusive\n"
      modelStr += "\t\t\t\tfuncs[\"#{demang_func}_#{tid}\"]=#{demang_func}_#{tid}.evalModel(cycPerMemOp,funcs#{loopItsParamStrDict[tid]})\n"
      modelStr += "\t\t\telse\n"
      modelStr += "\t\t\t\tfuncs[\"#{demang_func}_#{tid}\"]=0\n"
      modelStr += "\t\t\tend\n"
    end
    modelStr += "\t\tend\n"
    modelStr += "\t\tputs \"\#{vals}\"\n"
    modelStr += "\t\tmax = vals[0]\n"
    modelStr += "\t\tfor v in vals\n"
    modelStr += "\t\t\tif v[2] > max[2]\n"
    modelStr += "\t\t\t\tmax=v\n"
    modelStr += "\t\t\tend\n"
    modelStr += "\t\t\tputs \"\#{v[2]/#{ghz}}\"\n"
    modelStr += "\t\tend\n"
    modelStr += "\t\tif average\n"
    modelStr += "\t\t\tmax=vals[-1]\n"
    modelStr += "\t\tend\n"
    modelStr += "\t\tif useMax\n"
    modelStr += "\t\t\tfor tid in #{tids}\n"
    modelStr += "\t\t\t\tfuncs[\"#{demang_func}_\#{tid}\"] = max[0]\n"
    modelStr += "\t\t\tend\n"
    modelStr += "\t\tend\n"
    modelStr += "\t\tputs \"max: \#{max}\"\n"
    modelStr += "\t\treturn max[0],funcs\n"
    modelStr += "\tend\n"

    if @executable != ""
      mfile = File.open("#{@workDir}/#{demang_func}/#{demang_func}.rb","w")
      mfile.puts("#!/usr/bin/env ruby")
      mfile.puts(loadStr)
      mfile.puts(modelStr)
      mfile.puts()
      mfile.close
    end
  end


  def updateModelStrs(func,instances,loopItsParamStr,calledDict,uncapturedFuncs,miami)
    demang_func = @nameMap[func]
    if @verbose
      puts "#{demang_func}"
    end
    tmpStr ="\#----------------------------------------\n"
    tmpStr += "puts \"#{demang_func}\"\n"
    tmpStr+="tfuncs = {"
    finished = true
    for calledFn in calledDict.keys
      #puts "#{calledFn}"
      if @nameMap.keys.include? calledFn and !@finishedFuncs.keys.include? calledFn
        #puts"here 1"
        finished = false
      end
      if !@nameMap.keys.include? calledFn
        #puts "here 2"
        tmpStr+="\"#{makeNameNice(calledFn)}\"=>0,"
        uncapturedFuncs[calledFn]=true
      end
    end
    costPerMemOpLevel = [[5.0, 1.0], [10.0, 0.0], [30.0, 0.0], [100.0, 0.0], [300.0, 0.0]]# @latencies[func] #[[5.0, 1.0], [10.0, 0.0], [30.0, 0.0], [100.0, 0.0], [300.0, 0.0]]
    cycPerMemOpStr=""
    tmpStr=tmpStr[0..-1]+"}\n"
    tmpStr+="funcs.merge!(tfuncs)\n"
    if miami
      tmpStr+="val,funcs = #{demang_func}.evalModel(funcs,inclusive,average,useMax#{loopItsParamStr})\n"
    else
      tmpStr+="costPerMemOpLevel = #{costPerMemOpLevel}\n"
      tmpStr+="cycPerMemOp = 0\#getCycPerMemOp(costPerMemOpLevel)\n"
      tmpStr+="val,funcs = #{demang_func}.evalModel(cycPerMemOp,funcs,inclusive,average,useMax#{loopItsParamStr})\n"
    end
    tmpStr+="puts \"#{demang_func}: \#{val} cycles per instance\"\n"
    #if @inclusive
      #puts "inclusive!!!!!"
      tmpStr += "if inclusive\n"
      tmpStr+="\tfuncs[\"#{demang_func}\"]=val\n"
    #else
      tmpStr+="else\n"
      tmpStr+="\tfuncs[\"#{demang_func}\"]=0\n"
      tmpStr+="end\n"
    #end

    @modelStrs += "require_relative '#{demang_func}/#{demang_func}.rb'\n"
    @funcStr +="#{demang_func}=#{demang_func.capitalize}.new()\n"

    tmpStr += "#{demang_func}_its = #{instances}\n"
    if miami
      tmpStr += "puts \"exec time: \" + (#{demang_func}_its*val/ghz).to_s\n"
    else
      tmpStr += "puts \"exec time (\#{cycPerMemOp} cycles per mem op): \" + (#{demang_func}_its*val/ghz).to_s\n"
    end
    tmpStr += "puts\n"
    tmpStr +="\#---------------------------------------\n\n"

    if finished
      puts "* All dependencies satisfied for #{demang_func}"
      @itsStr += tmpStr
      @finishedFuncs[func] = tmpStr
      for u_func in @unfinishedFuncs.keys
        finished = true
        for calledFn in @unfinishedFuncs[u_func][1].keys
          if @nameMap.keys.include? calledFn and !@finishedFuncs.keys.include? calledFn
            puts "Not finished   #{u_func} #{calledFn} 1--------------------------------------------------------------------------"
            finished = false
          end
        end
        if finished
          puts "* All dependencies satisfied for #{u_func}"
          @itsStr += @unfinishedFuncs[u_func][0]
          @finishedFuncs[u_func] = tmpStr
          @unfinishedFuncs.delete(u_func)
        end
      end
    else
      puts "Not finished #{demang_func} 2--------------------------------------------------------------------------"
      @unfinishedFuncs[func] = [tmpStr,calledDict]
    end
  end

  def analyzeDependencies()
    for u_func in @unfinishedFuncs.keys
      finished = true
      for calledFn in @unfinishedFuncs[u_func][1].keys
        #puts "#{calledFn}"
        if @nameMap.keys.include? calledFn and !@finishedFuncs.keys.include? calledFn
          puts "\tNot finished  #{u_func} #{calledFn} 3--------------------------------------------------------------------------"
          finished = false
        end
      end
      if finished
        puts "All dependencies satisfied for #{u_func}--------------------------------------------------"
        @itsStr += @unfinishedFuncs[u_func][0]
        @finishedFuncs[u_func] = @unfinishedFuncs[u_func][0]
        @unfinishedFuncs.delete(u_func)
      end
    end
  end

  def writeOutModel(fname,miami)
    if @verbose
      puts "#{fname}"
    end
    cmfile = File.open(fname,"w")
    cmfile.puts("#!/usr/bin/env ruby")
    cmfile.puts(@modelStrs)
    if not miami
      defsStr  ="\n"
      defsStr +="def getCycPerMemOp(costPerMemOpLevel)\n"
      defsStr +="\ttotCost=0.0\n"
      defsStr +="\tfor cost,perc in costPerMemOpLevel\n"
      defsStr +="\t\ttotCost+=cost*perc\n"
      defsStr +="\tend\n"
      defsStr +="\treturn totCost\n"
      defsStr +="end\n"
      cmfile.puts(defsStr)
    end
    cmfile.puts("\n")
    cmfile.puts(@ghzStr)
    cmfile.puts("\n")
    cmfile.puts(@funcStr)
    cmfile.puts("\n")
    cmfile.puts(@itsStr)
    cmfile.puts("\n")
    cmfile.close
  end
end
