#include <iostream>



int __attribute__((noinline)) test1(int arg1, int arg2){
    int val = 0;
    for(int i = 0; i < arg1; i++){
        if(arg2 < i){
            val++; 
        }
        else if(arg2 == i){
            val *= 2;
        }
        else{
            val--;
        }

    }
    return val;
}


int main(int argc, const char** argv){
    int arg1 = atoi(argv[1]);
    int arg2 = atoi(argv[2]);

    std::cout<<test1(arg1,arg2)<<std::endl;
}