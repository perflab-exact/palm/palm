// -*-Mode: C++;-*-

//*BeginPNNLCopyright********************************************************
//
// $HeadURL$
// $Id$
//
//**********************************************************EndPNNLCopyright*

//***************************************************************************
// $HeadURL$
//
// Nathan Tallent, Ryan Friese
//***************************************************************************

//***************************************************************************
#include <boost/thread.hpp>
#include <fstream>
#include <regex>
#include <unordered_set>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <BPatch.h>
#include <BPatch_object.h>
#include <BPatch_point.h>
#include <BPatch_process.h>
#include <Function.h>

#include "CFGProf-Instrumentor.hpp"
#include "DyninstUtil.hpp"

//***************************************************************************

std::ostream ofs(NULL);

/* from HPCTOOLKIT StrUtils.cpp */
void tokenize_char(const std::string &tokenstr, const char *delim, std::vector<std::string> &tokenvec) {
  const size_t sz = tokenstr.size();
  for (size_t begp = 0, endp = 0; begp < sz; begp = endp + 1) {
    begp = tokenstr.find_first_not_of(delim, begp);
    if (begp == std::string::npos) {
      break;
    }

    endp = tokenstr.find_first_of(delim, begp);
    if (endp == std::string::npos) {
      endp = sz;
    }
    std::string x = tokenstr.substr(begp, endp - begp); // [begin, end)
    tokenvec.push_back(x);
  }
}

/* from HPCTOOLKIT StrUtils.cpp */
std::string join(const std::vector<std::string> &tokenvec, const char *delim, size_t begIdx, size_t endIdx) {
  std::string result;

  // N.B.: [begIdx, endIdx)
  for (size_t i = begIdx; i < endIdx; ++i) {
    std::cout << tokenvec[i] << std::endl;
    result += tokenvec[i];
    if (i + 1 < endIdx) {
      result += delim;
    }
  }

  return result;
}

/* from HPCTOOLKIT FileUtils.cpp */
std::string basenm(const char *fName) {
  std::string baseFileName;

  const char *lastSlash = strrchr(fName, '/');
  if (lastSlash) {
    // valid: "/foo" || ".../foo" AND invalid: "/" || ".../"
    baseFileName = lastSlash + 1;
  } else {
    // filename contains no slashes, already in short form
    baseFileName = fName;
  }
  return baseFileName;
}

/* from HPCTOOLKIT FileUtils.cpp */
bool isDir(const char *path) {
  struct stat sbuf;
  if (stat(path, &sbuf) == 0) {
    return (S_ISDIR(sbuf.st_mode)
            /*|| S_ISLNK(sbuf.st_mode) && isDir(readlink(path))*/);
  }
  return false; // unknown
}

/* from HPCTOOLKIT FileUtils.cpp */
int mkdir(const char *dir) {
  std::cout << "mkdir: " << dir << std::endl;
  if (!dir) {
    std::cerr << "Invalid mkdir argument: (NULL)" << std::endl;
  }

  std::string pathStr = dir;
  bool isAbsPath = (dir[0] == '/');

  // -------------------------------------------------------
  // 1. Convert path string to vector of path components
  //    "/p0/p1/p2/.../pn/px" ==> [p0 p1 p2 ... pn px]
  //    "/p0"                 ==> [p0]
  //    "p0"                  ==> [p0]
  //    "./p0"                ==> [. p0]
  //
  // Note: we could do tokenization in place (string::find_last_of()),
  // but (1) this is more elegant and (2) sytem calls and disk
  // accesses will overwhelm any possible difference in performance.
  // -------------------------------------------------------
  std::vector<std::string> pathVec;
  tokenize_char(pathStr, "/", pathVec);

  //DIAG_Assert(!pathVec.empty(), DIAG_UnexpectedInput);

  // -------------------------------------------------------
  // 2. Find 'curIdx' such that all paths before pathVec[curIdx] have
  // been created.
  //
  // Note: Start search from the last path component, assuming that in
  // the common case, intermediate directories are already created.
  //
  // Note: Could make this a binary search, but it would likely have
  // insignificant effects.
  // -------------------------------------------------------

  //size_t begIdx = 0;
  //size_t endIdx = pathVec.size() - 1;
  //size_t curIdx = endIdx;
  //change size_t to int -> for loop >= 0 will fail because size_t is unsigned
  int begIdx = 0;
  int endIdx = pathVec.size() - 1;
  int curIdx = endIdx;
  for (; curIdx >= begIdx; --curIdx) {
    std::cout << curIdx << " " << begIdx << " " << endIdx << std::endl;
    std::string x = join(pathVec, "/", 0, curIdx + 1);
    if (isAbsPath) {
      x = "/" + x;
    }

    if (isDir(x.c_str())) {
      break; // FIXME: double check: what if this is a symlink?
    }
  }

  curIdx++;

  // -------------------------------------------------------
  // 3. Build directories from pathVec[curIdx ... endIdx]
  // -------------------------------------------------------
  mode_t mode = S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH;

  for (; curIdx <= endIdx; ++curIdx) {
    std::string x = join(pathVec, "/", 0, curIdx + 1);
    if (isAbsPath) {
      x = "/" + x;
    }

    int ret = ::mkdir(x.c_str(), mode);
    if (ret != 0) {
      std::cerr << "[FileUtil::mkdir] '" << pathStr << "': Could not mkdir '"
                << x << "' (" << strerror(errno) << ")" << std::endl;
    }
  }

  return 0;
}

struct MyCallback {
  static Instrumentor *instrumentor;
  static std::string perfPath;

  static void dynLib(BPatch_thread *thr, BPatch_object *mod, bool loaded) {
    BPatch_addressSpace *app = dynamic_cast<BPatch_addressSpace *>(thr->getProcess());
    std::string appProcPid = std::to_string((long long)thr->getProcess()->getPid());
    std::vector<BPatch_thread *> thrds;
    thr->getProcess()->getThreads(thrds);
    ofs << "pid: " << ::getpid() << " " << appProcPid << " "
        << "library loaded: " << mod->name() << "num threads:" << thrds.size() << std::endl;
    // std::vector<BPatch_function*> funcs = *(app->getImage()->getProcedures());
    // for(int f=0;f<funcs.size();f++){
    // 	instrumentor->addFunction(funcs[f]->getMangledName());
    // }
    if (instrumentor->getCG()) {
      instrumentor->constructCallGraph(app, appProcPid);
    } else {
      instrumentor->instrumentFunctions(app, appProcPid);
    }
  }

  static void dynCall(BPatch_point *at_point, BPatch_function *called_function) {
    ofs << "here? " << at_point->getFunction()->getBaseAddr() << " " << called_function->getBaseAddr() << std::endl;

    //instrumentor->instrumentDynamicFunction(at_point,called_function);
    //at_point->stopMonitoring();
  }

  static void usr(BPatch_process *proc, void *addr, unsigned int bufsize) {
    ofs << "dyncall: " << proc->isMultithreaded() << " " << proc->isMultithreadCapable() << std::endl;
    if (!proc->isMultithreaded() | true) {
      instrumentor->instrumentDynamicFunction2(proc);
    } else {
      ofs << "proc is isMultithreaded" << std::endl;
    }
    ofs << "done with dynamic call" << std::endl;
    //proc->detach(true);
    //sleep(60);
  }

  static void exit(BPatch_thread *thr, BPatch_exitType et) { //gather counter values & potentially incorporate mem latency information as well?

    /*unsigned int nThreads = boost::thread::hardware_concurrency();
		if (!thr->getProcess()->isMultithreadCapable()){
			nThreads = 1;
		}*/

    BPatch_addressSpace *(app) = dynamic_cast<BPatch_addressSpace *>(thr->getProcess());
    //calculateLatency(app,perfPath);
    std::string appProcPid = std::to_string((long long)thr->getProcess()->getPid());
    ofs << "exiting: " << appProcPid << " " << (et == BPatch_exitType::NoExit) << " " << (et == BPatch_exitType::ExitedNormally) << " " << (et == BPatch_exitType::ExitedViaSignal) << std::endl;
    if (instrumentor->getUseOmp()) {
      instrumentor->gatherCountsOMP(app, appProcPid);
    } else {
      instrumentor->gatherCounts(app, appProcPid);
    }
  }

  static void err(BPatchErrorLevel sev, int number, const char *const *params) {
    ofs << (sev == BPatchErrorLevel::BPatchFatal) << " " << (sev == BPatchErrorLevel::BPatchSerious) << " " << (sev == BPatchErrorLevel::BPatchWarning) << " " << (sev == BPatchErrorLevel::BPatchInfo) << " error occured " << number << " " << params[0] << std::endl;
  }
};

//***************************************************************************
//
//***************************************************************************

Instrumentor *MyCallback::instrumentor = 0;
std::string MyCallback::perfPath = "";
int main(int argc, char* argv[], const char* envp[]) {
  // -------------------------------------------------------
  // Parse command line
  // -------------------------------------------------------
  if (argc <= 2) {
    return 1;
  }
  std::vector<int> argInds;
  unsigned int nThreads = 1;
  Instrumentor *instrumentor = new Instrumentor();

  int outPid = ::getpid();
  if (const char *env_p = std::getenv("PMI_RANK")) {
    outPid = atoi(env_p);
  }

  bool stdout = true;

  std::cout << "pid: " << ::getpid() << " outPid: " << outPid << " num args: " << argc << std::endl;
  for (int i = 1; i < argc; i++) {
    if (strcmp("-f", argv[i]) == 0 | strcmp("--function", argv[i]) == 0) {
      if (argInds.size() == 0) {
        std::string func = argv[++i];
        std::vector<std::string> fargs;
        std::stringstream ss(func);
        std::string item;
        while (getline(ss, item, ';')) {
          std::cout << item << std::endl;
          fargs.push_back(item);
        }
        instrumentor->addFunction(fargs);
      } else {
        argInds.push_back(i);
      }

    } else if (strcmp("-e", argv[i]) == 0 | strcmp("--instrument-edges", argv[i]) == 0) {
      instrumentor->setDoEdges(true);
    } else if (strcmp("-omp", argv[i]) == 0 | strcmp("--omp", argv[i]) == 0) {
      instrumentor->setUseOmp(true);
    } else if (strcmp("-l", argv[i]) == 0 | strcmp("--max-func-level", argv[i]) == 0) {
      instrumentor->setMaxFuncLevel(atoi(argv[++i]));
      std::cout << "setting max func level " << instrumentor->getMaxFuncLevel() << std::endl;
    } else if (strcmp("-cg", argv[i]) == 0 | strcmp("--call-graph", argv[i]) == 0) {
      instrumentor->setCG(true);
    } else if (strcmp("-d", argv[i]) == 0 | strcmp("--dynamic-calls", argv[i]) == 0) {
      if (atoi(argv[++i]) <= 0) {
        instrumentor->setDynamicCalls(false);
      }
    } else if (strcmp("--runtime-calls", argv[i]) == 0) {
      if (atoi(argv[++i]) <= 0) {
        instrumentor->setRunTimeCalls(false);
      }
    } else if (strcmp("-m", argv[i]) == 0 | strcmp("--mem-path", argv[i]) == 0) { //mem path
      MyCallback::perfPath = argv[++i];
    } else if (strcmp("--log", argv[i]) == 0) {
      stdout = false;
    } else {
      argInds.push_back(i);
    }
  }

  std::string binaryPath = argv[argInds[0]];
  std::string binaryNm = basenm(binaryPath.c_str());
  std::cout << binaryPath << std::endl;
  std::cout << binaryNm << std::endl;

  std::stringstream outPath;
  outPath << "palm-" << binaryNm << "/"
          << "cfgprof-" << outPid << "/";
  std::cout << outPath.str() << std::endl;
  mkdir(outPath.str().c_str());

  std::stringstream outFileName;
  outFileName << outPath.str() << binaryNm << ".cfgprof";

  std::ofstream outFile(outFileName.str());
  outFile << "binary-name|" << binaryNm << std::endl;
  outFile << "binary-path|" << binaryPath << std::endl; //does this need to be the full path?
  outFile.close();

  std::cout << "pid: " << outPid << " " << outFileName.str() << std::endl;

  std::streambuf *buf;
  std::ofstream of;
  if (stdout) {
    buf = std::cout.rdbuf();
  } else {
    std::ostringstream oss;
    outPath << "logs/";
    mkdir(outPath.str().c_str());
    oss << outPath.str() << "cfgprof-log-" << outPid << ".txt";
    of.open(oss.str());
    buf = of.rdbuf();
  }
  ofs.rdbuf(buf);

  const char *progName = binaryPath.c_str();
  const char *progArgv[argInds.size() + 1];
  for (int i = 0; i < argInds.size(); i++) {
    progArgv[i] = argv[argInds[i]];
  }
  progArgv[argInds.size()] = NULL;

  // -------------------------------------------------------
  //
  // -------------------------------------------------------

  BPatch bpatch;
  std::cout << "pid: " << outPid << " "
            << "binary-name: " << binaryNm << std::endl;
  std::cout << "pid: " << outPid << " "
            << "binary-path: " << binaryPath << std::endl;
  std::cout << "pid: " << outPid << " "
            << "palm-inst: " << progName << std::endl;
  for (int i = 0; i < argInds.size(); i++) {
    std::cout << "arg: " << progArgv[i] << std::endl;
  }
  std::cout << "tests here" << std::endl;
  BPatch_addressSpace *app = bpatch.processCreate(progName, progArgv, NULL, 0, 1, 2, BPatch_normalMode);

  if (instrumentor->getUseOmp()) {
    std::vector<BPatch_function *> tfunctions;
    app->getImage()->findFunction("omp_get_thread_num", tfunctions, false, true, false);
    if (tfunctions.size() == 0) {
      std::cout << "WARNING: \"omp_get_thread_num\" not found, are you instrumenting a non-omp program using \"-omp\"?" << std::endl;
      std::cout << "\t\tinstrumentation will proceed in single threaded mode" << std::endl;
      instrumentor->setUseOmp(false);
    } else {
      nThreads = boost::thread::hardware_concurrency();
    }
  }

  std::vector<BPatch_module *> mods;
  app->getImage()->getModules(mods);

  BPatch_process *appProc = dynamic_cast<BPatch_process *>(app);
  std::string appProcPid = std::to_string((long long)appProc->getPid());
  std::vector<BPatch_thread *> thrds;
  appProc->getThreads(thrds);
  std::cout << "threads: " << thrds.size() << " " << boost::thread::hardware_concurrency() << " " << appProc->isMultithreaded() << " " << appProc->isMultithreadCapable() << std::endl;
  // std::vector<BPatch_function*> funcs = *(app->getImage()->getProcedures());
  // for(int f=0;f<funcs.size();f++){
  // 	instrumentor->addFunction(funcs[f]->getMangledName());
  // }
  if (instrumentor->getCG()) {
    instrumentor->constructCallGraph(app, appProcPid);
  } else {
    instrumentor->instrumentFunctions(app, appProcPid);
  }

  MyCallback::instrumentor = instrumentor;

  bpatch.registerDynLibraryCallback(MyCallback::dynLib);
  bpatch.registerExitCallback(MyCallback::exit);
  bool temp = bpatch.registerDynamicCallCallback(MyCallback::dynCall);
  bpatch.registerUserEventCallback(MyCallback::usr);
  bpatch.registerErrorCallback(MyCallback::err);
  ofs << "dyn call register: " << temp << std::endl;

  //int pid = appProc->getPid();
  ofs << "pid: " << outPid << " "
      << "palm-inst: "
      << "continuing execution" << std::endl;
  if (!appProc->continueExecution()) {
    fprintf(stderr, "palm-inst: continueExecution failed\n");
  }
  //ofs << "term 1: " << appProc->isTerminated() << " " << appProc->isStopped() << " " << appProc->stopSignal() << std::endl;
  while (!appProc->isTerminated()) {
    ofs << "before" << std::endl;
    bpatch.waitForStatusChange();
    ofs << "after" << std::endl;
  }
  //ofs << "term 2: " << appProc->isTerminated() << " " << appProc->isStopped() << " " << appProc->stopSignal() << std::endl;
  ofs << "pid: " << outPid << " "
      << "finished" << std::endl;

  outFile.open(outFileName.str(), std::ofstream::out | std::ofstream::app);
  outFile << "nThreads|" << nThreads << std::endl;
  if (instrumentor->getCG()) {
  } else {

    for (std::map<std::string, graph *>::iterator git = instrumentor->getGraphs().begin(); git != instrumentor->getGraphs().end(); ++git) {
      graph *g = (*git).second;
      serialize(outFile, *g);
    }
  }
  outFile.close();
  delete instrumentor;
  std::cout << "finished with instrumentation" << std::endl;
  return 0;
}
