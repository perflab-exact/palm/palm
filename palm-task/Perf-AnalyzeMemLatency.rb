#!/usr/bin/env ruby
# -*- mode: ruby -*-

require 'fileutils'
require "parallel"
require 'open3'


#****************************************************************************
#
#****************************************************************************

def analyzePerfFiles(nprocs,perfDir)

  #-----------------------------------------------------------
  # Analyze each individual perf file
  #-----------------------------------------------------------

  files = Dir.glob("#{perfDir}memlat*.perf") # dir ends with "/"
  # puts "#{files}"
  tempData = Parallel.map(files, in_processes: nprocs) do |perfFile| #{ |tid| createGraphThread(func,demang_func,binary,tid) }
    analyzePerfOneFile(perfFile)
  end

  #puts "#{tempData.size}"
  data = {}
  for tdata in tempData
    for dso in tdata.keys
      if !data.key? dso
        data[dso]={}
      end
      for func in tdata[dso].keys
        if !data[dso].key? func
          data[dso][func]={}
        end
        for offset in tdata[dso][func].keys
          if !data[dso][func].key? offset
            data[dso][func][offset]=[[0,0],[0,0],[0,0],[0,0],[0,0]]
          end
          for i in (0..4)
            data[dso][func][offset][i][0]+=tdata[dso][func][offset][i][0]
            data[dso][func][offset][i][1]+=tdata[dso][func][offset][i][1]
          end
        end 
      end
    end
  end

  #-----------------------------------------------------------
  # Write combined file
  #-----------------------------------------------------------

  outFile = open("#{perfDir}/memlat.prof",'w')
  #outFile.write("offset: [[L1 Avg Lat, L1 percent hit], [L2 Avg Lat, L2 percent hit], [L3 Avg Lat, L3 percent hit], [LFB Avg Lat, LFB percent hit], [RAM Avg Lat,RAM percent hit]]: avg latency\n")
  for dso in data.keys.sort
    for func in data[dso].keys.sort
      outFile.write("func:#{func}\n")
      for offset in data[dso][func].keys.sort
        sum = 0.0
        for i in (0..4)
          sum+=data[dso][func][offset][i][1].to_f
        end
        avg=0.0
        tempStr=""
        cnt = 0
        for i in (0..4)
          data[dso][func][offset][i][0]=div0(data[dso][func][offset][i][0].to_f,data[dso][func][offset][i][1].to_f)

          #data[dso][func][offset][i][1]=div0(data[dso][func][offset][i][1].to_f,sum)
          data[dso][func][offset][i][1]=data[dso][func][offset][i][1].to_i
          if data[dso][func][offset][i][1] > 0
            cnt+=1
            tempStr << "#{i} #{data[dso][func][offset][i][1]} #{data[dso][func][offset][i][0]} "
          end
          avg += data[dso][func][offset][i][0]*div0(data[dso][func][offset][i][1].to_f,sum)
        end
        tempStr = "#{offset} #{cnt} #{tempStr} #{avg}"
        outFile.write("#{tempStr}\n")
      end
      outFile.write("\n")
    end
  end
  outFile.close()
end


def analyzePerfOneFile(perfFile)
  cmd = "perf mem report -v -U -i #{perfFile} -t \",\" --no-demangle | grep '^ [0-9]*\.[0-9]*'"

  puts "*** Analyze memory latency: #{cmd}"

  cmdPid = -1
  stdin, stdout, stderr, wait_thr = Open3.popen3(cmd)
  cmdPid = wait_thr[:pid]
  data = {}
  # outFile = open("#{perfFile}-info.dat",'w')
  # perfHitDat = open("#{perfFile}-hits.dat",'w')
  memLevelCnt ={}
  stdout.each do |line|
    offset,weight,samples,memLevel,func,dso = analyzePerfSample(line.chomp)
    #print "#{func} #{memLevel} #{weight} #{samples} "
    if !data.key? dso
      data[dso]={}
    end
    if !data[dso].key? func
      #data[dso][func]={"L1 hit"=>[0,0],"L2 hit"=>[0,0],"L3 hit"=>[0,0],"LFB hit"=>[0,0]}
      data[dso][func]={}
      #data[dso][func]=[{},0,0,0] #=> [offset[latency sum,num samples], function latency sum, function num samples, max samples (amongst offsets)]
    end
    if !data[dso][func].key? offset
      data[dso][func][offset]=[[0,0],[0,0],[0,0],[0,0],[0,0]]
    end
    #print "#{memLevel} #{memLevel.include? "L1"  or memLevel.include? "L2" or memLevel.include? "L3" or memLevel.include? "LFB" or memLevel.include? "ram"}"
    if memLevel.include? "L1" 
      data[dso][func][offset][0][0]+=weight*samples
      data[dso][func][offset][0][1]+=samples
    elsif memLevel.include? "L2"
      data[dso][func][offset][1][0]+=weight*samples
      data[dso][func][offset][1][1]+=samples
    elsif memLevel.include? "L3"
      data[dso][func][offset][2][0]+=weight*samples
      data[dso][func][offset][2][1]+=samples
    elsif memLevel.include? "LFB"
      data[dso][func][offset][3][0]+=weight*samples
      data[dso][func][offset][3][1]+=samples
    elsif memLevel.include? "RAM"
      data[dso][func][offset][4][0]+=weight*samples
      data[dso][func][offset][4][1]+=samples
    end
  end

  outFnm = File.dirname(perfFile) + "/" +
    File.basename(perfFile, ".perf") + ".prof"

  outFile = open(outFnm,'w')

  #outFile.write("offset: [[L1 Avg Lat, L1 percent hit], [L2 Avg Lat, L2 percent hit], [L3 Avg Lat, L3 percent hit], [LFB Avg Lat, LFB percent hit], [RAM Avg Lat,RAM percent hit]]: avg latency\n")
  for dso in data.keys.sort
    for func in data[dso].keys.sort
      outFile.write("func:#{func}\n")
      for offset in data[dso][func].keys.sort
        sum = 0.0
        for i in (0..4)
          sum+=data[dso][func][offset][i][1].to_f
        end
        avg=0.0
        cnt = 0
        tempStr=""
        for i in (0..4)
          temp = div0(data[dso][func][offset][i][0].to_f,data[dso][func][offset][i][1].to_f)
          #data[dso][func][offset][i][0]=div0(data[dso][func][offset][i][0].to_f,data[dso][func][offset][i][1].to_f)

          #data[dso][func][offset][i][1]=div0(data[dso][func][offset][i][1].to_f,sum)
          data[dso][func][offset][i][1]=data[dso][func][offset][i][1]
          if data[dso][func][offset][i][1] > 0
            cnt+=1
            tempStr << "#{i} #{data[dso][func][offset][i][1].to_i} #{temp} "
          end
          avg += temp*div0(data[dso][func][offset][i][1].to_f,sum)
        end
        tempStr = "#{offset} #{cnt} #{tempStr} #{avg}"
        outFile.write("#{tempStr}\n")
      end
      outFile.write("\n")
    end
  end
  outFile.close()

  # outFile.write("1. offset  2. avg. lat(offset)  3. norm. lat  4. avg. lat(func)  5. function  6 .binary  7. hit level\n\n")

  stdin.close()
  stdout.close()
  stderr.close()
  #puts "#{vals.length}"
  #outFile.close()
  return data
  #return vals
end


#****************************************************************************

def analyzePerfFiles1(nprocs,perfDir) #keep
  raise "Error: Should be unused for now!" # tallent
  
  files = `ls #{perfDir}perf-*.perf`.split
  puts "#{files}"
  tempData = Parallel.map(files, in_processes: nprocs) do |perfFile| #{ |tid| createGraphThread(func,demang_func,binary,tid) }
    analyzePerfFile(perfFile)
  end
  puts "#{tempData.size}"
  data = {}
  for tdata in tempData
    for dso in tdata.keys
      if !data.key? dso
        data[dso]={}
      end
      for func in tdata[dso].keys
        if !data[dso].key? func
          data[dso][func]=[{},0,0,0]
        end
        for offset in tdata[dso][func][0].keys.sort
          if !data[dso][func][0].key? offset
            data[dso][func][0][offset]=[0,0]
          end
          data[dso][func][1]+=tdata[dso][func][1]
          data[dso][func][2]+=tdata[dso][func][2]
          data[dso][func][0][offset][0]+=tdata[dso][func][0][offset][0]
          data[dso][func][0][offset][1]+=tdata[dso][func][0][offset][1]
          if data[dso][func][0][offset][1] > data[dso][func][3]
            data[dso][func][3] = data[dso][func][0][offset][1]
          end
        end
      end
    end
  end
  outFile = open("#{perfDir}.prof",'w')
  #outFile.write("offset\t normalized latency\t function\t binary\n")
  for dso in data.keys.sort
    for func in data[dso].keys.sort
      funcits = data[dso][func][3]
      for offset in data[dso][func][0].keys.sort
        latsum = data[dso][func][0][offset][0]
        outFile.write("#{offset}\t #{(latsum.to_f/funcits.to_f).round(2)} #{func}\t #{dso}\n")
      end
    end
  end
  outFile.close()
end


def analyzePerfFile(perfFile) #keep
  raise "Error: Should be unused for now!" # tallent

  puts "#{perfFile}"
  cmd = "perf mem report -v -U -i #{perfFile} -t \",\" --no-demangle | grep '^ [0-9]*\.[0-9]*\,'"
  cmdPid = -1
  stdin, stdout, stderr, wait_thr = Open3.popen3(cmd)
  cmdPid = wait_thr[:pid]
  data = {}
  #outFile = open("#{perfFile}-info.dat",'w')
  #perfHitDat = open("#{perfFile}-hits.dat",'w')
  memLevelCnt ={}
  stdout.each do |line|
    offset,weight,samples,memLevel,func,dso = analyzePerfSample(line.chomp)
    if !data.key? dso
      data[dso]={}
    end
    if !data[dso].key? func
      data[dso][func]=[{},0,0,0,""] #=> [offset[latency sum,num samples], function latency sum, function num samples, max samples (amongst offsets)]
    end
    if !data[dso][func][0].key? offset
      data[dso][func][0][offset]=[0,0]
    end
    data[dso][func][1]+=weight*samples
    data[dso][func][2]+=samples
    data[dso][func][0][offset][0]+=weight*samples
    data[dso][func][0][offset][1]+=samples
    if data[dso][func][0][offset][1] > data[dso][func][3]
      data[dso][func][3] = data[dso][func][0][offset][1]
      data[dso][func][4]= offset
    end
    if !memLevelCnt.key? dso
      memLevelCnt[dso]={}
    end
    if !memLevelCnt[dso].key? func
      memLevelCnt[dso][func]=[{},{}] #=> [offset[memlevelcnt], function memlevelcnt]
    end
    if !memLevelCnt[dso][func][0].key? offset
      memLevelCnt[dso][func][0][offset]={}
    end
    if !memLevelCnt[dso][func][0][offset].key? memLevel
      memLevelCnt[dso][func][0][offset][memLevel]=0
    end
    if !memLevelCnt[dso][func][1].key? memLevel
      memLevelCnt[dso][func][1][memLevel]=0
    end
    memLevelCnt[dso][func][0][offset][memLevel]+=samples
    memLevelCnt[dso][func][1][memLevel]+=samples
  end

  # outFile.write("1. offset  2. avg. lat(offset)  3. norm. lat  4. avg. lat(func)  5. function  6 .binary  7. hit level 8.temp\n\n")
  # for dso in data.keys.sort
  #   for func in data[dso].keys.sort
  #     funclatsum = data[dso][func][1]
  #     funccnt = data[dso][func][2]
  #     funcits = data[dso][func][3]
  #     memlf=''
  #     perfHitDat.write("function: #{func}\n")
  #     for offset in data[dso][func][0].keys.sort
  #       latsum = data[dso][func][0][offset][0]
  #       cnt = data[dso][func][0][offset][1]
  #       memlo=''
  #       for memlev in memLevelCnt[dso][func][1].keys.sort
  #         memlo="#{memlo} #{memlev}:#{memLevelCnt[dso][func][0][offset][memlev]}"
  #       end
  #       perfHitDat.write("#{offset}: #{memlo}\n")
  #       #outFile.write("#{offset}\t #{latsum}\t #{cnt}\t #{latsum.to_f/cnt.to_f}\t #{(latsum.to_f/funcits.to_f)} #{funclatsum}\t #{funccnt}\t #{funclatsum.to_f/funccnt.to_f}\t #{func}\t #{dso}\n")
  #       outFile.write("#{offset}\t #{(latsum.to_f/cnt.to_f).round(2)}\t #{(latsum.to_f/funcits.to_f).round(2)} #{(funclatsum.to_f/funccnt.to_f).round(2)}\t #{func}\t #{dso} #{memlo}\n")
  #       outFile.write("#{offset}\t #{(latsum.to_f).round(2)} #{(cnt.to_f).round(2)}\t #{(funcits.to_f).round(2)} #{(funclatsum.to_f).round(2)} #{(funccnt.to_f).round(2)}\t #{func}\t -- #{data[dso][func][3]} #{data[dso][func][4]}\n")
  #     end
  #     memlf=''
  #     for memlev in memLevelCnt[dso][func][1].keys.sort
  #       memlf="#{memlf} #{memlev}:#{memLevelCnt[dso][func][1][memlev]}"
  #     end
  #     perfHitDat.write("total: #{memlf}\n")
  #     perfHitDat.write("\n")
  #   end
  # end

 
    #for k2 in data[k1].keys.sort
      #outFile.write("#{k1} #{data[k1][k2][0]} #{data[k1][k2][1]} #{data[k1][k2][0]/data[k1][k2][1]} #{k2} #{data[k1][k2][2]} #{data[k1][k2][3]}\n")
      #outFile.write("#{k1} #{data[k1][0]} #{data[k1][1]} #{data[k1][0].to_f/data[k1][1].to_f} #{data[k1][2]} #{data[k1][3]}\n")

    #end
  #end
  stdin.close()
  stdout.close()
  stderr.close()
  #puts "#{vals.length}"
  #outFile.close()
  return data
  #return vals
end

#****************************************************************************

def analyzePerfSample(sample)
  memLevelMap={"L1 hit"=>0,"L2 hit"=>1,"L3 hit"=>2,"LFB hit"=>3,"Local RAM hit"=>4,"Remote RAM (1 hop) hit"=>5,"Remote Cache (1 hop) hit"=>6,"Uncached hit"=>7,"L3 miss"=>8}

  begin
    data = sample.split(",")
    samples = data[1].strip
    weight = data[2].strip
    memLevel = data[3].strip 
    offset =  data[4].split()[0]
    func = data[4].split()[-1]
    dso = data[5].strip
    return offset,weight.to_f,samples.to_f,memLevel,func,dso
  rescue Exception => error
    puts "#{error}"
    puts "#{sample}"
    puts
  end
end


def div0(num,den)
  if den == 0
    return 0
  end
  return num/den
end

#****************************************************************************

if __FILE__ == $0
  outFold = ARGV[0]
  nprocs = `nproc`.to_i
  analyzePerfFiles(nprocs,outFold)
end
