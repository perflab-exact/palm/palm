#ifndef DYNINSTHELPER_H
#define DYNINSTHELPER_H

struct graph {
  std::string name;
  std::string mangledName;
  std::string baseMangledName;
  std::map<std::string, std::string> declarations;
  std::map<std::string, std::string> lineNos;
  std::set<std::string> links;
  std::set<std::string> edges;
  std::map<std::string, int> elements;
  std::map<int, BPatch_basicBlock *> blks;
  std::map<std::string, std::string> loops;
  std::map<std::string, std::string> loopEntries;
  std::string libPath;
  std::map<int, int> basicBlockNoMap; // dyninst provide blk numbers arent gauranteed to be the same between runs, so use our own numbers k =dyninst numbering, v=palm numbering
  std::map<std::string, std::vector<std::pair<std::string, BPatch_variableExpr *>>> varMap;
  std::map<std::string, long long> counts;
  std::map<std::string, std::map<int, long long>> ncounts;
  std::map<std::string, std::tuple<std::string, Dyninst::Address, bool>> calledFunctionMap;
  std::map<Dyninst::Address, BPatch_point *> dynamicCallPnts;
  Dyninst::Address startAddr;
  Dyninst::Address endAddr;
  int entry;
  int exit;
  int cnt;
  int level;
  bool executed;
};

// struct graph{
// 	std::map<std::string, std::string> declarations;
// 	std::map<std::string, std::string> lineNos;
// 	std::vector<std::string> links;
// 	std::map<std::string,int> elements;
// 	std::map<int,bool> blkNos;
// 	std::map<int,int> nextBlk;
// 	std::vector<int> path;
// 	std::string entry;
// 	std::string exit;
// 	int startBlk;
// 	int cnt;
// 	std::map<int,int> basicBlockNoMap; // dyninst provide blk numbers are gauranteed to be the same between runs, so use our own numbers
// };

std::vector<BPatch_function *> findFunction(BPatch_addressSpace *app, std::string functionStr, std::string funcFile, int funcLine, bool regex);
void createBasicBlockMap(BPatch_function *function, graph &g);
std::vector<std::string> getCalledFunctions(BPatch_function *function, graph &g);
void printAllFunctions(BPatch_image *appImage);
std::vector<BPatch_function *> searchAllFunctions(BPatch_image *appImage, std::string functionStr);
std::ostream &serialize(std::ostream &os, graph &g);

#endif /* DYNINSTHELPER_H */