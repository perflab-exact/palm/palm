#include <cstdlib>
#include <map>

using namespace std;

struct node {
   node(double _val){ }
   double val[2];
};


// Expected:

// map is using a balanced tree with height = log2(size)
// half the entries in map's tree are at the leaves; the other half at interior nodes
// expect a tree walk to touch (height - 1) nodes

// find-fp = (log2(size) - 1) * sizeof(map node)*
//   *touches key and at least one child pointer

// find-fp (over many instances): reuses head node... how much more?

void shuffle(int *array, int n){ 
   if (n > 1) {
      int i;
      for (i = 0; i < n - 1; i++) {
         int j = i + rand() / (RAND_MAX / (n - i) + 1); 
         int t = array[j];
         array[j] = array[i];
         array[i] = t;
      }   
   }   
}


//Accessing whole data with rand()
int test1(std::map<int, node*> *mp, int size){
   for(int i = 0; i < size; i++){
      int key = rand()%size;
      std::map<int, node*>::iterator pair = mp->find(key);
      pair->second->val[0] = i; 
      pair->second->val[1] = key;
   }
}

//Accessing 1/10 of data with rand()
int test2(std::map<int, node*> *mp, int size){
   for(int i = 0; i < size; i++){
      int key = rand()%size;
      std::map<int, node*>::iterator pair = mp->find(key);
      pair->second->val[0] = i; 
      pair->second->val[1] = key;
   }
}

//Accessing whole data with random index array
int test3(std::map<int, node*> *mp, int *idx ,  int size){
   for(int i = 0; i < size; i++){
      int key = idx[i];
      std::map<int, node*>::iterator pair = mp->find(key);
      pair->second->val[0] = i; 
      pair->second->val[1] = key;
   }
}

//Accessing 1/10 of data with random index array
int test4(std::map<int, node*> *mp, int *idx ,  int size){
   for(int i = 0; i < size; i++){
      int key = idx[i];
      std::map<int, node*>::iterator pair = mp->find(key);
      pair->second->val[0] = i; 
      pair->second->val[1] = key;
   }
}

int main(int argc, const char** argv)
{
   int size = 10000000; // atol(argv[1])/sizeof(node); 10M

   int* idx = (int*)malloc(sizeof(int)*size);   

   std::map<int, node*> mp;
   for(int i = 0; i < size; i++){
      mp.insert(std::map<int, node*>::value_type(i, new node(i)));
      idx[i]= i;
   }

   shuffle(idx, size);

   // test 1
   // access all nodes;
   // with rand
   test1(&mp , size);

   // test 3
   // access all nodes;
   // with shuffled index
   test3(&mp, idx , size);


   // test 2
   // access 1/10 of data;
   // with rand
   test2(&mp , size/10);

   // test 4
   // access 1/10 of data;
   // with shuffled index
   test4(&mp, idx , size/10);


   mp.clear();
   free(idx);

   return 0;
}



