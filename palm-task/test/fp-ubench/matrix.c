#include <stdlib.h>
#include <unistd.h>
//#include <stdio.h>
#define TYPE int
//#define TYPE long

size_t SIZE  = 20000; /* 20 K */
size_t SIZE3 = 5000000; /* 5 M */

//size_t SIZE =  10000000; /* 10 M */
//size_t SIZE = 100000000; /* 100 M */
//size_t SIZE = 10000000000; /* 10000 M */

void shuffle(int *array, size_t n) {
  if (n > 1) {
    size_t i;
    for (i = 0; i < n - 1; i++) {
      size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
      int t = array[j];
      array[j] = array[i];
      array[i] = t;
    }
  }
}


//***************************************************************************
//
//***************************************************************************

// Expect: 'SIZE ' loads
void ubench_2D_Str1_x2(TYPE** a, size_t a_len) {
   for (size_t j = 0 ; j < a_len ; j++) {
      for (size_t i = 0; i < a_len; i++) {
         a[i][j] = a[j][i];
      }
   }
}


//***************************************************************************
//
//***************************************************************************

// Expect: 'SIZE ' loads
void ubench_2D_Ind_x1(TYPE** a, TYPE* idx, size_t a_len) {
   for (size_t j = 0 ; j<a_len ; j++){ 
      for (size_t i = 0; i < a_len; i++) {
         a[i][j]  = a[j][idx[i]];
      }
   }
}

// Expect: 'SIZE ' loads
void ubench_2D_Ind_rowxM(TYPE** a, TYPE* idx, size_t a_len) {
   for (size_t j = 0 ; j<a_len ; j++){ 
      for (size_t i = 0; i < a_len; i++) {
         a[0][i] = a[0][idx[i]];
      }
   }
}

// a_len=20.000   b_len=5.000.000
void ubench_2D_Ind_1DxM(TYPE** a,TYPE* b ,TYPE* idx, 
                        size_t a_len, size_t b_len) {
   for (size_t j = 0 ; j<a_len ; j++){ 
      for (size_t i = 0; i < a_len; i++) {
         a[i][j]  = a[j][idx[i]];
      }
      for (size_t k = 0 ; k < b_len; k++){
         b[0] = b[k];
      }
   }
}


//***************************************************************************
//
//***************************************************************************

int main() {
  TYPE **mat, i, j;

  mat = (TYPE**)malloc(sizeof(TYPE*)*SIZE);
  for ( i = 0; i < SIZE; i++ ) {
    mat[i] = (TYPE*)malloc(sizeof(TYPE)*SIZE);
  }
  for ( i = 0; i < SIZE; i++ ) {
    for ( j = 0; j < SIZE; j++ ) {
      mat[i][j]=j+i;
    }
  }

  TYPE* b = (TYPE*)malloc(sizeof(TYPE)*SIZE3);

  TYPE* idx2 = (TYPE*)malloc(sizeof(TYPE)*SIZE);
  for (size_t i =0; i<SIZE; i++){
    idx2[i] = i;
  }

  shuffle(idx2, SIZE);

  //----------------------------------------------------------
  // 2D/strided access
  //----------------------------------------------------------
  ubench_2D_Str1_x2(mat, SIZE);

  //----------------------------------------------------------
  // 2D/indirect access
  //----------------------------------------------------------
  ubench_2D_Ind_x1   (mat, idx2, SIZE);
  ubench_2D_Ind_rowxM(mat, idx2, SIZE);
  ubench_2D_Ind_1DxM (mat, b, idx2, SIZE, SIZE3);

  //----------------------------------------------------------
  //----------------------------------------------------------

  for ( i = 0; i < SIZE; i++ ) {
    free(mat[i]);
  }
  free(mat);
    
  free(b);
  free(idx2);
  return 0;
}
