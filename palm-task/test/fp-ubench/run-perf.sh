period="4986 9973 19946 39891"

bin="./array"
binNm=${bin##*/} # cf. $(basename ...)

binCmd="${bin} 0xf"

for p in ${period} ; do
    out1="${binNm}.ldlat-${p}.perf"
    printf "*** ${out1} ***\n"
    perf record -W -d -e cpu/mem-loads,ldlat=1,period=${p}/upp -o ${out1} ${binCmd}
    perf report -D -i ${out1} | grep THROTTLE

    #out2="${binNm}.dla-loads-${p}.perf"
    #printf "*** ${out2} ***\n"
    #perf record -e cpu/mem_inst_retired.all_loads,period=${p}/upp -o ${out2} ${binCmd}
    #perf report -D -i ${out2} | grep THROTTLE

    # perf report --stdio --header -n -i ${out}
done

# --sort=weight

# perf mem report -i array.ldlat-4986.perf
# perf report -n -i array.dla-loads-9973.perf

# perf script -F event,addr,bpf-output,ip,sym,symoff,dso -i <file>

# perf evlist -v -i <file>

#****************************************************************************

# *** array.ldlat-4986.perf ***
# [ perf record: Woken up 2 times to write data ]
# [ perf record: Wrote 0.543 MB array.ldlat-4986.perf (9720 samples) ]
# *** array.dla-loads-4986.perf ***
# [ perf record: Woken up 86 times to write data ]
# [ perf record: Wrote 21.541 MB array.dla-loads-4986.perf (704712 samples) ]
#         THROTTLE events:        137
#       UNTHROTTLE events:        137
# *** array.ldlat-9973.perf ***
# [ perf record: Woken up 2 times to write data ]
# [ perf record: Wrote 0.282 MB array.ldlat-9973.perf (4849 samples) ]
# *** array.dla-loads-9973.perf ***
# [ perf record: Woken up 45 times to write data ]
# [ perf record: Wrote 11.030 MB array.dla-loads-9973.perf (360694 samples) ]
# *** array.ldlat-19946.perf ***
# [ perf record: Woken up 1 times to write data ]
# [ perf record: Wrote 0.153 MB array.ldlat-19946.perf (2424 samples) ]
# *** array.dla-loads-19946.perf ***
# [ perf record: Woken up 23 times to write data ]
# [ perf record: Wrote 5.528 MB array.dla-loads-19946.perf (180411 samples) ]
# *** array.ldlat-39891.perf ***
# [ perf record: Woken up 1 times to write data ]
# [ perf record: Wrote 0.088 MB array.ldlat-39891.perf (1212 samples) ]
# *** array.dla-loads-39891.perf ***
# [ perf record: Woken up 12 times to write data ]
# [ perf record: Wrote 2.776 MB array.dla-loads-39891.perf (90240 samples) ]

#****************************************************************************

# Notes:
# 1. Radically different profiles between (a) ldlat:4986 (b) DLA:9973 (c) DLA:39891
#    
#    Why?
#    - different instruction definitions for loads? no!
#    - random: error due to memory atomics
#      - C source has conditionals; only locks (stdlib/random.c)
#      - ldlat has fairly even distribution, which is expected for non-mt run
#      - dla has radically skewed distribution; most samples are on a decl (the non-lock version!)
#      - dla total samples is far below ldlat
#    - random_r:
#      - ldlat has fairly even distribution, all attributed to loads
#      - dla has skewed dist, mostly attributed to ret!
#      - dla total samples is far below ldlat
#    - random@plt / shuffle:
#      - dla total is radically large; ldlat is self-consistent
#      - dla distribution is skewed -- straight-line code

#    - ubench_* routines have expected ratio of loads with both ldlat and dla

#    - main should have same as ubench_1D_Str1_x1 (idx_r); true with ldlat; not with dla

#     Can we clearly show that dla will be wrong even for loops? (We
#       have said that attribution is likely to be good for loops, even
#       with skid)

# 2. Ld-lat profiles are fairly similar 4986 vs. 39891


# TODO:
#   cfgprof to determine 
