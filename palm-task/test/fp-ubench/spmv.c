#include <stdio.h>
#include <stdlib.h>

// Initial code based on 
//   http://wgropp.cs.illinois.edu/courses/cs598-s16/spmv.c
// Updated by N. Tallent.

//
// Execute: <spmv> <N>, where N results in an N^2 x N^2 matrix
//

/* 
 * Example of sparse matrix-vector multiply, using CSR (compressed sparse
 * row format).  Because this code is in C, the index values in the ia and ja
 * arrays are zero origin rather than one origin as they would be in Fortran.
 *
 * While this code does time the sparse matrix-vector product, as noted below,
 * these timings are too simple to be used for serious analysis.
 */
int main(int argc, char *argv[])
{
    long   *ia, *ja;
    double *a, *x, *y;
    int    row, i, j, idx, n;
    long   nrows, nnz, nnzMax;

    // N results in an N^2 x N^2 matrix!
    n = 10;
    if (argc > 1) n = atoi(argv[1]);

    // Allocate enough storage for the matrix.  Allocate extra to simplify code
    nrows  = n * n;     // for diagonal
    nnzMax = nrows * 5; // penta-diagonal (5 diagonals)

    ia = (long*)malloc(nrows*sizeof(long));      // beginning of each row
    ja = (long*)malloc(nnzMax*sizeof(long));     // column idx of each element
    a  = (double*)malloc(nnzMax*sizeof(double)); // matrix elements

    /* Allocate the source and result vectors */
    x = (double*)malloc(nrows*sizeof(double));
    y = (double*)malloc(nrows*sizeof(double));

    /* Create a pentadiagonal matrix, representing very roughly a finite
       difference approximation to the Laplacian on a square n x n mesh */
    row = 0;
    nnz = 0;
    for (i = 0; i < n; i++) {
	for (j = 0; j < n; j++) {
	    ia[row] = nnz;
	    if (i>0) { ja[nnz] = row - n; a[nnz] = -1.0; nnz++; }
	    if (j>0) { ja[nnz] = row - 1; a[nnz] = -1.0; nnz++; }
	    ja[nnz] = row; a[nnz] = 4.0; nnz++;
	    if (j<n-1) { ja[nnz] = row + 1; a[nnz] = -1.0; nnz++; }
	    if (i<n-1) { ja[nnz] = row + n; a[nnz] = -1.0; nnz++; }
	    row++;
	}
    }
    ia[row] = nnz;

    /* Create the source (x) vector */
    for (i = 0; i < nrows; i++)
      x[i] = 1.0;


    // -----------------------------------------------------------------

    // nrows = N^2
    // nnz   = nrows + 4 (nrows - N) = 5 * N^2 - 4 * N

    // footprint (access pattern) [use]:
    //   a:   nnz (indirect/strided) [each x 1]
    //   ja:  nnz (indirect/strided) [each x 1]
    //   ia:  nrows (strided) [each x 2]
    //   x:   nrows (indirect) [each x 5 by pentadiagonal]
    //   y:   nrows (strided) [each x 1]

    /* Perform a matrix-vector multiply: y = A*x */
    for (row = 0; row < nrows; row++) {
	double sum = 0.0;
	for (idx = ia[row]; idx < ia[row+1]; idx++) {
	    sum += a[idx] * x[ja[idx]];
	}
	y[row] = sum;
    }

    // -----------------------------------------------------------------


    /* Compute with the result to keep the compiler for marking the
       code as dead */
    //for (row=0; row<nrows; row++) {
    //	if (y[row] < 0) {
    //	    fprintf(stderr,"y[%d]=%f, fails consistency test\n", row, y[row]);
    //	}
    //}
    printf("Sparse Ax (n = %d): nrows=%ld, nnz=%ld\n", n, nrows, nnz);

    free(ia); free(ja); free(a); free(x); free(y);
    return 0;
}
