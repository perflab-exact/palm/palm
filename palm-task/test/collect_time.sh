#!/bin/bash
rm time_output.txt
for file in test1.out  ifTest.out #test2.out test3.out test4.out random1.out random2.out random3.out random4.out ifTest.out MatrixSeq.out MatrixRand.out MatrixRand2Loop.out 
do
   echo "$file" >> time_output.txt
   for i in 1 2 3 4 5
   do
    echo "run $i" >> time_output.txt
    echo "Original" >> time_output.txt
    { time /tmp/$file >/dev/null; } |& grep  real >> time_output.txt 
    echo "Memreuse" >> time_output.txt
    { time ~/modsim-tools/palm-miami/MIAMI-v1/install/bin/memreuse -debug 0 -footprint -statistic -profile -o simpletest.mrdt -- /tmp/$file >/dev/null; } |& grep real >> time_output.txt
   done
done 
